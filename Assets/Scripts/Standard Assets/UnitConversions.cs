//using UnityEngine;
//using System.Collections.Generic;
//using System.Text;
//using System;

//public class UnitConversions
//{
//	public enum UnitOfLength : byte { Inches, Feet, Meters, Miles };

//	public const double MILES_TO_METERS    = 1609.34;
//	public const double METERS_TO_MILES    = 0.000621371;
//	public const double METERS_TO_FEET     = 3.28084;
//	public const double METERS_TO_INCHES   = 39.370;
//	public const double FEET_TO_METERS	   = 0.3048;
//	public const double MPS_TO_MPH		   = 2.23694185194;
//	public const double MPSPS_TO_MPHPM	   = 134.21616;
//	public const double MPH_TO_MPS		   = 0.44703888888;
//	public const double KG_TO_TONS		   = 0.00110231;
//	public const double TONS_TO_KG		   = 907.185;
//	public const double MILES_TO_FEET	   = 5280.0;
//	public const double FEET_TO_MILES	   = 0.000189394;
//	public const double LBF_TO_NEWTON	   = 4.44822216;
//	public const double NEWTON_TO_KLBS	   = 0.00022480894;
//	public const double KLBS_TO_NEWTON	   = 4448.2216;
//    public const double RADIANS_TO_DEGREES = 57.2957795786;
//    public const double DEGREES_TO_RADIANS = 0.0174532925;
//	public const double LITERS_TO_GALLONS  = 0.264172;

//	public static double TMTSUNITS_TO_PSI(double tmtsU)
//	{
//		return (tmtsU - 1.2) / 0.08165;
//	} 
//	public static float TMTSUNITS_TO_PSI(float tmtsU)
//	{
//		return (tmtsU - 1.2f) / 0.08165f;
//	}
	
//	public static int GetIntegerDigitCount(int value)
//    {
//		value = Mathf.Abs(value);

//    public static int BYTES_TO_MEGABYTES(long byteValue)
//    {
//        return (int)byteValue/(1024*1024);
//    }
		
//		for(int i = 10, j = 1; i < int.MaxValue; i*=10,j++)
//		{
//			if(value<i)
//			{
//				return j;	
//			}
//		}
//		return -1;
//	}
//	public static int GetDigit(int index, int value)
//	{
//		int digitCount = GetIntegerDigitCount(value);
//		if(index >= digitCount)
//		{
//			Debug.LogError("ERROR");
//			return -1;	
//		}
		
//		for(int i = digitCount-1; i > index; i--)
//		{
//			value = value / 10;
//		}
//		return value % 10;
//	}

//	public static double Truncate(double value)
//	{
//		return Truncate(value, 1);
//	}	
//	public static double Truncate(double value, int numberOfDecimals)
//	{
//		double precision = Math.Pow(10.0, numberOfDecimals);
//		return Math.Truncate(value * precision) / precision;
//	}

//    public static double RoundToPowerOfTwo(double value, int power)
//    {
//        double multiplier = 1 << power;
//        return Math.Round(value * multiplier) / multiplier;
//    }

//    public static Fraction RealToPowerOfTwoFraction(double value, int power, double error)
//    {
//        value = RoundToPowerOfTwo(value, power);
//        return RealToFraction(value, error);
//    }

//    public static void RealToPowerOfTwoFractionTest(double value, int power, double error)
//    {
//        Fraction fraction = RealToPowerOfTwoFraction(value, power, error);
//        double result = (double)fraction.numerator / (double)fraction.denominator;

//        Debug.Log(value + " = " + fraction + " = " + result);
//    }

//    public static Fraction RealToFraction(double value, double error)
//    {
//        //https://en.wikipedia.org/wiki/Stern%E2%80%93Brocot_tree
//        //http://stackoverflow.com/questions/5124743/algorithm-for-simplifying-decimal-to-fractions/5124773#5124773

//        if (error <= 0.0 || error >= 1.0)
//        {
//            throw new ArgumentOutOfRangeException("error", "Must be between 0 and 1 (exclusive).");
//        }

//        int sign = Math.Sign(value);

//        if (sign == -1)
//        {
//            value = Math.Abs(value);
//        }

//        if (sign != 0)
//        {
//            // error is the maximum relative error; convert to absolute
//            error *= value;
//        }

//        int n = (int)Math.Floor(value);
//        value -= n;

//        if (value < error)
//        {
//            return new Fraction(sign * n, 1);
//        }

//        if (1 - error < value)
//        {
//            return new Fraction(sign * (n + 1), 1);
//        }

//        // The lower fraction is 0/1
//        int lower_n = 0;
//        int lower_d = 1;

//        // The upper fraction is 1/1
//        int upper_n = 1;
//        int upper_d = 1;

//        while (true)
//        {
//            // The middle fraction is (lower_n + upper_n) / (lower_d + upper_d)
//            int middle_n = lower_n + upper_n;
//            int middle_d = lower_d + upper_d;

//            if (middle_d * (value + error) < middle_n)
//            {
//                // real + error < middle : middle is our new upper
//                upper_n = middle_n;
//                upper_d = middle_d;
//            }
//            else if (middle_n < (value - error) * middle_d)
//            {
//                // middle < real - error : middle is our new lower
//                lower_n = middle_n;
//                lower_d = middle_d;
//            }
//            else
//            {
//                // Middle is our best fraction
//                return new Fraction((n * middle_d + middle_n) * sign, middle_d);
//            }
//        }
//    }

//    public static void QuaternionToEulerAngles(Quaternion q, out float roll, out float pitch, out float yaw)
//    {
//        // roll (x-axis rotation)
//        float sinr = (float)(2.0 * (q.w * q.x + q.y * q.z));
//        float cosr = (float)(1.0 - 2.0 * (q.x * q.x + q.y * q.y));
//        roll = Mathf.Atan2(sinr, cosr) * (float)RADIANS_TO_DEGREES;

//        // pitch (y-axis rotation)
//        float sinp = (float)(2.0 * (q.w * q.y - q.z * q.x));
//        if (Mathf.Abs(sinp) >= 1)
//        {
//            double sign = System.Math.Sign(sinp);
//            pitch = (float)(sign * (System.Math.PI / 2)) * (float)RADIANS_TO_DEGREES; // use 90 degrees if out of range
//        }
//        else
//        {
//            pitch = Mathf.Asin(sinp) * (float)RADIANS_TO_DEGREES;
//        }

//        // yaw (z-axis rotation)
//        float siny = (float)(2.0 * (q.w * q.z + q.x * q.y));
//        float cosy = (float)(1.0 - 2.0 * (q.y * q.y + q.z * q.z));
//        yaw = Mathf.Atan2(siny, cosy) * (float)RADIANS_TO_DEGREES;
//    }
//}

//public struct Fraction
//{
//    public Fraction(int numerator, int denominator)
//    {
//        this.numerator = numerator;
//        this.denominator = denominator;
//    }

//    public int numerator { get; private set; }
//    public int denominator { get; private set; }

//    public override string ToString()
//    {
//        if (numerator == 0)
//        {
//            return "0";
//        }
//        else if (numerator == denominator)
//        {
//            return "1";
//        }
//        else if (numerator > denominator)
//        {
//            int whole = numerator / denominator;
//            int remainder = numerator % denominator;
//            if (remainder == 0)
//            {
//                return whole.ToString();
//            }
//            else
//            {
//                return whole + " " + remainder + "/" + denominator;
//            }
//        }
//        else
//        {
//            return numerator + "/" + denominator;
//        }
//    }
//}