using UnityEngine;
using System.Runtime.InteropServices;

public class Mercator : MercatorCS.Mercator
{
    public static double ConvertMetersToMercators(double meters)
    {
        return meters / GetScaleFromZ(0);
    }
    public static PointXYZ ConvertMetersToMercators(PointXYZ meters)
    {
        double scaleFromZ = GetScaleFromZ(0);

        PointXYZ result = PointXYZ.New(0.0, 0.0, 0.0);
        result.x = meters.x / scaleFromZ;
        result.y = meters.y / scaleFromZ;
        result.z = meters.z / scaleFromZ;
        return result;
    }
    public static double ConvertMercatorsToMeters(double mercators)
	{
		return mercators * GetScaleFromZ (0);
	}
	public static PointXYZ ConvertMercatorsToMeters(PointXYZ mercators)
	{
		double scaleFromZ = GetScaleFromZ (0);
		
		PointXYZ result = PointXYZ.New(0.0, 0.0, 0.0);
		result.x = mercators.x * scaleFromZ;
		result.y = mercators.y * scaleFromZ;
		result.z = mercators.z * scaleFromZ;
		return result;
	}
	
	public static double ConvertMphToMercatorsPerSecond(double mph)
	{
		return ((mph / 3600.0f) * 1609.34f) *  ( 1 / GetScaleFromZ (0));
	}
	
}