using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class cstToPrefab : MonoBehaviour
{
    [Tooltip("Generic Train prefab used to model all other train prefabs after.")]
    [SerializeField] private GameObject TrainPrefab;

    [Tooltip("Generic Train Car prefab used to model all other train car prefabs after.")]
    [SerializeField] private GameObject TrainCarPrefab;

    [Tooltip("Generic Locomotive Inidicator prefab used to model all other locomotive indicator prefabs after.")]
    [SerializeField] private GameObject LocomotiveIndicatorPrefab;

    [Tooltip("Local path to the Trains folder in your Unity Assets.")]
    [SerializeField] private string localTrainsFolderPath;

    [Tooltip("Check to True if you'd like to use the default path to the Train folder.")]
    [SerializeField] private bool useDefaultLocalTrainsFolderPath;

    [Tooltip("Local path to the Prefabs folder in your Unity Assets.")]
    [SerializeField] private string localPrefabsFolderPath;

    [Tooltip("Check to True if you'd like to use the default path to the Prefabs folder.")]
    [SerializeField] private bool useDefaultLocalPrefabsFolderPath = true;

    private static string LOCALTRAINSFOLDERPATH = "Assets/Trains";
    private static string LOCALPREFABFOLDERPATH = "Assets/Prefabs";

    private void Start()
    {
        ParseTrainFiles();
    }

    private void ParseTrainFiles()
    {
        // Init paths
        string prefabPath = useDefaultLocalPrefabsFolderPath ? LOCALPREFABFOLDERPATH : localPrefabsFolderPath;
        string trainPath = useDefaultLocalTrainsFolderPath ? LOCALTRAINSFOLDERPATH : localTrainsFolderPath;

        // Iterate through each train files
        DirectoryInfo trainDir = new DirectoryInfo(trainPath);
        FileInfo[] trainFiles = trainDir.GetFiles("*.cst");
        foreach (FileInfo file in trainFiles)
        {
            // Parse train file data
            string fileText = File.ReadAllText(file.FullName);
            string[] lines = fileText.Split("\n"[0]);
            string[] lineHeaders = lines[0].Split(","[0]);

            // Instantiate the Train
            GameObject train = Instantiate<GameObject>(TrainPrefab);
            TrainInfo trainInfo = train.GetComponent<TrainInfo>();
            float trainCarInsertionPoint = 0f;
            string trainName = file.Name.Substring(0, file.Name.Length - 4);
            train.name = trainName;
            trainInfo.SetName(trainName);

            // Calculate max weight
            float maximumWeight = 0f;
            for (int i = 1; i < lines.Length - 1; i++)
            {
                // Extract only the relevant parts
                string[] fields = lines[i].Split(","[0]);
                float lightWeight = float.Parse(fields[4]);
                float loadWeight = float.Parse(fields[3]);
                if (lightWeight + loadWeight > maximumWeight)
                {
                    maximumWeight = lightWeight + loadWeight;
                }
            }

            // Iterate through each train car
            for (int i = 1; i < lines.Length - 1; i++)
            {
                // Extract only the relevant parts
                string[] fields = lines[i].Split(","[0]);
                string name = fields[0];
                float lightWeight = float.Parse(fields[4]);
                float loadWeight = float.Parse(fields[3]);
                float length = float.Parse(fields[26]);
                int type = int.Parse(fields[24]);
                int direction = int.Parse(fields[2]); 

                // Instantiate the TrainCar
                GameObject trainCar = Instantiate<GameObject>(TrainCarPrefab, train.transform);
                trainCar.name = name;

                // Apply scale of new TrainCar
                float xScale = GameManager.S.ConvertMetersToUnityMeters(length);
                float yScale = GameManager.S.CalculateTrainCarHeight(maximumWeight);
                trainCar.transform.localScale = new Vector3(xScale, yScale, 1f);

                // Set new TrainCar position
                trainCar.transform.position = new Vector3(trainCarInsertionPoint - (xScale / 2f), yScale / 2f, 0f);
                trainCarInsertionPoint -= xScale;

                // Inject TrainCar with data
                TrainCarInfo trainCarInfo = trainCar.GetComponent<TrainCarInfo>();
                trainCarInfo.InjectData(name, lightWeight, loadWeight, length, type, direction);

                // Is trainCar a locomotive?
                if (type == 2 || type == 0)
                {
                    GameObject locomotiveIndicator = Instantiate<GameObject>(LocomotiveIndicatorPrefab, trainCar.transform);
                    locomotiveIndicator.transform.position = new Vector3(trainCarInsertionPoint + (xScale / 2f), yScale / 2f + yScale*2f, 0f);
                }
            }

            // Calculate some useful values for the whole train
            trainInfo.CalculateInfo();

#if UNITY_EDITOR
            // Create the Prefab for the Train
            string currentTrainPrefabPath = prefabPath + "/Trains/" + trainName + ".prefab";
            PrefabUtility.SaveAsPrefabAssetAndConnect(train, currentTrainPrefabPath, InteractionMode.UserAction);
#endif
        }
    }

}
