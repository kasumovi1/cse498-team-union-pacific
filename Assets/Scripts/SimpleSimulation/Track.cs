﻿using System;
using System.Collections.Generic;

namespace SimpleSimulation
{
	/// <summary>
	/// Represents the track network in SimpleSimulation.  Provides the ability to place a consist on the track and get track related data.
	/// </summary>
	public class Track
	{
		private global::Track track;

		private SplineLocation GetSplineLocation(double milepost)
		{
			bool forwardsIsIncreasing;
			return TrackUtility.GetSplineLocationFromMilepost(milepost, out forwardsIsIncreasing);
		}

		/// <summary>
		/// Constructor for simulation track object.
		/// </summary>
		/// <param name="trkFile">Fully qualified file name for simulation track file in TRK format.</param>
		public Track(string trkFile)
		{
			track = new global::Track();
			track.Initialize();

			if (!System.IO.File.Exists(trkFile))
			{
				throw new Exception("Track file does not exist: " + trkFile);
			}

			TrackLoader trackLoader = new TrackLoader(track);
			if (!trackLoader.LoadSplinesFromXML(trkFile))
			{
				throw new Exception("Failed to load track: " + trkFile);
			}

			ExtrudedSpline trackSpline = track.CreateSpline(0);
			ExtrudedSpline test = track.GetSpline(0);

			TrackUtility.globalTrack = track;
			TrackUtility.AddMilepostDataToSourcePosition(track);
		}

		/// <summary>
		/// Places a consist at a given milepost on the track
		/// </summary>
		/// <param name="consist">Consist to place</param>
		/// <param name="milepost">Milepost location</param>
		public void PlaceConsist(Consist consist, double milepost)
		{
			SplineLocation startLocation = GetSplineLocation(milepost);
			consist.innerConsist.Place(startLocation, true, true);
		}

		/// <summary>
		/// Gets the milepost location of a consist
		/// </summary>
		/// <param name="consist">Consist</param>
		/// <param name="front">Either the front (true) or rear (false) truck of the consist</param>
		/// <returns></returns>
		public double GetMilepost(Consist consist, bool front)
		{
			if (front)
			{
				return GetMilepost(consist.FirstCar, true);
			}
			else
			{
				return GetMilepost(consist.LastCar, false);
			}
		}

		/// <summary>
		/// Gets the milepost location of a car
		/// </summary>
		/// <param name="car">Car</param>
		/// <param name="front">Either the front (true) or rear (false) truck of the car relative to the front of the consist</param>
		/// <returns></returns>
		public double GetMilepost(Car car, bool front)
		{
			TrackLocation trackLocation = car.GetTrackLocation(front);
			return trackLocation.GetSplineLocation().GetSourcePosition();
		}

		/// <summary>
		/// Gets an elevation value at a specified milepost location
		/// </summary>
		/// <param name="milepost">Milepost location</param>
		/// <returns>Elevation in meters above sea level</returns>
		public double GetElevation(double milepost)
		{
			SplineLocation splineLocation = GetSplineLocation(milepost);
			return splineLocation.GetCoordinates().y;
		}

		/// <summary>
		/// Gets an array of elevation values within a given milepost range
		/// </summary>
		/// <param name="beginMilepost">Milepost of beginning of range</param>
		/// <param name="endMilepost">Milepost of end of range</param>
		/// <param name="elevationProfileOut">Elevation values of the given range in meters above sea level</param>
		public void GetElevation(double beginMilepost, double endMilepost, ref double[] elevationProfileOut)
		{
			double milesForward = endMilepost - beginMilepost;
			double metersForward = milesForward * UnitConversions.MILES_TO_METERS;
			double metersStep = metersForward / (elevationProfileOut.Length - 1);

			SplineLocation splineLocation = GetSplineLocation(beginMilepost);
			for (int i = 0; i <= elevationProfileOut.Length; i++)
			{
				elevationProfileOut[i] = splineLocation.GetCoordinates().y;
				splineLocation.MoveDistance(metersStep);
			}
		}

		/// <summary>
		/// Gets all mileposts sorted in increasing order
		/// </summary>
		/// <param name="milepostsOut">Array of all mileposts</param>
		public void GetMileposts(out int[] milepostsOut)
		{
			List<SplineMarker> milepostMarkers = new List<SplineMarker>();
			track.GetAllMarkersOfType(milepostMarkers, (int)SplineMarker.MarkerType.MilePost);

			HashSet<SplineMarker> unique = new HashSet<SplineMarker>(milepostMarkers);
			milepostsOut = new int[unique.Count];

			int i = 0;
			foreach (SplineMarker uniqueMarker in unique)
			{
				milepostsOut[i++] = int.Parse(uniqueMarker.GetMarkerText1());
			}
			Array.Sort(milepostsOut);
		}
	}
}