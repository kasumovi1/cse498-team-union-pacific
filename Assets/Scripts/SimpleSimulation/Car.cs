﻿namespace SimpleSimulation
{
	/// <summary>
	/// Represents a car in the SimpleSimulation.  Acts as a linked-list of cars in the consist and provides the ability to get car related data.
	/// </summary>
	public class Car
	{
		internal global::Car innerCar;
		internal Car(global::Car internalCar)
		{
			innerCar = internalCar;
		}
		internal TrackLocation GetTrackLocation(bool front)
		{
			return innerCar.GetTruckLocation(front);
		}

		/// <summary>
		/// The next (closer to the rear) car in the consist.
		/// </summary>
		public Car Next { get; internal set; }

		/// <summary>
		/// The previous (closer to the lead) car in the consist.
		/// </summary>
		public Car Prev { get; internal set; }

		/// <summary>
		/// Gets the current milepost of the car based on the front or rear truck.
		/// </summary>
		/// <param name="front">Defines whether to find the milepost of the front or rear truck of the car.</param>
		/// <returns>Milepost</returns>
		public double GetMilepost(bool front)
		{
			TrackLocation trackLocation = GetTrackLocation(front);
			SplineLocation splineLocation = trackLocation.GetSplineLocation();
			return splineLocation.GetSourcePosition();
		}

		/// <summary>
		/// Gets the current interaction force between this car and the next car in the consist.
		/// </summary>
		/// <returns>Force in Newtons</returns>
		public double GetInteractionForce()
		{
			if (Next == null) throw new System.IndexOutOfRangeException("The last car in the consist does not have an interaction.");
			return innerCar.carInteraction.force;
		}

		/// <summary>
		/// Gets the current velocity of the car.
		/// </summary>
		/// <returns>Velocity in meters per second</returns>
		public double GetVelocityMPS()
		{
			return innerCar.carPhysics.velocity;
		}
	}
}