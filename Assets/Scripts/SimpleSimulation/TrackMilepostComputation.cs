﻿using System;
using System.Collections.Generic;

namespace SimpleSimulation
{
	internal class TrackMilepostComputation
	{
		private Dictionary<Spline, SplineWork> splineWork;
		private Dictionary<TrackNode, double> nodeMileposts;

		internal TrackMilepostComputation(global::Track track)
		{
			splineWork = new Dictionary<Spline, SplineWork>();

			// This dictionary is only usable since spline instances are master index (database) objects, 
			// so that there is a one-to-one relationship between spline instances and inner spline 
			// (unmanaged) instances.

			uint splineCount = track.GetSplineCount();
			for (int i = 0; i < splineCount; i++)
			{
				ExtrudedSpline extrudedSpline = track.GetSpline((uint)i);
				SplineWork temp = new SplineWork(extrudedSpline);
				splineWork[temp.spline] = temp;
			}

			nodeMileposts = new Dictionary<TrackNode, double>();
		}

		private class SplineStep
		{
			public Spline spline;
			public bool forwards;

			public SplineStep(SplineEnd splineEnd)
			{
				spline = splineEnd.GetSpline();
				forwards = splineEnd.IsStart();
			}
		}

		private enum PathEndCondition
		{
			Undefined = 0,
			EndsAtNode = 1,
			DeadEnd = 2,
			DistanceLimit = 3,
			Milepost = 4
		}

		private class SplineWork
		{
			public Spline spline;
			public List<SplineStep> forwardPath;
			public List<SplineStep> reversePath;
			public List<SplineStep> throughPath;
			public PathEndCondition forwardTermination;
			public PathEndCondition reverseTermination;
			public SortedDictionary<double, double> milepostMapping;
			public bool haveStartMilepost;
			public bool haveEndMilepost;
			public bool haveMilepostDirection;
			public bool increasingMilepost;
			public bool throughPathNodesProcessed;      // have node mileposts been interpolated along this path?
			public bool forwardPathNodesProcessed;
			public bool reversePathNodesProcessed;

			public SplineWork(Spline _spline)
			{
				spline = _spline;
				forwardPath = null;
				reversePath = null;
				throughPath = null;
				forwardTermination = PathEndCondition.Undefined;
				reverseTermination = PathEndCondition.Undefined;
				milepostMapping = new SortedDictionary<double, double>();
				haveStartMilepost = false;
				haveEndMilepost = false;
				haveMilepostDirection = false;
				increasingMilepost = false;             // indeterminate
				throughPathNodesProcessed = false;
				forwardPathNodesProcessed = false;
				reversePathNodesProcessed = false;
			}

			public void GetMilepostsOnSpline()
			{
				List<SplineMarker> markers;
				SplineLocation splineLocation;
				double milepost;

				markers = new List<SplineMarker>();
				spline.GetAllMarkers(markers);

				for (int k = 0; k < markers.Count; k++)
				{
					if (markers[k].GetInfo().GetMarkerType() == (int)SplineMarker.MarkerType.MilePost)
					{
						splineLocation = markers[k].GetSplineLocation();

						if (double.TryParse(markers[k].GetInfo().GetText1(), out milepost))
						{
							milepostMapping[splineLocation.GetMeters()] = milepost;
						}

						haveMilepostDirection = true;
						increasingMilepost = markers[k].GetInfo().IsForward();

						SplineLocation.Reuse(splineLocation);
					}
				}
			}

			public double GetFirstMilepostPosition()
			{
				foreach (KeyValuePair<double, double> current in milepostMapping)
				{
					return current.Key;
				}

				return 0.0;
			}

			public double GetLastMilepostPosition()
			{
				double key = 0.0;

				foreach (KeyValuePair<double, double> current in milepostMapping)
				{
					key = current.Key;
				}

				return key;
			}
		}

		private List<SplineStep> BuildSplinePath(Spline initialSpline, bool initiallyForwards, double distanceLimitMeters, out PathEndCondition endCondition)
		{
			List<SplineStep> path;
			SplineStep step;
			SplineEnd splineEnd;
			Spline spline;
			SplineEnd endOut;
			SplineWork workEntry;
			double totalDistance;

			path = new List<SplineStep>();

			splineEnd = initialSpline.GetEnd(initiallyForwards);
			step = new SplineStep(splineEnd);
			SplineEnd.Reuse(splineEnd);
			path.Add(step);

			totalDistance = 0.0;        // Could count some of the distance on the initial spline also...
			splineEnd = initialSpline.GetEnd(!initiallyForwards);

			endOut = TrackUtility.GetStraightExitSplineEnd(splineEnd);
			if (endOut != null)
			{
				SplineEnd.Reuse(splineEnd);
			}

			while ((totalDistance < distanceLimitMeters) && (endOut != null))
			{
				spline = endOut.GetSpline();

				step = new SplineStep(endOut);
				path.Add(step);

				workEntry = splineWork[spline];
				if (workEntry.milepostMapping.Count > 0)
				{
					endCondition = PathEndCondition.Milepost;
					SplineEnd.Reuse(endOut);
					return path;
				}

				splineEnd = spline.GetEnd(!endOut.IsStart());
				SplineEnd.Reuse(endOut);
				endOut = TrackUtility.GetStraightExitSplineEnd(splineEnd);
				totalDistance += spline.GetMeters();
				if (endOut != null)
				{
					SplineEnd.Reuse(splineEnd);
				}
			}

			if (endOut == null)
			{
				if (TrackUtility.GetNodeFromSplineEnd(splineEnd) == null)
				{
					endCondition = PathEndCondition.DeadEnd;
				}
				else
				{
					endCondition = PathEndCondition.EndsAtNode;
				}
				SplineEnd.Reuse(splineEnd);
			}
			else
			{
				endCondition = PathEndCondition.DistanceLimit;
				SplineEnd.Reuse(endOut);
			}

			return path;
		}

		private SplineEnd FindStartofSplinePath(Spline startingSpline)
		{
			// Go backwards from 'startingSpline' until we reach a spline that dead ends or connects via a 
			// diverging node. In theory, this method should never reach another spline that has been 
			// processed (and added to a spline path), but check just in case.

			SplineWork workEntry;
			Spline spline;
			SplineEnd splineEnd;
			SplineEnd endOut;
			bool forwards = false;

			splineEnd = startingSpline.GetEnd(!forwards);
			endOut = TrackUtility.GetStraightExitSplineEnd(splineEnd);

			while (endOut != null)
			{
				spline = endOut.GetSpline();
				forwards = endOut.IsStart();

				workEntry = splineWork[spline];
				if (workEntry.forwardPath != null || workEntry.throughPath != null || workEntry.reversePath != null)
				{
					SplineEnd.Reuse(endOut);
					break;
				}

				SplineEnd.Reuse(splineEnd);
				splineEnd = spline.GetEnd(!forwards);
				SplineEnd.Reuse(endOut);
				endOut = TrackUtility.GetStraightExitSplineEnd(splineEnd);
			}

			return splineEnd;
		}

		private void InterpolateMileposts(List<SplineStep> path)
		{
			// 'path' connects two splines that have milepost markers on them.
			// Assign mileposts to nodes and splines along 'path'.
			SplineWork startingEntry;
			SplineWork endingEntry;
			bool startingForwards, endingForwards;
			double startingStepDistance, endingStepDistance;
			double startingMilepost, endingMilepost;

			startingEntry = splineWork[path[0].spline];
			endingEntry = splineWork[path[path.Count - 1].spline];

			startingForwards = path[0].forwards;
			endingForwards = path[path.Count - 1].forwards;

			double milepostPositionMeters;

			if (startingForwards)
			{
				milepostPositionMeters = startingEntry.GetLastMilepostPosition();
				startingStepDistance = startingEntry.spline.GetMeters() - milepostPositionMeters;
				startingMilepost = startingEntry.milepostMapping[milepostPositionMeters];
			}
			else
			{
				startingStepDistance = startingEntry.GetFirstMilepostPosition();
				startingMilepost = startingEntry.milepostMapping[startingStepDistance];
			}

			if (endingForwards)
			{
				endingStepDistance = endingEntry.GetFirstMilepostPosition();
				endingMilepost = endingEntry.milepostMapping[endingStepDistance];
			}
			else
			{
				milepostPositionMeters = endingEntry.GetLastMilepostPosition();
				endingStepDistance = endingEntry.spline.GetMeters() - milepostPositionMeters;
				endingMilepost = endingEntry.milepostMapping[milepostPositionMeters];
			}

			double totalDistanceMeters = startingStepDistance + endingStepDistance;
			for (int n = 1; n < path.Count - 1; n++)
			{
				totalDistanceMeters += path[n].spline.GetMeters();
			}

			double distanceMeters;

			distanceMeters = startingStepDistance;

			for (int n = 0; n < path.Count - 1; n++)
			{
				double milepostAtNode;
				SplineWork workEntry;
				SplineEnd splineEnd;
				double distanceAtNode;
				TrackNode node;

				milepostAtNode = startingMilepost + (endingMilepost - startingMilepost) * distanceMeters / totalDistanceMeters;

				splineEnd = path[n].spline.GetEnd(!path[n].forwards);
				node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
				SplineEnd.Reuse(splineEnd);

				nodeMileposts[node] = milepostAtNode;

				workEntry = splineWork[path[n].spline];
				distanceAtNode = path[n].forwards ? path[n].spline.GetMeters() : 0.0;
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry = splineWork[path[n + 1].spline];
				distanceAtNode = path[n + 1].forwards ? 0.0 : path[n + 1].spline.GetMeters();
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry.haveMilepostDirection = true;
				workEntry.increasingMilepost = (path[0].forwards == path[n + 1].forwards) ? startingEntry.increasingMilepost : !startingEntry.increasingMilepost;

				distanceMeters += path[n + 1].spline.GetMeters();
			}
		}

		private void InterpolateMilepostsToTerminalNode(List<SplineStep> path, double terminalMilepost)
		{
			SplineWork startingEntry;
			bool startingForwards;
			double startingStepDistance;
			double startingMilepost;

			startingEntry = splineWork[path[0].spline];
			startingForwards = path[0].forwards;

			if (startingForwards)
			{
				double milepostPositionMeters = startingEntry.GetLastMilepostPosition();
				startingStepDistance = startingEntry.spline.GetMeters() - milepostPositionMeters;
				startingMilepost = startingEntry.milepostMapping[milepostPositionMeters];
			}
			else
			{
				startingStepDistance = startingEntry.GetFirstMilepostPosition();
				startingMilepost = startingEntry.milepostMapping[startingStepDistance];
			}

			double totalDistanceMeters = startingStepDistance;
			for (int n = 1; n < path.Count; n++)
			{
				totalDistanceMeters += path[n].spline.GetMeters();
			}

			double distanceMeters = startingStepDistance;
			SplineWork workEntry;
			double distanceAtNode;

			for (int n = 0; n < path.Count - 1; n++)
			{
				double milepostAtNode;
				SplineEnd splineEnd;
				TrackNode node;

				milepostAtNode = startingMilepost + (terminalMilepost - startingMilepost) * distanceMeters / totalDistanceMeters;

				splineEnd = path[n].spline.GetEnd(!path[n].forwards);
				node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
				SplineEnd.Reuse(splineEnd);

				nodeMileposts[node] = milepostAtNode;

				workEntry = splineWork[path[n].spline];
				distanceAtNode = path[n].forwards ? path[n].spline.GetMeters() : 0.0;
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry = splineWork[path[n + 1].spline];
				distanceAtNode = path[n + 1].forwards ? 0.0 : path[n + 1].spline.GetMeters();
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry.haveMilepostDirection = true;
				workEntry.increasingMilepost = (path[0].forwards == path[n + 1].forwards) ? startingEntry.increasingMilepost : !startingEntry.increasingMilepost;

				distanceMeters += path[n + 1].spline.GetMeters();
			}

			workEntry = splineWork[path[path.Count - 1].spline];
			distanceAtNode = path[path.Count - 1].forwards ? path[path.Count - 1].spline.GetMeters() : 0.0;
			workEntry.milepostMapping[distanceAtNode] = terminalMilepost;

			if (distanceAtNode == 0.0)
			{
				workEntry.haveStartMilepost = true;
			}
			else
			{
				workEntry.haveEndMilepost = true;
			}
		}

		private void InterpolateMilepostsBetweenNodes(List<SplineStep> path, double startingMilepost, double endingMilepost)
		{
			SplineWork workEntry;
			double distanceAtNode;
			bool increasingMilepost;

			// Add startingMilepost and endingMilepost to the milepost mappings of the end splines.

			distanceAtNode = path[0].forwards ? 0.0 : path[0].spline.GetMeters();
			workEntry = splineWork[path[0].spline];
			workEntry.milepostMapping[distanceAtNode] = startingMilepost;

			workEntry.increasingMilepost = increasingMilepost = DetermineMilepostDirectionThroughNode(workEntry.spline, path[0].forwards);
			workEntry.haveMilepostDirection = true;

			distanceAtNode = path[path.Count - 1].forwards ? path[path.Count - 1].spline.GetMeters() : 0.0;
			workEntry = splineWork[path[path.Count - 1].spline];
			workEntry.milepostMapping[distanceAtNode] = endingMilepost;

			double totalDistanceMeters = 0.0;

			for (int i = 0; i < path.Count; i++)
			{
				totalDistanceMeters += path[i].spline.GetMeters();
			}

			double distanceMeters = path[0].spline.GetMeters();

			for (int i = 0; i < path.Count - 1; i++)
			{
				double milepostAtNode = startingMilepost + (endingMilepost - startingMilepost) * distanceMeters / totalDistanceMeters;

				SplineEnd splineEnd = path[i].spline.GetEnd(!path[i].forwards);
				TrackNode node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
				SplineEnd.Reuse(splineEnd);

				nodeMileposts[node] = milepostAtNode;

				workEntry = splineWork[path[i].spline];
				distanceAtNode = path[i].forwards ? path[i].spline.GetMeters() : 0.0;
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry = splineWork[path[i + 1].spline];
				distanceAtNode = path[i + 1].forwards ? 0.0 : path[i + 1].spline.GetMeters();
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry.increasingMilepost = (path[i + 1].forwards == path[0].forwards) ? increasingMilepost : !increasingMilepost;
				workEntry.haveMilepostDirection = true;

				distanceMeters += path[i + 1].spline.GetMeters();
			}
		}

		private void ExtrapolateMileposts(List<SplineStep> path)
		{
			// 'path' connects a spline with milepost markers to a dead end.
			// Assign mileposts to nodes and splines along 'path'.

			SplineWork startingEntry;
			bool startingForwards;
			double startingStepDistance;
			double startingMilepost;

			startingEntry = splineWork[path[0].spline];
			startingForwards = path[0].forwards;

			if (startingForwards)
			{
				double milepostPositionMeters = startingEntry.GetLastMilepostPosition();
				startingStepDistance = startingEntry.spline.GetMeters() - milepostPositionMeters;
				startingMilepost = startingEntry.milepostMapping[milepostPositionMeters];
			}
			else
			{
				startingStepDistance = startingEntry.GetFirstMilepostPosition();
				startingMilepost = startingEntry.milepostMapping[startingStepDistance];
			}

			double distanceMeters = startingStepDistance;
			double distanceSign = (startingEntry.increasingMilepost == startingForwards) ? 1.0 : -1.0;
			SplineWork workEntry;
			double distanceAtNode;

			for (int n = 0; n < path.Count - 1; n++)
			{
				double milepostAtNode;
				SplineEnd splineEnd;
				TrackNode node;

				milepostAtNode = startingMilepost + (UnitConversions.METERS_TO_MILES * distanceMeters * distanceSign);

				splineEnd = path[n].spline.GetEnd(!path[n].forwards);
				node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
				SplineEnd.Reuse(splineEnd);

				nodeMileposts[node] = milepostAtNode;

				workEntry = splineWork[path[n].spline];
				distanceAtNode = path[n].forwards ? path[n].spline.GetMeters() : 0.0;
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry = splineWork[path[n + 1].spline];
				distanceAtNode = path[n + 1].forwards ? 0.0 : path[n + 1].spline.GetMeters();
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;
				if (distanceAtNode == 0.0)
				{
					workEntry.haveStartMilepost = true;
				}
				else
				{
					workEntry.haveEndMilepost = true;
				}

				workEntry.haveMilepostDirection = true;
				workEntry.increasingMilepost = (path[0].forwards == path[n + 1].forwards) ? startingEntry.increasingMilepost : !startingEntry.increasingMilepost;

				distanceMeters += path[n + 1].spline.GetMeters();
			}

			double milepostAtEnd = startingMilepost + (UnitConversions.METERS_TO_MILES * distanceMeters * distanceSign);

			workEntry = splineWork[path[path.Count - 1].spline];
			distanceAtNode = path[path.Count - 1].forwards ? path[path.Count - 1].spline.GetMeters() : 0.0;
			workEntry.milepostMapping[distanceAtNode] = milepostAtEnd;
		}

		private void ExtrapolateMilepostsFromNode(List<SplineStep> path, double startingMilepost, double endingMilepost)
		{
			bool useStartingMilepost;
			bool pathHasIncreasingMilepost;

			// -999 flags that this is the end for which we don't have the milepost. In this case, 
			// we should extrapolate from the other end.
			useStartingMilepost = (startingMilepost != -999.0);

			if (useStartingMilepost)
			{
				pathHasIncreasingMilepost = DetermineMilepostDirectionThroughNode(path[0].spline, path[0].forwards);
				if (!path[0].forwards)
				{
					pathHasIncreasingMilepost = !pathHasIncreasingMilepost;
				}
			}
			else
			{
				pathHasIncreasingMilepost = DetermineMilepostDirectionThroughNode(path[path.Count - 1].spline, !path[path.Count - 1].forwards);
				if (!path[path.Count - 1].forwards)
				{
					pathHasIncreasingMilepost = !pathHasIncreasingMilepost;
				}
			}

			SplineWork workEntry;

			for (int n = 0; n < path.Count; n++)
			{
				workEntry = splineWork[path[n].spline];
				workEntry.haveMilepostDirection = true;
				workEntry.increasingMilepost = (pathHasIncreasingMilepost == path[n].forwards);
			}

			int indexStep = useStartingMilepost ? 1 : -1;
			int splineIndex = useStartingMilepost ? 0 : path.Count - 1;
			int endIndex = useStartingMilepost ? path.Count - 1 : 0;
			double distanceMeters = 0.0;
			double distanceAtNode;
			double milepostAtNode = useStartingMilepost ? startingMilepost : endingMilepost;
			double milepostSign = (pathHasIncreasingMilepost == useStartingMilepost) ? 1.0 : -1.0;

			while (true)
			{
				workEntry = splineWork[path[splineIndex].spline];
				workEntry.haveStartMilepost = true;
				workEntry.haveEndMilepost = true;

				distanceAtNode = path[splineIndex].forwards ? 0.0 : workEntry.spline.GetMeters();
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;

				distanceMeters += workEntry.spline.GetMeters();
				if (useStartingMilepost)
				{
					milepostAtNode = startingMilepost + (distanceMeters * UnitConversions.METERS_TO_MILES * milepostSign);
				}
				else
				{
					milepostAtNode = endingMilepost + (distanceMeters * UnitConversions.METERS_TO_MILES * milepostSign);
				}

				distanceAtNode = path[splineIndex].forwards ? workEntry.spline.GetMeters() : 0.0;
				workEntry.milepostMapping[distanceAtNode] = milepostAtNode;

				SplineEnd splineEnd = workEntry.spline.GetEnd(!path[splineIndex].forwards);
				TrackNode node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
				SplineEnd.Reuse(splineEnd);

				if (node != null)       // The path end without a milepost will sometimes not have a node.
				{
					nodeMileposts[node] = milepostAtNode;
				}

				if (splineIndex == endIndex) break;
				splineIndex += indexStep;
			}
		}

		private bool DetermineMilepostDirectionThroughNode(Spline spline, bool front)
		{
			// 'spline' is connected at end 'front' to a node. Determine whether or not mileposts are 
			// increasing on this spline by looking at other splines attached to this node.
			// Only use this method if you are sure that the milepost of this node has been determined 
			// (which implies also that the milepost direction of an attached spline has also been 
			// determined). Throws an exception if milepost direction cannot be determined.

			TrackNodeSlot slotIn;
			SplineEnd splineEnd;
			bool increasingMilepost;

			splineEnd = spline.GetEnd(front);
			slotIn = splineEnd.GetSlot();
			SplineEnd.Reuse(splineEnd);

			if (slotIn == null)
			{
				throw new Exception("Indeterminate milepost direction");
			}
			if (slotIn.GetInnerSlot() == IntPtr.Zero)
			{
				TrackNodeSlot.Reuse(slotIn);
				throw new Exception("Indeterminate milepost direction");
			}

			TrackNode node = slotIn.GetNode();
			TrackNodeType nodeType = node.GetNodeType();
			TrackNodeState nodeStateZero = nodeType.GetState(0, false);
			TrackNodeType.Reuse(nodeType);

			TrackNodeStateEntry nodeStateEntry = nodeStateZero.GetOutgoingEntry(slotIn.GetIndex());
			TrackNodeSlot slotOut = node.GetNodeSlot(nodeStateEntry.GetIndexOut());

			SplineEnd splineEndOut = slotOut.GetEnd();
			SplineWork workEntry;

			if ((splineEndOut != null) && (splineEndOut.GetInnerEnd() != IntPtr.Zero))
			{
				workEntry = splineWork[splineEndOut.GetSpline()];
				if (workEntry.haveMilepostDirection)
				{
					TrackNodeSlot.Reuse(slotIn);
					TrackNodeSlot.Reuse(slotOut);
					TrackNodeState.Reuse(nodeStateZero);
					TrackNodeStateEntry.Reuse(nodeStateEntry);

					increasingMilepost = (front == splineEndOut.IsStart()) ? !workEntry.increasingMilepost : workEntry.increasingMilepost;
					SplineEnd.Reuse(splineEndOut);

					return increasingMilepost;
				}
			}

			nodeStateEntry = nodeStateZero.GetOutgoingEntry(slotOut.GetIndex());
			TrackNodeSlot otherSlotOut = node.GetNodeSlot(nodeStateEntry.GetIndexOut());
			TrackNodeStateEntry.Reuse(nodeStateEntry);

			if (otherSlotOut.GetInnerSlot() != slotIn.GetInnerSlot())
			{
				TrackNodeSlot.Reuse(slotIn);
				TrackNodeSlot.Reuse(slotOut);
				TrackNodeState.Reuse(nodeStateZero);

				splineEndOut = otherSlotOut.GetEnd();
				TrackNodeSlot.Reuse(otherSlotOut);

				if (splineEndOut == null)
				{
					throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
				}
				if (splineEndOut.GetInnerEnd() == IntPtr.Zero)
				{
					SplineEnd.Reuse(splineEndOut);
					throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
				}

				workEntry = splineWork[splineEndOut.GetSpline()];
				if (workEntry.haveMilepostDirection)
				{
					increasingMilepost = (front == splineEndOut.IsStart()) ? workEntry.increasingMilepost : !workEntry.increasingMilepost;
					SplineEnd.Reuse(splineEndOut);

					return increasingMilepost;
				}

				throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
			}

			// Else going through the node twice has gotten us back to the same slot. We want the diverging slot instead.
			// Try the first slot index not already used.
			int slotIndex = 0;
			if (slotIndex == slotIn.GetIndex() || slotIndex == slotOut.GetIndex())
			{
				slotIndex++;
			}
			if (slotIndex == slotIn.GetIndex() || slotIndex == slotOut.GetIndex())
			{
				slotIndex++;
			}

			otherSlotOut = node.GetNodeSlot(slotIndex);
			int slotInIndex = slotIn.GetIndex();

			TrackNodeSlot.Reuse(slotIn);
			TrackNodeSlot.Reuse(slotOut);
			TrackNodeState.Reuse(nodeStateZero);

			splineEndOut = otherSlotOut.GetEnd();
			TrackNodeSlot.Reuse(otherSlotOut);

			if (splineEndOut == null)
			{
				throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
			}
			if (splineEndOut.GetInnerEnd() == IntPtr.Zero)
			{
				SplineEnd.Reuse(splineEndOut);
				throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
			}

			workEntry = splineWork[splineEndOut.GetSpline()];

			if (slotInIndex == 0)   // spline in and spline out are opposite
			{
				increasingMilepost = (front == splineEndOut.IsStart()) ? !workEntry.increasingMilepost : workEntry.increasingMilepost;
			}
			else                    // spline in and spline out are roughly parallel (exiting node together)
			{
				increasingMilepost = (front == splineEndOut.IsStart()) ? workEntry.increasingMilepost : !workEntry.increasingMilepost;
			}

			SplineEnd.Reuse(splineEndOut);

			if (workEntry.haveMilepostDirection)
			{
				return increasingMilepost;
			}

			throw new Exception("Indeterminate milepost direction at node " + node.GetSerialID());
		}

		internal void LookupMileposts()
		{
			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				pair.Value.GetMilepostsOnSpline();
			}
		}

		internal void ExtendSplinePathsFromMileposts()
		{
			// Build spline paths extending from splines containing milepost markers.

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;
				SplineStep finalStep;
				Spline otherSpline;

				if (workEntry.milepostMapping.Count > 0)
				{
					if (workEntry.forwardPath == null)
					{
						workEntry.forwardPath = BuildSplinePath(workEntry.spline, true, 2000.0, out workEntry.forwardTermination);

						if (workEntry.forwardTermination == PathEndCondition.Milepost)
						{
							// Connected with another spline that has milepost markers; update workEntry for this spline also.
							finalStep = workEntry.forwardPath[workEntry.forwardPath.Count - 1];
							otherSpline = finalStep.spline;
							if (finalStep.forwards)
							{
								splineWork[otherSpline].reversePath = workEntry.forwardPath;
								splineWork[otherSpline].reverseTermination = PathEndCondition.Milepost;
							}
							else
							{
								splineWork[otherSpline].forwardPath = workEntry.forwardPath;
								splineWork[otherSpline].forwardTermination = PathEndCondition.Milepost;
							}
						}

						for (int i = 1; i < workEntry.forwardPath.Count; i++)
						{
							if ((i < workEntry.forwardPath.Count - 1) || (workEntry.forwardTermination != PathEndCondition.Milepost))
							{
								splineWork[workEntry.forwardPath[i].spline].throughPath = workEntry.forwardPath;
							}
						}
					}

					if (workEntry.reversePath == null)
					{
						workEntry.reversePath = BuildSplinePath(workEntry.spline, false, 2000.0, out workEntry.reverseTermination);

						if (workEntry.reverseTermination == PathEndCondition.Milepost)
						{
							// Connected with another spline that has milepost markers; update workEntry for this spline also.
							finalStep = workEntry.forwardPath[workEntry.forwardPath.Count - 1];
							otherSpline = finalStep.spline;
							if (finalStep.forwards)
							{
								splineWork[otherSpline].reversePath = workEntry.reversePath;
								splineWork[otherSpline].reverseTermination = PathEndCondition.Milepost;
							}
							else
							{
								splineWork[otherSpline].forwardPath = workEntry.reversePath;
								splineWork[otherSpline].forwardTermination = PathEndCondition.Milepost;
							}
						}

						for (int i = 1; i < workEntry.reversePath.Count; i++)
						{
							if ((i < workEntry.reversePath.Count - 1) || (workEntry.reverseTermination != PathEndCondition.Milepost))
							{
								splineWork[workEntry.reversePath[i].spline].throughPath = workEntry.reversePath;
							}
						}
					}
				}
			}
		}

		internal void DetermineMilepostsAtNodesFirstPass()
		{
			// Assign mileposts to spline ends (and nodes) between splines with milepost markers (interpolation), 
			// or between a spline with milepost markers and a dead end (extrapolation).
			// Later passes will handle spline paths without milepost markers, and spline paths 
			// with milepost markers that end at a node.

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;

				if ((workEntry.forwardPath != null) && (workEntry.forwardPath[0].spline == workEntry.spline))
				{
					if (workEntry.forwardTermination == PathEndCondition.Milepost)
					{
						InterpolateMileposts(workEntry.forwardPath);
						workEntry.forwardPathNodesProcessed = true;
					}
					else if (workEntry.forwardTermination == PathEndCondition.DeadEnd)
					{
						ExtrapolateMileposts(workEntry.forwardPath);
						workEntry.forwardPathNodesProcessed = true;
					}
				}

				if ((workEntry.reversePath != null) && (workEntry.reversePath[0].spline == workEntry.spline))
				{
					if (workEntry.reverseTermination == PathEndCondition.Milepost)
					{
						InterpolateMileposts(workEntry.reversePath);
						workEntry.reversePathNodesProcessed = true;
					}
					else if (workEntry.reverseTermination == PathEndCondition.DeadEnd)
					{
						ExtrapolateMileposts(workEntry.reversePath);
						workEntry.reversePathNodesProcessed = true;
					}
				}
			}
		}

		internal bool DetermineMilepostsToTerminalNode(bool allowExtrapolation)
		{
			// Processes forward and reverse paths from milepost markers (like DetermineMilepostsFirstPass), 
			// but for paths that end at a node (instead of a dead end or a milepost marker).		
			// This happens after the first pass, because mileposts need to be assigned to terminal nodes 
			// before mileposts on these splines can be interpolated.

			bool nodesProcessed = false;

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;
				TrackNode terminalNode;
				double terminalMilepost;

				if ((workEntry.forwardPath != null) && !workEntry.forwardPathNodesProcessed && (workEntry.forwardTermination == PathEndCondition.EndsAtNode))
				{
					terminalNode = TrackUtility.GetNodeFromSpline(workEntry.forwardPath[workEntry.forwardPath.Count - 1].spline, !workEntry.forwardPath[workEntry.forwardPath.Count - 1].forwards);
					if ((terminalNode != null) && nodeMileposts.TryGetValue(terminalNode, out terminalMilepost))
					{
						InterpolateMilepostsToTerminalNode(workEntry.forwardPath, terminalMilepost);
						workEntry.forwardPathNodesProcessed = true;
						nodesProcessed = true;
					}
					else if (allowExtrapolation)
					{
						ExtrapolateMileposts(workEntry.forwardPath);
						workEntry.forwardPathNodesProcessed = true;
						nodesProcessed = true;
					}
				}

				if ((workEntry.reversePath != null) && !workEntry.reversePathNodesProcessed && (workEntry.reverseTermination == PathEndCondition.EndsAtNode))
				{
					terminalNode = TrackUtility.GetNodeFromSpline(workEntry.reversePath[workEntry.reversePath.Count - 1].spline, !workEntry.reversePath[workEntry.reversePath.Count - 1].forwards);
					if ((terminalNode != null) && nodeMileposts.TryGetValue(terminalNode, out terminalMilepost))
					{
						InterpolateMilepostsToTerminalNode(workEntry.reversePath, terminalMilepost);
						workEntry.reversePathNodesProcessed = true;
						nodesProcessed = true;
					}
					else if (allowExtrapolation)
					{
						ExtrapolateMileposts(workEntry.reversePath);
						workEntry.reversePathNodesProcessed = true;
						nodesProcessed = true;
					}
				}
			}

			return nodesProcessed;
		}

		internal void BuildRemainingSplinePaths()
		{
			// Build spline paths for splines that aren't between mileposts and aren't on track 
			// directly extending from mileposts. Typically, these are sidings or yard track.

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;
				SplineWork tempEntry;

				if (workEntry.forwardPath == null && workEntry.throughPath == null && workEntry.reversePath == null)
				{
					List<SplineStep> throughPath;
					SplineStep step;
					SplineEnd endOut;       // Spline end exiting a node (and starting a spline path step)
					SplineEnd endIn;        // Spline end entering a node

					// Find all splines connecting to the current spline (workEntry.spline) in a straight path
					// in either direction, and store them in throughPath.

					throughPath = new List<SplineStep>();
					endOut = FindStartofSplinePath(workEntry.spline);

					while (endOut != null)
					{
						step = new SplineStep(endOut);
						throughPath.Add(step);

						endIn = endOut.GetSpline().GetEnd(!endOut.IsStart());
						SplineEnd.Reuse(endOut);
						endOut = TrackUtility.GetStraightExitSplineEnd(endIn);
						SplineEnd.Reuse(endIn);

						if (endOut != null)
						{
							tempEntry = splineWork[endOut.GetSpline()];
							if (tempEntry.forwardPath != null || tempEntry.throughPath != null || tempEntry.reversePath != null)
							{
								SplineEnd.Reuse(endOut);
								break;
							}
						}
					}

					// Store a reference to throughPath in the SplineWork entries for all of the splines 
					// contained in throughPath.

					for (int n = 0; n < throughPath.Count; n++)
					{
						tempEntry = splineWork[throughPath[n].spline];
						tempEntry.throughPath = throughPath;
					}
				}
			}

			// It should be true now that every spline in the whole route is assigned to some spline path.
		}

		internal bool DetermineMilepostsAtNodes()
		{
			// Check spline paths to see if milepost has been determined at end nodes. 
			// If so, compute mileposts at nodes in between, either by interpolation
			// (if mileposts are known at both ends), or by extrapolation.

			bool nodesProcessed = false;

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;

				if (workEntry.throughPath != null && workEntry.throughPath[0].spline == workEntry.spline && !workEntry.throughPathNodesProcessed)
				{
					TrackNode startNode;
					TrackNode endNode;
					int lastIndex;

					startNode = TrackUtility.GetNodeFromSpline(workEntry.throughPath[0].spline, workEntry.throughPath[0].forwards);
					lastIndex = workEntry.throughPath.Count - 1;
					endNode = TrackUtility.GetNodeFromSpline(workEntry.throughPath[lastIndex].spline, !workEntry.throughPath[lastIndex].forwards);

					int startIndex = startNode == null ? -1 : startNode.GetSerialID();
					int endIndex = endNode == null ? -1 : endNode.GetSerialID();

					if (startNode == null && endNode == null)
					{
						continue;
					}

					double startingMilepost = -999.0;
					double endingMilepost = -999.0;

					if ((startNode != null) && !nodeMileposts.TryGetValue(startNode, out startingMilepost))
					{
						continue;
					}

					if ((endNode != null) && !nodeMileposts.TryGetValue(endNode, out endingMilepost))
					{
						continue;
					}

					workEntry.throughPathNodesProcessed = true;
					nodesProcessed = true;

					if ((startNode != null) && (endNode != null))
					{
						InterpolateMilepostsBetweenNodes(workEntry.throughPath, startingMilepost, endingMilepost);
					}
					else
					{
						ExtrapolateMilepostsFromNode(workEntry.throughPath, startingMilepost, endingMilepost);
					}
				}
			}

			nodesProcessed |= DetermineMilepostsToTerminalNode(false);

			return nodesProcessed;
		}

		internal bool DetermineMilepostsAtNodesForced()
		{
			// Similar to DetermineMilepostsAtNodes, except that extrapolation is forced when 
			// one of the ending nodes does not have a milepost assigned.
			// If forcing is not necessary, wait to do extrapolation until a regular 
			// DetermineMilepostsAtNodes call.
			// This method is only called after exhaustively calling DetermineMilepostsAtNodes, 
			// because it is preferable to have mileposts at both ends and do interpolation.

			bool nodesProcessed = false;

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;

				if (workEntry.throughPath != null && workEntry.throughPath[0].spline == workEntry.spline && !workEntry.throughPathNodesProcessed)
				{
					TrackNode startNode;
					TrackNode endNode;
					int lastIndex;

					startNode = TrackUtility.GetNodeFromSpline(workEntry.throughPath[0].spline, workEntry.throughPath[0].forwards);
					lastIndex = workEntry.throughPath.Count - 1;
					endNode = TrackUtility.GetNodeFromSpline(workEntry.throughPath[lastIndex].spline, !workEntry.throughPath[lastIndex].forwards);

					if (startNode == null || endNode == null)
					{
						continue;
					}

					double startingMilepost = -999.0;
					double endingMilepost = -999.0;
					int missingCount = 0;

					if (!nodeMileposts.TryGetValue(startNode, out startingMilepost))
					{
						startingMilepost = -999.0;
						missingCount++;
					}

					if (!nodeMileposts.TryGetValue(endNode, out endingMilepost))
					{
						endingMilepost = -999.0;
						missingCount++;
					}

					if (missingCount != 1)
					{
						continue;
					}

					workEntry.throughPathNodesProcessed = true;
					nodesProcessed = true;

					ExtrapolateMilepostsFromNode(workEntry.throughPath, startingMilepost, endingMilepost);
				}
			}

			return nodesProcessed;
		}

		internal bool SplitPathsAtMilepostNodes()
		{
			// Calling DetermineMilepostsAtNodesForced will sometimes set the milepost for a node in 
			// the middle of another spline path. In this case, the spline path must be broken up into 
			// shorter paths, so that no path has a milepost defined for a middle node.
			// If this is not done, interpolation or extrapolation on this path will overwrite the 
			// existing milepost value and cause inconsistency with connected splines.

			bool pathsSplit = false;

			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;

				if (workEntry.throughPath != null && workEntry.throughPath[0].spline == workEntry.spline && !workEntry.throughPathNodesProcessed)
				{
					int splineIndex = 0;
					while (splineIndex < workEntry.throughPath.Count - 1)
					{
						SplineEnd splineEnd = workEntry.throughPath[splineIndex].spline.GetEnd(!workEntry.throughPath[splineIndex].forwards);
						TrackNode node = TrackUtility.GetNodeFromSplineEnd(splineEnd);
						SplineEnd.Reuse(splineEnd);

						// 'node' is a middle node (between two splines). Check if a milepost is assigned to this node.
						if (nodeMileposts.ContainsKey(node))
						{
							// Split path at this node. 
							List<SplineStep> remainingPath = new List<SplineStep>();
							for (int i = splineIndex + 1; i < workEntry.throughPath.Count; i++)
							{
								remainingPath.Add(workEntry.throughPath[i]);
							}
							workEntry.throughPath.RemoveRange(splineIndex + 1, workEntry.throughPath.Count - splineIndex - 1);

							for (int i = 0; i < remainingPath.Count; i++)
							{
								workEntry = splineWork[remainingPath[i].spline];
								workEntry.throughPath = remainingPath;
							}

							// Continue processing remaining path:
							workEntry = splineWork[remainingPath[0].spline];
							splineIndex = 0;
							pathsSplit = true;
						}
						else
						{
							splineIndex++;
						}
					}
				}
			}

			return pathsSplit;
		}

		internal void ComputeSourcePosition()
		{
			foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
			{
				SplineWork workEntry = pair.Value;

				if (workEntry.milepostMapping.Count > 0)
				{
					SplineEnd frontEnd = workEntry.spline.GetFrontEnd();
					SplinePointIterator spi = new SplinePointIterator(frontEnd);
					SplineEnd.Reuse(frontEnd);

					double previousMilepost = -999.0;
					double previousPosition = 0.0;

					foreach (KeyValuePair<double, double> currentMilepost in workEntry.milepostMapping)
					{
						while (spi.IsBeforeEnd())
						{
							SplinePoint splinePt = spi.GetIteratorPoint();
							double sourcePosition;

							if (splinePt.GetMeters() > currentMilepost.Key)
							{
								previousPosition = currentMilepost.Key;
								previousMilepost = currentMilepost.Value;
								break;
							}

							if (previousMilepost == -999.0)
							{
								sourcePosition = currentMilepost.Value;
							}
							else
							{
								sourcePosition = previousMilepost + (splinePt.GetMeters() - previousPosition) * (currentMilepost.Value - previousMilepost) / (currentMilepost.Key - previousPosition);
							}

							splinePt.SetSourcePosition(sourcePosition);
							spi.Iterate();
						}
					}

					while (spi.IsBeforeEnd())
					{
						SplinePoint splinePt = spi.GetIteratorPoint();
						splinePt.SetSourcePosition(previousMilepost);
						spi.Iterate();
					}
				}
			}
		}

		//public void LogNodeMileposts(bool sortByMilepost)
		//{
		//	int[] nodeSerialIds;
		//	double[] mileposts;
		//	int index;

		//	nodeSerialIds = new int[nodeMileposts.Count];
		//	mileposts = new double[nodeMileposts.Count];
		//	index = 0;

		//	foreach (KeyValuePair<TrackNode, double> current in nodeMileposts)
		//	{
		//		nodeSerialIds[index] = current.Key.GetSerialID();
		//		mileposts[index] = current.Value;
		//		index++;
		//	}

		//	if (sortByMilepost)
		//	{
		//		Array.Sort(mileposts, nodeSerialIds);
		//	}
		//	else
		//	{
		//		Array.Sort(nodeSerialIds, mileposts);
		//	}

		//	Log.Write("MilepostComputation: mileposts of nodes");

		//	for (int k = 0; k < nodeSerialIds.Length; k++)
		//	{
		//		Log.Write("  node {0} at milepost {1:0.000}", nodeSerialIds[k], mileposts[k]);
		//	}

		//	Log.WriteAndFlush("");
		//}

		//public void LogSplineCount()
		//{
		//	int totalSplines = 0;
		//	int splinesWithMileposts = 0;
		//	foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
		//	{
		//		SplineWork workEntry = pair.Value;

		//		totalSplines++;
		//		if (workEntry.milepostMapping.Count > 0)
		//		{
		//			splinesWithMileposts++;
		//		}
		//	}

		//	Log.WriteAndFlush("Splines with mileposts: {0}, total splines: {1}", splinesWithMileposts, totalSplines);
		//}

		//public void LogSplinePathWithNode(int nodeId, bool allPathReferences)
		//{
		//	List<int> nodeList = new List<int>();

		//	foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
		//	{
		//		SplineWork workEntry = pair.Value;
		//		TrackNode node;

		//		if ((workEntry.throughPath != null) && ((workEntry.throughPath[0].spline == workEntry.spline) || allPathReferences))
		//		{
		//			nodeList.Clear();

		//			node = TrackUtility.GetNodeFromSpline(workEntry.throughPath[0].spline, workEntry.throughPath[0].forwards);
		//			nodeList.Add(node == null ? -1 : node.GetSerialID());
		//			for (int i = 0; i < workEntry.throughPath.Count; i++)
		//			{
		//				node = TrackUtility.GetNodeFromSpline(workEntry.throughPath[i].spline, !workEntry.throughPath[i].forwards);
		//				nodeList.Add(node == null ? -1 : node.GetSerialID());
		//			}

		//			if (nodeList.Contains(nodeId))
		//			{
		//				System.Text.StringBuilder sb = new System.Text.StringBuilder();
		//				sb.Append("  MilepostComputation: spline path ");
		//				for (int i = 0; i < nodeList.Count; i++)
		//				{
		//					if (i > 0) sb.Append('_');
		//					sb.Append(nodeList[i]);
		//				}
		//				Log.Write(sb.ToString());
		//			}
		//		}
		//	}
		//}

		//public void LogMissingSplines()
		//{
		//	Log.Write("The following splines do not have milepost data:");

		//	foreach (KeyValuePair<Spline, SplineWork> pair in splineWork)
		//	{
		//		SplineWork workEntry = pair.Value;

		//		if (workEntry.milepostMapping.Count == 0)
		//		{
		//			PointXYZSE startPt = workEntry.spline.GetPoint(true).GetPt();
		//			PointXYZSE endPt = workEntry.spline.GetPoint(false).GetPt();

		//			Log.Write("  spline from ({0:0.0}, {1:0.0}) to ({2:0.0}, {3:0.0})", startPt.x, startPt.z, endPt.x, endPt.z);

		//			TrackNode startNode = TrackUtility.GetNodeFromSpline(workEntry.spline, true);
		//			TrackNode endNode = TrackUtility.GetNodeFromSpline(workEntry.spline, false);

		//			int startIndex = startNode == null ? -1 : startNode.GetSerialID();
		//			int endIndex = endNode == null ? -1 : endNode.GetSerialID();
		//			Log.Write("    (node {0} to node {1})", startIndex, endIndex);
		//		}
		//	}
		//	Log.WriteAndFlush("");
		//}
	}
}