﻿using TMTSPluginsCS;

namespace SimpleSimulation
{
	/// <summary>
	/// Represents a locomotive in the SimpleSimulation.  Provides the ability to get and set locomotive control states.
	/// </summary>
	public class Locomotive : Car
	{
		private int locoPlugInId;

		internal Locomotive(global::Car internalCar) : base(internalCar)
		{
			locoPlugInId = TMTSPlugInsManager.GetPlugInId("DieselElectrics");
		}

		/// <summary>
		/// Sets the reverser position on the locomotive
		/// </summary>
		/// <param name="position">Reverser position</param>
		public void SetReverserPosition(ReverserPosition position)
		{
			PlugInMessage m = TMTSPlugInsManager.plugInSystem.CreateMessage(
				ScriptConstants.USER_ACTION_REVERSER_POSITION, innerCar.uniqueId, (int)position);
			TMTSPlugInsManager.plugInSystem.ScheduleMessage(m, 0, locoPlugInId, 0.0, 0.0);
		}

		/// <summary>
		/// Sets the throttle on the locomotive
		/// </summary>
		/// <param name="notch">Throttle notch</param>
		public void SetThrottleNotch(ThrottleNotch notch)
		{
			PlugInMessage throttleMsg = TMTSPlugInsManager.plugInSystem.CreateMessage(
					ScriptConstants.USER_ACTION_THROTTLE_POSITION, innerCar.uniqueId, (int)notch);
			TMTSPlugInsManager.plugInSystem.ScheduleMessage(throttleMsg, 0, locoPlugInId, 0.0, 0.0);
		}

		/// <summary>
		/// Sets dynamic brake on the locomotive
		/// </summary>
		/// <param name="percent">Dynamic brake percentage between 0.0 and 1.0</param>
		public void SetDynamicBrakePercent(double percent)
		{
			if (percent < 0.0 || percent > 100.0)
			{
				throw new System.ArgumentOutOfRangeException("percent", percent, "Dynamic brake percentage must be between 0.00 and 1.00.");
			}

			PlugInMessage dbMsg = TMTSPlugInsManager.plugInSystem.CreateMessage(
					ScriptConstants.USER_ACTION_DYNAMIC_BRAKE_POSITION, innerCar.uniqueId, percent);
			TMTSPlugInsManager.plugInSystem.ScheduleMessage(dbMsg, 0, locoPlugInId, 0.0, 0.0);
		}

		/// <summary>
		/// Gets the current reverser position of the locomotive
		/// </summary>
		/// <returns>Reverser Position</returns>
		public ReverserPosition GetReverserPosition()
		{
			PlugInMessage msg = TMTSPlugInsManager.plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_VALUE);
			msg.args.iParam = ScriptConstants.USER_ACTION_REVERSER_POSITION;
			if (TMTSPlugInsManager.plugInSystem.DeliverMessageNow(msg, 0, locoPlugInId) == 1)
			{
				return (ReverserPosition)msg.args.iParam;
			}
			else
			{
				throw new System.Exception("TMTS Message Failed: " + msg.ToString());
			}
		}

		/// <summary>
		/// Gets the current throttle notch of the locomotive
		/// </summary>
		/// <returns>Throttle notch</returns>
		public ThrottleNotch GetThrottleNotch()
		{
			PlugInMessage msg = TMTSPlugInsManager.plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_VALUE);
			msg.args.iParam = ScriptConstants.USER_ACTION_THROTTLE_POSITION;
			//msg.vParam.push_back(car.uniqueId);
			if (TMTSPlugInsManager.plugInSystem.DeliverMessageNow(msg, 0, locoPlugInId) == 1)
			{
				return (ThrottleNotch)msg.args.iParam;
			}
			else
			{
				throw new System.Exception("TMTS Message Failed: " + msg.ToString());
			}
		}

		/// <summary>
		/// Gets the current dynamic brake notch of the locomotive
		/// </summary>
		/// <returns>DB notch</returns>
		public DynamicBrakeNotch GetDynamicBrakeNotch()
		{
			PlugInMessage msg = TMTSPlugInsManager.plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_VALUE);
			msg.args.iParam = ScriptConstants.USER_ACTION_DYNAMIC_BRAKE_POSITION;
			//msg.vParam.push_back(car.uniqueId);
			if (TMTSPlugInsManager.plugInSystem.DeliverMessageNow(msg, 0, locoPlugInId) == 1)
			{
				return (DynamicBrakeNotch)msg.args.iParam;
			}
			else
			{
				throw new System.Exception("TMTS Message Failed: " + msg.ToString());
			}
		}

		/// <summary>
		/// Converts dynamic brake percentage to notch
		/// </summary>
		/// <param name="percent">Dynamic brake percentage</param>
		/// <returns>Dynamic brake notch</returns>
		public static DynamicBrakeNotch ConvertDbPercentToNotch(double percent)
		{
			if (percent < 0.0 || percent > 100.0)
			{
				throw new System.ArgumentOutOfRangeException("percent", percent, "Dynamic brake percentage must be between 0.00 and 1.00.");
			}

			if (0.0 == percent) return DynamicBrakeNotch.Off;
			else if (0.12 > percent) return DynamicBrakeNotch.Setup;
			else if (0.19 > percent) return DynamicBrakeNotch.DB1;
			else if (0.31 > percent) return DynamicBrakeNotch.DB2;
			else if (0.44 > percent) return DynamicBrakeNotch.DB3;
			else if (0.56 > percent) return DynamicBrakeNotch.DB4;
			else if (0.69 > percent) return DynamicBrakeNotch.DB5;
			else if (0.81 > percent) return DynamicBrakeNotch.DB6;
			else if (0.94 > percent) return DynamicBrakeNotch.DB7;
			else return DynamicBrakeNotch.DB8;
		}

		/// <summary>
		/// Converts dynamic brake notch to percentage
		/// </summary>
		/// <param name="notch">Dynamic brake notch</param>
		/// <returns>Dynamic brake percentage between 0.0 and 1.0.</returns>
		public static double ConvertDbNotchToPercent(DynamicBrakeNotch notch)
		{
			switch (notch)
			{
				case DynamicBrakeNotch.Off:
					return 0.0;
				case DynamicBrakeNotch.Setup:
					return 0.12;
				case DynamicBrakeNotch.DB1:
					return 0.19;
				case DynamicBrakeNotch.DB2:
					return 0.31;
				case DynamicBrakeNotch.DB3:
					return 0.44;
				case DynamicBrakeNotch.DB4:
					return 0.56;
				case DynamicBrakeNotch.DB5:
					return 0.69;
				case DynamicBrakeNotch.DB6:
					return 0.81;
				case DynamicBrakeNotch.DB7:
					return 0.94;
				case DynamicBrakeNotch.DB8:
					return 1.0;
				default:
					return -1.0;
			}
		}

		/// <summary>
		/// Reverser positions
		/// </summary>
		public enum ReverserPosition
		{
			/// <summary>Forward Reverser Position</summary>
			Forward = 1,
			/// <summary>Neutral Reverser Position</summary>
			Neutral = 0,
			/// <summary>Reverse Reverser Position</summary>
			Reverse = -1
		}

		/// <summary>
		/// Throttle notch positions
		/// </summary>
		public enum ThrottleNotch
		{
			/// <summary>Idle</summary>
			Idle = 0,
			/// <summary>Notch 1</summary>
			N1 = 1,
			/// <summary>Notch 2</summary>
			N2 = 2,
			/// <summary>Notch 3</summary>
			N3 = 3,
			/// <summary>Notch 4</summary>
			N4 = 4,
			/// <summary>Notch 5</summary>
			N5 = 5,
			/// <summary>Notch 6</summary>
			N6 = 6,
			/// <summary>Notch 7</summary>
			N7 = 7,
			/// <summary>Notch 8</summary>
			N8 = 8
		}

		/// <summary>
		/// Dynamic brake notch positions
		/// </summary>
		public enum DynamicBrakeNotch
		{
			/// <summary>Dynamic Brake Off</summary>
			Off = -1,
			/// <summary>Dynamic Brake Setup</summary>
			Setup = 0,
			/// <summary>Dynamic Brake Notch 1</summary>
			DB1 = 1,
			/// <summary>Dynamic Brake Notch 2</summary>
			DB2 = 2,
			/// <summary>Dynamic Brake Notch 3</summary>
			DB3 = 3,
			/// <summary>Dynamic Brake Notch 4</summary>
			DB4 = 4,
			/// <summary>Dynamic Brake Notch 5</summary>
			DB5 = 5,
			/// <summary>Dynamic Brake Notch 6</summary>
			DB6 = 6,
			/// <summary>Dynamic Brake Notch 7</summary>
			DB7 = 7,
			/// <summary>Dynamic Brake Notch 8</summary>
			DB8 = 8
		}
	}
}