﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMTSPluginsCS;

namespace SimpleSimulation
{
	internal class TrackUtility : TrackCS.TrackUtility
	{
		internal static global::Track globalTrack;

		internal static int ToggleNode(int trackFileID)
		{
			return ToggleNode(trackFileID, true);
		}
		internal static int ToggleNode(int trackFileID, bool broadCastOverNetwork)
		{
			int databasePlugInId = TMTSPlugInsManager.GetPlugInId("Database");
			if (databasePlugInId == -1)
			{
				return -1;
			}

			TrackNode node = globalTrack.GetNode(trackFileID, false);
			if (node == null)
			{
				return -1;
			}

			PlugInMessage msg = TMTSPlugInsManager.plugInSystem.CreateMessage(
				ScriptConstants.TMTS_MSG_SET_PROPERTY, node.uniqueId, 0.0);
			msg.args.sParam.assign("toggleNext");
			if (!broadCastOverNetwork)
			{
				msg.args.dParam = -1.0;
			}



			int result = TMTSPlugInsManager.plugInSystem.DeliverMessageNow(msg, -1, databasePlugInId);
			TMTSPlugInsManager.plugInSystem.DestroyMessage(msg);

			return result;
		}

		internal static TrackNode GetNodeFromTrackFileID(int id)
		{
			TrackNode trackNode;

			trackNode = globalTrack.GetNode(id, false);

			if (trackNode == null)
			{
				throw new Exception("Invalid Track Node Id: " + id);
			}

			return trackNode;
		}

		internal static SplineMarker GetMarkerFromTrackFileID(int id)
		{
			SplineMarker marker;

			marker = globalTrack.GetMarker(id);

			return marker;
		}

		internal static void AddMilepostDataToSourcePosition(global::Track track)
		{
			TrackMilepostComputation milepostComputation = new TrackMilepostComputation(track);
			bool processing = true;

			milepostComputation.LookupMileposts();
			milepostComputation.ExtendSplinePathsFromMileposts();
			milepostComputation.DetermineMilepostsAtNodesFirstPass();
			milepostComputation.BuildRemainingSplinePaths();

			while (processing)
			{
				while (milepostComputation.DetermineMilepostsAtNodes()) ;

				if (milepostComputation.DetermineMilepostsAtNodesForced())
				{
					milepostComputation.SplitPathsAtMilepostNodes();
				}
				else
				{
					processing = milepostComputation.DetermineMilepostsToTerminalNode(true);
				}
			}

			milepostComputation.ComputeSourcePosition();
		}

		internal static void GetAllNodes(global::Track track, List<global::Track.IdAndNode> nodesOut)
		{
			global::Track.IntIntptrPair[] nodes;
			int numNodes = track.GetNumberOfNodes(out nodes);

			for (int n = 0; n < numNodes; n++)
			{
				TrackNode node = TrackNode.GetNode(nodes[n].ptr);
				nodesOut.Add(new global::Track.IdAndNode(nodes[n].i, node));
			}
		}

		internal static bool GetCoordinatesFromMarker(int markerId, out double xCoordinate, out double zCoordinate)
		{
			SplineMarker marker = TrackUtility.GetMarkerFromTrackFileID(markerId);
			if (marker != null)
			{
				SplineLocation markerSplineLocation = marker.GetSplineLocation();
				xCoordinate = markerSplineLocation.GetCoordinates().x;
				zCoordinate = markerSplineLocation.GetCoordinates().z;
				SplineLocation.Reuse(markerSplineLocation);
				return true;
			}

			xCoordinate = 0.0;
			zCoordinate = 0.0;
			return false;
		}

		internal static SplineLocation GetSplineLocationFromMilepost(double milepostPosition, out bool forwardsIsIncreasing)
		{
			SplineMarker referenceMarker;
			double offsetMeters;
			int milepost;

			milepost = (int)milepostPosition;

			referenceMarker = globalTrack.GetMarkerFromMilepost(milepost);
			if (referenceMarker == null)
			{
				milepost++;
				referenceMarker = globalTrack.GetMarkerFromMilepost(milepost);
			}

			if (referenceMarker == null)
			{
				forwardsIsIncreasing = true;
				return null;
			}

			offsetMeters = (milepostPosition - (double)milepost) * UnitConversions.MILES_TO_METERS;

			SplineLocation splineLocation = referenceMarker.GetSplineLocation();
			TrackLocation trackLocation = new TrackLocation(splineLocation, referenceMarker.IsMarkerForward(), false);
			SplineLocation.Reuse(splineLocation);

			trackLocation.MoveDistance(offsetMeters);
			forwardsIsIncreasing = trackLocation.IsForwardOnTrack();

			return new SplineLocation(trackLocation.GetSplineLocation());
		}


		//public static SplineLocation GetSplineLocationFromStartLocation(double position, out bool forwardsOnSpline)
		//{
		//	List<SplineMarker> startLocationMarkers;

		//	startLocationMarkers = new List<SplineMarker>();
		//	globalTrack.GetAllMarkersOfType(startLocationMarkers, (int)SplineMarker.MarkerType.ConsistPlacements);
		//	if (startLocationMarkers.Count == 0)
		//	{
		//		forwardsOnSpline = true;
		//		return null;
		//	}

		//	SplineLocation splineLocation = startLocationMarkers[0].GetSplineLocation();
		//	TrackLocation startingLocation = new TrackLocation(splineLocation, startLocationMarkers[0].IsMarkerForward(), false);
		//	SplineLocation.Reuse(splineLocation);

		//	startingLocation.MoveDistance(position);

		//	forwardsOnSpline = startingLocation.IsForwardOnTrack();
		//	splineLocation = startingLocation.GetSplineLocation();
		//	TrackLocation.Reuse(startingLocation);

		//	return splineLocation;
		//}

		//public static SplineLocation GetSplineLocationFromEndLocation(double position, out bool forwardsOnSpline)
		//{
		//	List<SplineMarker> startLocationMarkers;

		//	startLocationMarkers = new List<SplineMarker>();
		//	globalTrack.GetAllMarkersOfType(startLocationMarkers, (int)SplineMarker.MarkerType.ConsistPlacements);
		//	if (startLocationMarkers.Count == 0)
		//	{
		//		forwardsOnSpline = true;
		//		return null;
		//	}

		//	SplineLocation splineLocation = startLocationMarkers[startLocationMarkers.Count - 1].GetSplineLocation();
		//	TrackLocation endingLocation = new TrackLocation(splineLocation, startLocationMarkers[startLocationMarkers.Count - 1].GetInfo().IsForward(), false);
		//	SplineLocation.Reuse(splineLocation);

		//	endingLocation.MoveDistance(position);

		//	forwardsOnSpline = endingLocation.IsForwardOnTrack();
		//	splineLocation = endingLocation.GetSplineLocation();
		//	TrackLocation.Reuse(endingLocation);

		//	return splineLocation;
		//}
	}
}