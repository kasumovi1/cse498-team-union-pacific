﻿using System;
using System.Collections.Generic;
using TMTSPluginsCS;

namespace SimpleSimulation
{
	/// <summary>
	/// Represents the consist in simple simulation.  Provides the ability to simulate and access data of the consist.
	/// </summary>
	public class Consist
	{
		/// <summary>
		/// Directory containing the DieselElectrics.ndll file
		/// </summary>
		public static string DIESEL_ELECTRICS_DIRECTORY { get; set; }

		static Consist()
		{
			allConsists = new List<Consist>();
		}

		public static List<Consist> allConsists;


		/// <summary>
		/// The lead car in the consist
		/// </summary>
		public Car FirstCar { get; private set; }

		/// <summary>
		/// The rear car in the consist
		/// </summary>
		public Car LastCar { get; private set; }

		/// <summary>
		/// Constructor for simulation Consist object
		/// </summary>
		/// <param name="cstFile">Consist filename</param>
		public Consist(string cstFile)
		{
			if (allConsists.Count >= 1)
			{
				throw new NotSupportedException("SimpleSimulation currently only supports 1 consist per simulation.");
			}
			allConsists.Add(this);

			TMTSPlugInsManager.LaunchPlugInSystem(DIESEL_ELECTRICS_DIRECTORY);
			//UpdatePlugInSystem(1);

			DatabasePlugIn databasePlugIn = new DatabasePlugIn();
			TMTSPlugInsManager.RegisterInternalPlugIn(databasePlugIn, "Database", string.Empty);
			//UpdatePlugInSystem(1);

			innerConsist = global::Consist.Load(cstFile);
			//UpdatePlugInSystem(1);

			SetupLinkedList();

			PlugInMessage msg = TMTSPlugInsManager.plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_CONTROL_LOCO, FirstCar.innerCar.uniqueId, 0.0);
			msg.vParam.push_back(FirstCar.innerCar.scriptID);
			msg.vParam.push_back(FirstCar.innerCar.uniqueId);
			TMTSPlugInsManager.plugInSystem.ScheduleMessage(msg, 0, ScriptConstants.BROADCAST_HANDLE, 0.0, 0.0);
			//UpdatePlugInSystem(1);
		}

		/// <summary>
		/// Starts the simulation of the consist
		/// </summary>
		public void StartSimulating()
		{
			TMTSPlugInsManager.PostMessage(ScriptConstants.BROADCAST_HANDLE, ScriptConstants.TMTS_MSG_VALUE, 0.0, ScriptConstants.TMTS_GLOBAL_SETTINGS_ACTIVE, 1.0, null, null);

			if (innerConsist != null && innerConsist.consistPhysics != null)
			{
				if (innerConsist.consistPhysics.QuasiPausedState != 0)
				{
					innerConsist.consistPhysics.QuasiPausedState = 0;
				}
			}

			TMTSPlugInsManager.SendMessage(ScriptConstants.BROADCAST_HANDLE, ScriptConstants.TMTS_MSG_VALUE, TMTSPlugInsManager.MessageConstant("TMTS_MSG_QUASI_PAUSE_ENDED"), 0.0, string.Empty);
		}

		/// <summary>
		/// Simulate the consist for the given timestep.
		/// </summary>
		/// <param name="timestep">Elapsed timestep to simulate in milliseconds</param>
		public void Simulate(double timestep)
		{
			realTime += timestep;
			simTime += timestep;

			TMTSPlugInsManager.plugInSystem.UpdateRealTime(realTime, timestep);
			TMTSPlugInsManager.plugInSystem.UpdateSimTime(simTime, timestep);
			TMTSPlugInsManager.plugInSystem.DeliverMessages();

			double meters = innerConsist.consistPhysics.DoPhysics((float)(50*timestep/1000.0));
			innerConsist.Move(meters);
		}

		/// <summary>
		/// Gets the number of cars in the consist.
		/// </summary>
		/// <returns>Number of cars in the consist</returns>
		public int GetCarCount()
		{
			return innerConsist.CountCars();
		}

		/// <summary>
		/// Gets all locomotives in the consist in order from lead to rear
		/// </summary>
		/// <returns>List of all locomotives in the consist</returns>
		public List<Locomotive> GetLocomotives()
		{
			List<Locomotive> allLocomotives = new List<Locomotive>();
			for (Car car = FirstCar; car != null; car = car.Next)
			{
				if (car is Locomotive)
				{
					allLocomotives.Add(car as Locomotive);
				}
			}
			return allLocomotives;
		}

		internal global::Consist innerConsist;

		private double realTime = 0.0;
		private double simTime = 0.0;

		private Car CreateCarOrLocomotive(global::Car internalCar)
		{
			if (internalCar.IsLocomotive())
				return new Locomotive(internalCar);
			else
				return new Car(internalCar);
		}

		private void SetupLinkedList()
		{
			global::Car internalCar = innerConsist.firstCar;
			FirstCar = CreateCarOrLocomotive(internalCar);
			Car previousCar = FirstCar;
			for (internalCar = internalCar.next; internalCar != null; internalCar = internalCar.next)
			{
				Car car = CreateCarOrLocomotive(internalCar);
				previousCar.Next = car;
				car.Prev = previousCar;
				previousCar = car;
			}
			LastCar = previousCar;
		}

		public static void Reset()
        {
			TMTSPlugInsManager.Reset();
			foreach(Consist consist in allConsists)
            {
				consist.innerConsist.consistPhysics = null;
            }
			allConsists.Clear();

			if (TrackUtility.globalTrack != null)
            {
				TrackUtility.globalTrack.Dispose();
				TrackUtility.globalTrack = null;
            }

			TmtsMasterIndex.Reset();
        }
	}
}