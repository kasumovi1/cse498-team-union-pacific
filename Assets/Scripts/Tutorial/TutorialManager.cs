using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class TutorialManager : MonoBehaviour
{
    public GameObject[] popups;
    protected int popupIndex = 0;
    public AudioSource clickSound;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    virtual protected void Update()
    {
        if (popupIndex < popups.Length && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)))
        {
            SkipText();
        }
        for (int i = 0; i < popups.Length; i++)
        {
            if (i == popupIndex)
            {
                if (!GameManager.GameisOver)
                    popups[i].SetActive(true);
                else
                    popups[i].SetActive(false);
            }
            else
            {
                popups[i].SetActive(false);
            }
        }
    }

    public void MoveOn()
    {
        ++popupIndex;
        clickSound.ignoreListenerPause = true;
        clickSound.Play();
    }

    public void SkipText()
    {
        TutorialText tt = FindObjectOfType<TutorialText>();
        if (!tt.finished)
        {
            tt.StopCoroutine("PlayText");
            tt.text.text = tt.originalContent;
        }
    }

}
