using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial1 : TutorialManager
{
    // Put any necessary objects here
    public GameObject tutorialCanvas;
    private List<TutorialText> popupTexts = new List<TutorialText>();
    public GameObject rlTime, milesT, mph, grad, pause, key, throttles, wg, fv;
    public ThrottleControl throttleC;
    // Start is called before the first frame update
    private float timer = 0f;
    public float blinkInterval = 0.5f;
    void Start()
    {
        clickSound = GameObject.Find("LevelEssentials/SoundBank/UI/Special3").GetComponent<AudioSource>();
        for (int i = 0; i < tutorialCanvas.transform.childCount; i++)
        {
            popupTexts.Add(tutorialCanvas.transform.GetChild(i).Find("Text").GetComponent<TutorialText>());
        }
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
        if ((popupIndex >= 0 && popupIndex <= 1) || (popupIndex >= 5 && popupIndex <= 7) || popupIndex == 13)
        {
            // condition when the user does what the tutorial explains
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
            }
        }
        else if (popupIndex == 2)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                rlTime.SetActive(true);
                milesT.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                rlTime.SetActive(!rlTime.activeInHierarchy);
                milesT.SetActive(!milesT.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 3)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                mph.SetActive(true);
                grad.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                mph.SetActive(!mph.activeInHierarchy);
                grad.SetActive(!grad.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 4)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                pause.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                pause.SetActive(!pause.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 8)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                key.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                key.SetActive(!key.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 9 || popupIndex == 10)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                throttles.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                throttles.SetActive(!throttles.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 11)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                fv.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                fv.SetActive(!fv.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }
        else if (popupIndex == 12)
        {
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                timer = 0f;
                wg.SetActive(true);
            }
            if (timer >= blinkInterval)
            {
                timer = 0f;
                wg.SetActive(!wg.activeInHierarchy);
            }
            timer += Time.unscaledDeltaTime;
        }

        else if (popupIndex == 14)
        {
            // Check if throttles are 6 or velocity is greater than 1
            if (popupTexts[popupIndex].finished && ((throttleC.currentNotches[0] == SimpleSimulation.Locomotive.ThrottleNotch.N6 && throttleC.currentNotches[1] == SimpleSimulation.Locomotive.ThrottleNotch.N6 && throttleC.consist.FirstCar.GetVelocityMPS() > 1) || throttleC.consist.FirstCar.GetVelocityMPS() > 5 )) // text is displayed and user clicked/touched
            {
                timer = 0f;
                MoveOn();
            }
        }
        else if (popupIndex == 15 || popupIndex == 16)
        {
            Time.timeScale = 0f;
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                Time.timeScale = 1f;
            }
            if (timer < 5f && popupTexts[popupIndex].finished)
            {
                timer += Time.unscaledDeltaTime;
            }
        }
        else
        {
            
        }

        // TODO: Next talk about train and locomotive indicators, then throttle controls, then force bar and thresholds and timer, then weight graph for convenience and how you can get rid of it
    }
}
