using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lordsburg5 : TutorialManager
{
    // Put any necessary objects here
    public GameObject tutorialCanvas;
    private List<TutorialText> popupTexts = new List<TutorialText>();
    public VictoryChecker distance;
    void Start()
    {
        clickSound = GameObject.Find("LevelEssentials/SoundBank/UI/Special3").GetComponent<AudioSource>();
        for (int i = 0; i < tutorialCanvas.transform.childCount; i++)
        {
            popupTexts.Add(tutorialCanvas.transform.GetChild(i).Find("Text").GetComponent<TutorialText>());
        }
    }

    // Update is called once per frame
    override protected void Update()
    {
        if (popupIndex == 0)
        {
            // condition when the user does what the tutorial explains
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                popups[popupIndex].SetActive(false);
            }
            if (distance.distance >= distance.endline * 0.995f)
            {
                MoveOn();
            }
            
        }
        else if (popupIndex == 1)
        {
            base.Update();
            Time.timeScale = 0f;
            // condition when the user does what the tutorial explains
            if (popupTexts[popupIndex].finished && (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) // text is displayed and user clicked/touched
            {
                MoveOn();
                Time.timeScale = 1f;
            }
        }
        else
        {
            base.Update();
        }
    }
}
