using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// attach to UI Text component (with the full text already there)

public class TutorialText : MonoBehaviour
{
	public Text text;
	public string originalContent;

	public bool finished = false;

	void Start()
	{
		text = GetComponent<Text>();
		originalContent = text.text;
		text.text = "";

		StartCoroutine("PlayText");
	}

    private void Update()
    {
        if (!finished && originalContent.Length == text.text.Length)
        {
			finished = true;
        }
    }

    IEnumerator PlayText()
	{
		foreach (char c in originalContent)
		{
			text.text += c;
			yield return new WaitForSecondsRealtime(0.04f);
		}
	}

}
