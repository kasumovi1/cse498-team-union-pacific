using System;
using System.Collections.Generic;

public partial class Consist : MasterIndexEntry
{
	public int Place(SplineLocation startLoc, bool faceEndOfTrack, bool startAtConsistFront)
	{
		TrackLocation currentLoc = new TrackLocation(startLoc, faceEndOfTrack, true);
		TrackLocation leadingLoc = null;
		TrackLocation trailingLoc = null;
		double trailingCouplerLength = 0.0;

		Car car;
		Car prevCar = null;

		// Check track for enough room
		double trainLength = 0.0;
		for (car = firstCar; car != null; car = car.next)
		{
			trainLength += car.carPhysics.p.frontTruck - car.carPhysics.p.rearTruck;
			trainLength += car.CalculateCouplerLength(true, false);
			trainLength += car.CalculateCouplerLength(false, false);
		}
		trainLength *= (startAtConsistFront) ? -1.0 : 1.0;

		TrackLocation testSpace = TrackLocation.New(currentLoc);
		double remainingDistance = testSpace.MoveDistance(trainLength);
		if (Math.Abs(remainingDistance) > 1e-3)
		{
			//Log.WriteAndFlush("TrackFollowing: Not enough space to place consist[{0}] here, adjusting starting location by {1} meters to fit.", this.consistFileName, Math.Abs(remainingDistance));
			currentLoc.MoveDistance(-remainingDistance);
		}
		TrackLocation.Reuse(testSpace);

		car = (startAtConsistFront ? firstCar : lastCar);

		//Loop through all cars in consist
		while (car != null)
		{
			bool startWithFrontTruck = (startAtConsistFront == (car.carPhysics.WagSet.direction == 1));

			// in case the consist was already placed once...
			if (null != car.truckLocationPair)
			{
				car.truckLocationPair = null;
			}
			if (null != car.frontTruckLocation)
			{
				car.frontTruckLocation = null;
			}
			if (null != car.rearTruckLocation)
			{
				car.rearTruckLocation = null;
			}

			TrackLocation frontTruckLocation = new TrackLocation(currentLoc);
			car.frontTruckLocation = frontTruckLocation;
			car.frontTruckLocation.GetSpeedLimit(); //avoid frame rate spike later on

			TrackLocation rearTruckLocation = new TrackLocation(frontTruckLocation);
			car.rearTruckLocation = rearTruckLocation;
			car.rearTruckLocation.GetSpeedLimit();  //avoid frame rate spike later on

			// The pair is always frontTruck, rearTruck so that adjustment works properly from both directions
			// Invert the direction during updates if the direction is -1, this way the pair direction is always
			// towards the front of the car (while the truck locations are normally toward the front of the consist).
			JoinedTrackLocationPair trucks = new JoinedTrackLocationPair(
				frontTruckLocation, rearTruckLocation,
				car.carPhysics.p.frontTruck - car.carPhysics.p.rearTruck,
				car.carPhysics.WagSet.direction != 1,
				car.carPhysics.WagSet.direction != 1);

			//trucks.SetRecordLastAdjustmentPath((TmtsParameters.MarkerMessageSetting & 1) == 1
			//	|| TmtsParameters.MarkerMessageSetting == 4);

			car.truckLocationPair = trucks;

			if (prevCar != null)
			{
				// Make couplers meet up				
				leadingLoc = startWithFrontTruck ? frontTruckLocation : rearTruckLocation;

				// In interaction, the pair is always front=[the trailing truck of the car closest to 0 in the car list]
				// No direction inversion needed in the pair b/c truck locations are towards the consist front already
				JoinedTrackLocationPair couplerJoint = new JoinedTrackLocationPair(
					(startAtConsistFront ? trailingLoc : leadingLoc),
					(startAtConsistFront ? leadingLoc : trailingLoc),
					trailingCouplerLength + car.CalculateCouplerLength(startWithFrontTruck, false));

				//couplerJoint.SetRecordLastAdjustmentPath((TmtsParameters.MarkerMessageSetting & 1) == 1
				//|| TmtsParameters.MarkerMessageSetting == 4);

				if (startAtConsistFront)
				{
					prevCar.joinedTruckLocations = couplerJoint;
				}
				else
				{
					car.joinedTruckLocations = couplerJoint;
				}

				couplerJoint.ResetLocation(startAtConsistFront);
			}

			trucks.ResetLocation(startWithFrontTruck);

			if (true)//!Customer.ignoreGradeDuringInitialQuasiPause)
			{
				// Set initial grade so that quasi-pause will calculate consist stretch correctly:
				CarPhysics.TrkParms tempTrkData = car.carPhysics.trkData;

				SplineLocation frontSplineLocation = frontTruckLocation.GetSplineLocation();
				SplineLocation rearSplineLocation = rearTruckLocation.GetSplineLocation();

				tempTrkData.avgMercsToMeters = (frontSplineLocation.CalculateMercsToMeters()
					+ rearSplineLocation.CalculateMercsToMeters()) / 2.0;

				SplineLocation.Reuse(frontSplineLocation);
				SplineLocation.Reuse(rearSplineLocation);

				PointXYZ carFrontCoordinates = frontTruckLocation.GetCoordinates();
				PointXYZ carRearCoordinates = rearTruckLocation.GetCoordinates();

				tempTrkData.grade = (float)((carFrontCoordinates.y - carRearCoordinates.y)
					* tempTrkData.avgMercsToMeters / (car.rearTruckOffset - car.frontTruckOffset));

				PointXYZ.Reuse(carFrontCoordinates);
				PointXYZ.Reuse(carRearCoordinates);

				car.carPhysics.trkData = tempTrkData;
			}

			currentLoc = startWithFrontTruck ? rearTruckLocation : frontTruckLocation;

			trailingCouplerLength = car.CalculateCouplerLength(!startWithFrontTruck, false);
			trailingLoc = startWithFrontTruck ? rearTruckLocation : frontTruckLocation;

			prevCar = car;
			car = startAtConsistFront ? car.next : car.previous;
		}

		PointXYZ uv;
		couplerCoords[0] = firstCar.CalculateCouplerCoords(
			startAtConsistFront == (firstCar.carPhysics.WagSet.direction == 1), out uv);
		couplerCoords[1] = lastCar.CalculateCouplerCoords(
			startAtConsistFront == (lastCar.carPhysics.WagSet.direction != 1), out uv);

		return 0;
	}

	bool MoveIndependently(TrackLocation loc, double distance, Car car, bool carFront)
	{
		for (double remainingD = loc.MoveDistance(distance);
			Math.Abs(remainingD) > MOVE_TOL;
			remainingD = loc.MoveDistance(remainingD))
		{
			SplineEnd splineEnd;
			SplineLocation splineLocation;

			switch (loc.GetLastTraversalType())
			{
				case NODE_TRAVERSAL_TYPE_STOP:
					car.carPhysics.derailed = 1;
					return false;
				case NODE_TRAVERSAL_TYPE_GAP:
					loc.MoveToSpline(loc.IsForwardOnTrack(), false);
					break;
				case NODE_TRAVERSAL_TYPE_NOTIFY:
					splineLocation = loc.GetSplineLocation();
					splineEnd = splineLocation.GetSpline().GetEnd(loc.IsForwardOnTrack() != (distance >= 0.0));

					SplineLocation.Reuse(splineLocation);
					SplineEnd.Reuse(splineEnd);

					{
						loc.MoveToSpline(loc.IsForwardOnTrack() == (distance >= 0.0), false);
						loc.ClearLastTraversalType();
						// Moving to spline can cause a "bounce" effect
						if (loc.IsForwardOnTrack() == (distance >= 0.0))
						{
							loc.SetForwardOnTrack(!loc.IsForwardOnTrack());
						}
					}
					break;
			}
		}
		return true;
	}

	public int Move(double distance)
	{
		// MoveConsistEx in original TMTS code.

		if (firstCar.frontTruckLocation == null || firstCar.rearTruckLocation == null
			|| lastCar.frontTruckLocation == null || lastCar.rearTruckLocation == null)
		{
			// Consist not place yet!	
			return 0;
		}

		TrackLocation origFrontLoc = TrackLocation.New(firstCar.GetTruckLocation(true));
		TrackLocation origRearLoc = TrackLocation.New(lastCar.GetTruckLocation(false));

		// Check for derailed cars (unhandled)
		Car prevCar = null;
		for (Car car = firstCar; prevCar != lastCar; prevCar = car, car = car.next)
		{
			if (1 == car.carPhysics.derailed)
			{
				return -1;
			}
		}

		TrackLocation trailingLoc = null;
		double trailingCouplerLength = 0.0;
		TrackLocation currentLoc = TrackLocation.New(origFrontLoc);
		currentLoc.SetRecordMoveDistancePath(true);


		SplineOrdering frontTruckPath = null;
		SplineOrdering rearTruckPath = null;

		// DO MoveIndependently code
		if (false == MoveIndependently(currentLoc, distance, firstCar, firstCar.carPhysics.WagSet.direction == 1))
		{
			// ran off tracks return -1 will cause TMTS.c to notify
			// TODO move that out of TMTS.c
			return -1;
		}

		origFrontLoc.SetForwardOnTrack(origFrontLoc.IsForwardOnTrack() == (distance >= 0));
		origRearLoc.SetForwardOnTrack(origRearLoc.IsForwardOnTrack() == (distance >= 0));

		// Loop through each car in consist
		prevCar = null;
		for (Car car = firstCar; prevCar != lastCar; prevCar = car, car = car.next)
		{
			car.carPointsIsValidFlag = 0;
			int direction = car.carPhysics.WagSet.direction;
			bool startWithFrontTruck = direction == 1;

			TrackLocation towardsConsistFront = car.GetTruckLocation(true, startWithFrontTruck);
			TrackLocation towardsConsistRear = car.GetTruckLocation(false, startWithFrontTruck);
			TrackLocation beforeCarFront;// = new TrackLocation();// = startWithFrontTruck ? towardsConsistFront : towardsConsistRear;
			TrackLocation beforeCarRear;// = new TrackLocation();// = !startWithFrontTruck ? towardsConsistFront : towardsConsistRear;
			SplineLocation towardsConsistFrontSplineLocation;
			SplineLocation towardsConsistRearSplineLocation;
			SplineLocation beforeCarFrontSplineLocation;
			SplineLocation beforeCarRearSplineLocation;

			towardsConsistFrontSplineLocation = towardsConsistFront.GetSplineLocation();
			towardsConsistRearSplineLocation = towardsConsistRear.GetSplineLocation();

			if (startWithFrontTruck)
			{
				beforeCarFront = TrackLocation.New(towardsConsistFront);
				beforeCarRear = TrackLocation.New(towardsConsistRear);
				beforeCarFrontSplineLocation = beforeCarFront.GetSplineLocation(); // towardsConsistFrontSplineLocation; // Breaks PSP Zones
				beforeCarRearSplineLocation = beforeCarRear.GetSplineLocation(); // towardsConsistRearSplineLocation;
			}
			else
			{
				beforeCarFront = TrackLocation.New(towardsConsistRear);
				beforeCarRear = TrackLocation.New(towardsConsistFront);
				beforeCarFrontSplineLocation = beforeCarRear.GetSplineLocation(); // towardsConsistRearSplineLocation;
				beforeCarRearSplineLocation = beforeCarFront.GetSplineLocation(); // towardsConsistFrontSplineLocation;
			}

			if (prevCar == null)
			{
				// First car: set the front location from movement
				towardsConsistFront = currentLoc;
				car.SetTruckLocation(currentLoc, true);

				if (startWithFrontTruck)
					frontTruckPath = currentLoc.GetLastMoveDistancePath();
				else
					rearTruckPath = currentLoc.GetLastMoveDistancePath();

			}
			else if (trailingLoc.IsFake() && trailingLoc.GetLastTraversalType() == NODE_TRAVERSAL_TYPE_GAP)
			{
				// Move forward independently of coupler
				currentLoc = car.GetTruckLocation(true, startWithFrontTruck);

				if (false == MoveIndependently(currentLoc, distance, car, startWithFrontTruck))
				{
					// Derail message will post in TMTS.c
					return -1;
				}
				towardsConsistFront = currentLoc;
				car.SetTruckLocation(currentLoc, true);

				if (startWithFrontTruck)
					frontTruckPath = currentLoc.GetLastMoveDistancePath();
				else
					rearTruckPath = currentLoc.GetLastMoveDistancePath();
			}
			else
			{
				// Set front location from couplers
				JoinedTrackLocationPair joinedCoupler = car.previous.joinedTruckLocations;
				double couplerLength = car.CalculateCouplerLength(startWithFrontTruck, true, startWithFrontTruck);
				joinedCoupler.SetDistance(trailingCouplerLength + couplerLength);

				// StartTiming
				if (false == joinedCoupler.UpdateLocation(true, distance))
				{
					// Move Independently
					currentLoc = car.GetTruckLocation(true, startWithFrontTruck);

					if (false == MoveIndependently(currentLoc, distance, car, startWithFrontTruck))
					{
						// Derail message will post in TMTS.c
						return -1;
					}
					towardsConsistFront = currentLoc;
					car.SetTruckLocation(currentLoc, true);

					if (startWithFrontTruck)
						frontTruckPath = currentLoc.GetLastMoveDistancePath();
					else
						rearTruckPath = currentLoc.GetLastMoveDistancePath();
				}
				else
				{
					if (startWithFrontTruck)
						frontTruckPath = joinedCoupler.GetLastAdjustmentPath();
					else
						rearTruckPath = joinedCoupler.GetLastAdjustmentPath();
				}
				//			StartTiming(&tTrackFollowing);
				// TODO could do better (multiple nodes per movement) using the front/rearTruckPath

				if (NODE_TRAVERSAL_TYPE_NOTIFY == towardsConsistFront.GetLastTraversalType())
				{
					SplineEnd splineEnd = towardsConsistFrontSplineLocation.GetSpline().GetEnd(
						towardsConsistFront.IsForwardOnTrack() == (distance >= 0.0));

					SplineEnd.Reuse(splineEnd);
					car.GetTruckLocation(true, startWithFrontTruck).ClearLastTraversalType();
				}
			}

			// Set rear location from truck pair
			// Start Timing
			JoinedTrackLocationPair joinedTrucks = car.truckLocationPair;
			if (false == joinedTrucks.UpdateLocation(startWithFrontTruck, startWithFrontTruck ? distance : -distance))
			{
				// Derail message will post in TMTS.c	
				car.carPhysics.derailed = 1;
				return -1;
			}
			else
			{
				if (startWithFrontTruck)
					rearTruckPath = joinedTrucks.GetLastAdjustmentPath();
				else
					frontTruckPath = joinedTrucks.GetLastAdjustmentPath();
			}

			// StartTiming
			if (NODE_TRAVERSAL_TYPE_NOTIFY == towardsConsistRear.GetLastTraversalType())
			{
				SplineEnd splineEnd = towardsConsistRearSplineLocation.GetSpline().GetEnd(
					towardsConsistRear.IsForwardOnTrack() == (distance >= 0.0));

				SplineEnd.Reuse(splineEnd);
				car.GetTruckLocation(false, startWithFrontTruck).ClearLastTraversalType();
			}

			// Set track parameters	
			CarPhysics.TrkParms trackParameters = car.carPhysics.trkData;
			trackParameters.avgMercsToMeters = (towardsConsistFrontSplineLocation.CalculateMercsToMeters()
				+ towardsConsistRearSplineLocation.CalculateMercsToMeters()) / 2.0;

			TrackLocation carFrontLoc = startWithFrontTruck ? towardsConsistFront : towardsConsistRear;
			TrackLocation carRearLoc = startWithFrontTruck ? towardsConsistRear : towardsConsistFront;
			SplineLocation carFrontLocSplineLocation = carFrontLoc.GetSplineLocation();
			SplineLocation carRearLocSplineLocation = carRearLoc.GetSplineLocation();

			// Get sourcePosition of Front truck
			//			tempTrkData.sourcePosition = (float)carFrontLocSplineLocation.GetSourcePosition();

			// Looks like Front and Rear here are in terms of near or far from front of consist
			PointXYZ FrontTruckDirectionVector = carFrontLoc.GetDirection();
			PointXYZ RearTruckDirectionVector = carRearLoc.GetDirection();

			// negative because we are going backwards in relation to the overall train direction
			if (FrontTruckDirectionVector.Normalize() && RearTruckDirectionVector.Normalize())
			{
				FrontTruckDirectionVector.Scale(-1.0);
				FrontTruckDirectionVector.y = 0.0;

				RearTruckDirectionVector.Scale(-1.0);
				RearTruckDirectionVector.y = 0.0;

				double TruckAngle1 = Math.Atan2(FrontTruckDirectionVector.z, FrontTruckDirectionVector.x);
				double TruckAngle2 = Math.Atan2(RearTruckDirectionVector.z, RearTruckDirectionVector.x);

				TruckAngle1 -= TruckAngle2;

				if (TruckAngle1 > Math.PI)
					TruckAngle1 -= (Math.PI * 2.0);
				else if (TruckAngle1 < -Math.PI)
					TruckAngle1 += (Math.PI * 2.0);

				trackParameters.curvature = (float)(direction * TruckAngle1
					/ (car.rearTruckOffset - car.frontTruckOffset));
			}

			PointXYZ.Reuse(FrontTruckDirectionVector);  // Minimize garbage collection
			PointXYZ.Reuse(RearTruckDirectionVector);

			PointXYZ carFrontCoordinates = carFrontLoc.GetCoordinates();
			PointXYZ carRearCoordinates = carRearLoc.GetCoordinates();

			trackParameters.grade = (float)((carFrontCoordinates.y - carRearCoordinates.y)
				* trackParameters.avgMercsToMeters / (car.rearTruckOffset - car.frontTruckOffset));

			trackParameters.superElevation = (float)((carFrontLoc.GetSe() + carRearLoc.GetSe()) / 2.0);

			if (direction > 0)
			{
				trackParameters.FrontTruckXYZMercs.x = carFrontCoordinates.x;
				trackParameters.FrontTruckXYZMercs.y = carFrontCoordinates.y;
				trackParameters.FrontTruckXYZMercs.z = carFrontCoordinates.z;
				trackParameters.RearTruckXYZMercs.x = carRearCoordinates.x;
				trackParameters.RearTruckXYZMercs.y = carRearCoordinates.y;
				trackParameters.RearTruckXYZMercs.z = carRearCoordinates.z;
			}
			else
			{
				trackParameters.RearTruckXYZMercs.x = carFrontCoordinates.x;
				trackParameters.RearTruckXYZMercs.y = carFrontCoordinates.y;
				trackParameters.RearTruckXYZMercs.z = carFrontCoordinates.z;
				trackParameters.FrontTruckXYZMercs.x = carRearCoordinates.x;
				trackParameters.FrontTruckXYZMercs.y = carRearCoordinates.y;
				trackParameters.FrontTruckXYZMercs.z = carRearCoordinates.z;
			}
			car.carPhysics.trkData = trackParameters;

			PointXYZ.Reuse(carFrontCoordinates);        // Minimize garbage collection
			PointXYZ.Reuse(carRearCoordinates);


			// Get ready for next iteration
			trailingLoc = towardsConsistRear;
			trailingCouplerLength = car.CalculateCouplerLength(!startWithFrontTruck, true, startWithFrontTruck);

			TrackLocation.Reuse(beforeCarFront);            // Minimize garbage collection		
			TrackLocation.Reuse(beforeCarRear);
			// Do NOT reuse towardsConsistFront and towardsConsistRear, or carFrontLoc and carRearLoc.

			SplineLocation.Reuse(towardsConsistFrontSplineLocation);
			SplineLocation.Reuse(towardsConsistRearSplineLocation);

			SplineLocation.Reuse(beforeCarFrontSplineLocation);
			SplineLocation.Reuse(beforeCarRearSplineLocation);

			SplineLocation.Reuse(carFrontLocSplineLocation);
			SplineLocation.Reuse(carRearLocSplineLocation);

			if (frontTruckPath != null)
			{
				SplineOrdering.Reuse(frontTruckPath);       // Minimize garbage collection (these get collected when they are replaced)
				frontTruckPath = null;
			}
			if (rearTruckPath != null)
			{
				SplineOrdering.Reuse(rearTruckPath);
				rearTruckPath = null;
			}
		}

		TrackLocation.Reuse(origFrontLoc);
		TrackLocation.Reuse(origRearLoc);

		return 0;
	}

	public static int GetEngineHandleFromConsist(int consistHandle)
	{
		Consist consist = TmtsMasterIndex.GetEntry(consistHandle) as Consist;
		if (consist == null)
		{
			return -1;
		}

		return consist.GetEngineHandle();
	}


	public const double MOVE_TOL = 0.00001;


	// This traversal type causes the traversal to stop and notifies anyone who cares.
	public const int NODE_TRAVERSAL_TYPE_STOP = 0;

	// Normal traversal type, nothing special happens.
	public const int NODE_TRAVERSAL_TYPE_ALLOWED = 1;

	// This traversal type allows the traversal to continue, but notifies anyone who cares.
	public const int NODE_TRAVERSAL_TYPE_NOTIFY = 2;

	// There was a large gap or overlap between the two endpoints in the traversal.
	public const int NODE_TRAVERSAL_TYPE_GAP = -1;

	public const double WALKING_COLLISION_RADIUS = 4.0;

	public Car RecoverFromAccident()
	{
		double consistVelocityDirection = (0.0 == firstCar.carPhysics.velocity) ? 0.0 : ((0.0 < firstCar.carPhysics.velocity) ? 1.0 : -1.0);
		//TrackLocation pLastGoodLoc = null;
		TrackLocation pFirstGoodLoc = null;
		//double firstGoodOrientation = 0.0;
		//double lastGoodOrientation = 0.0;
		Car pLastDerailedCar = null;
		Car pFirstDerailedCar = null;
		double consistLength = 0.0;
		double lengthOfDerailedCars = 0.0;
		Car pLoco = null;
		for (Car pCar = firstCar;
			pCar != null;
			pCar = pCar.next)
		{
			if (0 != pCar.carPhysics.derailed)
			{
				if (pFirstDerailedCar == null)
				{
					pFirstDerailedCar = pCar;
				}
				pLastDerailedCar = pCar;
				lengthOfDerailedCars += pCar.carPhysics.p.length;
			}
			else
			{
				if (pFirstGoodLoc == null)
				{
					pFirstGoodLoc = pCar.carPhysics.WagSet.direction == 1 ? pCar.frontTruckLocation : pCar.rearTruckLocation;
					//firstGoodOrientation = pCar.carPhysics.WagSet.direction;
				}
				//pLastGoodLoc = pCar.carPhysics.WagSet.direction == 1 ? pCar.rearTruckLocation : pCar.frontTruckLocation;
				//lastGoodOrientation = pCar.carPhysics.WagSet.direction;
			}
			consistLength += pCar.carPhysics.p.length;
			if (pCar.carPhysics.p.iType != 0 && pCar.scriptID > 0)
			{
				pLoco = pCar;
			}
		}

		for (Car pCar = firstCar;
			pCar != null;
			pCar = pCar.next)
		{
			pCar.carPhysics.derailed = 0;
			pCar.carPhysics.railRoll = 0;
			pCar.carPhysics.tipping = 0;
			pCar.carPhysics.velocity = 0.0f;
			// TODO find replacement? pCar.carPhysics.ed.TractionForce = 0.0f;
			if (pLoco == null)
			{
				CarPhysics.WagonSetup wagSet = pCar.carPhysics.WagSet;
				wagSet.handbrake = 100;
				pCar.carPhysics.WagSet = wagSet;
			}
		}

		if (pFirstDerailedCar != null)
		{
			double directionToMovePlacement = 0.0;
			if (pFirstDerailedCar == firstCar && pLastDerailedCar == lastCar)
			{
				directionToMovePlacement = -1.0 * consistVelocityDirection;
			}
			else if (pFirstDerailedCar == firstCar)
			{
				directionToMovePlacement = -1.0;
			}
			else if (pLastDerailedCar == lastCar)
			{
				directionToMovePlacement = 1.0;
			}
			double distanceToMovePlacement = directionToMovePlacement * lengthOfDerailedCars;
			TrackLocation newStartingLoc = new TrackLocation(firstCar.carPhysics.WagSet.direction == 1 ? firstCar.frontTruckLocation : firstCar.rearTruckLocation);

			if (distanceToMovePlacement != 0.0)
			{
				for (Car pCar = firstCar;
					pCar != null;
					pCar = pCar.next)
				{
					// Front truck in consist frame
					newStartingLoc = new TrackLocation(pCar.carPhysics.WagSet.direction == 1 ? pCar.frontTruckLocation : pCar.rearTruckLocation);
					if (Math.Abs(newStartingLoc.MoveDistance(distanceToMovePlacement)) < 1e-3)
					{
						if (newStartingLoc.GetLastTraversalType() != NODE_TRAVERSAL_TYPE_STOP)
						{
							if (newStartingLoc.GetLastTraversalType() == NODE_TRAVERSAL_TYPE_GAP)
							{
								newStartingLoc.MoveToSpline(newStartingLoc.IsForwardOnTrack(), false);
							}
							// found a good place to move from
							break;
						}
					}
					// Rear truck in consist frame
					newStartingLoc = new TrackLocation(pCar.carPhysics.WagSet.direction != 1 ? pCar.frontTruckLocation : pCar.rearTruckLocation);
					if (Math.Abs(newStartingLoc.MoveDistance(distanceToMovePlacement)) < 1e-3)
					{
						if (newStartingLoc.GetLastTraversalType() != NODE_TRAVERSAL_TYPE_STOP)
						{
							if (newStartingLoc.GetLastTraversalType() == NODE_TRAVERSAL_TYPE_GAP)
							{
								newStartingLoc.MoveToSpline(newStartingLoc.IsForwardOnTrack(), false);
							}
							// found a good place to move from
							break;
						}
					}
				}
			}

			Place(newStartingLoc.GetSplineLocation(), newStartingLoc.IsForwardOnTrack(), true);
		}
		return pLoco;
	}
}

public struct Pair<T>
{
	public T first;
	public T second;

	public Pair(T _first, T _second)
	{
		first = _first;
		second = _second;
	}
}

public struct Pair<T, U>
{
	public T first;
	public U second;

	public Pair(T _first, U _second)
	{
		first = _first;
		second = _second;
	}
}

public class CouplerRay
{
	public const double MAX_COUPLING_DIST_SQ = 1.0;
	public const double MAX_COUPLING_PLANE_DIST = 0.05;
	public const double MIN_COUPLING_PLANE_DIST = -1.0;

	public CouplerRay(PointXYZ _pointBefore, PointXYZ _pointAfter, PointXYZ _couplingDirection, double _mercsToMeters)
	{
		pointBefore = PointXYZ.New(_pointBefore);
		pointAfter = PointXYZ.New(_pointAfter);

		mercsToMeters = _mercsToMeters;
		couplingPlane = new GeometryPlane(_couplingDirection, _pointAfter);

		movementVector = pointAfter.SubtractD(pointBefore);
		mercsMoved = Math.Sqrt(movementVector.MagnitudeSquared());
		if (mercsMoved > 0.0)
		{
			movementVector.Scale(1.0 / mercsMoved);
		}
	}

	public static CouplerRay New(PointXYZ _pointBefore, PointXYZ _pointAfter, PointXYZ _couplingDirection, double _mercsToMeters)
	{
		CouplerRay ray;

		if (unusedIndex == 0)
		{
			return new CouplerRay(_pointBefore, _pointAfter, _couplingDirection, _mercsToMeters);
		}

		ray = unused[--unusedIndex];

		ray.pointBefore = PointXYZ.New(_pointBefore);
		ray.pointAfter = PointXYZ.New(_pointAfter);

		ray.mercsToMeters = _mercsToMeters;
		ray.couplingPlane = new GeometryPlane(_couplingDirection, _pointAfter);

		ray.movementVector = _pointAfter.SubtractD(_pointBefore);
		ray.mercsMoved = Math.Sqrt(ray.movementVector.MagnitudeSquared());
		if (ray.mercsMoved > 0.0)
		{
			ray.movementVector.Scale(1.0 / ray.mercsMoved);
		}

		return ray;
	}

	static CouplerRay()
	{
		unused = new CouplerRay[2000];
		unusedIndex = 0;
	}

	public bool Coupled(CouplerRay target)
	{
		// Couplers close enough to test
		double distSq = mercsToMeters * pointAfter.DistanceSquared(target.pointAfter);
		if (distSq > MAX_COUPLING_DIST_SQ)
		{
			return false;
		}

		// This coupler is beyond the other's plane defined by trucks to coupler (with coupler as base point)
		double planeDist = mercsToMeters * couplingPlane.Distance(target.pointAfter);

		PointXYZ couplingPlaneNormal = PointXYZ.New(couplingPlane.normal);
		if (planeDist > MAX_COUPLING_PLANE_DIST || planeDist < MIN_COUPLING_PLANE_DIST)
		{
			if (planeDist < MIN_COUPLING_PLANE_DIST
				&& mercsToMeters * (mercsMoved + target.mercsMoved) >= MAX_COUPLING_PLANE_DIST)
			{
				// Just for fun, check that a really fast train didn't jump past coupling range in one step

				// Simplification: the plane for the coupler's pos before mvmt is parallel and just a distance offset
				// from the current plane

				double oldPlaneDistOffset = (mercsMoved == 0.0) ? 0.0 : (mercsMoved * mercsToMeters
					* (movementVector.DotProduct(couplingPlaneNormal) >= 0.0 ? 1.0 : -1.0));
				double oldPlaneDist = mercsToMeters
					* couplingPlane.Distance(target.pointBefore) + oldPlaneDistOffset;
				if (oldPlaneDist < MAX_COUPLING_PLANE_DIST)
				{
					// Nope: should've had a fair shot at coupling last round and didn't make it
					return false;
				}
			}
			else
			{
				// ruled out any chance of jumping past range in one shot
				return false;
			}
		}

		if (couplingPlaneNormal.XZDotProduct(target.couplingPlane.normal) < -0.7) //couplers facing each other
		{
			if (mercsMoved > target.mercsMoved)
			{
				//movementVector is faster than target.movementVector
				if (movementVector.XZDotProduct(target.couplingPlane.normal) < -0.7)
				{
					return true;
				}
			}
			else
			{
				//target.movementVector is faster than movementVector
				if (target.movementVector.XZDotProduct(couplingPlane.normal) < -0.7)
				{
					return true;
				}
			}
		}

		PointXYZ.Reuse(couplingPlaneNormal);

		return false;
	}

	public static void Reuse(CouplerRay couplerRay)
	{
		PointXYZ.Reuse(couplerRay.pointBefore);
		PointXYZ.Reuse(couplerRay.pointAfter);
		PointXYZ.Reuse(couplerRay.movementVector);

		if (unusedIndex < 2000)
		{
			couplerRay.pointBefore = null;
			couplerRay.pointAfter = null;
			couplerRay.movementVector = null;

			unused[unusedIndex++] = couplerRay;
		}
	}

	public PointXYZ pointBefore, pointAfter, movementVector;
	public GeometryPlane couplingPlane;
	public double mercsToMeters, mercsMoved;

	private static CouplerRay[] unused;
	private static int unusedIndex;
}


class CarsOnSplinesWeakOrdering : IComparer<Car>
{
	public void SetSplineOrdering(SplineOrdering _so)
	{
		so = _so;
	}

	public int Compare(Car c1, Car c2)
	{
		if (null == so)
		{
			int u1 = c1.uniqueId;
			int u2 = c2.uniqueId;
			return (u1 == u2) ? 0 : ((u1 < u2) ? -1 : 1);
		}
		return so.Compare(c1.frontTruckLocation.GetSplineLocation(), c2.frontTruckLocation.GetSplineLocation());
	}

	public bool Equals(Car c1, Car c2)
	{
		return 0 == Compare(c1, c2);
	}

	public int GetHashCode(Car c1)
	{
		return c1.GetHashCode();
	}


	private SplineOrdering so;
}