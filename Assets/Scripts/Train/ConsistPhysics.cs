using System;
using System.Runtime.InteropServices;

public class ConsistPhysics
{
	// Wrapper for class Consist in Physics DLL

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr CreateConsist();

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern double DoPhysics(IntPtr consist, float time);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr BreakConsist(IntPtr consist, int breakPoint);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern void JoinConsist(IntPtr consist1, IntPtr consist2, float x, int facing);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern void InitializeConsist(IntPtr consist);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern void DestroyConsist(IntPtr consist);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern void SetLOD(IntPtr consist, int lod);

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr joinTailToHead(IntPtr c1, IntPtr c2, double x);

	[DllImport("Physics")]
	private static extern void ConsistSetNeutralPositions(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetRelPositions(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistDoAssociations(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistZeroPosition(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistReverse(IntPtr consist, out float dx, out int dmx);

	[DllImport("Physics")]
	private static extern IntPtr ConsistGetVehicle(IntPtr consist, int index);

	[DllImport("Physics")]
	private static extern void ConsistSetVehicle(IntPtr consist, IntPtr vehicle, int index);

	[DllImport("Physics")]
	private static extern IntPtr ConsistGetFirstVehicle(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetFirstVehicle(IntPtr consist, IntPtr vehicle);

	[DllImport("Physics")]
	private static extern IntPtr ConsistGetInteraction(IntPtr consist, int index);

	[DllImport("Physics")]
	private static extern void ConsistSetInteraction(IntPtr consist, IntPtr Interaction, int index);

	// Imports for get/set functions for public variables.

	[DllImport("Physics")]
	private static extern int ConsistGetConsistID(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetConsistID(IntPtr consist, int _consistID);

	[DllImport("Physics")]
	private static extern int ConsistGetTippingCarOrRolledRail(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetTippingCarOrRolledRail(IntPtr consist, int _tippingCarOrRolledRail);

	[DllImport("Physics")]
	private static extern int ConsistGetNWag(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetNWag(IntPtr consist, int _nWag);

	[DllImport("Physics")]
	private static extern int ConsistGetLoverVStepCnt(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetLoverVStepCnt(IntPtr consist, int _LoverVStepCnt);

	[DllImport("Physics")]
	private static extern int ConsistGetConsist(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetConsist(IntPtr consist, int _consist);

	[DllImport("Physics")]
	private static extern int ConsistGetLOD(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetLOD(IntPtr consist, int _LOD);

	[DllImport("Physics")]
	private static extern double ConsistGetDisplacement(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetDisplacement(IntPtr consist, double _displacement);

	[DllImport("Physics")]
	private static extern float ConsistGetPosition(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetPosition(IntPtr consist, float _position);

	[DllImport("Physics")]
	private static extern float ConsistGetPositionDot(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetPositionDot(IntPtr consist, float _positionDot);

	[DllImport("Physics")]
	private static extern float ConsistGetVelocity(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetVelocity(IntPtr consist, float _velocity);

	[DllImport("Physics")]
	private static extern float ConsistGetVelocityDot(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetVelocityDot(IntPtr consist, float _velocityDot);

	[DllImport("Physics")]
	private static extern float ConsistGetTractionForce(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetTractionForce(IntPtr consist, float _TractionForce);

	[DllImport("Physics")]
	private static extern float ConsistGetBrakeForce(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetBrakeForce(IntPtr consist, float _BrakeForce);

	[DllImport("Physics")]
	private static extern float ConsistGetVelocityDesired(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetVelocityDesired(IntPtr consist, float _velocityDesired);

	[DllImport("Physics")]
	private static extern float ConsistGetTotalEmptyMass(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetTotalEmptyMass(IntPtr consist, float _totalEmptyMass);

	[DllImport("Physics")]
	private static extern float ConsistGetTotalLoadedMass(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetTotalLoadedMass(IntPtr consist, float _totalLoadedMass);

	[DllImport("Physics")]
	private static extern float ConsistGetEngineMass(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetEngineMass(IntPtr consist, float _engineMass);

	[DllImport("Physics")]
	private static extern int ConsistGetBrokenFlag(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetBrokenFlag(IntPtr consist, int _brokenFlag);

	[DllImport("Physics")]
	private static extern int ConsistGetBrokenLink(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetBrokenLink(IntPtr consist, int _brokenLink);

	[DllImport("Physics")]
	private static extern float ConsistGetWindVelocity(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetWindVelocity(IntPtr consist, float _WindVelocity);

	[DllImport("Physics")]
	private static extern float ConsistGetAmbientTemperature(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetAmbientTemperature(IntPtr consist, float _ambientTemperature);

	[DllImport("Physics")]
	private static extern int ConsistGetMF(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetMF(IntPtr consist, int _mF);

	[DllImport("Physics")]
	private static extern int ConsistGetMS(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetMS(IntPtr consist, int _mS);

	[DllImport("Physics")]
	private static extern int ConsistGetMVS(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetMVS(IntPtr consist, int _mVS);

	[DllImport("Physics")]
	private static extern float ConsistGetTimeStopped(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetTimeStopped(IntPtr consist, float _TimeStopped);

	[DllImport("Physics")]
	private static extern int ConsistGetDumping(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetDumping(IntPtr consist, int _dumping);

	[DllImport("Physics")]
	private static extern float ConsistGetHeadDistance(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetHeadDistance(IntPtr consist, float _HeadDistance);

	[DllImport("Physics")]
	private static extern float ConsistGetInitialSpeed(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetInitialSpeed(IntPtr consist, float _InitialSpeed);

	[DllImport("Physics")]
	private static extern int ConsistGetQuasiPausedState(IntPtr consist);

	[DllImport("Physics")]
	private static extern void ConsistSetQuasiPausedState(IntPtr consist, int _QuasiPausedState);

	[DllImport("Physics")]
	private static extern void ConsistSetQuasiPausedStateTime(IntPtr consist, IntPtr _QuasiPausedStateTime);

	[DllImport("Physics")]
	private static extern void ConsistSetQuasiPausedStateRealTimeUsed(IntPtr consist, IntPtr _QuasiPausedStateRealTimeUsed);


	private IntPtr innerConsist;
	private bool isNative;

	public ConsistPhysics()
	{
		innerConsist = CreateConsist();
		isNative = true;
	}

	private ConsistPhysics(IntPtr _innerConsist)
	{
		innerConsist = _innerConsist;
		isNative = false;
	}

	~ConsistPhysics()
	{
		if (isNative)
		{
			Destroy();
		}
	}

	public IntPtr GetInnerConsist()
	{
		return innerConsist;
	}

	public double DoPhysics(float time)
	{
		return DoPhysics(innerConsist, time);
	}

	public ConsistPhysics Break(int breakPoint)
	{
		IntPtr ptr = BreakConsist(innerConsist, breakPoint);
		return new ConsistPhysics(ptr);
	}

	public void Join(ConsistPhysics consist, float x, int facing)
	{
		JoinConsist(innerConsist, consist.innerConsist, x, facing);
	}

	public void Initialize()
	{
		InitializeConsist(innerConsist);
	}

	public void Destroy()
	{
		DestroyConsist(innerConsist);
		innerConsist = new IntPtr(0);
	}

	public void SetLOD(int lod)
	{
		SetLOD(innerConsist, lod);
	}

	public static ConsistPhysics JoinTailToHead(ConsistPhysics c1, ConsistPhysics c2, double x)
	{
		IntPtr consist;
		consist = joinTailToHead(c1.innerConsist, c2.innerConsist, x);
		return new ConsistPhysics(consist);
	}

	public void SetNeutralPositions()
	{
		ConsistSetNeutralPositions(innerConsist);
	}

	public void SetRelPositions()
	{
		ConsistSetRelPositions(innerConsist);
	}

	public void DoAssociations()
	{
		ConsistDoAssociations(innerConsist);
	}

	public void ZeroPosition()
	{
		ConsistZeroPosition(innerConsist);
	}

	public void Reverse(out float dx, out int dmx)
	{
		ConsistReverse(innerConsist, out dx, out dmx);
	}

	// GetCar and SetCar will only work after consist has been initialized.

	public CarPhysics GetCar(int index)
	{
		return new CarPhysics(ConsistGetVehicle(innerConsist, index));
	}

	public void SetCar(int index, CarPhysics carPhysics)
	{
		ConsistSetVehicle(innerConsist, carPhysics.GetInnerVehicle(), index);
	}

	public CarPhysics V1
	{
		set
		{
			ConsistSetFirstVehicle(innerConsist, value.GetInnerVehicle());
		}
	}

	public CarInteraction GetInteraction(int index)
	{
		return new CarInteraction(ConsistGetInteraction(innerConsist, index));
	}

	// Generated properties

	public int consistID
	{
		get
		{
			return ConsistGetConsistID(innerConsist);
		}
		set
		{
			ConsistSetConsistID(innerConsist, value);
		}
	}

	public int tippingCarOrRolledRail
	{
		get
		{
			return ConsistGetTippingCarOrRolledRail(innerConsist);
		}
		set
		{
			ConsistSetTippingCarOrRolledRail(innerConsist, value);
		}
	}

	public int nWag
	{
		get
		{
			return ConsistGetNWag(innerConsist);
		}
		set
		{
			ConsistSetNWag(innerConsist, value);
		}
	}

	public int LoverVStepCnt
	{
		get
		{
			return ConsistGetLoverVStepCnt(innerConsist);
		}
		set
		{
			ConsistSetLoverVStepCnt(innerConsist, value);
		}
	}

	public int consist
	{
		get
		{
			return ConsistGetConsist(innerConsist);
		}
		set
		{
			ConsistSetConsist(innerConsist, value);
		}
	}

	public int LOD
	{
		get
		{
			return ConsistGetLOD(innerConsist);
		}
		set
		{
			ConsistSetLOD(innerConsist, value);
		}
	}

	public double displacement
	{
		get
		{
			return ConsistGetDisplacement(innerConsist);
		}
		set
		{
			ConsistSetDisplacement(innerConsist, value);
		}
	}

	public float position
	{
		get
		{
			return ConsistGetPosition(innerConsist);
		}
		set
		{
			ConsistSetPosition(innerConsist, value);
		}
	}

	public float positionDot
	{
		get
		{
			return ConsistGetPositionDot(innerConsist);
		}
		set
		{
			ConsistSetPositionDot(innerConsist, value);
		}
	}

	public float velocity
	{
		get
		{
			return ConsistGetVelocity(innerConsist);
		}
		set
		{
			ConsistSetVelocity(innerConsist, value);
		}
	}

	public float velocityDot
	{
		get
		{
			return ConsistGetVelocityDot(innerConsist);
		}
		set
		{
			ConsistSetVelocityDot(innerConsist, value);
		}
	}

	public float TractionForce
	{
		get
		{
			return ConsistGetTractionForce(innerConsist);
		}
		set
		{
			ConsistSetTractionForce(innerConsist, value);
		}
	}

	public float BrakeForce
	{
		get
		{
			return ConsistGetBrakeForce(innerConsist);
		}
		set
		{
			ConsistSetBrakeForce(innerConsist, value);
		}
	}

	public float velocityDesired
	{
		get
		{
			return ConsistGetVelocityDesired(innerConsist);
		}
		set
		{
			ConsistSetVelocityDesired(innerConsist, value);
		}
	}

	public float totalEmptyMass
	{
		get
		{
			return ConsistGetTotalEmptyMass(innerConsist);
		}
		set
		{
			ConsistSetTotalEmptyMass(innerConsist, value);
		}
	}

	public float totalLoadedMass
	{
		get
		{
			return ConsistGetTotalLoadedMass(innerConsist);
		}
		set
		{
			ConsistSetTotalLoadedMass(innerConsist, value);
		}
	}

	public float engineMass
	{
		get
		{
			return ConsistGetEngineMass(innerConsist);
		}
		set
		{
			ConsistSetEngineMass(innerConsist, value);
		}
	}

	public int brokenFlag
	{
		get
		{
			return ConsistGetBrokenFlag(innerConsist);
		}
		set
		{
			ConsistSetBrokenFlag(innerConsist, value);
		}
	}

	public int brokenLink
	{
		get
		{
			return ConsistGetBrokenLink(innerConsist);
		}
		set
		{
			ConsistSetBrokenLink(innerConsist, value);
		}
	}

	public float WindVelocity
	{
		get
		{
			return ConsistGetWindVelocity(innerConsist);
		}
		set
		{
			ConsistSetWindVelocity(innerConsist, value);
		}
	}

	public float ambientTemperature
	{
		get
		{
			return ConsistGetAmbientTemperature(innerConsist);
		}
		set
		{
			ConsistSetAmbientTemperature(innerConsist, value);
		}
	}

	public int mF
	{
		get
		{
			return ConsistGetMF(innerConsist);
		}
		set
		{
			ConsistSetMF(innerConsist, value);
		}
	}

	public int mS
	{
		get
		{
			return ConsistGetMS(innerConsist);
		}
		set
		{
			ConsistSetMS(innerConsist, value);
		}
	}

	public int mVS
	{
		get
		{
			return ConsistGetMVS(innerConsist);
		}
		set
		{
			ConsistSetMVS(innerConsist, value);
		}
	}

	public float TimeStopped
	{
		get
		{
			return ConsistGetTimeStopped(innerConsist);
		}
		set
		{
			ConsistSetTimeStopped(innerConsist, value);
		}
	}

	public int dumping
	{
		get
		{
			return ConsistGetDumping(innerConsist);
		}
		set
		{
			ConsistSetDumping(innerConsist, value);
		}
	}

	public float HeadDistance
	{
		get
		{
			return ConsistGetHeadDistance(innerConsist);
		}
		set
		{
			ConsistSetHeadDistance(innerConsist, value);
		}
	}

	public float InitialSpeed
	{
		get
		{
			return ConsistGetInitialSpeed(innerConsist);
		}
		set
		{
			ConsistSetInitialSpeed(innerConsist, value);
		}
	}

	public int QuasiPausedState
	{
		get
		{
			return ConsistGetQuasiPausedState(innerConsist);
		}
		set
		{
			ConsistSetQuasiPausedState(innerConsist, value);
		}
	}
}