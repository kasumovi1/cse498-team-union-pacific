﻿public class ConsistSpeedPrediction
{
	public double[] distancesInMeters;
	public double[] speeds;
	public double[] distanceProfile;
	public double[] velocityProfile;
	public bool halt;
	public double updateTime;
	public double timeSliceMax { get; private set; }

	public ConsistSpeedPrediction(double[] _distancesInMeters)
	{
		halt = false;
		updateTime = -1.0;
		distancesInMeters = _distancesInMeters;
		speeds = null;
		distanceProfile = null;
		velocityProfile = null;
		timeSliceMax = 3.0;
	}
	public ConsistSpeedPrediction(double[] _distancesInMeters, double millisecondsPerTimeSlice)
	{
		halt = false;
		updateTime = -1.0;
		distancesInMeters = _distancesInMeters;
		speeds = null;
		distanceProfile = null;
		velocityProfile = null;
		timeSliceMax = millisecondsPerTimeSlice > 0.0 ? millisecondsPerTimeSlice : 3.0;
	}

	public double InterpolateVelocity(double distanceInMeters)
	{
		bool distancePositive;
		int iFirst, iMiddle, iLast;

		if (distanceProfile == null || velocityProfile == null || distanceProfile.Length == 0)
		{
			return 0.0;
		}
		else if (distanceProfile.Length == 1)
		{
			return velocityProfile[0];
		}

		distancePositive = distanceProfile[distanceProfile.Length - 1] > distanceProfile[0];

		if (distancePositive == (distanceInMeters < distanceProfile[0]))
		{
			// Don't allow negative extrapolation...
			return velocityProfile[0];
		}

		// Find distanceProfile[n] and distanceProfile[n+1] that bracket distanceInMeters.
		iFirst = 0;
		iLast = distanceProfile.Length;

		while (iLast > (iFirst + 1))
		{
			iMiddle = (iFirst + iLast) / 2;
			if ((distancePositive && (distanceInMeters >= distanceProfile[iMiddle])) ||
				(!distancePositive && (distanceInMeters <= distanceProfile[iMiddle])))
			{
				iFirst = iMiddle;
			}
			else
			{
				iLast = iMiddle;
			}
		}

		if (iFirst == distanceProfile.Length - 1)
		{
			return velocityProfile[iFirst];
		}

		return velocityProfile[iFirst] +
			((velocityProfile[iFirst + 1] - velocityProfile[iFirst]) *
				((distanceInMeters - distanceProfile[iFirst]) / (distanceProfile[iFirst + 1] - distanceProfile[iFirst])));
	}
}