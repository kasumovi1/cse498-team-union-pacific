using System;
using System.Runtime.InteropServices;

public class CarPhysics
{
	// Wrapper for Vehicle class in Physics DLL
	[StructLayout(LayoutKind.Sequential)]
	public struct EngDynamics
	{
		public float TractionForce;
		public float TractionForceddTXSpeed;
		public float TargetTractionForceXSpeed;
		public float BPPress;
		public float EBPress;
		public float velocityDesired;
		public float ElectricMotorLoad;
	}
	[StructLayout(LayoutKind.Sequential)]
	public struct CarEnd
	{
		public float cock; //0=open,100=closed
		public int gladhand;//0=open,1=connected
		public int coupler; //0=open latchable, 1=latched 2=Open-Unlatchable, 3=pulling on Pin, 4=broken
		public float k1, k2, k3, k4, k5, k6, k8, k9, f1, f4, sBuff, sDraft, slack;
		public float R1, R2, R3, R8;//same in Buff as in Draft
		public int CouplingSystemType; // 0=friction, 2= spring, 3= Hydraulic EOCC, 4= Articulated(Shared truck)5= Solid drawbar  
		public float DraftGear_ViscosityFactor;
		public float DraftGear_ViscosityMaxForce;

		public float DraftGear_BuffSlack;
		public float DraftGear_BuffX1, DraftGear_BuffY1;
		public float DraftGear_BuffX2, DraftGear_BuffY2;
		public float DraftGear_BuffX3, DraftGear_BuffY3;
		public float DraftGear_BuffX4, DraftGear_BuffY4;

		public float DraftGear_DraftSlack;
		public float DraftGear_DraftX1, DraftGear_DraftY1;
		public float DraftGear_DraftX2, DraftGear_DraftY2;
		public float DraftGear_DraftX3, DraftGear_DraftY3;
		public float DraftGear_DraftX4, DraftGear_DraftY4;

		public float couplingStrength;
		public float couplingStretch;

		// Specific Hydraulic parameters
		public float PistonDiameter;
		public float PistonRodDiameter;
		public float BuffDirPistonHeadAreaCubed, DraftDirPistonHeadAreaCubed;
		public float HydraulicForceFactor;
		public float HydraulicFluidFactor;
		public float TotalOrificeArea_Buff, TotalOrificeArea_Draft;
		public float TotalOrificeArea_ReturnBuff, TotalOrificeArea_ReturnDraft;

		public float MaxTravelBuff, MaxTravelDraft;//, Slack;

		public float PreloadMin_Buff, PreloadMax_Buff;
		public float PreloadMin_Draft, PreloadMax_Draft;
		public float kCarBodyStiffness;
		public float BuffMinSpringForce, k_BuffSpring;
		public float DraftMinSpringForce, k_DraftSpring;
		public float PreloadOrificeRatio, MinPreloadOrificeRatio;
	};

	[StructLayout(LayoutKind.Sequential)]
	public struct WagonSetup
	{
		public int direction;  //direction in consist frame  
		public float loadMass;
		public float hLoadCOM;
		public int handbrake; //0=off(default) 100= full on
		public int airTanksFull;
		public int retainer; //0=open(default) 1=HP  2=LP 3=SD
		public int releaseRod; //0==off(default) 1==release BCylinder  2==releaseTanks and BCylinder
		public int cutOutValve; //0=open (default); 1=closed
		public CarEnd front;
		public CarEnd rear;
		public float ExtraLongitudinalMass;
		public float ExtraLongitudinalGradeForce;
	};

	[StructLayout(LayoutKind.Sequential)]
	public struct WagonParameters
	{
		public int iType;   //0=freight car, 1= engine
		public int subType;
		public float mass; //empty
		public int bearingType; //0=Journal, 1=Roller
								//geometric params
		public float trackWidth;
		public float frontCouplerPosition; //position of front coupler from ref (>0)
		public float rearCouplerPosition;  //position of rear coupler from ref (<0)
		public float length; //=frontCoupler-rearCoupler
		public float frontTruck; //position relative to ref
		public float rearTruck;  //position relative to ref
		public float frontCouplerLength;
		public float rearCouplerLength;
		public int FrontCouplerAlignmentType; // 0 = none, 1 = M380/381 (loco), 2=M17A (freight car)
		public int RearCouplerAlignmentType; // 0 = none, 1 = M380/381 (loco), 2=M17A (freight car)
		public float heightCOM;  //above top of rails
		public float heightCoupling; //above top of rails

		//brake parameters
		public float BPLength;
		public float BPDiameter;//default=.033 m
		public float BPVolume;
		public int BrakeType;  //1=ABD , 2=ABDW , 3=ElectroPneu ,4=other
		public float r1, r2, r3, r4, r5, r6, r7, r8, rs;//orifice flow rates;
		public float rLeak;  //leakage coef
		public float EmerResVol;
		public float AuxResVol;
		public float BCylVolume;
		public float PrelimQuickServiceVolume;
		public float KBrake; //sticky coef for brake
		public float RBrake; //sticky dissapation for brake
		public float cBrake;  //=1 for default settings
		public float shoeType; //0=CI  1=Comp.

		public float BrakeShoeFrictionFactor_MedApp_A;
		public float BrakeShoeFrictionFactor_MedApp_B;
		public float BrakeShoeFrictionFactor_MedApp_C;

		public float BrakeShoeFrictionFactor_HighApp_A;
		public float BrakeShoeFrictionFactor_HighApp_B;
		public float BrakeShoeFrictionFactor_HighApp_C;

		public float NormalBrakeShoeForceAt50PSI; // braking force  @ 50 psi Cylinder Pr / 50 psi (all in TMTS units, of course)

		public float LoadEmptyBrakeThreshold; // braking force  @ 50 psi Cylinder Pr / 50 psi (all in TMTS units, of course)
		public float LoadEmptyProportioningFactor; // braking force  @ 50 psi Cylinder Pr / 50 psi (all in TMTS units, of course)

		//draft gear parameters now in WagSet

		// car-environment interaction parameters
		public float dragCoef;//wind drag coef
		public float RFFcoef;//rolling coef of friction
		public float curvCoef; //RRF per mass per curvature
	};

	public struct LocPointXYZ
	{
		public double x;
		public double y;
		public double z;
	};

	[StructLayout(LayoutKind.Sequential)]
	public struct TrkParms
	{
		public float grade;//grade=sin(theta)=rise/(distance traveled on sloping track))
						   //positive if rising in direction of consist
						   // 100*grade-per-cent
		public float curvature;//1/R --positive if turning left in direction of consist
							   //radians per meter=(pi/180)*(39.4/(12*100))*degrees-per-100'
		public float superElevation;//radians positive if leaning left in direction of consist
		public float adhesion;
		public double avgMercsToMeters; // for L over V use
		public LocPointXYZ FrontTruckXYZMercs;
		public LocPointXYZ RearTruckXYZMercs;
		public LocPointXYZ FrontTruckXYZms;
		public LocPointXYZ RearTruckXYZms;
		public LocPointXYZ FrontCouplerPinXYZms;
		public LocPointXYZ RearCouplerPinXYZms;
		public LocPointXYZ NudgedFrontTruckXYZms;
		public LocPointXYZ NudgedRearTruckXYZms;
		public LocPointXYZ NudgedFrontCouplerPinXYZms;
		public LocPointXYZ NudgedRearCouplerPinXYZms;
		public double Bol2BolAngle;
		public double NudgedBol2BolAngle;
		public double RBol2PinX;
		public double FBol2PinX;
		// 
		public float sourcePosition;
	};

	// Physics DLL API Calls

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern IntPtr CreateVehicle();

	[DllImport("Physics", CallingConvention = CallingConvention.Cdecl)]
	private static extern void DestroyVehicle(IntPtr innerVehicle);

	[DllImport("Physics")]
	private static extern WagonSetup VehicleGetWagonSetup(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWagonSetup(IntPtr vehicle, ref WagonSetup wagSetup);

	[DllImport("Physics")]
	private static extern WagonParameters VehicleGetWagonParameters(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWagonParameters(IntPtr vehicle, ref WagonParameters wagParms);

	[DllImport("Physics")]
	private static extern TrkParms VehicleGetTrkParms(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTrkParms(IntPtr vehicle, ref TrkParms trkParms);

	[DllImport("Physics")]
	private static extern IntPtr VehicleGetNextLinked(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNextLinked(IntPtr vehicle, IntPtr nextLinked);

	[DllImport("Physics")]
	private static extern IntPtr VehicleGetPreviousLinked(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPreviousLinked(IntPtr vehicle, IntPtr previousLinked);

	[DllImport("Physics")]
	private static extern IntPtr VehicleGetExtraData(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetExtraData(IntPtr vehicle, IntPtr extraData);

	[DllImport("Physics")]
	private static extern void VehicleSetPCon(IntPtr vehicle, IntPtr pcon);

	// Generated DllImport statements

	[DllImport("Physics")]
	private static extern float VehicleGetFBrake(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFBrake(IntPtr vehicle, float _fBrake);

	[DllImport("Physics")]
	private static extern float VehicleGetFBrakeFade(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFBrakeFade(IntPtr vehicle, float _fBrakeFade);

	[DllImport("Physics")]
	private static extern int VehicleGetHandbrakeLast(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetHandbrakeLast(IntPtr vehicle, int _handbrakeLast);

	[DllImport("Physics")]
	private static extern int VehicleGetUseBrakeFade(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetUseBrakeFade(IntPtr vehicle, int _useBrakeFade);

	[DllImport("Physics")]
	private static extern EngDynamics VehicleGetEngDynamics(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern float VehicleGetAdhesion(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAdhesion(IntPtr vehicle, float _adhesion);

	[DllImport("Physics")]
	private static extern int VehicleGetSlipping(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSlipping(IntPtr vehicle, int _slipping);

	[DllImport("Physics")]
	private static extern float VehicleGetSlippingForce(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSlippingForce(IntPtr vehicle, float _slippingForce);

	[DllImport("Physics")]
	private static extern float VehicleGetAdhesionRatio(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAdhesionRatio(IntPtr vehicle, float _adhesionRatio);

	[DllImport("Physics")]
	private static extern int VehicleGetIndex(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetIndex(IntPtr vehicle, int _index);

	[DllImport("Physics")]
	private static extern int VehicleGetRefIndex(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRefIndex(IntPtr vehicle, int _refIndex);

	[DllImport("Physics")]
	private static extern float VehicleGetTMass(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTMass(IntPtr vehicle, float _tMass);

	[DllImport("Physics")]
	private static extern int VehicleGetConsist(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetConsist(IntPtr vehicle, int _consist);

	[DllImport("Physics")]
	private static extern int VehicleGetConsistIndex(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetConsistIndex(IntPtr vehicle, int _consistIndex);

	[DllImport("Physics")]
	private static extern float VehicleGetVelocityDesired(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetVelocityDesired(IntPtr vehicle, float _velocityDesired);

	[DllImport("Physics")]
	private static extern int VehicleGetMetersPosition(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMetersPosition(IntPtr vehicle, int _metersPosition);

	[DllImport("Physics")]
	private static extern double VehicleGetNeutralRelativePosition(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNeutralRelativePosition(IntPtr vehicle, double _neutralRelativePosition);

	[DllImport("Physics")]
	private static extern double VehicleGetTrueRelativePosition(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTrueRelativePosition(IntPtr vehicle, double _trueRelativePosition);

	[DllImport("Physics")]
	private static extern float VehicleGetPosition(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPosition(IntPtr vehicle, float _position);

	[DllImport("Physics")]
	private static extern float VehicleGetVelocity(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetVelocity(IntPtr vehicle, float _velocity);

	[DllImport("Physics")]
	private static extern float VehicleGetPositionDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPositionDot(IntPtr vehicle, float _positionDot);

	[DllImport("Physics")]
	private static extern float VehicleGetVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetVelocityDot(IntPtr vehicle, float _velocityDot);

	[DllImport("Physics")]
	private static extern float VehicleGetBPPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBPPress(IntPtr vehicle, float _BPPress);

	[DllImport("Physics")]
	private static extern float VehicleGetBPPressDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBPPressDot(IntPtr vehicle, float _BPPressDot);

	[DllImport("Physics")]
	private static extern float VehicleGetEmerResPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetEmerResPress(IntPtr vehicle, float _EmerResPress);

	[DllImport("Physics")]
	private static extern float VehicleGetEmerResPressDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetEmerResPressDot(IntPtr vehicle, float _EmerResPressDot);

	[DllImport("Physics")]
	private static extern float VehicleGetAuxResPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAuxResPress(IntPtr vehicle, float _AuxResPress);

	[DllImport("Physics")]
	private static extern float VehicleGetAuxResPressDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAuxResPressDot(IntPtr vehicle, float _AuxResPressDot);

	[DllImport("Physics")]
	private static extern float VehicleGetAuxResPress_To_BrCylPress_Offset(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAuxResPress_To_BrCylPress_Offset(IntPtr vehicle, float _AuxResPress_To_BrCylPress_Offset);

	[DllImport("Physics")]
	private static extern float VehicleGetBCylPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBCylPress(IntPtr vehicle, float _BCylPress);

	[DllImport("Physics")]
	private static extern float VehicleGetBCylPressDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBCylPressDot(IntPtr vehicle, float _BCylPressDot);

	[DllImport("Physics")]
	private static extern float VehicleGetBCylPressX(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBCylPressX(IntPtr vehicle, float _BCylPressX);

	[DllImport("Physics")]
	private static extern float VehicleGetAuxResPressLast(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAuxResPressLast(IntPtr vehicle, float _AuxResPressLast);

	[DllImport("Physics")]
	private static extern float VehicleGetEqualizingResPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetEqualizingResPress(IntPtr vehicle, float _EqualizingResPress);

	[DllImport("Physics")]
	private static extern float VehicleGetMainResPress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMainResPress(IntPtr vehicle, float _MainResPress);

	[DllImport("Physics")]
	private static extern float VehicleGetAirFlow(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAirFlow(IntPtr vehicle, float _AirFlow);

	[DllImport("Physics")]
	private static extern float VehicleGetMassFlow(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMassFlow(IntPtr vehicle, float _MassFlow);

	[DllImport("Physics")]
	private static extern float VehicleGetMomFlux(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMomFlux(IntPtr vehicle, float _MomFlux);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingFrictionForceMag(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingFrictionForceMag(IntPtr vehicle, float _RollingFrictionForceMag);

	[DllImport("Physics")]
	private static extern float VehicleGetBrakeForceMag(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBrakeForceMag(IntPtr vehicle, float _BrakeForceMag);

	[DllImport("Physics")]
	private static extern float VehicleGetHandBrakeForceMag(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetHandBrakeForceMag(IntPtr vehicle, float _HandBrakeForceMag);

	[DllImport("Physics")]
	private static extern float VehicleGetBrakeForce(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBrakeForce(IntPtr vehicle, float _BrakeForce);

	[DllImport("Physics")]
	private static extern float VehicleGetGradeForce(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetGradeForce(IntPtr vehicle, float _GradeForce);

	[DllImport("Physics")]
	private static extern float VehicleGetWindForce(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWindForce(IntPtr vehicle, float _WindForce);

	[DllImport("Physics")]
	private static extern float VehicleGetTensionLimit1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTensionLimit1(IntPtr vehicle, float _tensionLimit1);

	[DllImport("Physics")]
	private static extern float VehicleGetTensionLimit2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTensionLimit2(IntPtr vehicle, float _tensionLimit2);

	[DllImport("Physics")]
	private static extern float VehicleGetWheelInertia(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWheelInertia(IntPtr vehicle, float _wheelInertia);

	[DllImport("Physics")]
	private static extern int VehicleGetSticky(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSticky(IntPtr vehicle, int _sticky);

	[DllImport("Physics")]
	private static extern int VehicleGetFastState(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFastState(IntPtr vehicle, int _fastState);

	[DllImport("Physics")]
	private static extern float VehicleGetP0(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetP0(IntPtr vehicle, float _p0);

	[DllImport("Physics")]
	private static extern float VehicleGetV0(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetV0(IntPtr vehicle, float _v0);

	[DllImport("Physics")]
	private static extern int VehicleGetControlValveState(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetControlValveState(IntPtr vehicle, int _ControlValveState);

	[DllImport("Physics")]
	private static extern int VehicleGetAccelRel(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAccelRel(IntPtr vehicle, int _accelRel);

	[DllImport("Physics")]
	private static extern float VehicleGetBPPressDotAve(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBPPressDotAve(IntPtr vehicle, float _BPPressDotAve);

	[DllImport("Physics")]
	private static extern uint VehicleGetTic(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTic(IntPtr vehicle, uint _tic);

	[DllImport("Physics")]
	private static extern uint VehicleGetTic0(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTic0(IntPtr vehicle, uint _tic0);

	[DllImport("Physics")]
	private static extern uint VehicleGetTic1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTic1(IntPtr vehicle, uint _tic1);

	[DllImport("Physics")]
	private static extern uint VehicleGetTicWentToLap(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTicWentToLap(IntPtr vehicle, uint _ticWentToLap);

	[DllImport("Physics")]
	private static extern float VehicleGetMiscForces(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMiscForces(IntPtr vehicle, float _miscForces);

	[DllImport("Physics")]
	private static extern int VehicleGetABDW(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetABDW(IntPtr vehicle, int _ABDW);

	[DllImport("Physics")]
	private static extern int VehicleGetBailoff(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBailoff(IntPtr vehicle, int _bailoff);

	[DllImport("Physics")]
	private static extern float VehicleGetActualWidth(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetActualWidth(IntPtr vehicle, float _actualWidth);

	[DllImport("Physics")]
	private static extern float VehicleGetActualHeight(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetActualHeight(IntPtr vehicle, float _actualHeight);

	[DllImport("Physics")]
	private static extern float VehicleGetSpeedRestrictions(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSpeedRestrictions(IntPtr vehicle, float _speedRestrictions);

	[DllImport("Physics")]
	private static extern int VehicleGetFuelType(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFuelType(IntPtr vehicle, int _fuelType);

	[DllImport("Physics")]
	private static extern float VehicleGetFuelTankVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFuelTankVolume(IntPtr vehicle, float _fuelTankVolume);

	[DllImport("Physics")]
	private static extern float VehicleGetFuelVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFuelVolume(IntPtr vehicle, float _fuelVolume);

	[DllImport("Physics")]
	private static extern float VehicleGetFuelConsumed(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFuelConsumed(IntPtr vehicle, float _fuelConsumed);

	[DllImport("Physics")]
	private static extern float VehicleGetWaterTankVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWaterTankVolume(IntPtr vehicle, float _waterTankVolume);

	[DllImport("Physics")]
	private static extern float VehicleGetWaterVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWaterVolume(IntPtr vehicle, float _waterVolume);

	[DllImport("Physics")]
	private static extern float VehicleGetSandTankVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSandTankVolume(IntPtr vehicle, float _sandTankVolume);

	[DllImport("Physics")]
	private static extern float VehicleGetSandVolume(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSandVolume(IntPtr vehicle, float _sandVolume);

	[DllImport("Physics")]
	private static extern int VehicleGetPeopleAmount(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPeopleAmount(IntPtr vehicle, int _peopleAmount);

	[DllImport("Physics")]
	private static extern float VehicleGetAxleMass(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAxleMass(IntPtr vehicle, float _axleMass);

	[DllImport("Physics")]
	private static extern float VehicleGetWheelMass(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWheelMass(IntPtr vehicle, float _wheelMass);

	[DllImport("Physics")]
	private static extern float VehicleGetWheelRadius(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWheelRadius(IntPtr vehicle, float _wheelRadius);

	[DllImport("Physics")]
	private static extern float VehicleGetWheelTemperature(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetWheelTemperature(IntPtr vehicle, float _wheelTemperature);

	[DllImport("Physics")]
	private static extern int VehicleGetAxleCount(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAxleCount(IntPtr vehicle, int _axleCount);

	[DllImport("Physics")]
	private static extern float VehicleGetMomentOfInertia(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMomentOfInertia(IntPtr vehicle, float _momentOfInertia);

	[DllImport("Physics")]
	private static extern int VehicleGetDerailed(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetDerailed(IntPtr vehicle, int _derailed);

	[DllImport("Physics")]
	private static extern int VehicleGetTipping(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetTipping(IntPtr vehicle, int _tipping);

	[DllImport("Physics")]
	private static extern int VehicleGetRailRoll(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRailRoll(IntPtr vehicle, int _railRoll);

	[DllImport("Physics")]
	private static extern int VehicleGetFlangeSqueek(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFlangeSqueek(IntPtr vehicle, int _flangeSqueek);

	[DllImport("Physics")]
	private static extern float VehicleGetLOverV(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLOverV(IntPtr vehicle, float _LOverV);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_SumdForce1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_SumdForce1(IntPtr vehicle, float _LV_SumdForce1);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_SumdForce2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_SumdForce2(IntPtr vehicle, float _LV_SumdForce2);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_AvgdForce1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_AvgdForce1(IntPtr vehicle, float _LV_AvgdForce1);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_AvgdForce2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_AvgdForce2(IntPtr vehicle, float _LV_AvgdForce2);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_AlignControlMoment1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_AlignControlMoment1(IntPtr vehicle, float _LV_AlignControlMoment1);

	[DllImport("Physics")]
	private static extern float VehicleGetLV_AlignControlMoment2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLV_AlignControlMoment2(IntPtr vehicle, float _LV_AlignControlMoment2);

	[DllImport("Physics")]
	private static extern float VehicleGetCar2CouplerAngle1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetCar2CouplerAngle1(IntPtr vehicle, float _Car2CouplerAngle1);

	[DllImport("Physics")]
	private static extern float VehicleGetCar2CouplerAngle2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetCar2CouplerAngle2(IntPtr vehicle, float _Car2CouplerAngle2);

	[DllImport("Physics")]
	private static extern float VehicleGetNudgedCar2CouplerAngle1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNudgedCar2CouplerAngle1(IntPtr vehicle, float _NudgedCar2CouplerAngle1);

	[DllImport("Physics")]
	private static extern float VehicleGetNudgedCar2CouplerAngle2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNudgedCar2CouplerAngle2(IntPtr vehicle, float _NudgedCar2CouplerAngle2);

	[DllImport("Physics")]
	private static extern float VehicleGetMaxBolLatDisp(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMaxBolLatDisp(IntPtr vehicle, float _MaxBolLatDisp);

	[DllImport("Physics")]
	private static extern float VehicleGetLatCouplerForce1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLatCouplerForce1(IntPtr vehicle, float _LatCouplerForce1);

	[DllImport("Physics")]
	private static extern float VehicleGetLatCouplerForce2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLatCouplerForce2(IntPtr vehicle, float _LatCouplerForce2);

	[DllImport("Physics")]
	private static extern float VehicleGetLatBolsterForce1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLatBolsterForce1(IntPtr vehicle, float _LatBolsterForce1);

	[DllImport("Physics")]
	private static extern float VehicleGetLatBolsterForce2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLatBolsterForce2(IntPtr vehicle, float _LatBolsterForce2);

	[DllImport("Physics")]
	private static extern float VehicleGetLeadTruckLoverV(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetLeadTruckLoverV(IntPtr vehicle, float _LeadTruckLoverV);

	[DllImport("Physics")]
	private static extern float VehicleGetFollowTruckLoverV(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFollowTruckLoverV(IntPtr vehicle, float _FollowTruckLoverV);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC1(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC1(IntPtr vehicle, float _RollingResC1);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC2(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC2(IntPtr vehicle, float _RollingResC2);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC3(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC3(IntPtr vehicle, float _RollingResC3);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC4Lead(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC4Lead(IntPtr vehicle, float _RollingResC4Lead);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC4Follow(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern float VehicleGetRollingResC5(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC5(IntPtr vehicle, float _RollingResC5);

	[DllImport("Physics")]
	private static extern void VehicleSetRollingResC4Follow(IntPtr vehicle, float _RollingResC4Follow);

	[DllImport("Physics")]
	private static extern int VehicleGetNbOfAxles(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNbOfAxles(IntPtr vehicle, int _nbOfAxles);

	[DllImport("Physics")]
	private static extern float VehicleGetRotationalMass(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRotationalMass(IntPtr vehicle, float _RotationalMass);

	[DllImport("Physics")]
	private static extern float VehicleGetAirResistanceArea(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAirResistanceArea(IntPtr vehicle, float _AirResistanceArea);

	[DllImport("Physics")]
	private static extern int VehicleGetDPRole(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetDPRole(IntPtr vehicle, int _DPRole);

	[DllImport("Physics")]
	private static extern float VehicleGetPrelimQuickServicePressDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPrelimQuickServicePressDot(IntPtr vehicle, float _PrelimQuickServicePressDot);

	[DllImport("Physics")]
	private static extern float VehicleGetPrelimQuickServicePress(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPrelimQuickServicePress(IntPtr vehicle, float _PrelimQuickServicePress);

	[DllImport("Physics")]
	private static extern float VehicleGetPrelimQuickServiceVolumeChargeRate(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPrelimQuickServiceVolumeChargeRate(IntPtr vehicle, float _PrelimQuickServiceVolumeChargeRate);

	[DllImport("Physics")]
	private static extern float VehicleGetPrelimQuickServiceVolumeExhaustRate(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPrelimQuickServiceVolumeExhaustRate(IntPtr vehicle, float _PrelimQuickServiceVolumeExhaustRate);

	[DllImport("Physics")]
	private static extern int VehicleGetPrelimQuickServiceReady(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPrelimQuickServiceReady(IntPtr vehicle, int _PrelimQuickServiceReady);

	[DllImport("Physics")]
	private static extern float VehicleGetPowerEFS(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetPowerEFS(IntPtr vehicle, float _PowerEFS);

	[DllImport("Physics")]
	private static extern float VehicleGetBrakeEFS(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetBrakeEFS(IntPtr vehicle, float _BrakeEFS);

	[DllImport("Physics")]
	private static extern float VehicleGetAdhesionEFS(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetAdhesionEFS(IntPtr vehicle, float _AdhesionEFS);

	[DllImport("Physics")]
	private static extern int VehicleGetRailCarryingMode(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetRailCarryingMode(IntPtr vehicle, int _RailCarryingMode);

	[DllImport("Physics")]
	private static extern float VehicleGetFilteredMaxVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFilteredMaxVelocityDot(IntPtr vehicle, float _FilteredMaxVelocityDot);

	[DllImport("Physics")]
	private static extern float VehicleGetFilteredMinVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetFilteredMinVelocityDot(IntPtr vehicle, float _FilteredMinVelocityDot);

	[DllImport("Physics")]
	private static extern float VehicleGetSumVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetSumVelocityDot(IntPtr vehicle, float _SumVelocityDot);

	[DllImport("Physics")]
	private static extern int VehicleGetNbVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetNbVelocityDot(IntPtr vehicle, int _NbVelocityDot);

	[DllImport("Physics")]
	private static extern float VehicleGetMaxVelocityDot(IntPtr vehicle);

	[DllImport("Physics")]
	private static extern void VehicleSetMaxVelocityDot(IntPtr vehicle, float _MaxVelocityDot);

	//-------------------------------------------------------------------------

	private IntPtr innerVehicle;
	private bool isNative;

	public CarPhysics()
	{
		innerVehicle = CreateVehicle();
		VehicleSetPreviousLinked(innerVehicle, new IntPtr(0));
		VehicleSetNextLinked(innerVehicle, new IntPtr(0));
		isNative = true;
	}

	public CarPhysics(IntPtr _innerVehicle)
	{
		innerVehicle = _innerVehicle;
		isNative = false;
	}

	~CarPhysics()
	{
		if (isNative)
		{
			DestroyVehicle(innerVehicle);
		}
	}

	public IntPtr GetInnerVehicle()
	{
		return innerVehicle;
	}

	public WagonSetup WagSet
	{
		get
		{
			return VehicleGetWagonSetup(innerVehicle);
		}
		set
		{
			VehicleSetWagonSetup(innerVehicle, ref value);
		}
	}

	public WagonParameters p
	{
		get
		{
			return VehicleGetWagonParameters(innerVehicle);
		}
		set
		{
			VehicleSetWagonParameters(innerVehicle, ref value);
		}
	}

	public TrkParms trkData
	{
		get
		{
			return VehicleGetTrkParms(innerVehicle);
		}
		set
		{
			VehicleSetTrkParms(innerVehicle, ref value);
		}
	}

	public CarPhysics nextLinked
	{
		get
		{
			return new CarPhysics(VehicleGetNextLinked(innerVehicle));
		}
		set
		{
			VehicleSetNextLinked(innerVehicle, value.innerVehicle);
		}
	}

	public CarPhysics previousLinked
	{
		get
		{
			return new CarPhysics(VehicleGetPreviousLinked(innerVehicle));
		}
		set
		{
			VehicleSetPreviousLinked(innerVehicle, value.innerVehicle);
		}
	}

	public int CarID
	{
		get
		{
			return (int)(0xF0000000 ^ (uint)VehicleGetExtraData(innerVehicle).ToInt32());
		}
		set
		{
			VehicleSetExtraData(innerVehicle, new IntPtr(0xF0000000 ^ value));
		}
	}

	public ConsistPhysics consistPhysics
	{
		set
		{
			VehicleSetPCon(innerVehicle, value.GetInnerConsist());
		}
	}

	public float fBrake
	{
		get
		{
			return VehicleGetFBrake(innerVehicle);
		}
		set
		{
			VehicleSetFBrake(innerVehicle, value);
		}
	}

	public float fBrakeFade
	{
		get
		{
			return VehicleGetFBrakeFade(innerVehicle);
		}
		set
		{
			VehicleSetFBrakeFade(innerVehicle, value);
		}
	}

	public int handbrakeLast
	{
		get
		{
			return VehicleGetHandbrakeLast(innerVehicle);
		}
		set
		{
			VehicleSetHandbrakeLast(innerVehicle, value);
		}
	}

	public int useBrakeFade
	{
		get
		{
			return VehicleGetUseBrakeFade(innerVehicle);
		}
		set
		{
			VehicleSetUseBrakeFade(innerVehicle, value);
		}
	}

	public EngDynamics ed
	{
		get
		{
			return VehicleGetEngDynamics(innerVehicle);
		}
	}

	public float adhesion
	{
		get
		{
			return VehicleGetAdhesion(innerVehicle);
		}
		set
		{
			VehicleSetAdhesion(innerVehicle, value);
		}
	}

	public int slipping
	{
		get
		{
			return VehicleGetSlipping(innerVehicle);
		}
		set
		{
			VehicleSetSlipping(innerVehicle, value);
		}
	}

	public float slippingForce
	{
		get
		{
			return VehicleGetSlippingForce(innerVehicle);
		}
		set
		{
			VehicleSetSlippingForce(innerVehicle, value);
		}
	}

	public float adhesionRatio
	{
		get
		{
			return VehicleGetAdhesionRatio(innerVehicle);
		}
		set
		{
			VehicleSetAdhesionRatio(innerVehicle, value);
		}
	}

	public int index
	{
		get
		{
			return VehicleGetIndex(innerVehicle);
		}
		set
		{
			VehicleSetIndex(innerVehicle, value);
		}
	}

	public int refIndex
	{
		get
		{
			return VehicleGetRefIndex(innerVehicle);
		}
		set
		{
			VehicleSetRefIndex(innerVehicle, value);
		}
	}

	public float tMass
	{
		get
		{
			return VehicleGetTMass(innerVehicle);
		}
		set
		{
			VehicleSetTMass(innerVehicle, value);
		}
	}

	public int consist
	{
		get
		{
			return VehicleGetConsist(innerVehicle);
		}
		set
		{
			VehicleSetConsist(innerVehicle, value);
		}
	}

	public int consistIndex
	{
		get
		{
			return VehicleGetConsistIndex(innerVehicle);
		}
		set
		{
			VehicleSetConsistIndex(innerVehicle, value);
		}
	}

	public float velocityDesired
	{
		get
		{
			return VehicleGetVelocityDesired(innerVehicle);
		}
		set
		{
			VehicleSetVelocityDesired(innerVehicle, value);
		}
	}

	public int metersPosition
	{
		get
		{
			return VehicleGetMetersPosition(innerVehicle);
		}
		set
		{
			VehicleSetMetersPosition(innerVehicle, value);
		}
	}

	public double neutralRelativePosition
	{
		get
		{
			return VehicleGetNeutralRelativePosition(innerVehicle);
		}
		set
		{
			VehicleSetNeutralRelativePosition(innerVehicle, value);
		}
	}

	public double trueRelativePosition
	{
		get
		{
			return VehicleGetTrueRelativePosition(innerVehicle);
		}
		set
		{
			VehicleSetTrueRelativePosition(innerVehicle, value);
		}
	}

	public float position
	{
		get
		{
			return VehicleGetPosition(innerVehicle);
		}
		set
		{
			VehicleSetPosition(innerVehicle, value);
		}
	}

	public float velocity
	{
		get
		{
			return VehicleGetVelocity(innerVehicle);
		}
		set
		{
			VehicleSetVelocity(innerVehicle, value);
		}
	}

	public float positionDot
	{
		get
		{
			return VehicleGetPositionDot(innerVehicle);
		}
		set
		{
			VehicleSetPositionDot(innerVehicle, value);
		}
	}

	public float velocityDot
	{
		get
		{
			return VehicleGetVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetVelocityDot(innerVehicle, value);
		}
	}

	public float BPPress
	{
		get
		{
			return VehicleGetBPPress(innerVehicle);
		}
		set
		{
			VehicleSetBPPress(innerVehicle, value);
		}
	}

	public float BPPressDot
	{
		get
		{
			return VehicleGetBPPressDot(innerVehicle);
		}
		set
		{
			VehicleSetBPPressDot(innerVehicle, value);
		}
	}

	public float EmerResPress
	{
		get
		{
			return VehicleGetEmerResPress(innerVehicle);
		}
		set
		{
			VehicleSetEmerResPress(innerVehicle, value);
		}
	}

	public float EmerResPressDot
	{
		get
		{
			return VehicleGetEmerResPressDot(innerVehicle);
		}
		set
		{
			VehicleSetEmerResPressDot(innerVehicle, value);
		}
	}

	public float AuxResPress
	{
		get
		{
			return VehicleGetAuxResPress(innerVehicle);
		}
		set
		{
			VehicleSetAuxResPress(innerVehicle, value);
		}
	}

	public float AuxResPressDot
	{
		get
		{
			return VehicleGetAuxResPressDot(innerVehicle);
		}
		set
		{
			VehicleSetAuxResPressDot(innerVehicle, value);
		}
	}

	public float AuxResPress_To_BrCylPress_Offset
	{
		get
		{
			return VehicleGetAuxResPress_To_BrCylPress_Offset(innerVehicle);
		}
		set
		{
			VehicleSetAuxResPress_To_BrCylPress_Offset(innerVehicle, value);
		}
	}

	public float BCylPress
	{
		get
		{
			return VehicleGetBCylPress(innerVehicle);
		}
		set
		{
			VehicleSetBCylPress(innerVehicle, value);
		}
	}

	public float BCylPressDot
	{
		get
		{
			return VehicleGetBCylPressDot(innerVehicle);
		}
		set
		{
			VehicleSetBCylPressDot(innerVehicle, value);
		}
	}

	public float BCylPressX
	{
		get
		{
			return VehicleGetBCylPressX(innerVehicle);
		}
		set
		{
			VehicleSetBCylPressX(innerVehicle, value);
		}
	}

	public float AuxResPressLast
	{
		get
		{
			return VehicleGetAuxResPressLast(innerVehicle);
		}
		set
		{
			VehicleSetAuxResPressLast(innerVehicle, value);
		}
	}

	public float EqualizingResPress
	{
		get
		{
			return VehicleGetEqualizingResPress(innerVehicle);
		}
		set
		{
			VehicleSetEqualizingResPress(innerVehicle, value);
		}
	}

	public float MainResPress
	{
		get
		{
			return VehicleGetMainResPress(innerVehicle);
		}
		set
		{
			VehicleSetMainResPress(innerVehicle, value);
		}
	}

	public float AirFlow
	{
		get
		{
			return VehicleGetAirFlow(innerVehicle);
		}
		set
		{
			VehicleSetAirFlow(innerVehicle, value);
		}
	}

	public float MassFlow
	{
		get
		{
			return VehicleGetMassFlow(innerVehicle);
		}
		set
		{
			VehicleSetMassFlow(innerVehicle, value);
		}
	}

	public float MomFlux
	{
		get
		{
			return VehicleGetMomFlux(innerVehicle);
		}
		set
		{
			VehicleSetMomFlux(innerVehicle, value);
		}
	}

	public float RollingFrictionForceMag
	{
		get
		{
			return VehicleGetRollingFrictionForceMag(innerVehicle);
		}
		set
		{
			VehicleSetRollingFrictionForceMag(innerVehicle, value);
		}
	}

	public float BrakeForceMag
	{
		get
		{
			return VehicleGetBrakeForceMag(innerVehicle);
		}
		set
		{
			VehicleSetBrakeForceMag(innerVehicle, value);
		}
	}

	public float HandBrakeForceMag
	{
		get
		{
			return VehicleGetHandBrakeForceMag(innerVehicle);
		}
		set
		{
			VehicleSetHandBrakeForceMag(innerVehicle, value);
		}
	}

	public float BrakeForce
	{
		get
		{
			return VehicleGetBrakeForce(innerVehicle);
		}
		set
		{
			VehicleSetBrakeForce(innerVehicle, value);
		}
	}

	public float GradeForce
	{
		get
		{
			return VehicleGetGradeForce(innerVehicle);
		}
		set
		{
			VehicleSetGradeForce(innerVehicle, value);
		}
	}

	public float WindForce
	{
		get
		{
			return VehicleGetWindForce(innerVehicle);
		}
		set
		{
			VehicleSetWindForce(innerVehicle, value);
		}
	}

	public float tensionLimit1
	{
		get
		{
			return VehicleGetTensionLimit1(innerVehicle);
		}
		set
		{
			VehicleSetTensionLimit1(innerVehicle, value);
		}
	}

	public float tensionLimit2
	{
		get
		{
			return VehicleGetTensionLimit2(innerVehicle);
		}
		set
		{
			VehicleSetTensionLimit2(innerVehicle, value);
		}
	}

	public float wheelInertia
	{
		get
		{
			return VehicleGetWheelInertia(innerVehicle);
		}
		set
		{
			VehicleSetWheelInertia(innerVehicle, value);
		}
	}

	public int sticky
	{
		get
		{
			return VehicleGetSticky(innerVehicle);
		}
		set
		{
			VehicleSetSticky(innerVehicle, value);
		}
	}

	public int fastState
	{
		get
		{
			return VehicleGetFastState(innerVehicle);
		}
		set
		{
			VehicleSetFastState(innerVehicle, value);
		}
	}

	public float p0
	{
		get
		{
			return VehicleGetP0(innerVehicle);
		}
		set
		{
			VehicleSetP0(innerVehicle, value);
		}
	}

	public float v0
	{
		get
		{
			return VehicleGetV0(innerVehicle);
		}
		set
		{
			VehicleSetV0(innerVehicle, value);
		}
	}

	public int ControlValveState
	{
		get
		{
			return VehicleGetControlValveState(innerVehicle);
		}
		set
		{
			VehicleSetControlValveState(innerVehicle, value);
		}
	}

	public int accelRel
	{
		get
		{
			return VehicleGetAccelRel(innerVehicle);
		}
		set
		{
			VehicleSetAccelRel(innerVehicle, value);
		}
	}

	public float BPPressDotAve
	{
		get
		{
			return VehicleGetBPPressDotAve(innerVehicle);
		}
		set
		{
			VehicleSetBPPressDotAve(innerVehicle, value);
		}
	}

	public uint tic
	{
		get
		{
			return VehicleGetTic(innerVehicle);
		}
		set
		{
			VehicleSetTic(innerVehicle, value);
		}
	}

	public uint tic0
	{
		get
		{
			return VehicleGetTic0(innerVehicle);
		}
		set
		{
			VehicleSetTic0(innerVehicle, value);
		}
	}

	public uint tic1
	{
		get
		{
			return VehicleGetTic1(innerVehicle);
		}
		set
		{
			VehicleSetTic1(innerVehicle, value);
		}
	}

	public uint ticWentToLap
	{
		get
		{
			return VehicleGetTicWentToLap(innerVehicle);
		}
		set
		{
			VehicleSetTicWentToLap(innerVehicle, value);
		}
	}

	public float miscForces
	{
		get
		{
			return VehicleGetMiscForces(innerVehicle);
		}
		set
		{
			VehicleSetMiscForces(innerVehicle, value);
		}
	}

	public int ABDW
	{
		get
		{
			return VehicleGetABDW(innerVehicle);
		}
		set
		{
			VehicleSetABDW(innerVehicle, value);
		}
	}

	public int bailoff
	{
		get
		{
			return VehicleGetBailoff(innerVehicle);
		}
		set
		{
			VehicleSetBailoff(innerVehicle, value);
		}
	}

	public float actualWidth
	{
		get
		{
			return VehicleGetActualWidth(innerVehicle);
		}
		set
		{
			VehicleSetActualWidth(innerVehicle, value);
		}
	}

	public float actualHeight
	{
		get
		{
			return VehicleGetActualHeight(innerVehicle);
		}
		set
		{
			VehicleSetActualHeight(innerVehicle, value);
		}
	}

	public float speedRestrictions
	{
		get
		{
			return VehicleGetSpeedRestrictions(innerVehicle);
		}
		set
		{
			VehicleSetSpeedRestrictions(innerVehicle, value);
		}
	}

	public int fuelType
	{
		get
		{
			return VehicleGetFuelType(innerVehicle);
		}
		set
		{
			VehicleSetFuelType(innerVehicle, value);
		}
	}

	public float fuelTankVolume
	{
		get
		{
			return VehicleGetFuelTankVolume(innerVehicle);
		}
		set
		{
			VehicleSetFuelTankVolume(innerVehicle, value);
		}
	}

	public float fuelVolume
	{
		get
		{
			return VehicleGetFuelVolume(innerVehicle);
		}
		set
		{
			VehicleSetFuelVolume(innerVehicle, value);
		}
	}

	public float fuelConsumed
	{
		get
		{
			return VehicleGetFuelConsumed(innerVehicle);
		}
		set
		{
			VehicleSetFuelConsumed(innerVehicle, value);
		}
	}

	public float waterTankVolume
	{
		get
		{
			return VehicleGetWaterTankVolume(innerVehicle);
		}
		set
		{
			VehicleSetWaterTankVolume(innerVehicle, value);
		}
	}

	public float waterVolume
	{
		get
		{
			return VehicleGetWaterVolume(innerVehicle);
		}
		set
		{
			VehicleSetWaterVolume(innerVehicle, value);
		}
	}

	public float sandTankVolume
	{
		get
		{
			return VehicleGetSandTankVolume(innerVehicle);
		}
		set
		{
			VehicleSetSandTankVolume(innerVehicle, value);
		}
	}

	public float sandVolume
	{
		get
		{
			return VehicleGetSandVolume(innerVehicle);
		}
		set
		{
			VehicleSetSandVolume(innerVehicle, value);
		}
	}

	public int peopleAmount
	{
		get
		{
			return VehicleGetPeopleAmount(innerVehicle);
		}
		set
		{
			VehicleSetPeopleAmount(innerVehicle, value);
		}
	}

	public float axleMass
	{
		get
		{
			return VehicleGetAxleMass(innerVehicle);
		}
		set
		{
			VehicleSetAxleMass(innerVehicle, value);
		}
	}

	public float wheelMass
	{
		get
		{
			return VehicleGetWheelMass(innerVehicle);
		}
		set
		{
			VehicleSetWheelMass(innerVehicle, value);
		}
	}

	public float wheelRadius
	{
		get
		{
			return VehicleGetWheelRadius(innerVehicle);
		}
		set
		{
			VehicleSetWheelRadius(innerVehicle, value);
		}
	}

	public float wheelTemperature
	{
		get
		{
			return VehicleGetWheelTemperature(innerVehicle);
		}
		set
		{
			VehicleSetWheelTemperature(innerVehicle, value);
		}
	}

	public int axleCount
	{
		get
		{
			return VehicleGetAxleCount(innerVehicle);
		}
		set
		{
			VehicleSetAxleCount(innerVehicle, value);
		}
	}

	public float momentOfInertia
	{
		get
		{
			return VehicleGetMomentOfInertia(innerVehicle);
		}
		set
		{
			VehicleSetMomentOfInertia(innerVehicle, value);
		}
	}

	public int derailed
	{
		get
		{
			return VehicleGetDerailed(innerVehicle);
		}
		set
		{
			VehicleSetDerailed(innerVehicle, value);
		}
	}

	public int tipping
	{
		get
		{
			return VehicleGetTipping(innerVehicle);
		}
		set
		{
			VehicleSetTipping(innerVehicle, value);
		}
	}

	public int railRoll
	{
		get
		{
			return VehicleGetRailRoll(innerVehicle);
		}
		set
		{
			VehicleSetRailRoll(innerVehicle, value);
		}
	}

	public int flangeSqueek
	{
		get
		{
			return VehicleGetFlangeSqueek(innerVehicle);
		}
		set
		{
			VehicleSetFlangeSqueek(innerVehicle, value);
		}
	}

	public float LOverV
	{
		get
		{
			return VehicleGetLOverV(innerVehicle);
		}
		set
		{
			VehicleSetLOverV(innerVehicle, value);
		}
	}

	public float LV_SumdForce1
	{
		get
		{
			return VehicleGetLV_SumdForce1(innerVehicle);
		}
		set
		{
			VehicleSetLV_SumdForce1(innerVehicle, value);
		}
	}

	public float LV_SumdForce2
	{
		get
		{
			return VehicleGetLV_SumdForce2(innerVehicle);
		}
		set
		{
			VehicleSetLV_SumdForce2(innerVehicle, value);
		}
	}

	public float LV_AvgdForce1
	{
		get
		{
			return VehicleGetLV_AvgdForce1(innerVehicle);
		}
		set
		{
			VehicleSetLV_AvgdForce1(innerVehicle, value);
		}
	}

	public float LV_AvgdForce2
	{
		get
		{
			return VehicleGetLV_AvgdForce2(innerVehicle);
		}
		set
		{
			VehicleSetLV_AvgdForce2(innerVehicle, value);
		}
	}

	public float LV_AlignControlMoment1
	{
		get
		{
			return VehicleGetLV_AlignControlMoment1(innerVehicle);
		}
		set
		{
			VehicleSetLV_AlignControlMoment1(innerVehicle, value);
		}
	}

	public float LV_AlignControlMoment2
	{
		get
		{
			return VehicleGetLV_AlignControlMoment2(innerVehicle);
		}
		set
		{
			VehicleSetLV_AlignControlMoment2(innerVehicle, value);
		}
	}

	public float Car2CouplerAngle1
	{
		get
		{
			return VehicleGetCar2CouplerAngle1(innerVehicle);
		}
		set
		{
			VehicleSetCar2CouplerAngle1(innerVehicle, value);
		}
	}

	public float Car2CouplerAngle2
	{
		get
		{
			return VehicleGetCar2CouplerAngle2(innerVehicle);
		}
		set
		{
			VehicleSetCar2CouplerAngle2(innerVehicle, value);
		}
	}

	public float NudgedCar2CouplerAngle1
	{
		get
		{
			return VehicleGetNudgedCar2CouplerAngle1(innerVehicle);
		}
		set
		{
			VehicleSetNudgedCar2CouplerAngle1(innerVehicle, value);
		}
	}

	public float NudgedCar2CouplerAngle2
	{
		get
		{
			return VehicleGetNudgedCar2CouplerAngle2(innerVehicle);
		}
		set
		{
			VehicleSetNudgedCar2CouplerAngle2(innerVehicle, value);
		}
	}

	public float MaxBolLatDisp
	{
		get
		{
			return VehicleGetMaxBolLatDisp(innerVehicle);
		}
		set
		{
			VehicleSetMaxBolLatDisp(innerVehicle, value);
		}
	}

	public float LatCouplerForce1
	{
		get
		{
			return VehicleGetLatCouplerForce1(innerVehicle);
		}
		set
		{
			VehicleSetLatCouplerForce1(innerVehicle, value);
		}
	}

	public float LatCouplerForce2
	{
		get
		{
			return VehicleGetLatCouplerForce2(innerVehicle);
		}
		set
		{
			VehicleSetLatCouplerForce2(innerVehicle, value);
		}
	}

	public float LatBolsterForce1
	{
		get
		{
			return VehicleGetLatBolsterForce1(innerVehicle);
		}
		set
		{
			VehicleSetLatBolsterForce1(innerVehicle, value);
		}
	}

	public float LatBolsterForce2
	{
		get
		{
			return VehicleGetLatBolsterForce2(innerVehicle);
		}
		set
		{
			VehicleSetLatBolsterForce2(innerVehicle, value);
		}
	}

	public float LeadTruckLoverV
	{
		get
		{
			return VehicleGetLeadTruckLoverV(innerVehicle);
		}
		set
		{
			VehicleSetLeadTruckLoverV(innerVehicle, value);
		}
	}

	public float FollowTruckLoverV
	{
		get
		{
			return VehicleGetFollowTruckLoverV(innerVehicle);
		}
		set
		{
			VehicleSetFollowTruckLoverV(innerVehicle, value);
		}
	}

	public float RollingResC1
	{
		get
		{
			return VehicleGetRollingResC1(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC1(innerVehicle, value);
		}
	}

	public float RollingResC2
	{
		get
		{
			return VehicleGetRollingResC2(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC2(innerVehicle, value);
		}
	}

	public float RollingResC3
	{
		get
		{
			return VehicleGetRollingResC3(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC3(innerVehicle, value);
		}
	}

	public float RollingResC4Lead
	{
		get
		{
			return VehicleGetRollingResC4Lead(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC4Lead(innerVehicle, value);
		}
	}

	public float RollingResC4Follow
	{
		get
		{
			return VehicleGetRollingResC4Follow(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC4Follow(innerVehicle, value);
		}
	}

	public float RollingResC5
	{
		get
		{
			return VehicleGetRollingResC5(innerVehicle);
		}
		set
		{
			VehicleSetRollingResC5(innerVehicle, value);
		}
	}

	public int nbOfAxles
	{
		get
		{
			return VehicleGetNbOfAxles(innerVehicle);
		}
		set
		{
			VehicleSetNbOfAxles(innerVehicle, value);
		}
	}

	public float RotationalMass
	{
		get
		{
			return VehicleGetRotationalMass(innerVehicle);
		}
		set
		{
			VehicleSetRotationalMass(innerVehicle, value);
		}
	}

	public float AirResistanceArea
	{
		get
		{
			return VehicleGetAirResistanceArea(innerVehicle);
		}
		set
		{
			VehicleSetAirResistanceArea(innerVehicle, value);
		}
	}

	public int DPRole
	{
		get
		{
			return VehicleGetDPRole(innerVehicle);
		}
		set
		{
			VehicleSetDPRole(innerVehicle, value);
		}
	}

	public float PrelimQuickServicePressDot
	{
		get
		{
			return VehicleGetPrelimQuickServicePressDot(innerVehicle);
		}
		set
		{
			VehicleSetPrelimQuickServicePressDot(innerVehicle, value);
		}
	}

	public float PrelimQuickServicePress
	{
		get
		{
			return VehicleGetPrelimQuickServicePress(innerVehicle);
		}
		set
		{
			VehicleSetPrelimQuickServicePress(innerVehicle, value);
		}
	}

	public float PrelimQuickServiceVolumeChargeRate
	{
		get
		{
			return VehicleGetPrelimQuickServiceVolumeChargeRate(innerVehicle);
		}
		set
		{
			VehicleSetPrelimQuickServiceVolumeChargeRate(innerVehicle, value);
		}
	}

	public float PrelimQuickServiceVolumeExhaustRate
	{
		get
		{
			return VehicleGetPrelimQuickServiceVolumeExhaustRate(innerVehicle);
		}
		set
		{
			VehicleSetPrelimQuickServiceVolumeExhaustRate(innerVehicle, value);
		}
	}

	public int PrelimQuickServiceReady
	{
		get
		{
			return VehicleGetPrelimQuickServiceReady(innerVehicle);
		}
		set
		{
			VehicleSetPrelimQuickServiceReady(innerVehicle, value);
		}
	}

	public float PowerEFS
	{
		get
		{
			return VehicleGetPowerEFS(innerVehicle);
		}
		set
		{
			VehicleSetPowerEFS(innerVehicle, value);
		}
	}

	public float BrakeEFS //Brake efficiency 
	{
		get
		{
			return VehicleGetBrakeEFS(innerVehicle);
		}
		set
		{
			VehicleSetBrakeEFS(innerVehicle, value);
		}
	}

	public float AdhesionEFS
	{
		get
		{
			return VehicleGetAdhesionEFS(innerVehicle);
		}
		set
		{
			VehicleSetAdhesionEFS(innerVehicle, value);
		}
	}

	public int RailCarryingMode
	{
		get
		{
			return VehicleGetRailCarryingMode(innerVehicle);
		}
		set
		{
			VehicleSetRailCarryingMode(innerVehicle, value);
		}
	}

	public float FilteredMaxVelocityDot
	{
		get
		{
			return VehicleGetFilteredMaxVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetFilteredMaxVelocityDot(innerVehicle, value);
		}
	}

	public float FilteredMinVelocityDot
	{
		get
		{
			return VehicleGetFilteredMinVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetFilteredMinVelocityDot(innerVehicle, value);
		}
	}

	public float SumVelocityDot
	{
		get
		{
			return VehicleGetSumVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetSumVelocityDot(innerVehicle, value);
		}
	}

	public int NbVelocityDot
	{
		get
		{
			return VehicleGetNbVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetNbVelocityDot(innerVehicle, value);
		}
	}

	public float MaxVelocityDot
	{
		get
		{
			return VehicleGetMaxVelocityDot(innerVehicle);
		}
		set
		{
			VehicleSetMaxVelocityDot(innerVehicle, value);
		}
	}

}