using System;
using System.Collections.Generic;
using TMTSPluginsCS;

public partial class Consist : MasterIndexEntry
{
	public static List<Consist> AllConsists;

	public ConsistPhysics consistPhysics;       // Wrapper for Consist instance in Physics DLL
	public Car firstCar;
	public Car lastCar;
	public string consistFileName;
	public int orientation = 1;
	public int controllingCar = -1;
	public int collidingConsist;
	public int collisionNode;
	public int lod;
	public double offset;
	public PointXYZ[] couplerCoords;
	public bool disabled;
	public NotchSettingState notchSettingState;
	public int trackLineFront;
	public int trackLineRear;

	public static double IntegrationTimeStep = 0.030;
	public static double consistPhysicsTime = 0.0;
	public static double consistPhysicsUpdates = 0;

	static Consist()
	{
		AllConsists = new List<Consist>();
	}

	public Consist(string consistFileName) : base(EntryType.Consist)
	{
		this.consistFileName = consistFileName;
		firstCar = null;
		lastCar = null;
		couplerCoords = new PointXYZ[2];
		consistPhysics = new ConsistPhysics();
		notchSettingState = new NotchSettingState();
	}

	public Consist(string consistFileName, ConsistPhysics consistPhysics) : base(EntryType.Consist)
	{
		this.consistFileName = consistFileName;
		this.consistPhysics = consistPhysics;
		lod = 0;
		firstCar = null;
		lastCar = null;
		disabled = false;
		couplerCoords = new PointXYZ[2];
		consistPhysics = new ConsistPhysics();
		notchSettingState = new NotchSettingState();
	}

	public override string ToDataDebugString()
	{
		return "File Name:         " + consistFileName
			 + "\nDisabled:          " + disabled
			 + "\nControlling Car:   " + controllingCar
			 + "\nFirst Car:         " + (firstCar != null ? firstCar.vehNum : "null")
			 + "\nLast Car:          " + (lastCar != null ? lastCar.vehNum : "null")
			 + "\nOrientation:       " + orientation;
	}

	public static Consist LoadFromString(string csv, int allowScripts)
	{
		CommaSeparatedValueFile file = new CommaSeparatedValueFile();

		if (!file.LoadFromString(csv))
		{
			throw new Exception(string.Format("Loading consist file from text failed, error: {0}", file.errorMsg));
		}

		return Load(file, "unknown", allowScripts);
	}

	public static Consist Load(string cstFilepath)
	{
		return Load(cstFilepath, 1);
	}

	public static Consist Load(string cstFilepath, int allowScripts)
	{
		string consistFileName = System.IO.Path.GetFileNameWithoutExtension(cstFilepath);
		if (cstFilepath == null)
		{
			throw new Exception(string.Format("Unable to load consist; cannot find consist file {0}.", consistFileName));
		}

		CommaSeparatedValueFile file = new CommaSeparatedValueFile();
		if (!file.Load(cstFilepath))
		{
			throw new Exception(string.Format("Loading consist file {0} failed, error: {1}", consistFileName, file.errorMsg));
		}

		return Load(file, consistFileName, allowScripts);
	}

	public static Consist Load(CommaSeparatedValueFile file, string consistFileName, int allowScripts)
	{
		Consist consist;
		CarPhysics.WagonSetup WagSet;
		CarPhysics.WagonParameters WagParms;
		string filePath;
		Car car = null;
		Car prevCar = null;
		int numCars;
		int firstCar, lastCar;
		int scriptFileIndex;
		int DPGroup = 0;

		file.WaitUntilLoaded();
		if (!file.success)
		{
			throw new Exception(string.Format("Loading consist file {0} failed (after wait), error: {1}", consistFileName, file.errorMsg));
		}
		file.SetHeadersAreInFirstRow();

		float[] brakePipeFriction = new float[file.GetRowCount()];
		float[] brakePipeExponent = new float[file.GetRowCount()];

		scriptFileIndex = file.GetColumnIndex("Special Script");
		if (scriptFileIndex == -1)
		{
			scriptFileIndex = file.GetColumnIndex("Special Scripts");
		}

		consist = new Consist(consistFileName);

		numCars = file.GetRowCount();
		for (int carIndex = 0; carIndex < numCars; carIndex++)
		{
			car = new Car();

			WagSet = car.carPhysics.WagSet;     // Get defaults
			WagParms = car.carPhysics.p;        // Get defaults	

			if (carIndex == 0)
			{
				consist.firstCar = car;
				consist.consistPhysics.V1 = car.carPhysics;
			}

			car.vehNum = file.GetCellString(carIndex, "Equipment Number");

			WagSet.direction = file.GetCellInt(carIndex, "Direction");
			WagParms.iType = file.GetCellInt(carIndex, "Type");
			WagParms.subType = file.GetCellInt(carIndex, "SubType");


			car.path = file.GetCellString(carIndex, "Path");
			car.frontCouplerLength = (float)file.GetCellDouble(carIndex, "Front Coupler Length", 0.9);
			car.rearCouplerLength = (float)file.GetCellDouble(carIndex, "Rear Coupler Length", 0.9);
			car.LoadTypeString = file.GetCellString(carIndex, "Load Type");

			car.ownerName = file.GetCellString(carIndex, "Owner Name");
			car.remoteEquipped = file.GetCellInt(carIndex, "Remote Equipped");
			car.badOrder = file.GetCellInt(carIndex, "Bad Order");
			car.hazardous = file.GetCellInt(carIndex, "Hazardous");
			car.destination = file.GetCellString(carIndex, "Destination");
			car.zoneCarrier = file.GetCellString(carIndex, "Zone/Carrier");
			car.block = file.GetCellString(carIndex, "Block");
			car.carName = file.GetCellString(carIndex, "Name");
			car.consistDescription = file.GetCellString(carIndex, "Consist Description");
			car.sign = file.GetCellString(carIndex, "Sign");
			car.previewFile = file.GetCellString(carIndex, "Preview File");
			car.version = file.GetCellString(carIndex, "Version");
			car.nextBlock = file.GetCellString(carIndex, "Next Block");
			car.groupNumber = file.GetCellString(carIndex, "Group Number");

			WagParms.frontTruck = (float)file.GetCellDouble(carIndex, "Front Truck Offset", 5.0);
			WagParms.frontCouplerPosition = (float)file.GetCellDouble(carIndex, "Front Coupler Position", 7.0);

			WagParms.length = (float)file.GetCellDouble(carIndex, "Car Length", 15.0);
			WagParms.trackWidth = (float)file.GetCellDouble(carIndex, "Track Width", 1.6);

			WagSet.front.couplingStrength = (float)file.GetCellDouble(carIndex, "Front Coupler Strength", 1400000.0);
			WagSet.front.coupler = file.GetCellInt(carIndex, "Front Coupler State");    //State of the front coupler.																	0=open latchable, 1=latched 2=Open-Unlatchable, 3=pulling on Pin, 4=broken
			WagSet.front.gladhand = file.GetCellInt(carIndex, "Front Gladhand");            //State of the front gladhand.																0=open,1=connected
			WagSet.front.gladhand = file.GetCellInt(carIndex, "Front Hose Connection");             //State of the front gladhand.																0=open,1=connected
			WagSet.front.cock = (float)file.GetCellDouble(carIndex, "Front Angle Cock");            //State of the front angle cock.																0=open,100=closed
			WagSet.front.CouplingSystemType = file.GetCellInt(carIndex, "Front Draft Gear Type");   // 0=friction, 2= spring, 3= Hydraulic EOCC, 4= Articulated(Shared truck)5= Solid drawbar  
			WagSet.front.k1 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k1", 8000000);                      //Front draft gear coefficient.
			WagSet.front.k2 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k2", 80000000);                     //Front draft gear coefficient.
			WagSet.front.k3 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k3", 2600000);                      //Front draft gear coefficient.
			WagSet.front.k4 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k4", 8000000);                      //Front draft gear coefficient.
			WagSet.front.k5 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k5", 80000000);                     //Front draft gear coefficient.
			WagSet.front.k6 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k6", 2600000);                      //Front draft gear coefficient.
			WagSet.front.k8 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k8", 280000000);                        //Front draft gear coefficient.
			WagSet.front.k9 = (float)file.GetCellDouble(carIndex, "Front Draft Gear k9", 280000000);                        //Front draft gear coefficient.
			WagSet.front.slack = (float)file.GetCellDouble(carIndex, "Front Draft Gear Slack", 0.0125);                     //Amount of front draft gear slack.
			WagSet.front.R1 = (float)file.GetCellDouble(carIndex, "Front Draft Gear R1", 100000);                       //Front draft gear coefficient.
			WagSet.front.R2 = (float)file.GetCellDouble(carIndex, "Front Draft Gear R2", 1000000);                      //Front draft gear coefficient.
			WagSet.front.R3 = (float)file.GetCellDouble(carIndex, "Front Draft Gear R3", 100000);                       //Front draft gear coefficient.
			WagSet.front.R8 = (float)file.GetCellDouble(carIndex, "Front Draft Gear R8", 100000);                       //Front draft gear coefficient.
			WagSet.front.f1 = (float)file.GetCellDouble(carIndex, "Front Draft Gear f1", 200000);                       //Front draft gear coefficient.
			WagSet.front.f4 = (float)file.GetCellDouble(carIndex, "Front Draft Gear f4", 200000);                       //Front draft gear coefficient.
			WagSet.front.sDraft = (float)file.GetCellDouble(carIndex, "Front Draft Gear sDraft", 0.065);                            //Front draft gear coefficient.
			WagSet.front.sBuff = (float)file.GetCellDouble(carIndex, "Front Draft Gear sBuff", 0.065);                          //Front draft gear coefficient.

			WagParms.rearTruck = (float)file.GetCellDouble(carIndex, "Rear Truck Offset");
			WagParms.rearCouplerPosition = (float)file.GetCellDouble(carIndex, "Rear Coupler Position");

			WagSet.rear.couplingStrength = (float)file.GetCellDouble(carIndex, "Rear Coupler Strength", 1400000.0);                     //Strength of the rear coupler.
			WagSet.rear.coupler = file.GetCellInt(carIndex, "Rear Coupler State");                      //State of the rear coupler.																	0=open latchable, 1=latched 2=Open-Unlatchable, 3=pulling on Pin, 4=broken
			WagSet.rear.gladhand = file.GetCellInt(carIndex, "Rear Gladhand");                          //State of rear gladhand.																		0=open,1=connected
			WagSet.rear.gladhand = file.GetCellInt(carIndex, "Rear Hose Connection");                   //State of rear gladhand.																		0=open,1=connected
			WagSet.rear.cock = (float)file.GetCellDouble(carIndex, "Rear Angle Cock");                          //State of rear angle cock.																	0=open,100=closed
			WagSet.rear.CouplingSystemType = file.GetCellInt(carIndex, "Rear Draft Gear Type");                             // 0=friction, 2= spring, 3= Hydraulic EOCC, 4= Articulated(Shared truck) 5= Solid drawbar  
			WagSet.rear.k1 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k1", 8000000);                        //Rear draft gear coefficient.
			WagSet.rear.k2 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k2", 80000000);                       //Rear draft gear coefficient.
			WagSet.rear.k3 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k3", 2600000);                        //Rear draft gear coefficient.
			WagSet.rear.k4 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k4", 8000000);                        //Rear draft gear coefficient.
			WagSet.rear.k5 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k5", 80000000);                       //Rear draft gear coefficient.
			WagSet.rear.k6 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k6", 2600000);                        //Rear draft gear coefficient.
			WagSet.rear.k8 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k8", 280000000);                      //Rear draft gear coefficient.
			WagSet.rear.k9 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear k9", 280000000);                      //Rear draft gear coefficient.
			WagSet.rear.slack = (float)file.GetCellDouble(carIndex, "Rear Draft Gear Slack", 0.0125);                       //Amount of slack in rear draft gear.
			WagSet.rear.R1 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear R1", 100000);                     //Rear draft gear coefficient.
			WagSet.rear.R2 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear R2", 1000000);                        //Rear draft gear coefficient.
			WagSet.rear.R3 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear R3", 100000);                     //Rear draft gear coefficient.
			WagSet.rear.R8 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear R8", 100000);                     //Rear draft gear coefficient.
			WagSet.rear.f1 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear f1", 200000);                     //Rear draft gear coefficient.
			WagSet.rear.f4 = (float)file.GetCellDouble(carIndex, "Rear Draft Gear f4", 200000);                     //Rear draft gear coefficient.
			WagSet.rear.sDraft = (float)file.GetCellDouble(carIndex, "Rear Draft Gear sDraft", 0.065);                          //Rear draft gear coefficient.
			WagSet.rear.sBuff = (float)file.GetCellDouble(carIndex, "Rear Draft Gear sBuff", 0.065);                            //Rear draft gear coefficient.
			WagParms.heightCoupling = (float)file.GetCellDouble(carIndex, "Height of Coupling", 1.5);                           //Height of coupling above ground.
			WagSet.retainer = file.GetCellInt(carIndex, "Retainer");                    //State of car retainer.
			WagSet.cutOutValve = file.GetCellInt(carIndex, "Cutout Valve");                             //State of car cutout valve.
			WagParms.dragCoef = (float)file.GetCellDouble(carIndex, "Drag Coefficient", 0.002);                         //Car wind drag coefficient.
			WagSet.loadMass = (float)file.GetCellDouble(carIndex, "Load Weight", 0);                                //Mass of car's cargo.
			WagParms.mass = (float)file.GetCellDouble(carIndex, "Light Weight", 30000);                         //Mass of empty car.
			WagSet.hLoadCOM = (float)file.GetCellDouble(carIndex, "Height of Load COM", 4.0);                           //Load's height of center of mass.
			WagParms.heightCOM = (float)file.GetCellDouble(carIndex, "Height of COM", 3.0);                         //Car's height of center of mass.
			car.carPhysics.actualHeight = (float)file.GetCellDouble(carIndex, "Actual Height", 0.0);                            //????? Actual width
			car.carPhysics.actualWidth = (float)file.GetCellDouble(carIndex, "Actual Width", 0.0);                          //????? Actual height
			WagParms.BPLength = (float)file.GetCellDouble(carIndex, "Brake Pipe Length",
				(WagParms.frontCouplerPosition - WagParms.rearCouplerPosition) * 1.1f);         //Length of car's brake pipe.
			WagParms.BPDiameter = (float)file.GetCellDouble(carIndex, "Brake Pipe Diameter", 0.03175);                          //Diameter of car's brake pipe.
			WagParms.EmerResVol = (float)file.GetCellDouble(carIndex, "Emergency Reservoir Volume", 0.0574f);                           //Volume of emergency brake reservoir.
			car.carPhysics.EmerResPress = (float)file.GetCellDouble(carIndex, "Emergency Reservoir Pressure", 0.0);                           //Pressure in emergency brake reservoir.

			brakePipeFriction[carIndex] = (float)file.GetCellDouble(carIndex, "Brake Pipe Flow Friction", 0.4);
			brakePipeExponent[carIndex] = (float)file.GetCellDouble(carIndex, "Brake Pipe Flow Exponent", 2.0);

			if (file.GetColumnIndex("Auxiliary Reservoir Volume") != -1)
			{
				WagParms.AuxResVol = (float)file.GetCellDouble(carIndex, "Auxiliary Reservoir Volume", 0.0410f);        //Corrected spelling: Volume of auxillary brake reservoir.
			}
			else
			{
				WagParms.AuxResVol = (float)file.GetCellDouble(carIndex, "Auxillary Reservoir Volume", 0.0410f);        //Volume of auxillary brake reservoir.
			}
			if (file.GetColumnIndex("Auxiliary Reservoir Pressure") != -1)
			{
				car.carPhysics.AuxResPress = (float)file.GetCellDouble(carIndex, "Auxiliary Reservoir Pressure", 0.0);                          //Corrected spelling: Pressure in auxillary brake reservoir.
			}
			else
			{
				car.carPhysics.AuxResPress = (float)file.GetCellDouble(carIndex, "Auxillary Reservoir Pressure", 0.0);                          //Pressure in auxillary brake reservoir.
			}

			WagParms.BCylVolume = (float)file.GetCellDouble(carIndex, "Brake Cylinder Volume", 0.306 * WagParms.AuxResVol);         //Volume of  brake cylinder.
			car.carPhysics.BCylPress = (float)file.GetCellDouble(carIndex, "Brake Cylinder Pressure", 0.0);                         //Pressure in brake cylinder.
			WagParms.rLeak = (float)file.GetCellDouble(carIndex, "Leak Rate", 0.0004);//Rate of air leak for this car.
			car.carPhysics.BPPress = (float)file.GetCellDouble(carIndex, "Brake Pipe Pressure", 0.0);//Rate of air leak for this car.
			WagParms.BrakeType = file.GetCellInt(carIndex, "Brake Type", 2);                                //Type of brake pad.
			WagParms.shoeType = (float)file.GetCellDouble(carIndex, "Brake Shoe Type", 1);                              //Type of brake shoe.
			car.carPhysics.useBrakeFade = file.GetCellInt(carIndex, "Brake Fade", 0);

			if (file.GetColumnIndex("Hand Brake Set") != -1)
			{
				WagSet.handbrake = (int)file.GetCellDouble(carIndex, "Hand Brake Set", 0);                              //Corrected spelling: State of car's handbrake.
			}
			else
			{
				WagSet.handbrake = (int)file.GetCellDouble(carIndex, "Hand Brakes Set", 0);                             //State of car's handbrake.
			}
			//Special Script(s) is at top
			WagParms.bearingType = file.GetCellInt(carIndex, "Bearing Type", 0);
			car.carPhysics.speedRestrictions = (float)file.GetCellDouble(carIndex, "Speed Restrictions", 0);                            //???? Speed Restrictions
			car.carPhysics.fuelType = file.GetCellInt(carIndex, "Fuel Type", 0);                            //???? Fuel Type
			car.carPhysics.fuelTankVolume = (float)file.GetCellDouble(carIndex, "Fuel Tank Volume", 0);                         //???? Fuel Tank Volume
			car.carPhysics.fuelVolume = (float)file.GetCellDouble(carIndex, "Fuel Volume", 0);                          //???? Fuel Volume
			car.carPhysics.waterTankVolume = (float)file.GetCellDouble(carIndex, "Water Tank Volume", 0);                           //???? Water Tank Volume
			car.carPhysics.waterVolume = (float)file.GetCellDouble(carIndex, "Water Volume", 0);                            //???? Water Volume
			car.carPhysics.sandTankVolume = (float)file.GetCellDouble(carIndex, "Sand Tank Volume", 0);                         //???? Sand Tank Volume
			car.carPhysics.sandVolume = (float)file.GetCellDouble(carIndex, "Sand Volume", 0);                          //???? Sand Volume
			car.carPhysics.peopleAmount = file.GetCellInt(carIndex, "People Amount", 0);                            //???? People Amount
			car.carPhysics.axleMass = (float)file.GetCellDouble(carIndex, "Mass of axle", 50);                      // Mass of axle with two wheels
			car.carPhysics.wheelRadius = (float)file.GetCellDouble(carIndex, "Radius of wheel", 1);                     // Wheel radius
			if (file.GetColumnIndex("Diameter of Wheel") != -1)
			{
				float diameter = 0.0f;
				diameter = (float)file.GetCellDouble(carIndex, "Diameter of Wheel", car.carPhysics.wheelRadius * 2.0f);
				car.carPhysics.wheelRadius = diameter / 2;
			}
			car.carPhysics.momentOfInertia = (float)file.GetCellDouble(carIndex, "Moment of inertia", 0.75);                    // Moment of inertia of an axle
			car.carPhysics.axleCount = file.GetCellInt(carIndex, "Number of Axles", 6);                     // Number of axles
																											//EV 23 July 2007
			WagParms.NormalBrakeShoeForceAt50PSI = (float)file.GetCellDouble(carIndex, "Normal Brake Shoe Force @ 50 PSI", -99);
			WagParms.LoadEmptyBrakeThreshold = (float)file.GetCellDouble(carIndex, "Load_Empty Brake Threshold", 10000);
			WagParms.LoadEmptyProportioningFactor = (float)file.GetCellDouble(carIndex, "Load/Empty Proportioning Factor", 0.6);
			// This field is specific to clamp car types of simulation. It is undocumented and unsupported 
			// in consist builder and DSEditor
			car.carPhysics.RailCarryingMode = file.GetCellInt(carIndex, "RailCarryingMode", 0);                     // 0:Not Applicable, -1:roller car, 1 ClampCar
																													// if (car.carPhysics.RailCarryingMode != 0) RailCarryingCarCount++;
																													// End Clampcar specific code

			if ((WagParms.iType == 2) || (WagParms.iType == 0))
			{
				//overide defaults for locos
				if (WagParms.NormalBrakeShoeForceAt50PSI == -99) //if default used, so no override value was read
				{
					WagParms.NormalBrakeShoeForceAt50PSI = (WagSet.loadMass + WagParms.mass) * 9.81f * 0.32f;
				}
				// Load/Empty doesn't apply to locos, Cylinder Pressure calcs are done in DE
				WagParms.LoadEmptyBrakeThreshold = -99;
				WagParms.LoadEmptyProportioningFactor = 1;

			}
			else
			{
				if (WagParms.NormalBrakeShoeForceAt50PSI == -99) //if default used, so no override value was read
				{
					WagParms.NormalBrakeShoeForceAt50PSI = (286000f / 2.204f) * 9.81f * 0.065f;
					if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Automobiles") == 0))
					{
						WagParms.NormalBrakeShoeForceAt50PSI = (180000f / 2.204f) * 9.81f * 0.065f;
					}
				}

				if ((WagParms.LoadEmptyBrakeThreshold <= 1) ||
				(WagParms.LoadEmptyBrakeThreshold >= 999999))
				{
					WagParms.LoadEmptyBrakeThreshold = -99; // Will always be perceived as Loaded
				}

				if (WagParms.LoadEmptyProportioningFactor > 1.1)
				{
					WagParms.LoadEmptyProportioningFactor = WagParms.LoadEmptyProportioningFactor / 100;
				}
			}
			WagParms.frontCouplerLength = (float)car.frontCouplerLength;
			WagParms.rearCouplerLength = (float)car.rearCouplerLength;

			// Divide value by 50 psi
			car.carPhysics.PowerEFS = (float)file.GetCellDouble(carIndex, "Power EFS", 100);
			car.carPhysics.BrakeEFS = (float)file.GetCellDouble(carIndex, "Brake EFS", 100);
			car.carPhysics.AdhesionEFS = (float)file.GetCellDouble(carIndex, "Adhesion EFS", 100);

			// if ( car.carPhysics.PowerEFS <= 0.0001 ) car.carPhysics.PowerEFS = 	1;
			// if (car.carPhysics.BrakeEFS <= 0.0001  ) car.carPhysics.BrakeEFS = 1;	
			// if ( car.carPhysics.AdhesionEFS <= 0.0001  ) car.carPhysics.AdhesionEFS = 1;	

			// Values entered are always %, resize to 0.01-1.0 range
			car.carPhysics.PowerEFS = car.carPhysics.PowerEFS / 100.0f;
			car.carPhysics.BrakeEFS = car.carPhysics.BrakeEFS / 100.0f;
			car.carPhysics.AdhesionEFS = car.carPhysics.AdhesionEFS / 100.0f;

			WagParms.BPVolume = 3.1415f * WagParms.BPDiameter * WagParms.BPDiameter * WagParms.BPLength;
			// Formula should use pi times radius squared, not pi times diameter squared, but I am 
			// reluctant to change it, since the current value for BP volume has been used for a long 
			// time and the resulting air brake behavior has been validated. It might be good to obtain 
			// brake pipe pressure data from a real train and verify charging and application rates.

			if (WagParms.shoeType != 1) // Cast Iron
			{
				WagParms.shoeType = 1;
			}

			if (WagParms.shoeType == 0) // Cast Iron
			{
				WagParms.BrakeShoeFrictionFactor_MedApp_A = -0.349025928643463f;
				WagParms.BrakeShoeFrictionFactor_MedApp_B = 0.804537055971997f;
				WagParms.BrakeShoeFrictionFactor_MedApp_C = 0.353211591005462f;

				WagParms.BrakeShoeFrictionFactor_HighApp_A = -0.378652065538902f;
				WagParms.BrakeShoeFrictionFactor_HighApp_B = 0.728674900934418f;
				WagParms.BrakeShoeFrictionFactor_HighApp_C = 0.380215057519041f;
			}
			else // Only High Friction != 0 for now
			{
				WagParms.BrakeShoeFrictionFactor_MedApp_A = -0.197329639427947f;
				WagParms.BrakeShoeFrictionFactor_MedApp_B = 0.891548386051978f;
				WagParms.BrakeShoeFrictionFactor_MedApp_C = 0.197895516381018f;

				WagParms.BrakeShoeFrictionFactor_HighApp_A = -0.234328732438917f;
				WagParms.BrakeShoeFrictionFactor_HighApp_B = 0.881726837340723f;
				WagParms.BrakeShoeFrictionFactor_HighApp_C = 0.236856947048671f;
			}

			WagSet.front.couplingStrength = 999999999;
			WagSet.rear.couplingStrength = 999999999;

			WagSet.rear.DraftGear_DraftSlack = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftSlack", 0.025); //= 0.025;
			WagSet.rear.DraftGear_DraftX1 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftX1", 0.00381); //= 0.0025;
			WagSet.rear.DraftGear_DraftY1 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftY1", 133446.66);   //= 200000;
			WagSet.rear.DraftGear_DraftX2 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftX2", 0.01905); //= 0.0125;
			WagSet.rear.DraftGear_DraftY2 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftY2", 667233.3);    //= 600000;
			WagSet.rear.DraftGear_DraftX3 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftX3", 0.08001); //= 0.0875;
			WagSet.rear.DraftGear_DraftY3 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftY3", 1779288.8);   //= 2200000;
			WagSet.rear.DraftGear_DraftX4 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftX4", 0.0762);  //= 0.085;
			WagSet.rear.DraftGear_DraftY4 = (float)file.GetCellDouble(carIndex, "Rear DraftGear DraftY4", 333616.65);   //= 1600000;

			WagSet.rear.DraftGear_BuffSlack = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffSlack", 0.025);   //= 0.025;
			WagSet.rear.DraftGear_BuffX1 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffX1", 0.00381);   //= 0.0025;
			WagSet.rear.DraftGear_BuffY1 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffY1", 133446.66); //= 200000;
			WagSet.rear.DraftGear_BuffX2 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffX2", 0.01905);   //= 0.0125;
			WagSet.rear.DraftGear_BuffY2 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffY2", 667233.3);  //= 600000;
			WagSet.rear.DraftGear_BuffX3 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffX3", 0.08001);   //= 0.075;
			WagSet.rear.DraftGear_BuffY3 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffY3", 1779288.8); //= 1600000;
			WagSet.rear.DraftGear_BuffX4 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffX4", 0.0762);    //= 0.07;
			WagSet.rear.DraftGear_BuffY4 = (float)file.GetCellDouble(carIndex, "Rear DraftGear BuffY4", 333616.65); //= 600000;

			WagSet.rear.DraftGear_ViscosityFactor = (float)file.GetCellDouble(carIndex, "Rear DraftGear ViscosityFactor", 20000000.0);  //= 10000000.0;
			WagSet.rear.DraftGear_ViscosityMaxForce = (float)file.GetCellDouble(carIndex, "Rear DraftGear ViscosityMaxForce", 25000);   //= 25000.0;


			WagSet.front.DraftGear_DraftSlack = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftSlack", 0.025);   //= 0.025;
			WagSet.front.DraftGear_DraftX1 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftX1", 0.00381);   //= 0.0025;
			WagSet.front.DraftGear_DraftY1 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftY1", 133446.66); //= 200000;
			WagSet.front.DraftGear_DraftX2 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftX2", 0.01905);   //= 0.0125;
			WagSet.front.DraftGear_DraftY2 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftY2", 667233.3);  //= 600000;
			WagSet.front.DraftGear_DraftX3 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftX3", 0.08001);   //= 0.0875;
			WagSet.front.DraftGear_DraftY3 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftY3", 1779288.8); //= 2200000;
			WagSet.front.DraftGear_DraftX4 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftX4", 0.0762);    //= 0.085;
			WagSet.front.DraftGear_DraftY4 = (float)file.GetCellDouble(carIndex, "Front DraftGear DraftY4", 333616.65); //= 1600000;

			WagSet.front.DraftGear_BuffSlack = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffSlack", 0.025); //= 0.025;
			WagSet.front.DraftGear_BuffX1 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffX1", 0.00381); //= 0.0025;
			WagSet.front.DraftGear_BuffY1 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffY1", 133446.66);   //= 200000;
			WagSet.front.DraftGear_BuffX2 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffX2", 0.01905); //= 0.0125;
			WagSet.front.DraftGear_BuffY2 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffY2", 667233.3);    //= 600000;
			WagSet.front.DraftGear_BuffX3 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffX3", 0.08001); //= 0.075;
			WagSet.front.DraftGear_BuffY3 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffY3", 1779288.8);   //= 1600000;
			WagSet.front.DraftGear_BuffX4 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffX4", 0.0762);  //= 0.07;
			WagSet.front.DraftGear_BuffY4 = (float)file.GetCellDouble(carIndex, "Front DraftGear BuffY4", 333616.65);   //= 600000;

			WagSet.front.DraftGear_ViscosityFactor = (float)file.GetCellDouble(carIndex, "Front DraftGear ViscosityFactor", 20000000.0);    //= 10000000.0;

			WagSet.front.DraftGear_ViscosityMaxForce = (float)file.GetCellDouble(carIndex, "Front DraftGear ViscosityMaxForce", 25000); //= 25000.0;


			// Specific Hydraulic parameters					//Rear draft gear coefficient.
			// Rear
			WagSet.rear.PistonDiameter = (float)file.GetCellDouble(carIndex, "Rear Piston Diameter", 0.2032);
			WagSet.rear.PistonRodDiameter = (float)file.GetCellDouble(carIndex, "Rear Rod Diameter", 0.0762);

			WagSet.rear.HydraulicFluidFactor = (float)file.GetCellDouble(carIndex, "Rear Hydraulic Fluid Factor", 62500);    //62500
			WagSet.rear.TotalOrificeArea_Buff = (float)file.GetCellDouble(carIndex, "Rear Total Orifice Area Buff", 0.0032258);  //0.0032258
			WagSet.rear.TotalOrificeArea_Draft = (float)file.GetCellDouble(carIndex, "Rear Total Orifice Area Draft", 0.0032258); //0.0032258	

			WagSet.rear.TotalOrificeArea_ReturnBuff = (float)file.GetCellDouble(carIndex, "Rear Total Orifice Area ReturnBuff", 0.0032258); //0.0032258
			WagSet.rear.TotalOrificeArea_ReturnDraft = (float)file.GetCellDouble(carIndex, "Rear Total Orifice Area ReturnDraft", 0.0032258); //0.0032258

			WagSet.rear.MaxTravelBuff = (float)file.GetCellDouble(carIndex, "Rear Max Travel Buff", 0.35560);
			WagSet.rear.MaxTravelDraft = (float)file.GetCellDouble(carIndex, "Rear Max Travel Draft", 0.35560);

			WagSet.rear.PreloadMin_Buff = (float)file.GetCellDouble(carIndex, "Rear Preload Min Buff", 444822.0);
			WagSet.rear.PreloadMax_Buff = (float)file.GetCellDouble(carIndex, "Rear Preload Max Buff", 889644.0);

			WagSet.rear.PreloadMin_Draft = (float)file.GetCellDouble(carIndex, "Rear Preload Min Draft", 444822.0);
			WagSet.rear.PreloadMax_Draft = (float)file.GetCellDouble(carIndex, "Rear Preload Max Draft", 889644.0);

			WagSet.rear.kCarBodyStiffness = (float)file.GetCellDouble(carIndex, "Rear kCarBodyStiffness", 2E+8);

			WagSet.rear.BuffMinSpringForce = (float)file.GetCellDouble(carIndex, "Rear Buff Min Spring Force", 2669.0);
			WagSet.rear.k_BuffSpring = (float)file.GetCellDouble(carIndex, "Rear k_BuffSpring", 2335.0);

			WagSet.rear.DraftMinSpringForce = (float)file.GetCellDouble(carIndex, "Rear Draft Min Spring Force", 2669.0);
			WagSet.rear.k_DraftSpring = (float)file.GetCellDouble(carIndex, "Rear k_DraftSpring", 2335.0);


			// Front
			WagSet.front.PistonDiameter = (float)file.GetCellDouble(carIndex, "Front Piston Diameter", 0.2032);
			WagSet.front.PistonRodDiameter = (float)file.GetCellDouble(carIndex, "Front Rod Diameter", 0.0762);
			WagSet.front.HydraulicFluidFactor = (float)file.GetCellDouble(carIndex, "Front Hydraulic Fluid Factor", 62500);   //62500
			WagSet.front.TotalOrificeArea_Buff = (float)file.GetCellDouble(carIndex, "Front Total Orifice Area Buff", 0.0032258);   //0.0032258
			WagSet.front.TotalOrificeArea_Draft = (float)file.GetCellDouble(carIndex, "Front Total Orifice Area Draft", 0.0032258);  //0.0032258
			WagSet.front.TotalOrificeArea_ReturnBuff = (float)file.GetCellDouble(carIndex, "Front Total Orifice Area ReturnBuff", 0.0032258); //0.0032258	
			WagSet.front.TotalOrificeArea_ReturnDraft = (float)file.GetCellDouble(carIndex, "Front Total Orifice Area ReturnDraft", 0.0032258); //0.0032258

			WagSet.front.MaxTravelBuff = (float)file.GetCellDouble(carIndex, "Front Max Travel Buff", 0.35560);
			WagSet.front.MaxTravelDraft = (float)file.GetCellDouble(carIndex, "Front Max Travel Draft", 0.35560);

			WagSet.front.PreloadMin_Buff = (float)file.GetCellDouble(carIndex, "Front Preload Min Buff", 444822.0);
			WagSet.front.PreloadMax_Buff = (float)file.GetCellDouble(carIndex, "Front Preload Max Buff", 889644.0);

			WagSet.front.PreloadMin_Draft = (float)file.GetCellDouble(carIndex, "Front Preload Min Draft", 444822.0);
			WagSet.front.PreloadMax_Draft = (float)file.GetCellDouble(carIndex, "Front Preload Max Draft", 889644.0);

			WagSet.front.kCarBodyStiffness = (float)file.GetCellDouble(carIndex, "Front kCarBodyStiffness", 2E+8);

			WagSet.front.BuffMinSpringForce = (float)file.GetCellDouble(carIndex, "Front Buff Min Spring Force", 2669.0);
			WagSet.front.k_BuffSpring = (float)file.GetCellDouble(carIndex, "Front k_BuffSpring", 2335.0);

			WagSet.front.DraftMinSpringForce = (float)file.GetCellDouble(carIndex, "Front Draft Min Spring Force", 2669.0);
			WagSet.front.k_DraftSpring = (float)file.GetCellDouble(carIndex, "Front k_DraftSpring", 2335.0);

			WagSet.front.k1 = 40000000;    // 25000000;
			WagSet.front.k2 = 400000000;      // 80000000;
			WagSet.front.k3 = 20000000;   // 2600000;
			WagSet.front.k4 = 40000000;   // 25000000;
			WagSet.front.k5 = 400000000;      // 80000000;
			WagSet.front.k6 = 20000000;   // 2600000;
			WagSet.front.k8 = 280000000;      // 280000000;
			WagSet.front.k9 = 280000000;      // 280000000;
			WagSet.front.slack = 0.0125f;     // 0.0125;
			WagSet.front.R1 = 100000;     // 100000;
			WagSet.front.R2 = 10000000.0f;  // 10000000.0;	
			WagSet.front.R3 = 100000;     // 100000;
			WagSet.front.R8 = 100000;     // 100000;

			WagSet.front.f1 = 200000;     // 200000;
			WagSet.front.f4 = 200000;     // 200000;

			WagSet.front.sDraft = 0.081f;     // 0.081;
			WagSet.front.sBuff = 0.081f;      // 0.081;

			//===============================================================
			WagSet.rear.k1 = WagSet.front.k1;
			WagSet.rear.k2 = WagSet.front.k2;
			WagSet.rear.k3 = WagSet.front.k3;
			WagSet.rear.k4 = WagSet.front.k4;
			WagSet.rear.k5 = WagSet.front.k5;
			WagSet.rear.k6 = WagSet.front.k6;
			WagSet.rear.k8 = WagSet.front.k8;
			WagSet.rear.k9 = WagSet.front.k9;
			WagSet.rear.slack = WagSet.front.slack;
			WagSet.rear.R1 = WagSet.front.R1;
			WagSet.rear.R2 = WagSet.front.R2;
			WagSet.rear.R3 = WagSet.front.R3;
			WagSet.rear.R8 = WagSet.front.R8;

			WagSet.rear.f1 = WagSet.front.f1;
			WagSet.rear.f4 = WagSet.front.f4;

			WagSet.rear.sDraft = WagSet.front.sDraft;
			WagSet.rear.sBuff = WagSet.front.sBuff;

			//Translate pressures.
			car.carPhysics.BCylPress = (0.01f * car.carPhysics.BCylPress + 1.0f) * 1.2f;
			car.carPhysics.AuxResPress = (0.01f * car.carPhysics.AuxResPress + 1.0f) * 1.2f;
			car.carPhysics.EmerResPress = (0.01f * car.carPhysics.EmerResPress + 1.0f) * 1.2f;
			car.carPhysics.BPPress = (0.01f * car.carPhysics.BPPress + 1.0f) * 1.2f;
			car.carPhysics.RollingResC1 = 1.5f;
			car.carPhysics.RollingResC2 = 18.0f;
			car.carPhysics.RollingResC3 = 0.03f;
			car.carPhysics.RollingResC4Lead = 0.0024f;
			car.carPhysics.RollingResC4Follow = 0.0005f;
			car.carPhysics.RollingResC5 = 0.0004f;
			car.carPhysics.nbOfAxles = 4;
			// ECV 20 June 2006: For lack of better data, I'm using a Rotational mass based on 5% of total mass;
			car.carPhysics.RotationalMass = 0.05f * WagParms.mass;
			car.carPhysics.AirResistanceArea = 120.0f;


			// ECV 20 June 2006: AREMA Manual, section on TPCs, is used for values
			if (WagParms.iType == 0 || WagParms.iType == 2) // like Arema Arema freight locomotive

			{
				// ECV 20 June 2006: For lack of better data, I'm using a Rotational mass based on 10% of total mass for locos;
				car.carPhysics.RotationalMass = 0.10f * WagParms.mass;
				car.carPhysics.RollingResC4Follow = 0.00055f;
				car.carPhysics.nbOfAxles = 6;
				car.carPhysics.AirResistanceArea = 160.0f;
			}
			else if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Ballast") == 0)) // like Arema Coal Gondola - loaded
			{
				car.carPhysics.RollingResC4Follow = 0.00042f;
				car.carPhysics.AirResistanceArea = 105.0f;
			}
			else if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Grain") == 0))
			{
				car.carPhysics.RollingResC4Follow = 0.00053f;
				car.carPhysics.AirResistanceArea = 140.0f;
			}
			else if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Diesel") == 0))
			{
				car.carPhysics.RollingResC4Follow = 0.00055f;
				car.carPhysics.AirResistanceArea = 95.0f;
			}
			else if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Gen Freight") == 0))
			{
				car.carPhysics.RollingResC4Follow = 0.00049f;
				car.carPhysics.AirResistanceArea = 140.0f;
			}
			else if ((car.LoadTypeString != null) && (car.LoadTypeString.CompareTo("Automobiles") == 0))
			{
				car.carPhysics.RollingResC4Follow = 0.00071f;
				car.carPhysics.AirResistanceArea = 170.0f;
			}
			else
			{
				//Keep defaults 
				car.carPhysics.AirResistanceArea = 120.0f;
			}

			// Now read from Consist file if available
			car.carPhysics.RollingResC1 = (float)file.GetCellDouble(carIndex, "RRes C1", car.carPhysics.RollingResC1);
			car.carPhysics.RollingResC2 = (float)file.GetCellDouble(carIndex, "RRes C2", car.carPhysics.RollingResC2);
			car.carPhysics.RollingResC3 = (float)file.GetCellDouble(carIndex, "RRes C3", car.carPhysics.RollingResC3);
			car.carPhysics.RollingResC4Lead = (float)file.GetCellDouble(carIndex, "RRes C4Lead", car.carPhysics.RollingResC4Lead);
			car.carPhysics.RollingResC4Follow = (float)file.GetCellDouble(carIndex, "RRes C4Follow", car.carPhysics.RollingResC4Follow);
			car.carPhysics.RollingResC5 = (float)file.GetCellDouble(carIndex, "RRes C5", car.carPhysics.RollingResC5);
			car.carPhysics.nbOfAxles = file.GetCellInt(carIndex, "RRes Axles", car.carPhysics.nbOfAxles);
			car.carPhysics.AirResistanceArea = (float)file.GetCellDouble(carIndex, "RRes AreaFt2", car.carPhysics.AirResistanceArea);
			car.carPhysics.RotationalMass = (float)file.GetCellDouble(carIndex, "RRes RotMass", car.carPhysics.RotationalMass);

			// EV 23 Jule 2007
			// Doing unit Conversion here
			car.carPhysics.RollingResC1 = (float)(car.carPhysics.RollingResC1 * UnitConversions.KG_TO_TONS * UnitConversions.LBF_TO_NEWTON);
			car.carPhysics.RollingResC2 = (float)(car.carPhysics.RollingResC2 * UnitConversions.LBF_TO_NEWTON);
			car.carPhysics.RollingResC3 = (float)(car.carPhysics.RollingResC3 * UnitConversions.MPS_TO_MPH * UnitConversions.KG_TO_TONS * UnitConversions.LBF_TO_NEWTON);
			car.carPhysics.RollingResC4Lead = (float)(car.carPhysics.RollingResC4Lead * UnitConversions.MPS_TO_MPH * UnitConversions.MPS_TO_MPH * UnitConversions.LBF_TO_NEWTON * car.carPhysics.AirResistanceArea);
			car.carPhysics.RollingResC4Follow = (float)(car.carPhysics.RollingResC4Follow * UnitConversions.MPS_TO_MPH * UnitConversions.MPS_TO_MPH * UnitConversions.LBF_TO_NEWTON * car.carPhysics.AirResistanceArea);
			// I incorporated Area into C4 coeffs		  

			car.carPhysics.velocityDesired = (float)file.GetCellDouble(carIndex, "Initial Velocity", 0);

			if (WagParms.BrakeType == 1) // ABD (or AB)
			{
				car.carPhysics.PrelimQuickServiceVolumeChargeRate = 0.06f;
				car.carPhysics.PrelimQuickServiceVolumeExhaustRate = 0.001f;
				car.carPhysics.PrelimQuickServiceReady = 1;

				WagParms.PrelimQuickServiceVolume = WagParms.BPVolume / 50.0f; // a 80 psi pressure increase in the QSVol should be equivalent to about 6.5 psi drop in BPPress
				WagParms.r1 = .003f;
				WagParms.r2 = .003f;
				WagParms.r3 = 0.001f;
				WagParms.r4 = .0015f;
				WagParms.r5 = .01f;
				//WagParms.r6=0.05f;
				//Dec 2009
				WagParms.r6 = 0.25f;
				WagParms.r7 = .0002f;
				WagParms.r8 = .002f;
				WagParms.rs = .0001f;
			}
			else //else if ( WagParms.BrakeType == 2 ) // ABDW	 // Just else for now, so all non 1 brake type will be ABDW   
			{
				if (WagParms.BrakeType > 3)     // 3 is ECP
				{
					WagParms.BrakeType = 2; // Make it 2 by default
				}
				car.carPhysics.PrelimQuickServiceVolumeChargeRate = 0.08f;
				car.carPhysics.PrelimQuickServiceVolumeExhaustRate = 0.001f;
				car.carPhysics.PrelimQuickServiceReady = 1;

				WagParms.PrelimQuickServiceVolume = WagParms.BPVolume / 10.0f; // a 80 psi pressure increase in the QSVol should be equivalent to about 6.5 psi drop in BPPress
				WagParms.r1 = .003f;
				WagParms.r2 = .003f;
				WagParms.r3 = 0.001f;
				WagParms.r4 = .0015f;
				WagParms.r5 = .01f;
				WagParms.r6 = 0.05f;
				//Dec 2009
				WagParms.r6 = 0.25f;
				WagParms.r7 = .0002f;
				WagParms.r8 = .002f;
				WagParms.rs = .0001f;
			}

			//Translate car type.
			if (WagParms.iType == 2)
			{
				WagParms.iType = 1;
				if (consist.controllingCar == -1)
				{
					consist.controllingCar = car.uniqueId;
					WagParms.subType = 22222;
				}
			}
			else if (WagParms.iType == 0)
			{
				WagParms.iType = 1;
			}
			else if (WagParms.iType == 3)
			{
				WagParms.iType = 3;
			}
			else
			{
				WagParms.iType = 0;
			}



			if (WagParms.iType == 1) // loco
			{
				if (DPGroup == 0) // First loco in consist
				{
					car.carPhysics.DPRole = 1; //lrPrimaryLead
					DPGroup++;
				}
				else
				{
					if (prevCar.carPhysics.p.iType == 1)
					{
						car.carPhysics.DPRole = 10 + DPGroup;
					}
					else
					{
						DPGroup++;
						car.carPhysics.DPRole = DPGroup;
					}
				}
			}

			if (allowScripts > 0)
			{
				string plugInArg;
				string tempString;
				int plugInArgIndex;

				//Load the car script if one is specified.
				tempString = file.GetCellString(carIndex, scriptFileIndex);

				// Temporarily, we have added a string argument to pass to the script when loaded
				// the scriptFile field now takes the format:
				// [PlugInName]:[Argument]
				// everything after the first colon is passed as the sParam in the TMTS_MSG_CAR_SCRIPT_LOADED function
				// everything before the first colon is used as the name of the PlugIn.

				plugInArgIndex = tempString.LastIndexOf(':');

				if (plugInArgIndex == -1)
				{
					plugInArg = null;
				}
				else
				{
					plugInArg = tempString.Substring(plugInArgIndex + 1);
					tempString = tempString.Substring(0, plugInArgIndex);
				}
				TrimExtension(ref tempString);
				//If a script file is not called for in the CST file don't load one.
				if (!String.IsNullOrEmpty(tempString))
				{
					if (allowScripts == 1 || (tempString.StartsWith("EoTDevice") && allowScripts == 2))
					{
						AbstractString tempStringAS = new AbstractStringImpl(tempString);
						bool alreadyRegistered = false;

						if (TMTSPlugInsManager.plugInSystem != null)
						{
							TMTSPluginsCS.PlugIn plugIn = TMTSPlugInsManager.plugInSystem.LoadPlugIn(tempStringAS, ref alreadyRegistered);

							car.scriptID = (plugIn == null || plugIn.innerPlugIn == IntPtr.Zero) ? -1 : plugIn.GetId();
							if (plugIn.innerPlugIn == IntPtr.Zero)
								throw new Exception("InnerPlugIn is NULL");

							if (-1 != car.scriptID)
							{
								TMTSPlugInsManager.PostMessage(car.scriptID, ScriptConstants.TMTS_MSG_CAR_SCRIPT_LOADED, 0, car.uniqueId, 0.0, plugInArg, new int[] { consist.uniqueId });
							}
						}
					}
				}

			}

			// LoadCarViewData(pCar);		// Load named views from LS file in: pCar->modelFile

			car.frontTruckOffset = WagParms.frontCouplerPosition - car.frontCouplerLength - WagParms.frontTruck;
			car.rearTruckOffset = car.frontTruckOffset - WagParms.rearTruck + WagParms.frontTruck;//jhh 2-1-06
			car.originOffset = WagParms.frontCouplerPosition - car.frontCouplerLength;

			WagParms.length = (WagParms.frontCouplerPosition - WagParms.rearCouplerPosition);//jhh 2-1-06
			car.length = WagParms.length;
			car.length -= (car.frontCouplerLength + car.rearCouplerLength);

			car.carPhysics.WagSet = WagSet;
			car.carPhysics.p = WagParms;

			car.consist = consist;
			car.previous = prevCar;

			if (prevCar != null)
			{
				prevCar.next = car;

				car.carPhysics.previousLinked = prevCar.carPhysics;
				prevCar.carPhysics.nextLinked = car.carPhysics;
			}

			prevCar = car;
		} // end of Cst file reading loop

		consist.lastCar = car;

		if (car != null)
		{
			car.next = null;
			consist.firstCar.previous = null;
		}

		consist.consistPhysics.SetLOD(1);
		consist.consistPhysics.Initialize();        // Also initializes consistPhysics.pwag, if consistPhysics.V1 has been set.

		car = consist.firstCar;
		for (int i = 0; i < consist.consistPhysics.nWag - 1; i++)       // Originally in TMTSInitializeConsist
		{
			if (car.next != null)
			{
				car.carInteraction = consist.consistPhysics.GetInteraction(i);
				// Now that car interaction instances actually exist, we can initialize car interaction parameters that loaded from the consist file...
				car.carInteraction.F = brakePipeFriction[i];
				car.carInteraction.flowExponent = brakePipeExponent[i];
			}
			car = car.next;
		}
		// Note: last car does not have a CarInteraction instance.

		consist.consistPhysics.WindVelocity = 0.0f;
		consist.consistPhysics.position = 0.0f;

		AddConsist(consist);

		return consist;
	}

	static int SaveCarType(int iType, int controllingCar, int carId)
	{
		if (iType == 1)
		{
			if (carId == controllingCar)
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
		else if (iType == 0)
		{
			return 1;
		}
		return iType;
	}

	private static void TrimExtension(ref String fileName)
	{
		int dotPos;

		dotPos = fileName.LastIndexOf('.');
		if (dotPos != -1)
		{
			fileName = fileName.Substring(0, dotPos);
		}
	}

	public static void AddConsist(Consist consist)
	{
		AllConsists.Add(consist);
	}

	public static void RemoveConsist(Consist consist)
	{
		AllConsists.Remove(consist);
	}

	public static void ReplaceConsist(Consist oldConsist, Consist newConsist)
	{
		int index = AllConsists.IndexOf(oldConsist);
		if (index != -1)
		{
			AllConsists[index] = newConsist;
		}
	}

	public static Consist GetConsistHead(int index)
	{
		if (index < 0 || index >= AllConsists.Count)
		{
			return null;
		}

		return AllConsists[index];

	}

	public static int GetConsistCount()
	{
		return AllConsists.Count;
	}

	public static int GetTotalCarCount()
	{
		int totalCars = 0;
		foreach (Consist consist in AllConsists)
		{
			for (Car car = consist.firstCar; car != null; car = car.next)
			{
				totalCars++;
			}
		}

		return totalCars;
	}

	public static void ResetSpeedLimit()
	{
		foreach (Consist consist in AllConsists)
		{
			for (Car car = consist.firstCar; car != null; car = car.next)
			{
				car.frontTruckLocation.ResetSpeedLimit();
				car.rearTruckLocation.ResetSpeedLimit();
			}
		}
	}

	public int GetEngineHandle()
	{
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.carPhysics.p.iType != 0)
			{
				return car.uniqueId;
			}
		}

		return -1;
	}

	public float GetLengthMeters(bool includeLocos)
	{
		float length = 0.0f;
		for (Car c = firstCar; c != null; c = c.next)
		{
			if (includeLocos || c.carPhysics.p.iType != 1)
				length += c.carPhysics.p.length;
		}
		return length;
	}

	public double GetActiveLengthMeters(bool useTruePosition)
	{
		double activeLength = 0.0f;
		for (Car car = firstCar; car != null; car = car.next)
		{
			activeLength += car.length;
			activeLength += car.CalculateCouplerLength(true, useTruePosition);
			activeLength += car.CalculateCouplerLength(false, useTruePosition);
		}
		return activeLength;
	}

	public double GetApproximateLength()
	{
		if (firstCar == null || lastCar == null)
		{
			return 0.0;
		}

		return (double)(firstCar.carPhysics.metersPosition - lastCar.carPhysics.metersPosition) + (firstCar.carPhysics.position - lastCar.carPhysics.position);
	}

	public float GetWeightKilograms()
	{
		float weight = 0.0f;
		for (Car c = firstCar; c != null; c = c.next)
		{
			weight += (c.carPhysics.WagSet.loadMass + c.carPhysics.p.mass);
		}
		return weight;
	}
	//SIM-11085
	public float GetCarWeightKilograms()
	{
		float weight = 0.0f;
		for (Car c = firstCar; c != null; c = c.next)
		{
			if (c.carPhysics.p.iType == 0)
			{
				weight += (c.carPhysics.WagSet.loadMass + c.carPhysics.p.mass);
			}
		}
		return weight;
	}

	public string GetCarList(bool includeLocos)
	{
		string carList = string.Empty;

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar != null)
		{
			bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
			Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

			while (car != null)
			{
				if (includeLocos || car.carPhysics.p.iType == 0)
				{
					carList += car.vehNum;
				}

				car = (controlCarForward ? car.previous : car.next);

				if (car != null && (includeLocos || car.carPhysics.p.iType == 0) && carList != string.Empty)
				{
					carList += ",";
				}
			}
		}

		return carList;
	}

	public List<Car> GetLocoList()
	{
		List<Car> carList = new List<Car>();

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar != null)
		{
			bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
			Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

			while (car != null)
			{
				if (car.carPhysics.p.iType != 0)
				{
					carList.Add(car);
				}

				car = (controlCarForward ? car.previous : car.next);

			}
		}

		return carList;
	}

	public void GetEndCutTrackLocations(out TrackLocation frontEndCutTrackLocation, out TrackLocation rearEndCutTruckLocation, double offsetMeters)
	{
		bool firstCarForward = (1 == firstCar.carPhysics.WagSet.direction);
		frontEndCutTrackLocation = TrackLocation.New(firstCarForward ? firstCar.frontTruckLocation : firstCar.rearTruckLocation);
		double moveMeters = (offsetMeters + firstCar.CalculateCouplerLength(firstCarForward, false)) * (firstCarForward ? 1.0 : -1.0);
		frontEndCutTrackLocation.MoveDistance(moveMeters);

		bool lastCarForward = (1 == lastCar.carPhysics.WagSet.direction);
		rearEndCutTruckLocation = TrackLocation.New(!lastCarForward ? lastCar.frontTruckLocation : lastCar.rearTruckLocation);
		moveMeters = (offsetMeters + lastCar.CalculateCouplerLength(!lastCarForward, false)) * (!lastCarForward && (lastCarForward == firstCarForward) ? 1.0 : -1.0);
		rearEndCutTruckLocation.MoveDistance(moveMeters);
	}

	private double[,] prevForces; //i,0 - buff and i,1 - draft
	public void GetGreatestBuffDraftForcesChangeKlbs(out double greatestBuffChange, out double greatestDraftChange)
	{
		greatestBuffChange = 0.0f;
		greatestDraftChange = 0.0f;

		int carCount = CountCars();
		if (carCount == 0)
		{
			return;
		}

		if (prevForces == null || prevForces.GetLength(0) != carCount)
		{
			prevForces = new double[carCount, 2];
			for (int i = 0; i < carCount; i++)
			{
				prevForces[i, 0] = 0.0;
				prevForces[i, 1] = 0.0;
			}
		}

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar == null)
		{
			return;
		}

		bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
		Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

		int prevForcesIter = 0;
		while (car != null && prevForcesIter < prevForces.GetLength(0))
		{
			bool carForward = 1 == car.carPhysics.WagSet.direction;
			double force = 0.0;

			Car nextCar = (controlCarForward ? car.previous : car.next);
			if (null != nextCar)
			{
				force = (carForward ? car.carPhysics.tensionLimit1 : car.carPhysics.tensionLimit2) / UnitConversions.KLBS_TO_NEWTON;
				if (force > 0.0)
				{
					double draftChange = force - prevForces[prevForcesIter, 1];
					greatestDraftChange = Math.Max(greatestDraftChange, draftChange);
					prevForces[prevForcesIter, 1] = force;
				}
				else if (force < 0.0)
				{
					double buffChange = force - prevForces[prevForcesIter, 0];
					greatestBuffChange = Math.Min(greatestBuffChange, buffChange);
					prevForces[prevForcesIter, 0] = force;
				}

				prevForcesIter++;
			}

			car = nextCar;
		}
	}

	public void GetGreatestBuffDraftForces(out float greatestBuffForce, out float greatestDraftForce)
	{
		greatestBuffForce = 0.0f;
		greatestDraftForce = 0.0f;

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar != null)
		{
			bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
			Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

			while (car != null)
			{
				bool carForward = 1 == car.carPhysics.WagSet.direction;
				float force = 0.0f;

				Car nextCar = (controlCarForward ? car.previous : car.next);
				if (null != nextCar)
				{
					force = (carForward ? car.carPhysics.tensionLimit1 : car.carPhysics.tensionLimit2) / (float)UnitConversions.KLBS_TO_NEWTON;
					greatestBuffForce = Math.Min(force, greatestBuffForce);
					greatestDraftForce = Math.Max(force, greatestDraftForce);
				}

				car = nextCar;
			}
		}
	}

	public float GetGreatestSpeedMph()
	{
		float greatestSpeed = 0.0f;

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar != null)
		{
			bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
			Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

			while (car != null)
			{
				float speed = car.carPhysics.velocity * (float)UnitConversions.MPS_TO_MPH;
				greatestSpeed = Math.Max(speed, greatestSpeed);

				car = (controlCarForward ? car.previous : car.next);
			}
		}

		return greatestSpeed;
	}

	public float GetAverageSpeedMps()
	{
		float sumSpeed = 0.0f;
		int count = 0;
		for (Car car = firstCar; null != car; car = car.next)
		{
			sumSpeed += Math.Abs(car.carPhysics.velocity);
			count++;
		}
		return (sumSpeed /= count);
	}

	public bool IsDistributedPowerConsist()
	{
		bool isDP = false;
		int lastLocoIndex = 0;
		int i = 0;

		for (Car car = firstCar; null != car; car = car.next)
		{
			i++;
			if (car.carPhysics.p.iType == 1)
			{
				if (i - lastLocoIndex > 1)
				{
					isDP = true;
					break;
				}

				lastLocoIndex = i;
			}
		}

		return isDP;
	}

	public bool IsProperlyLaced()
	{
		bool properlyLaced = true;

		Car controlCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (controlCar != null)
		{
			bool controlCarForward = 1 == controlCar.carPhysics.WagSet.direction;
			Car car = (controlCarForward ? controlCar.consist.lastCar : controlCar.consist.firstCar);

			while (car != null)
			{
				CarPhysics.WagonSetup wagonSetup = car.carPhysics.WagSet;
				float totalCock = wagonSetup.front.cock + wagonSetup.rear.cock;
				int totalGladhand = wagonSetup.front.gladhand + wagonSetup.rear.gladhand;

				if (IsCarFirstInConsist(car) || IsCarLastInConsist(car))
				{
					properlyLaced &= (totalCock == 100.0 && totalGladhand == 1);
				}
				else
				{
					properlyLaced &= (totalCock == 0.0 && totalGladhand == 2);
				}

				car = (controlCarForward ? car.previous : car.next);
			}
		}

		return properlyLaced;
	}

	public static bool IsCarFirstInConsist(Car car)
	{
		return (car.uniqueId == car.consist.firstCar.uniqueId);
	}

	public static bool IsCarLastInConsist(Car car)
	{
		return (car.uniqueId == car.consist.lastCar.uniqueId);
	}

	public static int GetVehicleIDFromVehicleNumber(string vehicleNumber)
	{
		int index = 0;

		for (Consist consist = GetConsistHead(index); consist != null; index++, consist = GetConsistHead(index))
		{
			for (Car car = consist.firstCar; car != null; car = car.next)
			{
				if (vehicleNumber == null)
				{
					return car.uniqueId;
				}
				else if (car.vehNum.CompareTo(vehicleNumber) == 0)
				{
					return car.uniqueId;
				}
			}
		}

		return -1;
	}

	/**
	* Returns true if the specified consist has matching numbers. Blocks of cars are delineated by
	* empty strings in the numbers vector. Returns false if blocks appear out of order, cars are
	* missing from blocks, or there are a different number of cars than those specified. Order of
	* cars within each block does not matter.
	*/
	public static int ConsistHasCars(Consist consistHead, List<string> carNums, bool forward)
	{
		int blockSize = 0;
		int totalCarsRequired = 0;
		Car startOfBlock = forward ? consistHead.firstCar : consistHead.lastCar;
		int maxCountFromStartOfBlock = 0;
		Car furthestCarFromStartOfBlock = startOfBlock;

		foreach (string carNum in carNums)
		{
			if (string.Empty == carNum)
			{
				// start of a new block
				if (blockSize != maxCountFromStartOfBlock)
				{
					// mismatched block sizes
					return 0;
				}
				startOfBlock = forward ? furthestCarFromStartOfBlock.next : furthestCarFromStartOfBlock.previous;
				maxCountFromStartOfBlock = 0;
				furthestCarFromStartOfBlock = startOfBlock;
				blockSize = 0;
			}
			else
			{
				int count = 1;
				Car car = null;
				for (car = forward ? startOfBlock : startOfBlock;
					(car != null) && (car.vehNum != carNum);
					count++, car = forward ? car.next : car.previous) { }
				if (null == car)
				{
					return 0;
				}
				if (count > maxCountFromStartOfBlock)
				{
					maxCountFromStartOfBlock = count;
					furthestCarFromStartOfBlock = car;
				}
				totalCarsRequired++;
				blockSize++;
			}
		}
		if (totalCarsRequired != consistHead.CountCars())
		{
			return 0;
		}
		return totalCarsRequired;
	}

	public static Consist FindConsistWithCars(List<string> carNums, bool allowSubsetOfConsist)
	{
		foreach (Consist consist in AllConsists)
		{
			int consistCount = consist.CountCars();
			int matchesCount = ConsistHasCars(consist, carNums, true);
			bool returnThisConsist = ((allowSubsetOfConsist) ? (0 < matchesCount) : (consistCount == matchesCount));
			if (returnThisConsist)
			{
				return consist;
			}
			matchesCount = ConsistHasCars(consist, carNums, false);
			returnThisConsist = ((allowSubsetOfConsist) ? (0 < matchesCount) : (consistCount == matchesCount));
			if (returnThisConsist)
			{
				return consist;
			}
		}

		return null;
	}

	public bool ContainsCar(int carUniqueId)
	{
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.uniqueId == carUniqueId)
			{
				return true;
			}
		}

		return false;
	}

	public int CountCars()
	{
		return consistPhysics.nWag;
	}

	public int CountLoadedCars()
	{
		int loadedCarCount = 0;
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.carPhysics.WagSet.loadMass > 0)
			{
				loadedCarCount++;
			}
		}
		return loadedCarCount;
	}

	//SIM-11085
	public int CountOnlyLoadedCars()
	{
		int loadedCarCount = 0;
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.carPhysics.WagSet.loadMass > 0 && car.carPhysics.p.iType == 0)
			{
				loadedCarCount++;
			}
		}
		return loadedCarCount;
	}

	public int CountEmptyCars()
	{
		int emptyCarCount = 0;
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.carPhysics.WagSet.loadMass == 0)
			{
				emptyCarCount++;
			}
		}
		return emptyCarCount;
	}

	//SIM-11085
	public int CountOnlyEmptyCars()
	{
		int emptyCarCount = 0;
		for (Car car = firstCar; car != null; car = car.next)
		{
			if (car.carPhysics.WagSet.loadMass == 0 && car.carPhysics.p.iType == 0)
			{
				emptyCarCount++;
			}
		}
		return emptyCarCount;
	}

	public int CountLocos()
	{
		int locoCount = 0;
		Car cCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (cCar != null)
		{
			locoCount++;
			bool forward = (1 == cCar.carPhysics.WagSet.direction);
			Car car = forward ? cCar.consist.lastCar : cCar.consist.firstCar;
			while (null != car)
			{
				if (null != (forward ? car.previous : car.next))
				{
					if (car.carPhysics.p.iType == 1)
						locoCount++;
				}
				car = forward ? car.previous : car.next;
			}
		}

		return locoCount;
	}

	public int CountAxles(bool includeLocos)
	{
		int axleCount = 0;
		Car cCar = TmtsMasterIndex.GetEntry(controllingCar) as Car;
		if (cCar != null)
		{
			bool forward = (1 == cCar.carPhysics.WagSet.direction);
			Car car = forward ? cCar.consist.lastCar : cCar.consist.firstCar;
			while (null != car)
			{
				if (null != (forward ? car.previous : car.next) && (includeLocos || car.carPhysics.p.iType != 1))
				{
					axleCount += car.carPhysics.axleCount;
				}
				car = forward ? car.previous : car.next;
			}
		}

		return axleCount;
	}

	public void StretchOrBunchCars(bool stretch)
	{
		double firstCarPosition = firstCar.carPhysics.position + firstCar.carPhysics.metersPosition;
		double currPosition = firstCarPosition;
		double totalSlack = 0.0;

		for (Car car = firstCar; car != null; car = car.next)
		{
			currPosition = firstCarPosition + (car.carPhysics.neutralRelativePosition + (totalSlack * (stretch ? -1 : 1)));

			car.carPhysics.metersPosition = (int)currPosition;
			car.carPhysics.position = (float)(currPosition - (double)car.carPhysics.metersPosition);

			totalSlack += (car.carPhysics.WagSet.direction == 1) ? car.carPhysics.WagSet.front.slack : car.carPhysics.WagSet.rear.slack;
		}

		//Update truck positions
		consistPhysics.SetRelPositions();
		Move(0.0);
	}

	public void SetVelocityMph(float velocity)
	{
		SetVelocityMps(velocity * (float)UnitConversions.MPH_TO_MPS);
	}
	public void SetVelocityMps(float velocity)
	{
		for (Car car = firstCar; car != null; car = car.next)
		{
			car.carPhysics.velocity = velocity;
		}
	}

	int[] ctcMsg = new int[3];
	public void Update()
	{
		Consist consist;

		for (int consistIndex = 0; (consistIndex < AllConsists.Count) && (consist = GetConsistHead(consistIndex)) != null; consistIndex++)
		{
			int carIndex = 0;
			for (Car car = consist.firstCar; car != null; car = car.next)
			{
				if (car.carPhysics.tipping != 0)
				{
					// TODO: more-responsible derail events
					ctcMsg[0] = car.uniqueId;
					ctcMsg[1] = consist.uniqueId;
					ctcMsg[2] = carIndex;
					TMTSPlugInsManager.PostMessage(TMTSPlugInsManager.DefaultActivityPlugInId, ScriptConstants.TMTS_MSG_CTC_EVENT, 0.0, ScriptConstants.CTC_EVENT_PHYSICS_CAR_TIP, 0.0, null, ctcMsg);
				}
				if (car.carPhysics.railRoll != 0)
				{
					// TODO: more-responsible derail events
					ctcMsg[0] = car.uniqueId;
					ctcMsg[1] = consist.uniqueId;
					ctcMsg[2] = carIndex;
					TMTSPlugInsManager.PostMessage(TMTSPlugInsManager.DefaultActivityPlugInId, ScriptConstants.TMTS_MSG_CTC_EVENT, 0.0, ScriptConstants.CTC_EVENT_PHYSICS_RAIL_ROLL, 0.0, null, ctcMsg);
				}
				if (car.carPhysics.derailed != 0)
				{
					// TODO: more-responsible derail events
					TrackLocation trackLocation;
					trackLocation = (car.carPhysics.WagSet.direction == 1) ? car.frontTruckLocation : car.rearTruckLocation;
					ctcMsg[0] = car.uniqueId;
					ctcMsg[1] = consist.uniqueId;
					ctcMsg[2] = trackLocation.GetSplineLocation().GetSpline().uniqueId;
					// = GetPrecedingTrackPoint()->base.id;
					TMTSPlugInsManager.PostMessage(TMTSPlugInsManager.DefaultActivityPlugInId, ScriptConstants.TMTS_MSG_CTC_EVENT, 0.0, ScriptConstants.CTC_EVENT_RAN_OFF_TRACK, 0.0, null, ctcMsg);
				}
				carIndex++;
			}

			consist.lod = consist.consistPhysics.LOD;
		}
	}

	public void EndQuasiPause()
	{
		if (consistPhysics != null)
		{
			if (consistPhysics.QuasiPausedState != 0)
			{
				consistPhysics.QuasiPausedState = 0;
			}
		}
		TMTSPlugInsManager.SendMessage(ScriptConstants.BROADCAST_HANDLE, ScriptConstants.TMTS_MSG_VALUE, TMTSPlugInsManager.MessageConstant("TMTS_MSG_QUASI_PAUSE_ENDED"), 0.0, string.Empty);
	}
}