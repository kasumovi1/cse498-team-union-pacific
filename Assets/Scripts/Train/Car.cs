using System;
using TMTSPluginsCS;

public class Car : MasterIndexEntry
{
	public float previousScale;

	public Consist consist;
	public Car next;
	public Car previous;

	public CarPhysics carPhysics;               // Wrapper for Vehicle in Physics DLL
	public CarInteraction carInteraction;

	public bool loaded;

	public int scriptID;

	public double length;
	public double frontTruckOffset;
	public double rearTruckOffset;
	public double originOffset;
	public double frontCouplerLength;
	public double rearCouplerLength;
	public double r;
	public TrackLocation frontTruckLocation;
	public TrackLocation rearTruckLocation;
	public JoinedTrackLocationPair
							truckLocationPair;      // Front and rear trucks
	public JoinedTrackLocationPair
							joinedTruckLocations;   // Rear truck and truck of next car

	public string vehNum;
	public string path;
	public string ownerName;
	public string conductorName;
	public string conductorScript;
	public string engineerName;
	public string engineerScript;
	public string destination;
	public string zoneCarrier;
	public string block;
	public string carName;
	public string consistDescription;
	public string sign;
	public string previewFile;
	public string version;
	public string nextBlock;
	public string groupNumber;
	public string FrontCouplingFile;
	public string RearCouplingFile;
	public string LoadTypeString;

	public int remoteEquipped;
	public int badOrder;
	public int hazardous;
	public int soundFocusFront;
	public int soundFocusRear;

	public PointXYZ currentPosition;
	public double currentMercatorScale;

	private string frontTruckTrackSectionModelName = string.Empty;
	private float yPositionOffsetTrackSection = 0.0f;


	public static int carFixedUpdateCount = 0;

	public float trackSpeedLimit = 0f;

	public Car() : base(EntryType.Car)
	{
		// Don't instantiate a gameObject here. The gameObject will be instantiated when LoadCarModel executes.

		carPhysics = new CarPhysics();
		carInteraction = null;

		loaded = false;

		scriptID = -1;
		frontTruckLocation = null;
		rearTruckLocation = null;
		truckLocationPair = null;
		vehNum = string.Empty;
		path = string.Empty;
		ownerName = string.Empty;
		conductorName = string.Empty;
		conductorScript = string.Empty;
		engineerName = string.Empty;
		engineerScript = string.Empty;
		destination = string.Empty;
		zoneCarrier = string.Empty;
		LoadTypeString = string.Empty;
		currentPosition = PointXYZ.Zero;
		carPhysics.CarID = uniqueId;
	}

	public override string ToDataDebugString()
	{
		CarPhysics.WagonSetup wagonSetup = carPhysics.WagSet;

		return "Vehicle Number:    " + vehNum
			 + "\nName:              " + carName
			 + "\nPosition:          " + currentPosition.ToString()
			 + "\nBad Order:         " + badOrder
			 + "\nNext:              " + (next != null ? next.vehNum : "null")
			 + "\nPrevious:          " + (previous != null ? previous.vehNum : "null")
			 + "\nLast Car:          " + (uniqueId == consist.lastCar.uniqueId)
			 + "\nFirst Car:         " + (uniqueId == consist.firstCar.uniqueId)
			 + "\nScript Id:         " + scriptID
			 + "\nSelection Id:      " + selectionId
			 + "\nConsist File Name: " + consist.consistFileName
			 + "\nForward:           " + (wagonSetup.direction == 1)
			 + "\nFront Coupler:     " + wagonSetup.front.coupler
			 + "\nRear Coupler:      " + wagonSetup.rear.coupler
			 + "\nHandbrake:         " + wagonSetup.handbrake
			 + "\nFront Gladhand:    " + wagonSetup.front.gladhand
			 + "\nRear Gladhand:     " + wagonSetup.rear.gladhand
			 + "\nFront Angle Cock:  " + wagonSetup.front.cock
			 + "\nRear Angle Cock:   " + wagonSetup.rear.cock;
	}

	public float GetMilepost()
	{
		SplineLocation splineLocation = frontTruckLocation.GetSplineLocation();
		float milepost = (float)UnitConversions.Truncate(splineLocation.GetSourcePosition(), 2);
		SplineLocation.Reuse(splineLocation);
		return milepost;
	}

	public float GetRearMilepost()
	{
		SplineLocation splineLocation = rearTruckLocation.GetSplineLocation();
		float milepost = (float)UnitConversions.Truncate(splineLocation.GetSourcePosition(), 2);
		SplineLocation.Reuse(splineLocation);
		return milepost;
	}

	public int GetIndexInConsist()
	{
		int index = -1;

		if (consist != null)
		{
			for (Car curr = consist.firstCar; curr != null; curr = curr.next)
			{
				index++;
				if (curr == this)
				{
					return index;
				}
			}
		}

		return index;
	}

	public TrackLocation GetTruckLocation(bool towardsConsistFront)
	{
		return GetTruckLocation(towardsConsistFront, carPhysics.WagSet.direction == 1);
	}
	public TrackLocation GetTruckLocation(bool towardsConsistFront, bool directionIsForward)
	{
		return towardsConsistFront == directionIsForward ? frontTruckLocation : rearTruckLocation;
	}

	public void SetTruckLocation(TrackLocation newTruckLocation, bool towardsConsistFront)
	{
		if (towardsConsistFront == (carPhysics.WagSet.direction == 1))
		{
			frontTruckLocation.Assign(newTruckLocation);
		}
		else
		{
			rearTruckLocation.Assign(newTruckLocation);
		}
	}

	public double CalculateCouplerLength(bool front, bool useTruePosition)
	{
		CarPhysics.WagonParameters wagonParameters = carPhysics.p;
		double distance = front ? wagonParameters.frontCouplerPosition - wagonParameters.frontTruck
			: -wagonParameters.rearCouplerPosition + wagonParameters.rearTruck;

		if (useTruePosition)
		{
			distance += (front == (carPhysics.WagSet.direction == 1)) ? (carPhysics.neutralRelativePosition - carPhysics.trueRelativePosition)
				: -(carPhysics.neutralRelativePosition - carPhysics.trueRelativePosition);
		}

		return distance;
	}

	public double CalculateCouplerLength(bool front, bool useTruePosition, bool directionIsForward)
	{
		CarPhysics.WagonParameters wagonParameters = carPhysics.p;
		double distance = front ? wagonParameters.frontCouplerPosition - wagonParameters.frontTruck
			: -wagonParameters.rearCouplerPosition + wagonParameters.rearTruck;

		if (useTruePosition)
		{
			distance += (front == directionIsForward) ? (carPhysics.neutralRelativePosition - carPhysics.trueRelativePosition)
				: -(carPhysics.neutralRelativePosition - carPhysics.trueRelativePosition);
		}

		return distance;
	}

	public PointXYZ CalculateCouplerCoords(bool frontEnd, out PointXYZ unitVectorOut)
	{
		PointXYZ front = GetFrontTruckCoordinate();
		PointXYZ rear = GetRearTruckCoordinate();
		PointXYZ delta;
		PointXYZ result;

		if (frontEnd)
			delta = front - rear;
		else
			delta = rear - front;

		unitVectorOut = delta.GetNormalized();

		if (delta.MagnitudeSquared() == 0.0)
		{
			PointXYZ.Reuse(delta);
			PointXYZ.Reuse(frontEnd ? rear : front);
			return frontEnd ? front : rear;
		}

		double couplerLength = CalculateCouplerLength(frontEnd, false);

		if (frontEnd)
			couplerLength *= Mercator.GetMercatorUnitsPerMeter(front.z);
		else
			couplerLength *= Mercator.GetMercatorUnitsPerMeter(rear.z);

		PointXYZ.Reuse(delta);

		delta = unitVectorOut * couplerLength;

		result = delta + (frontEnd ? front : rear);

		PointXYZ.Reuse(front);      // Minimize garbage collection
		PointXYZ.Reuse(rear);
		PointXYZ.Reuse(delta);

		return result;
	}

	public bool IsHandbrakeSet()
	{
		return carPhysics.WagSet.handbrake > 0;
	}

	public bool IsLocomotive()
	{
		return carPhysics.p.iType == 1;
	}

	public int carPointsIsValidFlag { private get; set; }           //0 = inValid;	1 = Valid;	2 = return false
	public void CalculateNearestTwoCarPointPairs(PointXYZ[] car1Points, PointXYZ[] car2Points, out int[] nearestPointIndexes, out int[] secondNearestPointIndexes)
	{
		if (car1Points == null || car2Points == null || car1Points.Length < 8 || car2Points.Length < 8)
		{
			nearestPointIndexes = null;
			secondNearestPointIndexes = null;
			return;
		}

		nearestPointIndexes = new int[2] { 4, 4 };
		secondNearestPointIndexes = new int[2] { 4, 4 };

		double nearestPointsDistance = double.MaxValue;
		double secondNearestPointsDistance = double.MaxValue;
		for (int i = 4; i < 8; i++)
		{
			for (int j = 4; j < 8; j++)
			{
				double distance = Math.Abs(car1Points[i].Distance(car2Points[j]));
				if (distance < nearestPointsDistance)
				{
					secondNearestPointsDistance = nearestPointsDistance;
					secondNearestPointIndexes[0] = nearestPointIndexes[0];
					secondNearestPointIndexes[1] = nearestPointIndexes[1];
					nearestPointsDistance = distance;
					nearestPointIndexes[0] = i;
					nearestPointIndexes[1] = j;
				}
				else if (distance < secondNearestPointsDistance)
				{
					secondNearestPointsDistance = distance;
					secondNearestPointIndexes[0] = i;
					secondNearestPointIndexes[1] = j;
				}
			}
		}
	}

	public PointXYZ GetTruckCoordinates(bool front)
	{
		return front ? frontTruckLocation.GetCoordinates() : rearTruckLocation.GetCoordinates();
	}

	public PointXYZ GetFrontTruckCoordinate()
	{
		return GetTruckCoordinates(true);
	}
	public PointXYZ GetRearTruckCoordinate()
	{
		return GetTruckCoordinates(false);
	}

	public void GetOrigin(out PointXYZ originOut)
	{
		originOut = PointXYZ.Zero;
		PointXYZ p1 = GetFrontTruckCoordinate();
		PointXYZ p2 = GetRearTruckCoordinate();
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		double dz = p1.z - p2.z;

		originOut.x = p2.x + (0.5 * dx);
		originOut.y = p2.y + (0.5 * dy);
		originOut.z = p2.z + (0.5 * dz);

		PointXYZ.Reuse(p1);     // Minimize garbage collection
		PointXYZ.Reuse(p2);
	}

	public void Uncouple(bool front)
	{
		CarPhysics.WagonSetup wagonSetup = carPhysics.WagSet;
		Uncouple(front, uniqueId, false, front ? wagonSetup.front.coupler : wagonSetup.rear.coupler);
	}
	public static void Uncouple(bool front, int carId, int couplerState)
	{
		Uncouple(front, carId, false, couplerState);
	}
	public static void Uncouple(bool front, int carId, bool fromNetwork, int couplerState)
	{
		int message = front ? ScriptConstants.TMTS_MSG_UNCOUPLE_FRONT : ScriptConstants.TMTS_MSG_UNCOUPLE_REAR;
		int[] vParam = fromNetwork ? new int[1] { 1 } : null;
		TMTSPlugInsManager.PostMessage(TMTSPlugInsManager.DefaultActivityPlugInId, message, 0.0, carId, couplerState, string.Empty, vParam);
	}

	public double CalculateLocalGrade()
	{
		double localGrade = 0.0;
		double gradeSpread = 100.0; // meters
		TrackLocation front = TrackLocation.New(this.frontTruckLocation);
		TrackLocation rear = TrackLocation.New(this.rearTruckLocation);
		double locMove = (gradeSpread - (this.frontTruckOffset - this.rearTruckOffset)) / 2;
		front.MoveDistance(locMove);
		rear.MoveDistance(-locMove);
		PointXYZ rearCoords = GetTruckCoordinates(false);
		PointXYZ gradeVector = GetTruckCoordinates(true);
		gradeVector.Subtract(rearCoords);
		gradeVector.Scale((double)this.carPhysics.WagSet.direction);
		if (gradeVector.Normalize())
		{
			localGrade = gradeVector.y;
		}
		PointXYZ.Reuse(rearCoords);
		PointXYZ.Reuse(gradeVector);
		TrackLocation.Reuse(front);
		TrackLocation.Reuse(rear);
		return localGrade;
	}
}