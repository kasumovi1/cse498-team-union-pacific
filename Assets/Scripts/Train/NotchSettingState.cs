﻿public class NotchSettingState
{
	public double prevTime;
	public double prevSpeed;
	public int iTargetNotchOffset;
	public int prevNotch;

	public NotchSettingState()
	{
		prevTime = -1.0;
		iTargetNotchOffset = 0;
	}
}