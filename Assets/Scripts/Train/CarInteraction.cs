using System;
using System.Runtime.InteropServices;

public class CarInteraction
{
	// Wrapper for class Interaction in Physics DLL

	[DllImport("Physics")]
	private static extern CarPhysics.CarEnd InteractionGetLead(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetLead(IntPtr interaction, ref CarPhysics.CarEnd lead);

	[DllImport("Physics")]
	private static extern CarPhysics.CarEnd InteractionGetFollow(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFollow(IntPtr interaction, ref CarPhysics.CarEnd follow);

	[DllImport("Physics")]
	private static extern void InteractionSetPCon(IntPtr interaction, IntPtr pcon);

	// Generated Dll Imports:

	[DllImport("Physics")]
	private static extern int InteractionGetIndex(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetIndex(IntPtr interaction, int _index);

	[DllImport("Physics")]
	private static extern int InteractionGetConsist(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetConsist(IntPtr interaction, int _consist);

	[DllImport("Physics")]
	private static extern int InteractionGetCouplerMode(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplerMode(IntPtr interaction, int _couplerMode);

	[DllImport("Physics")]
	private static extern int InteractionGetGladhand(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetGladhand(IntPtr interaction, int _gladhand);

	[DllImport("Physics")]
	private static extern int InteractionGetDirection(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDirection(IntPtr interaction, int _direction);

	[DllImport("Physics")]
	private static extern int InteractionGetBrokenflag(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBrokenflag(IntPtr interaction, int _brokenflag);

	[DllImport("Physics")]
	private static extern int InteractionGetPinBreakFlag(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPinBreakFlag(IntPtr interaction, int _pinBreakFlag);

	[DllImport("Physics")]
	public static extern float InteractionGetMaxForce(IntPtr interaction);

	[DllImport("Physics")]
	public static extern void InteractionSetMaxForce(IntPtr interaction, float _maxForce);

	[DllImport("Physics")]
	private static extern float InteractionGetMinForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMinForce(IntPtr interaction, float _minForce);

	[DllImport("Physics")]
	private static extern float InteractionGetDebugDumpMaxAbsForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDebugDumpMaxAbsForce(IntPtr interaction, float _DebugDumpMaxAbsForce);

	[DllImport("Physics")]
	private static extern float InteractionGetFilteringForcesSum(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFilteringForcesSum(IntPtr interaction, float _FilteringForcesSum);

	[DllImport("Physics")]
	private static extern double InteractionGetGap(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetGap(IntPtr interaction, double _gap);

	[DllImport("Physics")]
	private static extern float InteractionGetF(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetF(IntPtr interaction, float _F);

	[DllImport("Physics")]
	private static extern float InteractionGetFlowExponent(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFlowExponent(IntPtr interaction, float _F);

	[DllImport("Physics")]
	private static extern float InteractionGetK1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK1(IntPtr interaction, float _k1);

	[DllImport("Physics")]
	private static extern float InteractionGetK2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK2(IntPtr interaction, float _k2);

	[DllImport("Physics")]
	private static extern float InteractionGetK3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK3(IntPtr interaction, float _k3);

	[DllImport("Physics")]
	private static extern float InteractionGetK4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK4(IntPtr interaction, float _k4);

	[DllImport("Physics")]
	private static extern float InteractionGetK5(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK5(IntPtr interaction, float _k5);

	[DllImport("Physics")]
	private static extern float InteractionGetK6(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK6(IntPtr interaction, float _k6);

	[DllImport("Physics")]
	private static extern float InteractionGetSlack(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlack(IntPtr interaction, float _slack);

	[DllImport("Physics")]
	private static extern float InteractionGetK8(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK8(IntPtr interaction, float _k8);

	[DllImport("Physics")]
	private static extern float InteractionGetK9(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK9(IntPtr interaction, float _k9);

	[DllImport("Physics")]
	private static extern float InteractionGetF1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetF1(IntPtr interaction, float _f1);

	[DllImport("Physics")]
	private static extern float InteractionGetF4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetF4(IntPtr interaction, float _f4);

	[DllImport("Physics")]
	private static extern float InteractionGetSDraft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSDraft(IntPtr interaction, float _sDraft);

	[DllImport("Physics")]
	private static extern float InteractionGetSBuff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSBuff(IntPtr interaction, float _sBuff);

	[DllImport("Physics")]
	private static extern float InteractionGetR1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR1(IntPtr interaction, float _R1);

	[DllImport("Physics")]
	private static extern float InteractionGetR2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR2(IntPtr interaction, float _R2);

	[DllImport("Physics")]
	private static extern float InteractionGetR3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR3(IntPtr interaction, float _R3);

	[DllImport("Physics")]
	private static extern float InteractionGetR4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR4(IntPtr interaction, float _R4);

	[DllImport("Physics")]
	private static extern float InteractionGetR5(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR5(IntPtr interaction, float _R5);

	[DllImport("Physics")]
	private static extern float InteractionGetR6(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR6(IntPtr interaction, float _R6);

	[DllImport("Physics")]
	private static extern float InteractionGetR8(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR8(IntPtr interaction, float _R8);

	[DllImport("Physics")]
	private static extern float InteractionGetR9(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetR9(IntPtr interaction, float _R9);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_ViscosityFactor(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_ViscosityFactor(IntPtr interaction, float _Coupler_ViscosityFactor);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_ViscosityMaxForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_ViscosityMaxForce(IntPtr interaction, float _Coupler_ViscosityMaxForce);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffSlack(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffSlack(IntPtr interaction, float _Coupler_BuffSlack);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffX1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffX1(IntPtr interaction, float _Coupler_BuffX1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffY1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffY1(IntPtr interaction, float _Coupler_BuffY1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffX2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffX2(IntPtr interaction, float _Coupler_BuffX2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffY2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffY2(IntPtr interaction, float _Coupler_BuffY2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffX3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffX3(IntPtr interaction, float _Coupler_BuffX3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffY3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffY3(IntPtr interaction, float _Coupler_BuffY3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffX4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffX4(IntPtr interaction, float _Coupler_BuffX4);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffY4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffY4(IntPtr interaction, float _Coupler_BuffY4);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftSlack(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftSlack(IntPtr interaction, float _Coupler_DraftSlack);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftX1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftX1(IntPtr interaction, float _Coupler_DraftX1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftY1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftY1(IntPtr interaction, float _Coupler_DraftY1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftX2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftX2(IntPtr interaction, float _Coupler_DraftX2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftY2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftY2(IntPtr interaction, float _Coupler_DraftY2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftX3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftX3(IntPtr interaction, float _Coupler_DraftX3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftY3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftY3(IntPtr interaction, float _Coupler_DraftY3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftX4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftX4(IntPtr interaction, float _Coupler_DraftX4);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftY4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftY4(IntPtr interaction, float _Coupler_DraftY4);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftK5(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftK5(IntPtr interaction, float _Coupler_DraftK5);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftK1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftK1(IntPtr interaction, float _Coupler_DraftK1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftK2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftK2(IntPtr interaction, float _Coupler_DraftK2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftK3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftK3(IntPtr interaction, float _Coupler_DraftK3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_DraftK4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_DraftK4(IntPtr interaction, float _Coupler_DraftK4);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffK5(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffK5(IntPtr interaction, float _Coupler_BuffK5);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffK1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffK1(IntPtr interaction, float _Coupler_BuffK1);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffK2(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffK2(IntPtr interaction, float _Coupler_BuffK2);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffK3(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffK3(IntPtr interaction, float _Coupler_BuffK3);

	[DllImport("Physics")]
	private static extern float InteractionGetCoupler_BuffK4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCoupler_BuffK4(IntPtr interaction, float _Coupler_BuffK4);

	[DllImport("Physics")]
	private static extern float InteractionGetTopCapForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTopCapForce(IntPtr interaction, float _TopCapForce);

	[DllImport("Physics")]
	private static extern float InteractionGetBottomCapForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBottomCapForce(IntPtr interaction, float _BottomCapForce);

	[DllImport("Physics")]
	private static extern float InteractionGetTopS(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTopS(IntPtr interaction, float _TopS);

	[DllImport("Physics")]
	private static extern float InteractionGetBottomS(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBottomS(IntPtr interaction, float _BottomS);

	[DllImport("Physics")]
	private static extern float InteractionGetCouplerS0(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplerS0(IntPtr interaction, float _CouplerS0);

	[DllImport("Physics")]
	private static extern int InteractionGetCouplerStateOfGear(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplerStateOfGear(IntPtr interaction, int _CouplerStateOfGear);

	[DllImport("Physics")]
	private static extern int InteractionGetCouplingSystemType(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplingSystemType(IntPtr interaction, int _CouplingSystemType);

	[DllImport("Physics")]
	private static extern float InteractionGetBuffDirPistonHeadAreaCubed(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBuffDirPistonHeadAreaCubed(IntPtr interaction, float _BuffDirPistonHeadAreaCubed);

	[DllImport("Physics")]
	private static extern float InteractionGetDraftDirPistonHeadAreaCubed(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDraftDirPistonHeadAreaCubed(IntPtr interaction, float _DraftDirPistonHeadAreaCubed);

	[DllImport("Physics")]
	private static extern float InteractionGetHydraulicForceFactor(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetHydraulicForceFactor(IntPtr interaction, float _HydraulicForceFactor);

	[DllImport("Physics")]
	private static extern float InteractionGetHydraulicFluidFactor(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetHydraulicFluidFactor(IntPtr interaction, float _HydraulicFluidFactor);

	[DllImport("Physics")]
	private static extern float InteractionGetTotalOrificeArea_Buff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTotalOrificeArea_Buff(IntPtr interaction, float _TotalOrificeArea_Buff);

	[DllImport("Physics")]
	private static extern float InteractionGetTotalOrificeArea_Draft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTotalOrificeArea_Draft(IntPtr interaction, float _TotalOrificeArea_Draft);

	[DllImport("Physics")]
	private static extern float InteractionGetTotalOrificeArea_ReturnBuff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTotalOrificeArea_ReturnBuff(IntPtr interaction, float _TotalOrificeArea_ReturnBuff);

	[DllImport("Physics")]
	private static extern float InteractionGetTotalOrificeArea_ReturnDraft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTotalOrificeArea_ReturnDraft(IntPtr interaction, float _TotalOrificeArea_ReturnDraft);

	[DllImport("Physics")]
	private static extern float InteractionGetMaxTravelBuff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMaxTravelBuff(IntPtr interaction, float _MaxTravelBuff);

	[DllImport("Physics")]
	private static extern float InteractionGetMaxTravelDraft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMaxTravelDraft(IntPtr interaction, float _MaxTravelDraft);

	[DllImport("Physics")]
	private static extern float InteractionGetMaxTravelBeforeOpening(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMaxTravelBeforeOpening(IntPtr interaction, float _MaxTravelBeforeOpening);

	[DllImport("Physics")]
	private static extern float InteractionGetMaxTravelBeforeBreakingTrain(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMaxTravelBeforeBreakingTrain(IntPtr interaction, float _MaxTravelBeforeBreakingTrain);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadMin_Buff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadMin_Buff(IntPtr interaction, float _PreloadMin_Buff);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadMax_Buff(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadMax_Buff(IntPtr interaction, float _PreloadMax_Buff);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadMin_Draft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadMin_Draft(IntPtr interaction, float _PreloadMin_Draft);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadMax_Draft(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadMax_Draft(IntPtr interaction, float _PreloadMax_Draft);

	[DllImport("Physics")]
	private static extern float InteractionGetKCarBodyStiffness(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetKCarBodyStiffness(IntPtr interaction, float _kCarBodyStiffness);

	[DllImport("Physics")]
	private static extern float InteractionGetBuffMinSpringForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBuffMinSpringForce(IntPtr interaction, float _BuffMinSpringForce);

	[DllImport("Physics")]
	private static extern float InteractionGetK_BuffSpring(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK_BuffSpring(IntPtr interaction, float _k_BuffSpring);

	[DllImport("Physics")]
	private static extern float InteractionGetDraftMinSpringForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDraftMinSpringForce(IntPtr interaction, float _DraftMinSpringForce);

	[DllImport("Physics")]
	private static extern float InteractionGetK_DraftSpring(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetK_DraftSpring(IntPtr interaction, float _k_DraftSpring);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadOrificeRatio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadOrificeRatio(IntPtr interaction, float _PreloadOrificeRatio);

	[DllImport("Physics")]
	private static extern float InteractionGetMinPreloadOrificeRatio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMinPreloadOrificeRatio(IntPtr interaction, float _MinPreloadOrificeRatio);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyx(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyx(IntPtr interaction, float _Bodyx);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyxdot(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyxdot(IntPtr interaction, float _Bodyxdot);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyxdot_ratio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyxdot_ratio(IntPtr interaction, float _Bodyxdot_ratio);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackx(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackx(IntPtr interaction, float _Slackx);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackxdot(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackxdot(IntPtr interaction, float _Slackxdot);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackxdot_ratio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackxdot_ratio(IntPtr interaction, float _Slackxdot_ratio);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonx(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonx(IntPtr interaction, float _Pistonx);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonxdot(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonxdot(IntPtr interaction, float _Pistonxdot);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonxdot_ratio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonxdot_ratio(IntPtr interaction, float _Pistonxdot_ratio);

	[DllImport("Physics")]
	private static extern float InteractionGetTravelRatio(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTravelRatio(IntPtr interaction, float _TravelRatio);

	[DllImport("Physics")]
	private static extern float InteractionGetX_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetX_nm1(IntPtr interaction, float _x_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetXdot_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetXdot_nm1(IntPtr interaction, float _xdot_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyx_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyx_nm1(IntPtr interaction, float _Bodyx_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyxdot_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyxdot_nm1(IntPtr interaction, float _Bodyxdot_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetBodyxdot_RKSum(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBodyxdot_RKSum(IntPtr interaction, float _Bodyxdot_RKSum);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackx_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackx_nm1(IntPtr interaction, float _Slackx_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackxdot_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackxdot_nm1(IntPtr interaction, float _Slackxdot_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetSlackxdot_RKSum(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSlackxdot_RKSum(IntPtr interaction, float _Slackxdot_RKSum);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonx_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonx_nm1(IntPtr interaction, float _Pistonx_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonxdot_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonxdot_nm1(IntPtr interaction, float _Pistonxdot_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetPistonxdot_RKSum(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPistonxdot_RKSum(IntPtr interaction, float _Pistonxdot_RKSum);

	[DllImport("Physics")]
	private static extern float InteractionGetForce_nm1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetForce_nm1(IntPtr interaction, float _Force_nm1);

	[DllImport("Physics")]
	private static extern float InteractionGetPreloadStartx(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetPreloadStartx(IntPtr interaction, float _PreloadStartx);

	[DllImport("Physics")]
	private static extern float InteractionGetOrificeArea(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetOrificeArea(IntPtr interaction, float _OrificeArea);

	[DllImport("Physics")]
	private static extern int InteractionGetStateOfGear(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetStateOfGear(IntPtr interaction, int _stateOfGear);

	[DllImport("Physics")]
	private static extern float InteractionGetV(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetV(IntPtr interaction, float _v);

	[DllImport("Physics")]
	private static extern float InteractionGetDelta0(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDelta0(IntPtr interaction, float _delta0);

	[DllImport("Physics")]
	private static extern float InteractionGetForceBreak(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetForceBreak(IntPtr interaction, float _forceBreak);

	[DllImport("Physics")]
	private static extern float InteractionGetS(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetS(IntPtr interaction, float _s);

	[DllImport("Physics")]
	private static extern float InteractionGetS0(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetS0(IntPtr interaction, float _s0);

	[DllImport("Physics")]
	private static extern float InteractionGetMassFlow(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMassFlow(IntPtr interaction, float _MassFlow);

	[DllImport("Physics")]
	private static extern float InteractionGetMassFlowDot(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMassFlowDot(IntPtr interaction, float _MassFlowDot);

	[DllImport("Physics")]
	private static extern float InteractionGetDelta(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDelta(IntPtr interaction, float _delta);

	[DllImport("Physics")]
	private static extern float InteractionGetDelta_LeadCar(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDelta_LeadCar(IntPtr interaction, float _delta_LeadCar);

	[DllImport("Physics")]
	private static extern float InteractionGetDelta_FollowCar(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDelta_FollowCar(IntPtr interaction, float _delta_FollowCar);

	[DllImport("Physics")]
	private static extern float InteractionGetTrackAngleBetweenLeadAndFollowCarCenters(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetTrackAngleBetweenLeadAndFollowCarCenters(IntPtr interaction, float _TrackAngleBetweenLeadAndFollowCarCenters);

	[DllImport("Physics")]
	private static extern float InteractionGetCouplerAngle(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplerAngle(IntPtr interaction, float _CouplerAngle);

	[DllImport("Physics")]
	private static extern float InteractionGetNudgedCouplerAngle(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetNudgedCouplerAngle(IntPtr interaction, float _NudgedCouplerAngle);

	[DllImport("Physics")]
	private static extern float InteractionGetForce(IntPtr interaction);

	[DllImport("Physics")]
	public static extern void InteractionSetForce(IntPtr interaction, float _force);

	[DllImport("Physics")]
	private static extern float InteractionGetFilteredForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFilteredForce(IntPtr interaction, float _force);

	[DllImport("Physics")]
	private static extern float InteractionGetForceFilterTimeConstant(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetForceFilterTimeConstant(IntPtr interaction, float _timeConst);

	[DllImport("Physics")]
	private static extern float InteractionGetDampenedForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetDampenedForce(IntPtr interaction, float _DampenedForce);

	[DllImport("Physics")]
	private static extern float InteractionGetLength(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetLength(IntPtr interaction, float _length);

	[DllImport("Physics")]
	private static extern float InteractionGetBPLength(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBPLength(IntPtr interaction, float _BPLength);

	[DllImport("Physics")]
	private static extern float InteractionGetLength1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetLength1(IntPtr interaction, float _length1);

	[DllImport("Physics")]
	private static extern float InteractionGetBunchedPin2PinCouplingLength(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBunchedPin2PinCouplingLength(IntPtr interaction, float _BunchedPin2PinCouplingLength);

	[DllImport("Physics")]
	private static extern float InteractionGetLeadCarCouplerLength(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetLeadCarCouplerLength(IntPtr interaction, float _leadCarCouplerLength);

	[DllImport("Physics")]
	private static extern float InteractionGetFollowCarCouplerLength(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFollowCarCouplerLength(IntPtr interaction, float _followCarCouplerLength);

	[DllImport("Physics")]
	private static extern float InteractionGetLeadCarCouplerPosition(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetLeadCarCouplerPosition(IntPtr interaction, float _leadCarCouplerPosition);

	[DllImport("Physics")]
	private static extern float InteractionGetFollowCarCouplerPosition(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetFollowCarCouplerPosition(IntPtr interaction, float _followCarCouplerPosition);

	[DllImport("Physics")]
	private static extern float InteractionGetBPPress(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetBPPress(IntPtr interaction, float _BPPress);

	[DllImport("Physics")]
	private static extern float InteractionGetMaxForceLast(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMaxForceLast(IntPtr interaction, float _maxForceLast);

	[DllImport("Physics")]
	private static extern float InteractionGetMinForceLast(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetMinForceLast(IntPtr interaction, float _minForceLast);

	[DllImport("Physics")]
	private static extern int InteractionGetGladhandLast(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetGladhandLast(IntPtr interaction, int _gladhandLast);

	[DllImport("Physics")]
	private static extern float InteractionGetCouplerModeLast(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetCouplerModeLast(IntPtr interaction, float _couplerModeLast);

	[DllImport("Physics")]
	private static extern int InteractionGetAttenuation(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetAttenuation(IntPtr interaction, int _Attenuation);

	[DllImport("Physics")]
	private static extern float InteractionGetForceLimit(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetForceLimit(IntPtr interaction, float _forceLimit);

	[DllImport("Physics")]
	private static extern float InteractionGetState1OrState4CapForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetState1OrState4CapForce(IntPtr interaction, float _State1OrState4CapForce);

	[DllImport("Physics")]
	private static extern float InteractionGetState3OrState6CapForce(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetState3OrState6CapForce(IntPtr interaction, float _State3OrState6CapForce);

	[DllImport("Physics")]
	private static extern float InteractionGetState1OrState4CapS(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetState1OrState4CapS(IntPtr interaction, float _State1OrState4CapS);

	[DllImport("Physics")]
	private static extern float InteractionGetState3OrState6CapS(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetState3OrState6CapS(IntPtr interaction, float _State3OrState6CapS);

	[DllImport("Physics")]
	private static extern float InteractionGetSStartOfState1(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSStartOfState1(IntPtr interaction, float _SStartOfState1);

	[DllImport("Physics")]
	private static extern float InteractionGetSStartOfState4(IntPtr interaction);

	[DllImport("Physics")]
	private static extern void InteractionSetSStartOfState4(IntPtr interaction, float _SStartOfState4);

	public IntPtr innerInteraction;

	public CarInteraction(IntPtr _innerInteraction)
	{
		innerInteraction = _innerInteraction;
	}

	public CarPhysics.CarEnd lead
	{
		get
		{
			return InteractionGetLead(innerInteraction);
		}
		set
		{
			InteractionSetLead(innerInteraction, ref value);
		}
	}

	public CarPhysics.CarEnd follow
	{
		get
		{
			return InteractionGetFollow(innerInteraction);
		}
		set
		{
			InteractionSetFollow(innerInteraction, ref value);
		}
	}

	public ConsistPhysics consistPhysics
	{
		set
		{
			InteractionSetPCon(innerInteraction, value.GetInnerConsist());
		}
	}

	// Generated properties:

	public int index
	{
		get
		{
			return InteractionGetIndex(innerInteraction);
		}
		set
		{
			InteractionSetIndex(innerInteraction, value);
		}
	}

	public int consist
	{
		get
		{
			return InteractionGetConsist(innerInteraction);
		}
		set
		{
			InteractionSetConsist(innerInteraction, value);
		}
	}

	public int couplerMode
	{
		get
		{
			return InteractionGetCouplerMode(innerInteraction);
		}
		set
		{
			InteractionSetCouplerMode(innerInteraction, value);
		}
	}

	public int gladhand
	{
		get
		{
			return InteractionGetGladhand(innerInteraction);
		}
		set
		{
			InteractionSetGladhand(innerInteraction, value);
		}
	}

	public int direction
	{
		get
		{
			return InteractionGetDirection(innerInteraction);
		}
		set
		{
			InteractionSetDirection(innerInteraction, value);
		}
	}

	public int brokenflag
	{
		get
		{
			return InteractionGetBrokenflag(innerInteraction);
		}
		set
		{
			InteractionSetBrokenflag(innerInteraction, value);
		}
	}

	public int pinBreakFlag
	{
		get
		{
			return InteractionGetPinBreakFlag(innerInteraction);
		}
		set
		{
			InteractionSetPinBreakFlag(innerInteraction, value);
		}
	}

	public float maxForce
	{
		get
		{
			return InteractionGetMaxForce(innerInteraction);
		}
		set
		{
			InteractionSetMaxForce(innerInteraction, value);
		}
	}

	public float minForce
	{
		get
		{
			return InteractionGetMinForce(innerInteraction);
		}
		set
		{
			InteractionSetMinForce(innerInteraction, value);
		}
	}

	public float DebugDumpMaxAbsForce
	{
		get
		{
			return InteractionGetDebugDumpMaxAbsForce(innerInteraction);
		}
		set
		{
			InteractionSetDebugDumpMaxAbsForce(innerInteraction, value);
		}
	}

	public float FilteringForcesSum
	{
		get
		{
			return InteractionGetFilteringForcesSum(innerInteraction);
		}
		set
		{
			InteractionSetFilteringForcesSum(innerInteraction, value);
		}
	}

	public double gap
	{
		get
		{
			return InteractionGetGap(innerInteraction);
		}
		set
		{
			InteractionSetGap(innerInteraction, value);
		}
	}

	public float F
	{
		get
		{
			return InteractionGetF(innerInteraction);
		}
		set
		{
			InteractionSetF(innerInteraction, value);
		}
	}

	public float flowExponent
	{
		get
		{
			return InteractionGetFlowExponent(innerInteraction);
		}
		set
		{
			InteractionSetFlowExponent(innerInteraction, value);
		}
	}

	public float k1
	{
		get
		{
			return InteractionGetK1(innerInteraction);
		}
		set
		{
			InteractionSetK1(innerInteraction, value);
		}
	}

	public float k2
	{
		get
		{
			return InteractionGetK2(innerInteraction);
		}
		set
		{
			InteractionSetK2(innerInteraction, value);
		}
	}

	public float k3
	{
		get
		{
			return InteractionGetK3(innerInteraction);
		}
		set
		{
			InteractionSetK3(innerInteraction, value);
		}
	}

	public float k4
	{
		get
		{
			return InteractionGetK4(innerInteraction);
		}
		set
		{
			InteractionSetK4(innerInteraction, value);
		}
	}

	public float k5
	{
		get
		{
			return InteractionGetK5(innerInteraction);
		}
		set
		{
			InteractionSetK5(innerInteraction, value);
		}
	}

	public float k6
	{
		get
		{
			return InteractionGetK6(innerInteraction);
		}
		set
		{
			InteractionSetK6(innerInteraction, value);
		}
	}

	public float slack
	{
		get
		{
			return InteractionGetSlack(innerInteraction);
		}
		set
		{
			InteractionSetSlack(innerInteraction, value);
		}
	}

	public float k8
	{
		get
		{
			return InteractionGetK8(innerInteraction);
		}
		set
		{
			InteractionSetK8(innerInteraction, value);
		}
	}

	public float k9
	{
		get
		{
			return InteractionGetK9(innerInteraction);
		}
		set
		{
			InteractionSetK9(innerInteraction, value);
		}
	}

	public float f1
	{
		get
		{
			return InteractionGetF1(innerInteraction);
		}
		set
		{
			InteractionSetF1(innerInteraction, value);
		}
	}

	public float f4
	{
		get
		{
			return InteractionGetF4(innerInteraction);
		}
		set
		{
			InteractionSetF4(innerInteraction, value);
		}
	}

	public float sDraft
	{
		get
		{
			return InteractionGetSDraft(innerInteraction);
		}
		set
		{
			InteractionSetSDraft(innerInteraction, value);
		}
	}

	public float sBuff
	{
		get
		{
			return InteractionGetSBuff(innerInteraction);
		}
		set
		{
			InteractionSetSBuff(innerInteraction, value);
		}
	}

	public float R1
	{
		get
		{
			return InteractionGetR1(innerInteraction);
		}
		set
		{
			InteractionSetR1(innerInteraction, value);
		}
	}

	public float R2
	{
		get
		{
			return InteractionGetR2(innerInteraction);
		}
		set
		{
			InteractionSetR2(innerInteraction, value);
		}
	}

	public float R3
	{
		get
		{
			return InteractionGetR3(innerInteraction);
		}
		set
		{
			InteractionSetR3(innerInteraction, value);
		}
	}

	public float R4
	{
		get
		{
			return InteractionGetR4(innerInteraction);
		}
		set
		{
			InteractionSetR4(innerInteraction, value);
		}
	}

	public float R5
	{
		get
		{
			return InteractionGetR5(innerInteraction);
		}
		set
		{
			InteractionSetR5(innerInteraction, value);
		}
	}

	public float R6
	{
		get
		{
			return InteractionGetR6(innerInteraction);
		}
		set
		{
			InteractionSetR6(innerInteraction, value);
		}
	}

	public float R8
	{
		get
		{
			return InteractionGetR8(innerInteraction);
		}
		set
		{
			InteractionSetR8(innerInteraction, value);
		}
	}

	public float R9
	{
		get
		{
			return InteractionGetR9(innerInteraction);
		}
		set
		{
			InteractionSetR9(innerInteraction, value);
		}
	}

	public float Coupler_ViscosityFactor
	{
		get
		{
			return InteractionGetCoupler_ViscosityFactor(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_ViscosityFactor(innerInteraction, value);
		}
	}

	public float Coupler_ViscosityMaxForce
	{
		get
		{
			return InteractionGetCoupler_ViscosityMaxForce(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_ViscosityMaxForce(innerInteraction, value);
		}
	}

	public float Coupler_BuffSlack
	{
		get
		{
			return InteractionGetCoupler_BuffSlack(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffSlack(innerInteraction, value);
		}
	}

	public float Coupler_BuffX1
	{
		get
		{
			return InteractionGetCoupler_BuffX1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffX1(innerInteraction, value);
		}
	}

	public float Coupler_BuffY1
	{
		get
		{
			return InteractionGetCoupler_BuffY1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffY1(innerInteraction, value);
		}
	}

	public float Coupler_BuffX2
	{
		get
		{
			return InteractionGetCoupler_BuffX2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffX2(innerInteraction, value);
		}
	}

	public float Coupler_BuffY2
	{
		get
		{
			return InteractionGetCoupler_BuffY2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffY2(innerInteraction, value);
		}
	}

	public float Coupler_BuffX3
	{
		get
		{
			return InteractionGetCoupler_BuffX3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffX3(innerInteraction, value);
		}
	}

	public float Coupler_BuffY3
	{
		get
		{
			return InteractionGetCoupler_BuffY3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffY3(innerInteraction, value);
		}
	}

	public float Coupler_BuffX4
	{
		get
		{
			return InteractionGetCoupler_BuffX4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffX4(innerInteraction, value);
		}
	}

	public float Coupler_BuffY4
	{
		get
		{
			return InteractionGetCoupler_BuffY4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffY4(innerInteraction, value);
		}
	}

	public float Coupler_DraftSlack
	{
		get
		{
			return InteractionGetCoupler_DraftSlack(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftSlack(innerInteraction, value);
		}
	}

	public float Coupler_DraftX1
	{
		get
		{
			return InteractionGetCoupler_DraftX1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftX1(innerInteraction, value);
		}
	}

	public float Coupler_DraftY1
	{
		get
		{
			return InteractionGetCoupler_DraftY1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftY1(innerInteraction, value);
		}
	}

	public float Coupler_DraftX2
	{
		get
		{
			return InteractionGetCoupler_DraftX2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftX2(innerInteraction, value);
		}
	}

	public float Coupler_DraftY2
	{
		get
		{
			return InteractionGetCoupler_DraftY2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftY2(innerInteraction, value);
		}
	}

	public float Coupler_DraftX3
	{
		get
		{
			return InteractionGetCoupler_DraftX3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftX3(innerInteraction, value);
		}
	}

	public float Coupler_DraftY3
	{
		get
		{
			return InteractionGetCoupler_DraftY3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftY3(innerInteraction, value);
		}
	}

	public float Coupler_DraftX4
	{
		get
		{
			return InteractionGetCoupler_DraftX4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftX4(innerInteraction, value);
		}
	}

	public float Coupler_DraftY4
	{
		get
		{
			return InteractionGetCoupler_DraftY4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftY4(innerInteraction, value);
		}
	}

	public float Coupler_DraftK5
	{
		get
		{
			return InteractionGetCoupler_DraftK5(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftK5(innerInteraction, value);
		}
	}

	public float Coupler_DraftK1
	{
		get
		{
			return InteractionGetCoupler_DraftK1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftK1(innerInteraction, value);
		}
	}

	public float Coupler_DraftK2
	{
		get
		{
			return InteractionGetCoupler_DraftK2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftK2(innerInteraction, value);
		}
	}

	public float Coupler_DraftK3
	{
		get
		{
			return InteractionGetCoupler_DraftK3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftK3(innerInteraction, value);
		}
	}

	public float Coupler_DraftK4
	{
		get
		{
			return InteractionGetCoupler_DraftK4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_DraftK4(innerInteraction, value);
		}
	}

	public float Coupler_BuffK5
	{
		get
		{
			return InteractionGetCoupler_BuffK5(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffK5(innerInteraction, value);
		}
	}

	public float Coupler_BuffK1
	{
		get
		{
			return InteractionGetCoupler_BuffK1(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffK1(innerInteraction, value);
		}
	}

	public float Coupler_BuffK2
	{
		get
		{
			return InteractionGetCoupler_BuffK2(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffK2(innerInteraction, value);
		}
	}

	public float Coupler_BuffK3
	{
		get
		{
			return InteractionGetCoupler_BuffK3(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffK3(innerInteraction, value);
		}
	}

	public float Coupler_BuffK4
	{
		get
		{
			return InteractionGetCoupler_BuffK4(innerInteraction);
		}
		set
		{
			InteractionSetCoupler_BuffK4(innerInteraction, value);
		}
	}

	public float TopCapForce
	{
		get
		{
			return InteractionGetTopCapForce(innerInteraction);
		}
		set
		{
			InteractionSetTopCapForce(innerInteraction, value);
		}
	}

	public float BottomCapForce
	{
		get
		{
			return InteractionGetBottomCapForce(innerInteraction);
		}
		set
		{
			InteractionSetBottomCapForce(innerInteraction, value);
		}
	}

	public float TopS
	{
		get
		{
			return InteractionGetTopS(innerInteraction);
		}
		set
		{
			InteractionSetTopS(innerInteraction, value);
		}
	}

	public float BottomS
	{
		get
		{
			return InteractionGetBottomS(innerInteraction);
		}
		set
		{
			InteractionSetBottomS(innerInteraction, value);
		}
	}

	public float CouplerS0
	{
		get
		{
			return InteractionGetCouplerS0(innerInteraction);
		}
		set
		{
			InteractionSetCouplerS0(innerInteraction, value);
		}
	}

	public int CouplerStateOfGear
	{
		get
		{
			return InteractionGetCouplerStateOfGear(innerInteraction);
		}
		set
		{
			InteractionSetCouplerStateOfGear(innerInteraction, value);
		}
	}

	public int CouplingSystemType
	{
		get
		{
			return InteractionGetCouplingSystemType(innerInteraction);
		}
		set
		{
			InteractionSetCouplingSystemType(innerInteraction, value);
		}
	}

	public float BuffDirPistonHeadAreaCubed
	{
		get
		{
			return InteractionGetBuffDirPistonHeadAreaCubed(innerInteraction);
		}
		set
		{
			InteractionSetBuffDirPistonHeadAreaCubed(innerInteraction, value);
		}
	}

	public float DraftDirPistonHeadAreaCubed
	{
		get
		{
			return InteractionGetDraftDirPistonHeadAreaCubed(innerInteraction);
		}
		set
		{
			InteractionSetDraftDirPistonHeadAreaCubed(innerInteraction, value);
		}
	}

	public float HydraulicForceFactor
	{
		get
		{
			return InteractionGetHydraulicForceFactor(innerInteraction);
		}
		set
		{
			InteractionSetHydraulicForceFactor(innerInteraction, value);
		}
	}

	public float HydraulicFluidFactor
	{
		get
		{
			return InteractionGetHydraulicFluidFactor(innerInteraction);
		}
		set
		{
			InteractionSetHydraulicFluidFactor(innerInteraction, value);
		}
	}

	public float TotalOrificeArea_Buff
	{
		get
		{
			return InteractionGetTotalOrificeArea_Buff(innerInteraction);
		}
		set
		{
			InteractionSetTotalOrificeArea_Buff(innerInteraction, value);
		}
	}

	public float TotalOrificeArea_Draft
	{
		get
		{
			return InteractionGetTotalOrificeArea_Draft(innerInteraction);
		}
		set
		{
			InteractionSetTotalOrificeArea_Draft(innerInteraction, value);
		}
	}

	public float TotalOrificeArea_ReturnBuff
	{
		get
		{
			return InteractionGetTotalOrificeArea_ReturnBuff(innerInteraction);
		}
		set
		{
			InteractionSetTotalOrificeArea_ReturnBuff(innerInteraction, value);
		}
	}

	public float TotalOrificeArea_ReturnDraft
	{
		get
		{
			return InteractionGetTotalOrificeArea_ReturnDraft(innerInteraction);
		}
		set
		{
			InteractionSetTotalOrificeArea_ReturnDraft(innerInteraction, value);
		}
	}

	public float MaxTravelBuff
	{
		get
		{
			return InteractionGetMaxTravelBuff(innerInteraction);
		}
		set
		{
			InteractionSetMaxTravelBuff(innerInteraction, value);
		}
	}

	public float MaxTravelDraft
	{
		get
		{
			return InteractionGetMaxTravelDraft(innerInteraction);
		}
		set
		{
			InteractionSetMaxTravelDraft(innerInteraction, value);
		}
	}

	public float MaxTravelBeforeOpening
	{
		get
		{
			return InteractionGetMaxTravelBeforeOpening(innerInteraction);
		}
		set
		{
			InteractionSetMaxTravelBeforeOpening(innerInteraction, value);
		}
	}

	public float MaxTravelBeforeBreakingTrain
	{
		get
		{
			return InteractionGetMaxTravelBeforeBreakingTrain(innerInteraction);
		}
		set
		{
			InteractionSetMaxTravelBeforeBreakingTrain(innerInteraction, value);
		}
	}

	public float PreloadMin_Buff
	{
		get
		{
			return InteractionGetPreloadMin_Buff(innerInteraction);
		}
		set
		{
			InteractionSetPreloadMin_Buff(innerInteraction, value);
		}
	}

	public float PreloadMax_Buff
	{
		get
		{
			return InteractionGetPreloadMax_Buff(innerInteraction);
		}
		set
		{
			InteractionSetPreloadMax_Buff(innerInteraction, value);
		}
	}

	public float PreloadMin_Draft
	{
		get
		{
			return InteractionGetPreloadMin_Draft(innerInteraction);
		}
		set
		{
			InteractionSetPreloadMin_Draft(innerInteraction, value);
		}
	}

	public float PreloadMax_Draft
	{
		get
		{
			return InteractionGetPreloadMax_Draft(innerInteraction);
		}
		set
		{
			InteractionSetPreloadMax_Draft(innerInteraction, value);
		}
	}

	public float kCarBodyStiffness
	{
		get
		{
			return InteractionGetKCarBodyStiffness(innerInteraction);
		}
		set
		{
			InteractionSetKCarBodyStiffness(innerInteraction, value);
		}
	}

	public float BuffMinSpringForce
	{
		get
		{
			return InteractionGetBuffMinSpringForce(innerInteraction);
		}
		set
		{
			InteractionSetBuffMinSpringForce(innerInteraction, value);
		}
	}

	public float k_BuffSpring
	{
		get
		{
			return InteractionGetK_BuffSpring(innerInteraction);
		}
		set
		{
			InteractionSetK_BuffSpring(innerInteraction, value);
		}
	}

	public float DraftMinSpringForce
	{
		get
		{
			return InteractionGetDraftMinSpringForce(innerInteraction);
		}
		set
		{
			InteractionSetDraftMinSpringForce(innerInteraction, value);
		}
	}

	public float k_DraftSpring
	{
		get
		{
			return InteractionGetK_DraftSpring(innerInteraction);
		}
		set
		{
			InteractionSetK_DraftSpring(innerInteraction, value);
		}
	}

	public float PreloadOrificeRatio
	{
		get
		{
			return InteractionGetPreloadOrificeRatio(innerInteraction);
		}
		set
		{
			InteractionSetPreloadOrificeRatio(innerInteraction, value);
		}
	}

	public float MinPreloadOrificeRatio
	{
		get
		{
			return InteractionGetMinPreloadOrificeRatio(innerInteraction);
		}
		set
		{
			InteractionSetMinPreloadOrificeRatio(innerInteraction, value);
		}
	}

	public float Bodyx
	{
		get
		{
			return InteractionGetBodyx(innerInteraction);
		}
		set
		{
			InteractionSetBodyx(innerInteraction, value);
		}
	}

	public float Bodyxdot
	{
		get
		{
			return InteractionGetBodyxdot(innerInteraction);
		}
		set
		{
			InteractionSetBodyxdot(innerInteraction, value);
		}
	}

	public float Bodyxdot_ratio
	{
		get
		{
			return InteractionGetBodyxdot_ratio(innerInteraction);
		}
		set
		{
			InteractionSetBodyxdot_ratio(innerInteraction, value);
		}
	}

	public float Slackx
	{
		get
		{
			return InteractionGetSlackx(innerInteraction);
		}
		set
		{
			InteractionSetSlackx(innerInteraction, value);
		}
	}

	public float Slackxdot
	{
		get
		{
			return InteractionGetSlackxdot(innerInteraction);
		}
		set
		{
			InteractionSetSlackxdot(innerInteraction, value);
		}
	}

	public float Slackxdot_ratio
	{
		get
		{
			return InteractionGetSlackxdot_ratio(innerInteraction);
		}
		set
		{
			InteractionSetSlackxdot_ratio(innerInteraction, value);
		}
	}

	public float Pistonx
	{
		get
		{
			return InteractionGetPistonx(innerInteraction);
		}
		set
		{
			InteractionSetPistonx(innerInteraction, value);
		}
	}

	public float Pistonxdot
	{
		get
		{
			return InteractionGetPistonxdot(innerInteraction);
		}
		set
		{
			InteractionSetPistonxdot(innerInteraction, value);
		}
	}

	public float Pistonxdot_ratio
	{
		get
		{
			return InteractionGetPistonxdot_ratio(innerInteraction);
		}
		set
		{
			InteractionSetPistonxdot_ratio(innerInteraction, value);
		}
	}

	public float TravelRatio
	{
		get
		{
			return InteractionGetTravelRatio(innerInteraction);
		}
		set
		{
			InteractionSetTravelRatio(innerInteraction, value);
		}
	}

	public float x_nm1
	{
		get
		{
			return InteractionGetX_nm1(innerInteraction);
		}
		set
		{
			InteractionSetX_nm1(innerInteraction, value);
		}
	}

	public float xdot_nm1
	{
		get
		{
			return InteractionGetXdot_nm1(innerInteraction);
		}
		set
		{
			InteractionSetXdot_nm1(innerInteraction, value);
		}
	}

	public float Bodyx_nm1
	{
		get
		{
			return InteractionGetBodyx_nm1(innerInteraction);
		}
		set
		{
			InteractionSetBodyx_nm1(innerInteraction, value);
		}
	}

	public float Bodyxdot_nm1
	{
		get
		{
			return InteractionGetBodyxdot_nm1(innerInteraction);
		}
		set
		{
			InteractionSetBodyxdot_nm1(innerInteraction, value);
		}
	}

	public float Bodyxdot_RKSum
	{
		get
		{
			return InteractionGetBodyxdot_RKSum(innerInteraction);
		}
		set
		{
			InteractionSetBodyxdot_RKSum(innerInteraction, value);
		}
	}

	public float Slackx_nm1
	{
		get
		{
			return InteractionGetSlackx_nm1(innerInteraction);
		}
		set
		{
			InteractionSetSlackx_nm1(innerInteraction, value);
		}
	}

	public float Slackxdot_nm1
	{
		get
		{
			return InteractionGetSlackxdot_nm1(innerInteraction);
		}
		set
		{
			InteractionSetSlackxdot_nm1(innerInteraction, value);
		}
	}

	public float Slackxdot_RKSum
	{
		get
		{
			return InteractionGetSlackxdot_RKSum(innerInteraction);
		}
		set
		{
			InteractionSetSlackxdot_RKSum(innerInteraction, value);
		}
	}

	public float Pistonx_nm1
	{
		get
		{
			return InteractionGetPistonx_nm1(innerInteraction);
		}
		set
		{
			InteractionSetPistonx_nm1(innerInteraction, value);
		}
	}

	public float Pistonxdot_nm1
	{
		get
		{
			return InteractionGetPistonxdot_nm1(innerInteraction);
		}
		set
		{
			InteractionSetPistonxdot_nm1(innerInteraction, value);
		}
	}

	public float Pistonxdot_RKSum
	{
		get
		{
			return InteractionGetPistonxdot_RKSum(innerInteraction);
		}
		set
		{
			InteractionSetPistonxdot_RKSum(innerInteraction, value);
		}
	}

	public float Force_nm1
	{
		get
		{
			return InteractionGetForce_nm1(innerInteraction);
		}
		set
		{
			InteractionSetForce_nm1(innerInteraction, value);
		}
	}

	public float PreloadStartx
	{
		get
		{
			return InteractionGetPreloadStartx(innerInteraction);
		}
		set
		{
			InteractionSetPreloadStartx(innerInteraction, value);
		}
	}

	public float OrificeArea
	{
		get
		{
			return InteractionGetOrificeArea(innerInteraction);
		}
		set
		{
			InteractionSetOrificeArea(innerInteraction, value);
		}
	}

	public int stateOfGear
	{
		get
		{
			return InteractionGetStateOfGear(innerInteraction);
		}
		set
		{
			InteractionSetStateOfGear(innerInteraction, value);
		}
	}

	public float v
	{
		get
		{
			return InteractionGetV(innerInteraction);
		}
		set
		{
			InteractionSetV(innerInteraction, value);
		}
	}

	public float delta0
	{
		get
		{
			return InteractionGetDelta0(innerInteraction);
		}
		set
		{
			InteractionSetDelta0(innerInteraction, value);
		}
	}

	public float forceBreak
	{
		get
		{
			return InteractionGetForceBreak(innerInteraction);
		}
		set
		{
			InteractionSetForceBreak(innerInteraction, value);
		}
	}

	public float s
	{
		get
		{
			return InteractionGetS(innerInteraction);
		}
		set
		{
			InteractionSetS(innerInteraction, value);
		}
	}

	public float s0
	{
		get
		{
			return InteractionGetS0(innerInteraction);
		}
		set
		{
			InteractionSetS0(innerInteraction, value);
		}
	}

	public float MassFlow
	{
		get
		{
			return InteractionGetMassFlow(innerInteraction);
		}
		set
		{
			InteractionSetMassFlow(innerInteraction, value);
		}
	}

	public float MassFlowDot
	{
		get
		{
			return InteractionGetMassFlowDot(innerInteraction);
		}
		set
		{
			InteractionSetMassFlowDot(innerInteraction, value);
		}
	}

	public float delta
	{
		get
		{
			return InteractionGetDelta(innerInteraction);
		}
		set
		{
			InteractionSetDelta(innerInteraction, value);
		}
	}

	public float delta_LeadCar
	{
		get
		{
			return InteractionGetDelta_LeadCar(innerInteraction);
		}
		set
		{
			InteractionSetDelta_LeadCar(innerInteraction, value);
		}
	}

	public float delta_FollowCar
	{
		get
		{
			return InteractionGetDelta_FollowCar(innerInteraction);
		}
		set
		{
			InteractionSetDelta_FollowCar(innerInteraction, value);
		}
	}

	public float TrackAngleBetweenLeadAndFollowCarCenters
	{
		get
		{
			return InteractionGetTrackAngleBetweenLeadAndFollowCarCenters(innerInteraction);
		}
		set
		{
			InteractionSetTrackAngleBetweenLeadAndFollowCarCenters(innerInteraction, value);
		}
	}

	public float CouplerAngle
	{
		get
		{
			return InteractionGetCouplerAngle(innerInteraction);
		}
		set
		{
			InteractionSetCouplerAngle(innerInteraction, value);
		}
	}

	public float NudgedCouplerAngle
	{
		get
		{
			return InteractionGetNudgedCouplerAngle(innerInteraction);
		}
		set
		{
			InteractionSetNudgedCouplerAngle(innerInteraction, value);
		}
	}

	public float force
	{
		get
		{
			return InteractionGetForce(innerInteraction);
		}
		set
		{
			InteractionSetForce(innerInteraction, value);
		}
	}

	public float filteredForce
	{
		get
		{
			return InteractionGetFilteredForce(innerInteraction);
		}
		set
		{
			InteractionSetFilteredForce(innerInteraction, value);
		}
	}

	public float forceFilterTimeConstant
	{
		get
		{
			return InteractionGetForceFilterTimeConstant(innerInteraction);
		}
		set
		{
			InteractionSetForceFilterTimeConstant(innerInteraction, value);
		}
	}

	public float DampenedForce
	{
		get
		{
			return InteractionGetDampenedForce(innerInteraction);
		}
		set
		{
			InteractionSetDampenedForce(innerInteraction, value);
		}
	}

	public float length
	{
		get
		{
			return InteractionGetLength(innerInteraction);
		}
		set
		{
			InteractionSetLength(innerInteraction, value);
		}
	}

	public float BPLength
	{
		get
		{
			return InteractionGetBPLength(innerInteraction);
		}
		set
		{
			InteractionSetBPLength(innerInteraction, value);
		}
	}

	public float length1
	{
		get
		{
			return InteractionGetLength1(innerInteraction);
		}
		set
		{
			InteractionSetLength1(innerInteraction, value);
		}
	}

	public float BunchedPin2PinCouplingLength
	{
		get
		{
			return InteractionGetBunchedPin2PinCouplingLength(innerInteraction);
		}
		set
		{
			InteractionSetBunchedPin2PinCouplingLength(innerInteraction, value);
		}
	}

	public float leadCarCouplerLength
	{
		get
		{
			return InteractionGetLeadCarCouplerLength(innerInteraction);
		}
		set
		{
			InteractionSetLeadCarCouplerLength(innerInteraction, value);
		}
	}

	public float followCarCouplerLength
	{
		get
		{
			return InteractionGetFollowCarCouplerLength(innerInteraction);
		}
		set
		{
			InteractionSetFollowCarCouplerLength(innerInteraction, value);
		}
	}

	public float leadCarCouplerPosition
	{
		get
		{
			return InteractionGetLeadCarCouplerPosition(innerInteraction);
		}
		set
		{
			InteractionSetLeadCarCouplerPosition(innerInteraction, value);
		}
	}

	public float followCarCouplerPosition
	{
		get
		{
			return InteractionGetFollowCarCouplerPosition(innerInteraction);
		}
		set
		{
			InteractionSetFollowCarCouplerPosition(innerInteraction, value);
		}
	}

	public float BPPress
	{
		get
		{
			return InteractionGetBPPress(innerInteraction);
		}
		set
		{
			InteractionSetBPPress(innerInteraction, value);
		}
	}

	public float maxForceLast
	{
		get
		{
			return InteractionGetMaxForceLast(innerInteraction);
		}
		set
		{
			InteractionSetMaxForceLast(innerInteraction, value);
		}
	}

	public float minForceLast
	{
		get
		{
			return InteractionGetMinForceLast(innerInteraction);
		}
		set
		{
			InteractionSetMinForceLast(innerInteraction, value);
		}
	}

	public int gladhandLast
	{
		get
		{
			return InteractionGetGladhandLast(innerInteraction);
		}
		set
		{
			InteractionSetGladhandLast(innerInteraction, value);
		}
	}

	public float couplerModeLast
	{
		get
		{
			return InteractionGetCouplerModeLast(innerInteraction);
		}
		set
		{
			InteractionSetCouplerModeLast(innerInteraction, value);
		}
	}

	public int Attenuation
	{
		get
		{
			return InteractionGetAttenuation(innerInteraction);
		}
		set
		{
			InteractionSetAttenuation(innerInteraction, value);
		}
	}

	public float forceLimit
	{
		get
		{
			return InteractionGetForceLimit(innerInteraction);
		}
		set
		{
			InteractionSetForceLimit(innerInteraction, value);
		}
	}

	public float State1OrState4CapForce
	{
		get
		{
			return InteractionGetState1OrState4CapForce(innerInteraction);
		}
		set
		{
			InteractionSetState1OrState4CapForce(innerInteraction, value);
		}
	}

	public float State3OrState6CapForce
	{
		get
		{
			return InteractionGetState3OrState6CapForce(innerInteraction);
		}
		set
		{
			InteractionSetState3OrState6CapForce(innerInteraction, value);
		}
	}

	public float State1OrState4CapS
	{
		get
		{
			return InteractionGetState1OrState4CapS(innerInteraction);
		}
		set
		{
			InteractionSetState1OrState4CapS(innerInteraction, value);
		}
	}

	public float State3OrState6CapS
	{
		get
		{
			return InteractionGetState3OrState6CapS(innerInteraction);
		}
		set
		{
			InteractionSetState3OrState6CapS(innerInteraction, value);
		}
	}

	public float SStartOfState1
	{
		get
		{
			return InteractionGetSStartOfState1(innerInteraction);
		}
		set
		{
			InteractionSetSStartOfState1(innerInteraction, value);
		}
	}

	public float SStartOfState4
	{
		get
		{
			return InteractionGetSStartOfState4(innerInteraction);
		}
		set
		{
			InteractionSetSStartOfState4(innerInteraction, value);
		}
	}
}