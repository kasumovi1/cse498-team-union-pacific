﻿using System.Collections.Generic;
using System;
using System.Threading;
using System.Text;

public class CommaSeparatedValueFile
{
	public string fileName;
	private string fileText;
	public bool success;
	public bool done;
	public bool warning;
	public String errorMsg;

	public List<List<String>> cells;
	private Thread thread;

	private String[] headerNames;
	private int[] headerIndices;
	private bool headersInFirstRow;

	public CommaSeparatedValueFile()
	{
		fileName = string.Empty;
		fileText = null;
		done = false;
		warning = false;
		success = true;
		cells = new List<List<string>>();
		thread = null;

		headerNames = null;
		headerIndices = null;
		headersInFirstRow = false;
	}

	public bool Load(String name)
	{
		if (done)
		{
			return false;
		}

		fileName = name;
		fileText = null;

		try
		{
			thread = new Thread(new ThreadStart(Run));
			thread.Start();
		}
		catch (Exception e)
		{
			errorMsg = e.Message;
			success = false;
			done = true;
			return false;
		}

		return true;
	}

	public bool LoadFromString(string csv)
	{
		if (done)
		{
			return false;
		}

		fileName = null;
		fileText = csv;

		try
		{
			thread = new Thread(new ThreadStart(Run));
			thread.Start();
		}
		catch (Exception e)
		{
			errorMsg = e.Message;
			success = false;
			done = true;
			return false;
		}

		return true;
	}

	public void Run()
	{
		String[] lines;

		try
		{
			if (fileText == null)
			{
				lines = System.IO.File.ReadAllLines(fileName);
			}
			else
			{
				lines = fileText.Split('\n');
			}
			Parse(lines);
		}
		catch (Exception e)
		{
			errorMsg = e.Message;
			success = false;
		}

		if (headersInFirstRow)
		{
			SetHeaders(GetRow(0));
		}

		done = true;
	}

	public void WaitUntilLoaded()
	{
		if ((thread != null) && !done)
		{
			thread.Join();
		}
	}

	public enum ParsingState
	{
		normal,
		escaped,
		quoted,
		complete
	}

	public void Parse(String[] lines)
	{
		String line;
		StringBuilder buffer = new StringBuilder();
		ParsingState state;
		int iCell;

		for (int iCurrentLine = 0; iCurrentLine < lines.Length; iCurrentLine++)
		{
			iCell = 0;
			buffer.Length = 0;
			state = ParsingState.normal;
			line = lines[iCurrentLine];
			if (line.Trim().Length == 0)
			{
				continue;
			}
			cells.Add(new List<String>());

			foreach (Char c in line)
			{
				switch (state)
				{
					case ParsingState.normal:
						if (c == '"')
						{
							state = ParsingState.quoted;
							foreach (Char bc in buffer.ToString())
							{
								if (!Char.IsWhiteSpace(bc))
								{
									warning = true;
									break;
								}
							}
							buffer.Length = 0;
						}
						else if (c == ',')
						{
							cells[iCurrentLine].Add(buffer.ToString());
							buffer.Length = 0;
							iCell++;
						}
						else
						{
							buffer.Append(c);
						}
						break;

					case ParsingState.quoted:
						if (c == '\\')
						{
							state = ParsingState.escaped;
						}
						else if (c == '"')
						{
							state = ParsingState.complete;
						}
						else
						{
							buffer.Append(c);
						}

						break;

					case ParsingState.escaped:

						if (c == '"')
						{
							buffer.Append('"');
						}
						else if (c == '\\')
						{
							buffer.Append('\\');
						}
						else if (c == 't')
						{
							buffer.Append('\t');
						}
						else if (c == 'r')
						{
							buffer.Append('\r');
						}
						else if (c == 'n')
						{
							buffer.Append('\n');
						}
						state = ParsingState.quoted;

						break;

					case ParsingState.complete:
						if (c == ',')   // Next value follows comma...
						{
							cells[iCurrentLine].Add(buffer.ToString());
							buffer.Length = 0;
							state = ParsingState.normal;
							iCell++;
						}
						else if (!Char.IsWhiteSpace(c))
						{
							warning = true;
						}

						break;
				}
			}

			if (state == ParsingState.quoted || state == ParsingState.escaped)
			{
				warning = true;
			}

			cells[iCurrentLine].Add(buffer.ToString());
		}
	}

	public void Abort()
	{
		if ((thread != null) && !done)
		{
			thread.Abort();
		}
	}

	public int GetCellInt(int row, int col)
	{
		return GetCellInt(row, col, 0);
	}

	public int GetCellInt(int row, int col, int defaultValue)
	{
		int value;

		if (!done || !success)
		{
			return defaultValue;
		}

		if (headersInFirstRow)
		{
			if (row < 0 || row >= cells.Count - 1 || col < 0 || col > cells[row + 1].Count)
			{
				return defaultValue;
			}

			if (!Int32.TryParse(cells[row + 1][col], out value))
			{
				return defaultValue;
			}
		}
		else
		{
			if (row < 0 || row >= cells.Count || col < 0 || col > cells[row].Count)
			{
				return defaultValue;
			}

			if (!Int32.TryParse(cells[row][col], out value))
			{
				return defaultValue;
			}
		}

		return value;
	}

	public double GetCellDouble(int row, int col)
	{
		return GetCellDouble(row, col, 0.0);
	}

	public double GetCellDouble(int row, int col, double defaultValue)
	{
		double value;

		if (!done || !success)
		{
			return defaultValue;
		}

		if (headersInFirstRow)
		{
			if (row < 0 || row >= cells.Count - 1 || col < 0 || col > cells[row + 1].Count)
			{
				return defaultValue;
			}

			if (!Double.TryParse(cells[row + 1][col], out value))
			{
				return defaultValue;
			}
		}
		else
		{
			if (row < 0 || row >= cells.Count || col < 0 || col > cells[row].Count)
			{
				return defaultValue;
			}

			if (!double.TryParse(cells[row][col], out value))
			{
				return defaultValue;
			}
		}

		return value;
	}

	public string GetCellString(int row, int col)
	{
		if (!done || !success)
		{
			return null;
		}

		if (headersInFirstRow)
		{
			if (row < 0 || row >= cells.Count - 1 || col < 0 || col > cells[row + 1].Count)
			{
				return null;
			}

			return cells[row + 1][col];
		}

		if (row < 0 || row >= cells.Count || col < 0 || col > cells[row].Count)
		{
			return null;
		}

		return cells[row][col];

	}

	public int GetCellInt(int row, String columnName)
	{
		return GetCellInt(row, GetColumnIndex(columnName));
	}

	public int GetCellInt(int row, String columnName, int defaultValue)
	{
		return GetCellInt(row, GetColumnIndex(columnName), defaultValue);
	}

	public double GetCellDouble(int row, String columnName)
	{
		return GetCellDouble(row, GetColumnIndex(columnName));
	}

	public double GetCellDouble(int row, String columnName, double defaultValue)
	{
		return GetCellDouble(row, GetColumnIndex(columnName), defaultValue);
	}

	public String GetCellString(int row, String columnName)
	{
		return GetCellString(row, GetColumnIndex(columnName));
	}

	public void SetHeadersAreInFirstRow()
	{
		headersInFirstRow = true;

		if (done)
		{
			SetHeaders(GetRow(0));
		}
	}

	public int GetColumnIndex(string name)
	{
		int nameIndex;

		if (headerNames == null)
		{
			return -1;
		}

		nameIndex = Array.BinarySearch(headerNames, name);
		if (nameIndex < 0)
		{
			return -1;
		}

		return headerIndices[nameIndex];
	}

	public void SetHeaders(string[] headers)
	{
		if (headers == null)
		{
			return;
		}
		headerNames = new string[headers.Length];
		headerIndices = new int[headers.Length];

		Array.Copy(headers, headerNames, headers.Length);
		for (int n = 0; n < headers.Length; n++)
		{
			headerIndices[n] = n;
		}

		Array.Sort(headerNames, headerIndices);
	}

	public string[] GetRow(int row)
	{
		if (!done || !success)
		{
			return null;
		}

		if (row < 0 || row >= cells.Count)
		{
			return null;
		}

		return cells[row].ToArray();
	}

	public int GetRowCount()
	{
		if (!done || !success)
		{
			return -1;
		}

		if (headersInFirstRow)
		{
			return cells.Count - 1;
		}
		else
		{
			return cells.Count;
		}
	}
}