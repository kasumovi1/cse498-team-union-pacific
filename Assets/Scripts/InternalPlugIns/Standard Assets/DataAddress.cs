using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class DataAddress
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr DataAddress_GetAddress(IntPtr dataAddress);
		[DllImport("TMTSPlugIns")]
		private static extern int DataAddress_GetId(IntPtr dataAddress);
		[DllImport("TMTSPlugIns")]
		private static extern void DataAddress_SetWatcher(IntPtr dataAddress, IntPtr watcher);
		[DllImport("TMTSPlugIns")]
		private static extern PlugIn DataAddress_GetWatcher(IntPtr dataAddress);

		internal IntPtr innerDataAddress;

		public DataAddress(IntPtr inner) { innerDataAddress = inner; }
		public IntPtr GetAddress() { return DataAddress_GetAddress(innerDataAddress); }
		public int GetId() { return DataAddress_GetId(innerDataAddress); }
		public void SetWatcher(PlugIn watcher) { DataAddress_SetWatcher(innerDataAddress, watcher.innerPlugIn); }
		public PlugIn GetWatcher() { return DataAddress_GetWatcher(innerDataAddress); }

	}
}