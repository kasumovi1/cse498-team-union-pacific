using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInMessageImpl : PlugInMessage
	{
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInMessageImpl_IsQueued(IntPtr plugInMessageImpl);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInMessageImpl_IsDelivered(IntPtr plugInMessageImpl);

		private static PlugInMessageImpl[] unused;
		private static int unusedIndex;
		private static int reuseLockFlag;

		public PlugInMessageImpl(IntPtr inner) : base(inner) { }
		public bool IsQueued() { return PlugInMessageImpl_IsQueued(innerPlugInMessage); }
		public bool IsDelivered() { return PlugInMessageImpl_IsDelivered(innerPlugInMessage); }

		static PlugInMessageImpl()
		{
			unused = new PlugInMessageImpl[100];
			unusedIndex = 0;
			reuseLockFlag = 0;
		}

		new public static PlugInMessageImpl New(IntPtr inner)
		{
			PlugInMessageImpl msg;

			acquireLock(ref reuseLockFlag);
			// Don't use native C# lock

			if (unusedIndex == 0)
			{
				releaseLock(ref reuseLockFlag);
				return new PlugInMessageImpl(inner);
			}

			msg = unused[--unusedIndex];

			releaseLock(ref reuseLockFlag);

			msg.innerPlugInMessage = inner;

			return msg;
		}

		public static void Reuse(PlugInMessageImpl msg)
		{
			if (msg == null)
			{
				return;
			}

			msg.innerPlugInMessage = IntPtr.Zero;

			acquireLock(ref reuseLockFlag);
			// Don't use native C# lock

			if (unusedIndex < 100)
			{
				unused[unusedIndex++] = msg;
			}

			releaseLock(ref reuseLockFlag);
		}

		public override string ToString()
		{
			string output = "{";
				output += args.msg.ToString();
			output += ", ";
			output += args.iParam.ToString();
			output += ", ";
			output += args.dParam.ToString();
			output += ", \"";
			output += args.sParam.c_str();
			output += "\", [";
			for (int i = 0; i < args.vParam.size(); i++)
			{
				if (0 < i)
				{
					output += ", ";
				}
				output += args.vParam.at(i).ToString();
			}
			output += "]} ";
			PlugIn plugIn = GetSource();
			output += (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn) ? plugIn.GetName().c_str() : "???";
			output += "->";
			plugIn = GetTarget();
			output += (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn) ? plugIn.GetName().c_str() : "???";
			return output;
		}
	}
}