using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInManager
	{
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInManager_GetId(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInManager_GetSystem(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInManager_CreatePlugIn(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInManager_LoadPlugIn(IntPtr plugInManager, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInManager_InitializePlugIn(IntPtr plugInManager, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInManager_DestroyPlugIn(IntPtr plugInManager, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInManager_ShutDown(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInManager_DeliverMessage(IntPtr plugInManager, IntPtr plugIn, IntPtr msg);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInManager_GetName(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInManager_GetPlugInFileExtension(IntPtr plugInManager);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInManager_CleanUpMessages(IntPtr plugInManager);

		internal IntPtr innerPlugInManager;

		public PlugInManager(IntPtr inner) { innerPlugInManager = inner; }
		public virtual int GetId() { return PlugInManager_GetId(innerPlugInManager); }
		public virtual PlugInSystem GetSystem() { return new PlugInSystem(PlugInManager_GetSystem(innerPlugInManager)); }
		public virtual PlugInImpl CreatePlugIn() { return new PlugInImpl(PlugInManager_CreatePlugIn(innerPlugInManager)); }
		public virtual bool LoadPlugIn(PlugInImpl plugIn) { return PlugInManager_LoadPlugIn(innerPlugInManager, plugIn.innerPlugIn); }
		public virtual bool InitializePlugIn(PlugInImpl plugIn) { return PlugInManager_InitializePlugIn(innerPlugInManager, plugIn.innerPlugIn); }
		public virtual void DestroyPlugIn(PlugInImpl plugIn) { PlugInManager_DestroyPlugIn(innerPlugInManager, plugIn.innerPlugIn); }
		public virtual void ShutDown() { PlugInManager_ShutDown(innerPlugInManager); }
		public virtual int DeliverMessage(PlugInImpl plugIn, PlugInMessageImpl msg) { return PlugInManager_DeliverMessage(innerPlugInManager, plugIn.innerPlugIn, msg.innerPlugInMessage); }
		public virtual AbstractString GetName() { return new AbstractString(PlugInManager_GetName(innerPlugInManager)); }
		public virtual AbstractString GetPlugInFileExtension() { return new AbstractString(PlugInManager_GetPlugInFileExtension(innerPlugInManager)); }
		public virtual void CleanUpMessages() { PlugInManager_CleanUpMessages(innerPlugInManager); }
	}
}