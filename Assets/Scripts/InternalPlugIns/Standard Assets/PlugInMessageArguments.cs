using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInMessageArguments
	{
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInMessageArguments_GetMsg(IntPtr plugInMessageArguments);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInMessageArguments_SetMsg(IntPtr plugInMessageArguments, int msg);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInMessageArguments_GetIParam(IntPtr plugInMessageArguments);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInMessageArguments_SetIParam(IntPtr plugInMessageArguments, int iParam);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInMessageArguments_GetDParam(IntPtr plugInMessageArguments);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInMessageArguments_SetDParam(IntPtr plugInMessageArguments, double dParam);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessageArguments_GetSParam(IntPtr plugInMessageArguments);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessageArguments_GetVParam(IntPtr plugInMessageArguments);

		internal IntPtr innerPlugInMessageArguments;

		public PlugInMessageArguments(IntPtr inner) { innerPlugInMessageArguments = inner; }

		internal void UpdateInner(IntPtr inner) { innerPlugInMessageArguments = inner; }


		public int msg
		{
			get { return PlugInMessageArguments_GetMsg(innerPlugInMessageArguments); }
			set { PlugInMessageArguments_SetMsg(innerPlugInMessageArguments, value); }
		}
		public int iParam
		{
			get { return PlugInMessageArguments_GetIParam(innerPlugInMessageArguments); }
			set { PlugInMessageArguments_SetIParam(innerPlugInMessageArguments, value); }
		}
		public double dParam
		{
			get { return PlugInMessageArguments_GetDParam(innerPlugInMessageArguments); }
			set { PlugInMessageArguments_SetDParam(innerPlugInMessageArguments, value); }
		}

		private AbstractString _sParam = null;
		public AbstractString sParam
		{
			get
			{
				IntPtr innerAbstractString = PlugInMessageArguments_GetSParam(innerPlugInMessageArguments);
				if (null == _sParam || _sParam.innerAbstractString != innerAbstractString)
				{
					_sParam = new AbstractString(innerAbstractString);
				}
				return _sParam;
			}
		}
		private AbstractVectorInt _vParam = null;
		public AbstractVectorInt vParam
		{
			get
			{
				IntPtr innerAbstractVectorInt = PlugInMessageArguments_GetVParam(innerPlugInMessageArguments);
				if (null == _vParam || _vParam.innerAbstractVectorInt != innerAbstractVectorInt)
				{
					_vParam = new AbstractVectorInt(innerAbstractVectorInt);
				}
				return _vParam;
			}
		}
	}
}