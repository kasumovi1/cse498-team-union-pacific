using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugIn
	{
		[DllImport("TMTSPlugIns")]
		private static extern int PlugIn_GetId(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugIn_GetName(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugIn_GetFilePath(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugIn_IsLoaded(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugIn_Load(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugIn_IsInitialized(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugIn_Initialize(IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugIn_IsInitializing(IntPtr plugIn);

		public IntPtr innerPlugIn;

		public PlugIn(IntPtr plugIn) { innerPlugIn = plugIn; }

		public int GetId() { return PlugIn_GetId(innerPlugIn); }
		public AbstractString GetName() { return new AbstractString(PlugIn_GetName(innerPlugIn)); }
		public AbstractString GetFilePath() { return new AbstractString(PlugIn_GetFilePath(innerPlugIn)); }
		public bool IsLoaded() { return PlugIn_IsLoaded(innerPlugIn); }
		public bool Load() { return PlugIn_Load(innerPlugIn); }
		public bool IsInitialized() { return PlugIn_IsInitialized(innerPlugIn); }
		public bool Initialize() { return PlugIn_Initialize(innerPlugIn); }
		public bool IsInitializing() { return PlugIn_IsInitializing(innerPlugIn); }
	}
}