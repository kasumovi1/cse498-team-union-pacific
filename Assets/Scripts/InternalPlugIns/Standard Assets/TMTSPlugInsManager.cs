using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace TMTSPluginsCS
{
	public class TMTSPlugInsManager : MonoBehaviour
	{
		public static BasePlugInSystem plugInSystem = null;
		private static InternalPlugInManager internalPlugInManager;
		private static string pluginDirectory;

		public static void LaunchPlugInSystem(string plugInDirectory)
		{
			pluginDirectory = plugInDirectory;

			PlugInFilePathLookup_FunctionPointerSet(Marshal.GetFunctionPointerForDelegate(plugInFilePathLookup_Delegate));
			LogTMTSDataReplacement_FunctionPointerSet(Marshal.GetFunctionPointerForDelegate(logTMTSDataReplacement_Delegate));
			FlushTMTSLogReplacement_FunctionPointerSet(Marshal.GetFunctionPointerForDelegate(flushTMTSLogReplacement_Delegate));

			plugInSystem = new BasePlugInSystem();
			internalPlugInManager = new InternalPlugInManager();

			plugInSystem.RegisterPlugInManager(internalPlugInManager);
		}

		public static void AddDefaultListeners()
		{

		}

		public static bool RegisterInternalPlugIn(InternalPlugIn plugIn, string name, string path)
		{
			if (null == plugInSystem || IntPtr.Zero == plugInSystem.innerPlugInSystem)
			{
				return false;
			}

			if (!plugInSystem.RegisterPlugIn(plugIn, name, path, internalPlugInManager))
			{
				return false;
			}

			internalPlugInManager.StoreInternalPlugIn(plugIn);

			PlugInMessage m = plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_STARTUP, 0, 0.0);
			if (null == m || IntPtr.Zero == m.innerPlugInMessage)
			{
				return false;
			}

			plugInSystem.ScheduleMessage(m, -1, plugIn.GetId(), 0.0, 0.0);

			PlugInMessage.Reuse(m);

			return true;
		}

		public static void StartRunPlugIns(params string[] contentFiles)
		{
			foreach (string plugInFilePath in contentFiles)
			{
				string directory = Path.GetDirectoryName(plugInFilePath);
				if (directory.EndsWith("RUN"))
				{
					LoadAndStartPlugIn(plugInFilePath);
				}
			}
		}

		public static PlugIn LoadAndStartPlugIn(string plugInFilePath)
		{
			string plugInFile = Path.GetFileName(plugInFilePath);
			bool created = true;
			PlugIn plugIn = plugInSystem.GetPlugInFromChars(plugInFile, ref created);

			if (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn)
			{
				if (created)
				{
					PlugInMessage m = plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_STARTUP, 0, 0.0);
					if (null != m && IntPtr.Zero != m.innerPlugInMessage)
					{
						plugInSystem.ScheduleMessage(m, -1, plugIn.GetId(), 0.0, 0.0);
						PlugInMessage.Reuse(m);
					}
				}
			}

			return plugIn;
		}

		[DllImport("TMTSPlugIns")]
		private static extern void PlugInFilePathLookup_FunctionPointerSet(IntPtr plugInFilePathLookup_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void LogTMTSDataReplacement_FunctionPointerSet(IntPtr logTMTSDataReplacement_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void FlushTMTSLogReplacement_FunctionPointerSet(IntPtr flushTMTSLogReplacement_FunctionPointer);

		private delegate bool PlugInFilePathLookup_Delegate(IntPtr name, IntPtr path, int maxPath);
		private delegate void LogTMTSDataReplacement_Delegate(IntPtr data);
		private delegate void FlushTMTSLogReplacement_Delegate();

		private static PlugInFilePathLookup_Delegate plugInFilePathLookup_Delegate = new PlugInFilePathLookup_Delegate(PlugInFilePathLookup_Callback);
		private static LogTMTSDataReplacement_Delegate logTMTSDataReplacement_Delegate = new LogTMTSDataReplacement_Delegate(LogTMTSDataReplacement_Callback);
		private static FlushTMTSLogReplacement_Delegate flushTMTSLogReplacement_Delegate = new FlushTMTSLogReplacement_Delegate(FlushTMTSLogReplacement_Callback);

		private static string[] supportedPlugInExtensions = { ".dll", ".ndll" };
		private static bool PlugInFilePathLookup_Callback(IntPtr name, IntPtr path, int maxPath)
		{
			
			string sName = Marshal.PtrToStringAnsi(name);
			string sPath = null;
			foreach (string extension in supportedPlugInExtensions)
			{
				string[] files = System.IO.Directory.GetFiles(Directory.GetCurrentDirectory() + @"\SimulationBuild\General Content\Trains", sName + extension, SearchOption.TopDirectoryOnly);
				if (files != null && files.Length > 0)
				{
					sPath = files[0];
					break;
				}
			}
			if (null == sPath)
			{
				if (internalPlugInManager.IsDelayLoadedInternalPlugIn(sName))
				{
					sPath = sName + ".?";
				}
				else
				{
					return false;
				}
			}
			int i = 0;
			for (; i < sPath.Length && i < maxPath - 1; i++)
			{
				Marshal.WriteByte(path, i, (byte)sPath[i]);
			}
			Marshal.WriteByte(path, i, (byte)0);
			Console.WriteLine("Name: " + sName + "    Path: " + sPath);
			if (i == maxPath - 1)
			{
				return false;
			}
			return true;
		}
		private static void LogTMTSDataReplacement_Callback(IntPtr data)
		{
			string s = Marshal.PtrToStringAnsi(data);
			if (s.EndsWith("\n"))
			{
				s = s.Substring(0, s.Length - 1);
			}
			Console.WriteLine(s);
			//Log.Write(s);
		}
		private static void FlushTMTSLogReplacement_Callback()
		{
			//Log.Flush();
		}

		[DllImport("TMTSPlugIns")]
		private static extern int TMTSMessageConstant(IntPtr name, IntPtr creationFlag);

		public static int MessageConstant(string name, ref bool creationFlag)
		{
			IntPtr pName = Marshal.StringToHGlobalAnsi(name);
			int iCreate = (creationFlag) ? 1 : 0;
			GCHandle hCreate = GCHandle.Alloc(iCreate, GCHandleType.Pinned);
			IntPtr pCreate = hCreate.AddrOfPinnedObject();
			int ret = TMTSMessageConstant(pName, pCreate);
			creationFlag = (iCreate != 0);
			hCreate.Free();
			Marshal.FreeHGlobal(pName);
			if (creationFlag)
			{
				Console.WriteLine("New Msg Const - Name: {0}    Value: {1}", name, ret);
			}
			return ret;
		}
		public static int MessageConstant(string name)
		{
			bool creationFlag = true;
			return MessageConstant(name, ref creationFlag);
		}

		public static PlugIn GetPlugIn(string name)
		{
			bool create = false;
			return GetPlugIn(name, ref create);
		}
		public static PlugIn GetPlugIn(string name, ref bool create)
		{
			AbstractString aName = plugInSystem.CreateAbstractString();
			aName.assign(name);

			PlugIn plugIn = plugInSystem.GetPlugIn(aName, ref create);
			plugInSystem.DestroyAbstractString(aName);
			if (IntPtr.Zero == plugIn.innerPlugIn)
			{
				return null;
			}

			return plugIn;
		}
		public static int GetPlugInId(string name)
		{
			bool create = false;
			return GetPlugInId(name, ref create);
		}
		public static int GetPlugInId(string name, ref bool create)
		{
			PlugIn plugIn = GetPlugIn(name, ref create);
			if (null == plugIn || IntPtr.Zero == plugIn.innerPlugIn)
			{
				create = false;
				return -1;
			}
			return plugIn.GetId();
		}
		public static string GetPlugInName(int id)
		{
			PlugIn plugIn = TMTSPlugInsManager.plugInSystem.GetPlugInFromId(id);
			if (plugIn != null && plugIn.innerPlugIn != IntPtr.Zero)
			{
				AbstractString abString = plugIn.GetName();
				if (abString != null && abString.innerAbstractString != IntPtr.Zero)
				{
					return abString.c_str();
				}
			}
			return "";
		}


		private static int CurrentMessageTarget
		{
			get
			{
				int ret = -1;
				if (null != plugInSystem)
				{
					PlugInMessage m = plugInSystem.GetCurrentMessage();

					if (null != m && IntPtr.Zero != m.innerPlugInMessage)
					{
						ret = m.GetTargetId();
					}

					if (m != null)
					{
						PlugInMessage.Reuse(m);     // Reusing managed wrapper ONLY. Reuse minimizes garbage collection.
					}
				}
				return ret;
			}
		}

		public static void PostMessageSimTime(string plugInName, int message, double delay, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PostMessageSimTime(GetPlugInId(plugInName), message, delay, iParam, dParam, sParam, vParam);
		}
		public static void PostMessageSimTime(int plugInId, int message, double delay, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PlugInMessage m = plugInSystem.CreateMessage(message, iParam, dParam);
			if (null != sParam)
			{
				m.sParam.assign(sParam);
			}
			if (null != vParam && 0 < vParam.Length)
			{
				m.vParam.clear();
				foreach (int i in vParam)
				{
					m.vParam.push_back(i);
				}
			}
			plugInSystem.ScheduleMessage(m, CurrentMessageTarget, plugInId, 0.0, delay);

			PlugInMessage.Reuse(m);     // Reuse managed wrapper ONLY. Minimizes garbage collection.
		}

		public static void PostMessage(string plugInName, int message, double delay, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PostMessage(GetPlugInId(plugInName), message, delay, iParam, dParam, sParam, vParam);
		}
		public static void PostMessage(int plugInId, int message, double delay, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PlugInMessage m = plugInSystem.CreateMessage(message, iParam, dParam);
			if (null != sParam)
			{
				m.sParam.assign(sParam);
			}
			if (null != vParam && 0 < vParam.Length)
			{
				m.vParam.clear();
				foreach (int i in vParam)
				{
					m.vParam.push_back(i);
				}
			}
			plugInSystem.ScheduleMessage(m, CurrentMessageTarget, plugInId, delay, 0.0);

			PlugInMessage.Reuse(m);     // Reuse managed wrapper ONLY. Minimizes garbage collection.
		}

		public static int SendMessage(string plugInName, int message, int iParam, double dParam, string sParam, params int[] vParam)
		{
			return SendMessage(GetPlugInId(plugInName), message, iParam, dParam, sParam, vParam);
		}
		public static int SendMessage(int plugInId, int message, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PlugInMessage m = plugInSystem.CreateMessage(message, iParam, dParam);
			if (null != sParam)
			{
				m.sParam.assign(sParam);
			}
			if (null != vParam && 0 < vParam.Length)
			{
				m.vParam.clear();
				foreach (int i in vParam)
				{
					m.vParam.push_back(i);
				}
			}
			int ret = plugInSystem.DeliverMessageNow(m, CurrentMessageTarget, plugInId);
			plugInSystem.DestroyMessage(m);
			return ret;
		}

		public static int DefaultActivityPlugInId
		{
			get
			{
				int ret = -1;
				if (null != plugInSystem)
				{
					PlugIn plugIn = plugInSystem.GetDefaultActivityPlugIn();
					if (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn)
					{
						ret = plugIn.GetId();
					}
				}
				return ret;
			}
		}

		public static int DefaultKeyboardPlugInId
		{
			get
			{
				int ret = -1;
				if (null != plugInSystem)
				{
					PlugIn plugIn = plugInSystem.GetDefaultKeyboardPlugIn();
					if (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn)
					{
						ret = plugIn.GetId();
					}
				}
				return ret;
			}
		}

		public static int DefaultMousePlugInId
		{
			get
			{
				int ret = -1;
				if (null != plugInSystem)
				{
					PlugIn plugIn = plugInSystem.GetDefaultMousePlugIn();
					if (null != plugIn && IntPtr.Zero != plugIn.innerPlugIn)
					{
						ret = plugIn.GetId();
					}
				}
				return ret;
			}
		}

		//public static void Initialize()
		//{
		//	LaunchPlugInSystem();
		//	//LoadStoredVariables();
		//	foreach (string extension in supportedPlugInExtensions)
		//	{
		//		StartRunPlugIns(extension);
		//	}
		//}

		//public static void Manage()
		//{
		//	plugInSystem.UpdateRealTime(GlobalSettings.realTime * 1000.0, GlobalSettings.realTimeDelta * 1000.0);
		//	plugInSystem.UpdateSimTime(GlobalSettings.gameTime * 1000.0, GlobalSettings.gameTimeDelta * 1000.0);
		//	plugInSystem.DeliverMessages();
		//}

		public static void Reset()
		{
			if (plugInSystem != null)
			{
				PlugInMessage msg = plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_SHUTDOWN, 0, 0.0);
				plugInSystem.DeliverMessageNow(msg, -1, ScriptConstants.BROADCAST_HANDLE);
				plugInSystem.DestroyMessage(msg);
				plugInSystem.ShutDown();
				plugInSystem = null;
			}

			internalPlugInManager = null;
		}

		//public static void SaveStoredVariables(ref string saveStatePath)
		//{
		//	saveStatePath = ContentIndexer.SelectedActivity.Path + @"\SavedState";
		//	System.IO.Directory.CreateDirectory(saveStatePath);

		//	string saveFileName = saveStatePath + @"\storedVariables.xml";
		//	plugInSystem.StoreVariables(saveFileName);
		//}
		//public static void LoadStoredVariables()
		//{
		//	string saveStatePath = ContentIndexer.SelectedActivity.Path + @"\SavedState\storedVariables.xml";
		//	plugInSystem.LoadVariables(saveStatePath);
		//}

		public static int RegisterForChannel(int plugInId, string channel)
		{
			return RegisterForChannel(GetPlugInName(plugInId), channel);
		}

		public static int RegisterForChannel(string plugInName, string channel)
		{
			return SendMessage(ScriptConstants.BROADCAST_HANDLE, ScriptConstants.TMTS_MSG_REGISTER, 1, 0.0, plugInName + "#" + channel);
		}
	}
}