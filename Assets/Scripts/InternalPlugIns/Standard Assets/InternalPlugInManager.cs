using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class InternalPlugInManager : PlugInManager
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr CreateCallbackPlugInManager();
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_CreatePlugIn_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_CreatePlugIn_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_LoadPlugIn_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_LoadPlugIn_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_InitializePlugIn_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_InitializePlugIn_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_DestroyPlugIn_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_DestroyPlugIn_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_ShutDown_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_ShutDown_FunctionPointer);
		[DllImport("TMTSPlugIns")]
		private static extern void CallbackPlugInManager_DeliverMessage_FunctionPointerSet(IntPtr plugInManager, IntPtr plugInManager_DeliverMessage_FunctionPointer);

		private delegate IntPtr PlugInManager_CreatePlugIn_Delegate();
		private delegate bool PlugInManager_LoadPlugIn_Delegate(IntPtr plugIn);
		private delegate bool PlugInManager_InitializePlugIn_Delegate(IntPtr plugIn);
		private delegate void PlugInManager_DestroyPlugIn_Delegate(IntPtr plugIn);
		private delegate void PlugInManager_ShutDown_Delegate();
		private delegate int PlugInManager_DeliverMessage_Delegate(IntPtr plugIn, IntPtr msg);

		private PlugInManager_CreatePlugIn_Delegate plugInManager_CreatePlugIn_Delegate = new PlugInManager_CreatePlugIn_Delegate(PlugInManager_CreatePlugIn_Callback);
		private PlugInManager_LoadPlugIn_Delegate plugInManager_LoadPlugIn_Delegate = new PlugInManager_LoadPlugIn_Delegate(PlugInManager_LoadPlugIn_Callback);
		private PlugInManager_InitializePlugIn_Delegate plugInManager_InitializePlugIn_Delegate = new PlugInManager_InitializePlugIn_Delegate(PlugInManager_InitializePlugIn_Callback);
		private PlugInManager_DestroyPlugIn_Delegate plugInManager_DestroyPlugIn_Delegate = new PlugInManager_DestroyPlugIn_Delegate(PlugInManager_DestroyPlugIn_Callback);
		private PlugInManager_ShutDown_Delegate plugInManager_ShutDown_Delegate = new PlugInManager_ShutDown_Delegate(PlugInManager_ShutDown_Callback);
		private PlugInManager_DeliverMessage_Delegate plugInManager_DeliverMessage_Delegate = new PlugInManager_DeliverMessage_Delegate(PlugInManager_DeliverMessage_Callback);

		private static InternalPlugInManager instance;

		private static IntPtr PlugInManager_CreatePlugIn_Callback() { return instance.CreatePlugIn().innerPlugIn; }
		private static bool PlugInManager_LoadPlugIn_Callback(IntPtr plugIn) { return instance.LoadPlugIn(new PlugInImpl(plugIn)); }
		private static bool PlugInManager_InitializePlugIn_Callback(IntPtr plugIn) { return instance.InitializePlugIn(new PlugInImpl(plugIn)); }
		private static void PlugInManager_DestroyPlugIn_Callback(IntPtr plugIn) { instance.DestroyPlugIn(new PlugInImpl(plugIn)); }
		private static void PlugInManager_ShutDown_Callback() { instance.ShutDown(); }
		private static int PlugInManager_DeliverMessage_Callback(IntPtr plugIn, IntPtr msg)
		{
			PlugInMessageImpl msgImpl = PlugInMessageImpl.New(msg);
			int result = instance.DeliverMessage(new PlugInImpl(plugIn), msgImpl);
			PlugInMessageImpl.Reuse(msgImpl);
			// It's ok to do this because the inner message is about to be destroyed anyway.
			return result;
		}

		private Dictionary<string, Type> delayLoadedInternalPlugIns = new Dictionary<string, Type>();
		public InternalPlugInManager() : base(CreateCallbackPlugInManager())
		{
			instance = this;
			GetName().assign("Internal");
			GetPlugInFileExtension().assign("?");
			CallbackPlugInManager_CreatePlugIn_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_CreatePlugIn_Delegate));
			CallbackPlugInManager_LoadPlugIn_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_LoadPlugIn_Delegate));
			CallbackPlugInManager_InitializePlugIn_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_InitializePlugIn_Delegate));
			CallbackPlugInManager_DestroyPlugIn_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_DestroyPlugIn_Delegate));
			CallbackPlugInManager_ShutDown_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_ShutDown_Delegate));
			CallbackPlugInManager_DeliverMessage_FunctionPointerSet(innerPlugInManager, Marshal.GetFunctionPointerForDelegate(plugInManager_DeliverMessage_Delegate));
		}

		public bool IsDelayLoadedInternalPlugIn(string s)
		{
			return delayLoadedInternalPlugIns.ContainsKey(s);
		}

		public override PlugInImpl CreatePlugIn()
		{
			InternalPlugIn internalPlugIn = new InternalPlugIn();
			internalPlugInStorage[internalPlugIn.innerPlugIn] = null;
			return internalPlugIn;
		}

		public override bool LoadPlugIn(PlugInImpl plugIn)
		{
			return internalPlugInStorage.ContainsKey(plugIn.innerPlugIn);
		}

		public override bool InitializePlugIn(PlugInImpl plugIn)
		{
			if (!internalPlugInStorage.ContainsKey(plugIn.innerPlugIn))
			{
				return false;
			}
			InternalPlugIn internalPlugIn = internalPlugInStorage[plugIn.innerPlugIn];
			if (null != internalPlugIn)
			{
				return true;
			}
			string plugInName = plugIn.GetName().c_str();

			if (IsDelayLoadedInternalPlugIn(plugInName))
			{
				Type type = delayLoadedInternalPlugIns[plugInName];
				internalPlugIn = Activator.CreateInstance(type) as InternalPlugIn;
				internalPlugIn.innerPlugIn = plugIn.innerPlugIn;
				internalPlugInStorage[plugIn.innerPlugIn] = internalPlugIn;

				PlugInMessage m = TMTSPlugInsManager.plugInSystem.CreateMessage(ScriptConstants.TMTS_MSG_STARTUP, 0, 0.0);
				if (null == m || IntPtr.Zero == m.innerPlugInMessage)
				{
					return false;
				}

				TMTSPlugInsManager.plugInSystem.DeliverMessageNow(m, -1, plugIn.GetId());
				TMTSPlugInsManager.plugInSystem.DestroyMessage(m);

				return true;
			}
			return false;
		}

		public override void DestroyPlugIn(PlugInImpl plugIn)
		{
			// TODO delete plugIn;
		}

		public override void ShutDown()
		{
		}

		public override int DeliverMessage(PlugInImpl plugIn, PlugInMessageImpl msg)
		{
			InternalPlugIn ip = internalPlugInStorage[plugIn.innerPlugIn];
			int ret = 0;
			try
			{
				ret = ip.HandleMessage(msg);
			}
			catch (Exception e)
			{
				string errorString = "InternalPlugInManager found an exception while " + plugIn.GetName().c_str() + " handled a message";
				Console.WriteLine(errorString, e, msg.ToString());
			}
			return ret;
		}

		private Dictionary<IntPtr, InternalPlugIn> internalPlugInStorage = new Dictionary<IntPtr, InternalPlugIn>();
		public void StoreInternalPlugIn(InternalPlugIn plugIn)
		{
			if (!internalPlugInStorage.ContainsKey(plugIn.innerPlugIn))
			{
				internalPlugInStorage[plugIn.innerPlugIn] = plugIn;
			}
		}
	}
}