using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class AbstractString
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr AbstractString_c_str(IntPtr abstractString);
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractString_assign(IntPtr abstractString, IntPtr c);
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractString_append(IntPtr abstractString, IntPtr c);
		[DllImport("TMTSPlugIns")]
		private static extern char AbstractString_at(IntPtr abstractString, int index);
		[DllImport("TMTSPlugIns")]
		private static extern int AbstractString_size(IntPtr abstractString);
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractString_clear(IntPtr abstractString);

		internal IntPtr innerAbstractString;

		public AbstractString(IntPtr inner) { innerAbstractString = inner; }

		public string c_str() { return Marshal.PtrToStringAnsi(AbstractString_c_str(innerAbstractString)); }
		public void assign(string s)
		{
			IntPtr p = Marshal.StringToHGlobalAnsi(s);
			AbstractString_assign(innerAbstractString, p);
			Marshal.FreeHGlobal(p);
		}
		public void append(string s)
		{
			IntPtr p = Marshal.StringToHGlobalAnsi(s);
			AbstractString_append(innerAbstractString, p);
			Marshal.FreeHGlobal(p);
		}
		public char at(int index) { return AbstractString_at(innerAbstractString, index); }
		public int size() { return AbstractString_size(innerAbstractString); }
		public void clear() { AbstractString_clear(innerAbstractString); }
	}
}