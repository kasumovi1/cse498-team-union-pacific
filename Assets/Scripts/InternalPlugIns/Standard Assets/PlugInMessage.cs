using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInMessage
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessage_GetArgs(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessage_AddDataBlock(IntPtr plugInMessage, int byteCount);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessage_GetDataBlock(IntPtr plugInMessage, int index);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInMessage_GetDataBlockCount(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessage_GetSource(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInMessage_GetTarget(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInMessage_GetSourceId(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInMessage_GetTargetId(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInMessage_GetScheduledRealTime(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInMessage_GetScheduledSimTime(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInMessage_GetDeliveredRealTime(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInMessage_GetDeliveredSimTime(IntPtr plugInMessage);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInMessage_IsInDelivery(IntPtr plugInMessage);

		internal IntPtr innerPlugInMessage;
		private PlugInMessageArguments myArgs;


		private static PlugInMessage[] unused;
		private static int unusedIndex;
		private static int reuseLockFlag;

		public PlugInMessage(IntPtr inner)
		{
			innerPlugInMessage = inner;
			myArgs = null;
		}
		public PlugInMessageArguments GetArgs()
		{
			IntPtr innerArgs = PlugInMessage_GetArgs(innerPlugInMessage);
			if (myArgs == null)
			{
				myArgs = new PlugInMessageArguments(innerArgs);
			}
			else
			{
				myArgs.UpdateInner(innerArgs);
			}
			return myArgs;
		}
		public MessageDataBlock AddDataBlock(int byteCount) { return new MessageDataBlock(PlugInMessage_AddDataBlock(innerPlugInMessage, byteCount)); }
		public MessageDataBlock GetDataBlock(int index) { return new MessageDataBlock(PlugInMessage_GetDataBlock(innerPlugInMessage, index)); }
		public int GetDataBlockCount() { return PlugInMessage_GetDataBlockCount(innerPlugInMessage); }
		public PlugIn GetSource() { return new PlugIn(PlugInMessage_GetSource(innerPlugInMessage)); }
		public PlugIn GetTarget() { return new PlugIn(PlugInMessage_GetTarget(innerPlugInMessage)); }
		public int GetSourceId() { return PlugInMessage_GetSourceId(innerPlugInMessage); }
		public int GetTargetId() { return PlugInMessage_GetTargetId(innerPlugInMessage); }
		public double GetScheduledRealTime() { return PlugInMessage_GetScheduledRealTime(innerPlugInMessage); }
		public double GetScheduledSimTime() { return PlugInMessage_GetScheduledSimTime(innerPlugInMessage); }
		public double GetDeliveredRealTime() { return PlugInMessage_GetDeliveredRealTime(innerPlugInMessage); }
		public double GetDeliveredSimTime() { return PlugInMessage_GetDeliveredSimTime(innerPlugInMessage); }
		public bool IsInDelivery() { return PlugInMessage_IsInDelivery(innerPlugInMessage); }

		public PlugInMessageArguments args { get { return GetArgs(); } }

		public AbstractString sParam { get { return GetArgs().sParam; } }
		public AbstractVectorInt vParam { get { return GetArgs().vParam; } }

		static PlugInMessage()
		{
			unused = new PlugInMessage[100];
			unusedIndex = 0;
			reuseLockFlag = 0;
		}

		public static PlugInMessage New(IntPtr inner)
		{
			PlugInMessage msg;

			acquireLock(ref reuseLockFlag);
			// Don't use native C# lock

			if (unusedIndex == 0)
			{
				releaseLock(ref reuseLockFlag);
				return new PlugInMessage(inner);
			}

			msg = unused[--unusedIndex];

			releaseLock(ref reuseLockFlag);

			msg.innerPlugInMessage = inner;

			return msg;
		}

		public static void Reuse(PlugInMessage msg)
		{
			if (msg == null)
			{
				return;
			}

			msg.innerPlugInMessage = IntPtr.Zero;

			acquireLock(ref reuseLockFlag);
			// Don't use native C# lock

			if (unusedIndex < 100)
			{
				unused[unusedIndex++] = msg;
			}

			releaseLock(ref reuseLockFlag);
		}

		protected static void acquireLock(ref int lockFlag)
		{
			int iterations = 0;

			while (true)
			{
				if (Interlocked.CompareExchange(ref lockFlag, 1, 0) == 0)
				{
					break;
				}

				if ((++iterations % 100) == 0)
				{
					Thread.Sleep(0);
				}
			}
		}

		protected static void releaseLock(ref int lockFlag)
		{
			lockFlag = 0;
		}

		public override string ToString()
		{
			string output = "{";
			output += args.msg.ToString();
			output += ", ";
			output += args.iParam.ToString();
			output += ", ";
			output += args.dParam.ToString();
			output += ", \"";
			output += args.sParam.c_str();
			output += "\", [";
			for (int i = 0; i < args.vParam.size(); i++)
			{
				if (0 < i)
				{
					output += ", ";
				}
				output += args.vParam.at(i).ToString();
			}
			output += "]}";
			return output;
		}
	}
}