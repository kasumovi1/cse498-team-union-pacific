using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class MessageDataBlock
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr MessageDataBlock_GetAddress(IntPtr messageDataBlock);
		[DllImport("TMTSPlugIns")]
		private static extern int MessageDataBlock_GetByteCount(IntPtr messageDataBlock);
		[DllImport("TMTSPlugIns")]
		private static extern void MessageDataBlock_Clear(IntPtr messageDataBlock);

		internal IntPtr innerMessageDataBlock;

		public MessageDataBlock(IntPtr inner) { innerMessageDataBlock = inner; }
		public IntPtr GetAddress() { return MessageDataBlock_GetAddress(innerMessageDataBlock); }
		public int GetByteCount() { return MessageDataBlock_GetByteCount(innerMessageDataBlock); }
		public void Clear() { MessageDataBlock_Clear(innerMessageDataBlock); }
	}
}