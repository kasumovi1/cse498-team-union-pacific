using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInImpl : PlugIn
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr CreatePlugInImpl();
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInImpl_GetManager(IntPtr plugInImpl);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInImpl_GetMessagesHandled(IntPtr plugInImpl);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInImpl_GetSecondsSpentHandlingMessages(IntPtr plugInImpl);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInImpl_GetLongMessages(IntPtr plugInImpl);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInImpl_GetSecondsSpentHandlingLongMessages(IntPtr plugInImpl);

		public PlugInImpl() : base(CreatePlugInImpl()) { }
		public PlugInImpl(IntPtr inner) : base(inner) { }

		public PlugInManager GetManager() { return new PlugInManager(PlugInImpl_GetManager(innerPlugIn)); }
		public int GetMessagesHandled() { return PlugInImpl_GetMessagesHandled(innerPlugIn); }
		public double GetSecondsSpentHandlingMessages() { return PlugInImpl_GetSecondsSpentHandlingMessages(innerPlugIn); }
		public int GetLongMessages() { return PlugInImpl_GetLongMessages(innerPlugIn); }
		public double GetSecondsSpentHandlingLongMessages() { return PlugInImpl_GetSecondsSpentHandlingLongMessages(innerPlugIn); }

	}
}