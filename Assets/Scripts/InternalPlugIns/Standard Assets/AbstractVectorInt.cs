using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class AbstractVectorInt
	{
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractVectorInt_push_back(IntPtr abstractVectorInt, int v);
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractVectorInt_pop_back(IntPtr abstractVectorInt);
		[DllImport("TMTSPlugIns")]
		private static extern int AbstractVectorInt_at(IntPtr abstractVectorInt, int index);
		[DllImport("TMTSPlugIns")]
		private static extern int AbstractVectorInt_size(IntPtr abstractVectorInt);
		[DllImport("TMTSPlugIns")]
		private static extern void AbstractVectorInt_clear(IntPtr abstractVectorInt);

		internal IntPtr innerAbstractVectorInt;

		public AbstractVectorInt(IntPtr inner) { innerAbstractVectorInt = inner; }

		public void push_back(int v) { AbstractVectorInt_push_back(innerAbstractVectorInt, v); }
		public void pop_back() { AbstractVectorInt_pop_back(innerAbstractVectorInt); }
		public int at(int index)
		{
			if (index < 0 || index > size() - 1)
			{
				Console.WriteLine("AbstractVectorInt Exception " + index + " " + size());
				throw new ArgumentOutOfRangeException("index");
			}
			return AbstractVectorInt_at(innerAbstractVectorInt, index);
		}
		public int size() { return AbstractVectorInt_size(innerAbstractVectorInt); }
		public void clear() { AbstractVectorInt_clear(innerAbstractVectorInt); }
		public void ToIntArray(out int[] intArray)
		{
			intArray = new int[size()];
			for (int i = 0; i < size(); i++)
			{
				intArray[i] = at(i);
			}
		}
		public void ToDoubleArray(out double[] doubleArray)
		{
			doubleArray = new double[size()];
			for (int i = 0; i < size(); i++)
			{
				doubleArray[i] = Convert.ToDouble(at(i));
			}
		}
		public void ToBoolArray(out bool[] boolArray)
		{
			boolArray = new bool[size()];
			for (int i = 0; i < size(); i++)
			{
				boolArray[i] = (at(i) != 0);
			}
		}
	}
}