using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class BasePlugInSystem : PlugInSystem
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr CreateBasePlugInSystem();
		[DllImport("TMTSPlugIns")]
		private static extern bool BasePlugInSystem_RegisterPlugInManager(IntPtr basePlugInSystem, IntPtr manager);
		[DllImport("TMTSPlugIns")]
		private static extern bool BasePlugInSystem_RegisterPlugIn(IntPtr basePlugInSystem, IntPtr plugIn, IntPtr name, IntPtr path, IntPtr mgr);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr BasePlugInSystem_GetManagerFromFileExtension(IntPtr basePlugInSystem, IntPtr ext);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_UpdateRealTime(IntPtr basePlugInSystem, double newTime, double lastTimeStep);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_UpdateSimTime(IntPtr basePlugInSystem, double newTime, double lastTimeStep);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_DeliverMessages(IntPtr basePlugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_ShutDown(IntPtr basePlugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_LoadVariables(IntPtr basePlugInSystem, IntPtr path);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_StoreVariables(IntPtr basePlugInSystem, IntPtr path);
		[DllImport("TMTSPlugIns")]
		private static extern void BasePlugInSystem_SetLogLongMessagesSet(IntPtr basePlugInSystem, IntPtr longMessagesSet);
		//[DllImport("TMTSPlugIns")] private static extern bool BasePlugInSystem_GetNameAndExtension(IntPtr basePlugInSystem, [MarshalAs(UnmanagedType.LPStr)]string name, ref [MarshalAs(UnmanagedType.LPStr)]string nameOut, ref string extOut);

		public BasePlugInSystem() : base(CreateBasePlugInSystem()) { }

		public bool RegisterPlugInManager(PlugInManager manager)
		{
			bool ret = BasePlugInSystem_RegisterPlugInManager(innerPlugInSystem, manager.innerPlugInManager);
			Console.WriteLine(ret);
			return ret;
		}
		public bool RegisterPlugIn(PlugInImpl plugIn, string name, string path, PlugInManager mgr)
		{
			IntPtr pName = Marshal.StringToHGlobalAnsi(name);
			IntPtr pPath = Marshal.StringToHGlobalAnsi(path);
			bool ret = BasePlugInSystem_RegisterPlugIn(innerPlugInSystem, plugIn.innerPlugIn, pName, pPath, mgr.innerPlugInManager);
			Marshal.FreeHGlobal(pName);
			Marshal.FreeHGlobal(pPath);
			return ret;
		}
		public PlugInManager GetManagerFromFileExtension(string ext)
		{
			IntPtr pExt = Marshal.StringToHGlobalAnsi(ext);
			PlugInManager ret = new PlugInManager(BasePlugInSystem_GetManagerFromFileExtension(innerPlugInSystem, pExt));
			Marshal.FreeHGlobal(pExt);
			return ret;
		}
		public void UpdateRealTime(double newTime, double lastTimeStep) { BasePlugInSystem_UpdateRealTime(innerPlugInSystem, newTime, lastTimeStep); }
		public void UpdateSimTime(double newTime, double lastTimeStep) { BasePlugInSystem_UpdateSimTime(innerPlugInSystem, newTime, lastTimeStep); }
		public void DeliverMessages() { BasePlugInSystem_DeliverMessages(innerPlugInSystem); }
		public void ShutDown()
		{
			BasePlugInSystem_ShutDown(innerPlugInSystem);
			foreach (GCHandle gcHandle in internalDataAddresses.Values)
			{
				gcHandle.Free();
			}
			internalDataAddresses.Clear();
		}
		public void LoadVariables(string path)
		{
			IntPtr pPath = Marshal.StringToHGlobalAnsi(path);
			BasePlugInSystem_LoadVariables(innerPlugInSystem, pPath);
			Marshal.FreeHGlobal(pPath);
		}
		public void StoreVariables(string path)
		{
			IntPtr pPath = Marshal.StringToHGlobalAnsi(path);
			BasePlugInSystem_StoreVariables(innerPlugInSystem, pPath);
			Marshal.FreeHGlobal(pPath);
		}
		public void SetLogLongMessagesSet(string longMessagesSet)
		{
			IntPtr pSet = Marshal.StringToHGlobalAnsi(longMessagesSet);
			BasePlugInSystem_SetLogLongMessagesSet(innerPlugInSystem, pSet);
			Marshal.FreeHGlobal(pSet);
		}
		//public bool GetNameAndExtension(string name, ref string nameOut, ref string extOut) { return BasePlugInSystem_GetNameAndExtension(innerPlugInSystem, name, ref nameOut, ref extOut); }
	}
}