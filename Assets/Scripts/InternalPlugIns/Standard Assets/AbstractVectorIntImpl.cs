using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class AbstractVectorIntImpl : AbstractVectorInt
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr CreateAbstractVectorIntImpl();

		AbstractVectorIntImpl() : base(CreateAbstractVectorIntImpl()) { }
		AbstractVectorIntImpl(int[] val) : base(CreateAbstractVectorIntImpl()) { foreach (int v in val) push_back(v); }
		AbstractVectorIntImpl(List<int> val) : base(CreateAbstractVectorIntImpl()) { foreach (int v in val) push_back(v); }
	}
}