using System;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class AbstractStringImpl : AbstractString
	{
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr CreateAbstractStringImpl();

		public AbstractStringImpl() : base(CreateAbstractStringImpl()) { }
		public AbstractStringImpl(string s) : base(CreateAbstractStringImpl()) { assign(s); }
	}
}