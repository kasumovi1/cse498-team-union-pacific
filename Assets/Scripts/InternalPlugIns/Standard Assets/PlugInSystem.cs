using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TMTSPluginsCS
{
	public class PlugInSystem
	{
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInSystem_GetRealTime(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInSystem_GetLastRealTimeStep(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInSystem_GetSimTime(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInSystem_GetLastSimTimeStep(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetPlugIn(IntPtr plugInSystem, IntPtr name, ref bool creationFlag);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetPlugInFromChars(IntPtr plugInSystem, IntPtr name, ref bool creationFlag);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetPlugInFromId(IntPtr plugInSystem, int id);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_LoadPlugIn(IntPtr plugInSystem, IntPtr name, ref bool alreadyRegistered);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_SetDefaultKeyboardPlugIn(IntPtr plugInSystem, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetDefaultKeyboardPlugIn(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_SetDefaultMousePlugIn(IntPtr plugInSystem, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetDefaultMousePlugIn(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_SetDefaultActivityPlugIn(IntPtr plugInSystem, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetDefaultActivityPlugIn(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_UnregisterPlugIn(IntPtr plugInSystem, IntPtr plugIn);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_CreateEmptyMessage(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_CreateMessage(IntPtr plugInSystem, int msg, int iParam, double dParam);
		[DllImport("TMTSPlugIns")]
		private static extern bool PlugInSystem_ScheduleMessage(IntPtr plugInSystem, IntPtr msg, int sourceId, int targetId, double realTimeDelay, double simTimeDelay);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInSystem_ForwardMessage(IntPtr plugInSystem, IntPtr msg, IntPtr newTarget);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInSystem_DeliverMessageNow(IntPtr plugInSystem, IntPtr msg, int sourceId, int targetId);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetCurrentMessage(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_DestroyMessage(IntPtr plugInSystem, IntPtr msg);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_RegisterAddress(IntPtr plugInSystem, IntPtr addr);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_GetAddress(IntPtr plugInSystem, int id);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_UnRegisterAddress(IntPtr plugInSystem, IntPtr addr);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_CreateAbstractString(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_DestroyAbstractString(IntPtr plugInSystem, IntPtr s);
		[DllImport("TMTSPlugIns")]
		private static extern IntPtr PlugInSystem_CreateAbstractVectorInt(IntPtr plugInSystem);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_DestroyAbstractVectorInt(IntPtr plugInSystem, IntPtr v);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_StoreVariableInt(IntPtr plugInSystem, IntPtr varName, int val);
		[DllImport("TMTSPlugIns")]
		private static extern int PlugInSystem_LoadVariableInt(IntPtr plugInSystem, IntPtr varName, int defaultVal);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_StoreVariableFloat(IntPtr plugInSystem, IntPtr varName, float val);
		[DllImport("TMTSPlugIns")]
		private static extern float PlugInSystem_LoadVariableFloat(IntPtr plugInSystem, IntPtr varName, float defaultVal);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_StoreVariableDouble(IntPtr plugInSystem, IntPtr varName, double val);
		[DllImport("TMTSPlugIns")]
		private static extern double PlugInSystem_LoadVariableDouble(IntPtr plugInSystem, IntPtr varName, double defaultVal);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_StoreVariableString(IntPtr plugInSystem, IntPtr varName, IntPtr val);
		[DllImport("TMTSPlugIns")]
		private static extern void PlugInSystem_LoadVariableString(IntPtr plugInSystem, IntPtr varName, IntPtr inAndOut);

		internal IntPtr innerPlugInSystem;

		protected static Dictionary<IntPtr, GCHandle> internalDataAddresses = new Dictionary<IntPtr, GCHandle>();

		public PlugInSystem(IntPtr inner) { innerPlugInSystem = inner; }

		public double GetRealTime() { return PlugInSystem_GetRealTime(innerPlugInSystem); }
		public double GetLastRealTimeStep() { return PlugInSystem_GetLastRealTimeStep(innerPlugInSystem); }
		public double GetSimTime() { return PlugInSystem_GetSimTime(innerPlugInSystem); }
		public double GetLastSimTimeStep() { return PlugInSystem_GetLastSimTimeStep(innerPlugInSystem); }
		public PlugIn GetPlugIn(AbstractString name, ref bool creationFlag) { return new PlugIn(PlugInSystem_GetPlugIn(innerPlugInSystem, name.innerAbstractString, ref creationFlag)); }
		public PlugIn GetPlugInFromChars(string name, ref bool creationFlag)
		{
			IntPtr pName = Marshal.StringToHGlobalAnsi(name);
			PlugIn ret = new PlugIn(PlugInSystem_GetPlugInFromChars(innerPlugInSystem, pName, ref creationFlag));
			Marshal.FreeHGlobal(pName);
			return ret;
		}
		public PlugIn GetPlugInFromId(int id) { return new PlugIn(PlugInSystem_GetPlugInFromId(innerPlugInSystem, id)); }
		public PlugIn LoadPlugIn(AbstractString name, ref bool alreadyRegistered) { return new PlugIn(PlugInSystem_LoadPlugIn(innerPlugInSystem, name.innerAbstractString, ref alreadyRegistered)); }
		public void SetDefaultKeyboardPlugIn(PlugIn plugIn) { PlugInSystem_SetDefaultKeyboardPlugIn(innerPlugInSystem, plugIn.innerPlugIn); }
		private PlugIn defaultKeyboardPlugIn;
		public PlugIn GetDefaultKeyboardPlugIn()
		{
			IntPtr innerPlugIn = PlugInSystem_GetDefaultKeyboardPlugIn(innerPlugInSystem);
			if (defaultKeyboardPlugIn == null || defaultKeyboardPlugIn.innerPlugIn != innerPlugIn)
			{
				defaultKeyboardPlugIn = new PlugIn(innerPlugIn);
			}
			return defaultKeyboardPlugIn;
		}
		public void SetDefaultMousePlugIn(PlugIn plugIn) { PlugInSystem_SetDefaultMousePlugIn(innerPlugInSystem, plugIn.innerPlugIn); }
		private PlugIn defaultMousePlugIn;
		public PlugIn GetDefaultMousePlugIn()
		{
			IntPtr innerPlugIn = PlugInSystem_GetDefaultMousePlugIn(innerPlugInSystem);
			if (defaultMousePlugIn == null || defaultMousePlugIn.innerPlugIn != innerPlugIn)
			{
				defaultMousePlugIn = new PlugIn(innerPlugIn);
			}
			return defaultMousePlugIn;
		}
		public void SetDefaultActivityPlugIn(PlugIn plugIn) { PlugInSystem_SetDefaultActivityPlugIn(innerPlugInSystem, plugIn.innerPlugIn); }
		private PlugIn defaultActivityPlugIn;
		public PlugIn GetDefaultActivityPlugIn()
		{
			IntPtr innerPlugIn = PlugInSystem_GetDefaultActivityPlugIn(innerPlugInSystem);
			if (defaultActivityPlugIn == null || defaultActivityPlugIn.innerPlugIn != innerPlugIn)
			{
				defaultActivityPlugIn = new PlugIn(innerPlugIn);
			}
			return defaultActivityPlugIn;
		}
		public void UnregisterPlugIn(PlugIn plugIn) { PlugInSystem_UnregisterPlugIn(innerPlugInSystem, plugIn.innerPlugIn); }
		public PlugInMessage CreateEmptyMessage() { return PlugInMessage.New(PlugInSystem_CreateEmptyMessage(innerPlugInSystem)); }
		public PlugInMessage CreateMessage(int msg) { return PlugInMessage.New(PlugInSystem_CreateMessage(innerPlugInSystem, msg, 0, 0.0)); }
		public PlugInMessage CreateMessage(int msg, int iParam) { return PlugInMessage.New(PlugInSystem_CreateMessage(innerPlugInSystem, msg, iParam, 0.0)); }
		public PlugInMessage CreateMessage(int msg, int iParam, double dParam) { return PlugInMessage.New(PlugInSystem_CreateMessage(innerPlugInSystem, msg, iParam, dParam)); }
		public PlugInMessage CreateMessage(int msg, int iParam, double dParam, string sParam)
		{
			PlugInMessage message = PlugInMessage.New(PlugInSystem_CreateMessage(innerPlugInSystem, msg, iParam, dParam));
			message.sParam.assign(sParam);
			return message;
		}
		public PlugInMessage CreateMessage(int msg, int iParam, double dParam, string sParam, params int[] vParam)
		{
			PlugInMessage message = PlugInMessage.New(PlugInSystem_CreateMessage(innerPlugInSystem, msg, iParam, dParam));
			message.sParam.assign(sParam);
			for (int i = 0; i < vParam.Length; i++)
			{
				message.vParam.push_back(vParam[i]);
			}
			return message;
		}
		public bool ScheduleMessage(PlugInMessage msg, int sourceId, int targetId, double realTimeDelay, double simTimeDelay) { return PlugInSystem_ScheduleMessage(innerPlugInSystem, msg.innerPlugInMessage, sourceId, targetId, realTimeDelay, simTimeDelay); }
		public int ForwardMessage(PlugInMessage msg, PlugIn newTarget) { return PlugInSystem_ForwardMessage(innerPlugInSystem, msg.innerPlugInMessage, newTarget.innerPlugIn); }
		public int ForwardMessage(PlugInMessage msg, string newTargetName)
		{
			PlugIn newTarget = TMTSPlugInsManager.GetPlugIn(newTargetName);
			return ForwardMessage(msg, newTarget);
		}
		public int DeliverMessageNow(PlugInMessage msg, int sourceId, int targetId) { return PlugInSystem_DeliverMessageNow(innerPlugInSystem, msg.innerPlugInMessage, sourceId, targetId); }
		public int DeliverMessageNow(PlugInMessage msg, int sourceId, string target)
		{
			int targetId = TMTSPlugInsManager.GetPlugInId(target);
			return PlugInSystem_DeliverMessageNow(innerPlugInSystem, msg.innerPlugInMessage, sourceId, targetId);
		}

		public PlugInMessage GetCurrentMessage() { return PlugInMessage.New(PlugInSystem_GetCurrentMessage(innerPlugInSystem)); }
		public void DestroyMessage(PlugInMessage msg)
		{
			PlugInSystem_DestroyMessage(innerPlugInSystem, msg.innerPlugInMessage);
			PlugInMessage.Reuse(msg);
		}
		public DataAddress RegisterAddress(object o)
		{
			GCHandle gcHandle = GCHandle.Alloc(o, GCHandleType.Pinned);
			IntPtr innerDataAddress = PlugInSystem_RegisterAddress(innerPlugInSystem, gcHandle.AddrOfPinnedObject());
			internalDataAddresses[innerDataAddress] = gcHandle;
			return new DataAddress(innerDataAddress);
		}
		public DataAddress RegisterAddress(IntPtr unmanagedPointer)
		{
			IntPtr innerDataAddress = PlugInSystem_RegisterAddress(innerPlugInSystem, unmanagedPointer);
			return new DataAddress(innerDataAddress);
		}
		public DataAddress GetAddress(int id) { return new DataAddress(PlugInSystem_GetAddress(innerPlugInSystem, id)); }
		public void UnRegisterAddress(DataAddress addr)
		{
			if (internalDataAddresses.ContainsKey(addr.innerDataAddress))
			{
				GCHandle gcHandle = internalDataAddresses[addr.innerDataAddress];
				internalDataAddresses.Remove(addr.innerDataAddress);
				gcHandle.Free();
			}
			PlugInSystem_UnRegisterAddress(innerPlugInSystem, addr.innerDataAddress);
		}
		public AbstractString CreateAbstractString() { return new AbstractString(PlugInSystem_CreateAbstractString(innerPlugInSystem)); }
		public void DestroyAbstractString(AbstractString s) { PlugInSystem_DestroyAbstractString(innerPlugInSystem, s.innerAbstractString); }
		public AbstractVectorInt CreateAbstractVectorInt() { return new AbstractVectorInt(PlugInSystem_CreateAbstractVectorInt(innerPlugInSystem)); }
		public void DestroyAbstractVectorInt(AbstractVectorInt v) { PlugInSystem_DestroyAbstractVectorInt(innerPlugInSystem, v.innerAbstractVectorInt); }
		public void StoreVariableInt(AbstractString varName, int val) { PlugInSystem_StoreVariableInt(innerPlugInSystem, varName.innerAbstractString, val); }
		public int LoadVariableInt(AbstractString varName, int defaultVal) { return PlugInSystem_LoadVariableInt(innerPlugInSystem, varName.innerAbstractString, defaultVal); }
		public void StoreVariableFloat(AbstractString varName, float val) { PlugInSystem_StoreVariableFloat(innerPlugInSystem, varName.innerAbstractString, val); }
		public float LoadVariableFloat(AbstractString varName, float defaultVal) { return PlugInSystem_LoadVariableFloat(innerPlugInSystem, varName.innerAbstractString, defaultVal); }
		public void StoreVariableDouble(AbstractString varName, double val) { PlugInSystem_StoreVariableDouble(innerPlugInSystem, varName.innerAbstractString, val); }
		public double LoadVariableDouble(AbstractString varName, double defaultVal) { return PlugInSystem_LoadVariableDouble(innerPlugInSystem, varName.innerAbstractString, defaultVal); }
		public void StoreVariableString(AbstractString varName, AbstractString val) { PlugInSystem_StoreVariableString(innerPlugInSystem, varName.innerAbstractString, val.innerAbstractString); }
		public void LoadVariableString(AbstractString varName, AbstractString inAndOut) { PlugInSystem_LoadVariableString(innerPlugInSystem, varName.innerAbstractString, inAndOut.innerAbstractString); }

		public AbstractString CreateAbstractString(string s)
		{
			AbstractString abString = new AbstractString(PlugInSystem_CreateAbstractString(innerPlugInSystem));
			abString.assign(s);
			return abString;
		}
		public void StoreVariableInt(string varName, int val)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			StoreVariableInt(abstractVarName, val);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
		}
		public int LoadVariableInt(string varName, int defaultVal)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			int ret = LoadVariableInt(abstractVarName, defaultVal);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
			return ret;
		}
		public void StoreVariableFloat(string varName, float val)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			StoreVariableFloat(abstractVarName, val);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
		}
		public float LoadVariableFloat(string varName, float defaultVal)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			float ret = LoadVariableFloat(abstractVarName, defaultVal);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
			return ret;
		}
		public void StoreVariableDouble(string varName, double val)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			StoreVariableDouble(abstractVarName, val);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
		}
		public double LoadVariableDouble(string varName, double defaultVal)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			double ret = LoadVariableDouble(abstractVarName, defaultVal);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
			return ret;
		}
		public void StoreVariableString(string varName, string val)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			AbstractString abstractVal = CreateAbstractString(val);
			StoreVariableString(abstractVarName, abstractVal);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVal);
		}
		public string LoadVariableString(string varName, string defaultVal)
		{
			AbstractString abstractVarName = CreateAbstractString(varName);
			AbstractString abstractDefaultVal = CreateAbstractString(defaultVal);
			LoadVariableString(abstractVarName, abstractDefaultVal);
			string ret = abstractDefaultVal.c_str();
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractVarName);
			TMTSPlugInsManager.plugInSystem.DestroyAbstractString(abstractDefaultVal);
			return ret;
		}
		public void StoreArrayInt(string name, int[] val)
		{
			int count = val.Length;
			StoreVariableInt(name + ".size", count);
			string fullName;
			for (int i = 0; i < count; i++)
			{
				fullName = name + "[" + i + "]";
				StoreVariableInt(fullName, val[i]);
			}
		}
		void LoadArrayInt(string name, ref int[] val)
		{
			int count = LoadVariableInt(name + ".size", 0);
			if (val.Length < count)
			{
				val = new int[count];
			}
			string fullName;
			for (int i = 0; i < count; i++)
			{
				fullName = name + "[" + i + "]";
				val[i] = LoadVariableInt(fullName, val[i]);
			}
		}
		public void StoreArrayString(string name, string[] val)
		{
			int count = val.Length;
			StoreVariableInt(name + ".size", count);
			string fullName;
			string sTemp;
			for (int i = 0; i < count; i++)
			{
				fullName = name + "[" + i + "]";
				sTemp = val[i];
				StoreVariableString(fullName, sTemp);
			}
		}
		void LoadArrayString(string name, ref string[] val)
		{
			int count = LoadVariableInt(name + ".size", 0);
			if (val.Length < count)
			{
				val = new string[count];
			}
			string fullName;
			string sTemp = null;
			for (int i = 0; i < count; i++)
			{
				fullName = name + "[" + i + "]";
				LoadVariableString(fullName, sTemp);
				val[i] = sTemp;
			}
		}


	}
}