using TMTSPluginsCS;

public class DatabasePlugIn : InternalPlugIn
{
	public override int HandleMessage(PlugInMessageImpl msg)
	{
		if (msg.args.msg == ScriptConstants.TMTS_MSG_REGISTER)
		{
			if (null == msg.GetSource())
			{
				return 0;
			}

			global::Car innerCar = TmtsMasterIndex.GetEntry(msg.args.iParam) as global::Car; // typecast from MasterIndexEntry to Car			
			if (innerCar == null)
			{
				return 0;
			}
			DataAddress addr = TMTSPlugInsManager.plugInSystem.RegisterAddress(innerCar.carPhysics.GetInnerVehicle());
			if (addr == null)
			{
				return 0;
			}

			addr.SetWatcher(msg.GetSource());
			return addr.GetId();
		}

		return 0;
	}
}