
























// Generated from [Main\TMTS\TMTSCore\ScriptConstants.h] - Make any changes and additions there.
public class ScriptConstants
{





public const int TMTS_MSG_INITIALIZE = unchecked((int)0x00000001);
public const int TMTS_MSG_STARTUP = unchecked((int)0x00000002);
public const int TMTS_MSG_UPDATE = unchecked((int)0x00000003);
public const int TMTS_MSG_CAR_PLACED = unchecked((int)0x00000004);
public const int TMTS_MSG_KEY_DOWN = unchecked((int)0x00000005);
public const int TMTS_MSG_KEY_UP = unchecked((int)0x00000006);
public const int TMTS_MSG_SPLAT_PACKET = unchecked((int)0x00000007);
public const int TMTS_MSG_SET_VARIABLE = unchecked((int)0x00000008);
public const int TMTS_MSG_CALLBACK = unchecked((int)0x00000009);
public const int TMTS_MSG_ACTION = unchecked((int)0x0000000a);
public const int TMTS_MSG_MOUSE_DOWN = unchecked((int)0x0000000b);
public const int TMTS_MSG_MOUSE_UP = unchecked((int)0x0000000c);
public const int TMTS_MSG_MOUSE_CLICK = unchecked((int)0x0000000d);
public const int TMTS_MSG_MOUSE_MOVE = unchecked((int)0x0000000e);
public const int TMTS_MSG_DESELECT_ALL = unchecked((int)0x0000000f);
public const int TMTS_MSG_UNCOUPLE_FRONT = unchecked((int)0x00000010);                   
public const int TMTS_MSG_UNCOUPLE_REAR = unchecked((int)0x00000011);
public const int TMTS_MSG_RELEASE = unchecked((int)0x00000012);
public const int TMTS_MSG_COCK_FRONT = unchecked((int)0x00000013);
public const int TMTS_MSG_COCK_REAR = unchecked((int)0x00000014);
public const int TMTS_MSG_HANDBRAKE = unchecked((int)0x00000015);
public const int TMTS_MSG_RETAINER = unchecked((int)0x00000016);
public const int TMTS_MSG_HEADUP = unchecked((int)0x00000017);
public const int TMTS_MSG_SHOW_UI_BOX = unchecked((int)0x00000018);
public const int TMTS_MSG_GET_SELECTIONS = unchecked((int)0x00000019);
public const int TMTS_MSG_GET_CONSIST_NAME = unchecked((int)0x0000001a);
public const int TMTS_MSG_SPLAT_BLOCK_READ = unchecked((int)0x0000001b);
public const int TMTS_MSG_MOUSE_CAB_CLICK = unchecked((int)0x0000001c);
public const int TMTS_MSG_HTML_CLOSE = unchecked((int)0x0000001d);
public const int TMTS_MSG_HTML_COMMAND = unchecked((int)0x0000001e);
public const int TMTS_MSG_CTC_EVENT = unchecked((int)0x0000001f);
public const int TMTS_MSG_MOUSE_DRAG = unchecked((int)0x00000020);
public const int TMTS_MSG_MOUSE_WHEEL = unchecked((int)0x00000021);
public const int TMTS_MSG_KEY_MODIFIERS_CHANGED = unchecked((int)0x00000022);
public const int TMTS_3D_RENDERING_CHANGED = unchecked((int)0x00000023);	// When 3d rendering enabled, disabled, turned on for the first time
public const int TMTS_MSG_SAVE_STATE = unchecked((int)0x00000024);
public const int TMTS_MSG_CAR_SCRIPT_LOADED = unchecked((int)0x00000025);	// When a plugin is loaded as part of a car in a consist
public const int TMTS_MSG_REGISTER = unchecked((int)0x00000026);	// A plugin registers itself (usually as a listener to something, eg. a broadcaster) 
public const int TMTS_MSG_VALUE = unchecked((int)0x00000027);	// A value has changed
public const int TMTS_MSG_INVALIDATE_ADDRESS = unchecked((int)0x00000028);	// Address invalidated
public const int TMTS_MSG_PLACE_CONSIST = unchecked((int)0x00000029);   // Loads and Places a consist in the world, named by a Plug In
public const int TMTS_MSG_CONTROL_LOCO = unchecked((int)0X0000002a);   // Broadcasts Locomotive being controlled
public const int TMTS_MSG_GET_CONTROLLING_LOCO = unchecked((int)0X0000002b);   // Returns Locomotive in control of consist
public const int TMTS_MSG_SHUTDOWN = unchecked((int)0x0000002c);	// Send to "Global" to halt TMTS, it will be broadcast when shutting down
public const int TMTS_MSG_SET_VIEW = unchecked((int)0x0000002d);	// Send to "DefaultView", sets the view (iParam = view number)
public const int TMTS_MSG_OCU_LEFT_JOYSTICK_BUTTON = unchecked((int)0x0000002e);
public const int TMTS_MSG_OCU_RIGHT_JOYSTICK_BUTTON = unchecked((int)0x0000002f);
public const int TMTS_MSG_OCU_JOYSTICK_MOVED = unchecked((int)0x00000030);	// use iParam to determine which joystick (bit 0) and axis (bit 1)
public const int TMTS_MSG_SUBMIT_SCORE = unchecked((int)0x00000031);	// Send to "Global" to submit the current score card
public const int TMTS_MSG_LOG_DATA = unchecked((int)0x00000032);	// Send to "Global" to push sParam on to Log file
public const int TMTS_MSG_GET_PROPERTY = unchecked((int)0x00000033);	// For Database plug-in: Get the property named in the sParam from the Data Element id in the iParam, value returned in appropriate field (iParam, dParam, sParam)
public const int TMTS_MSG_SET_PROPERTY = unchecked((int)0x00000034);	// For Database plug-in: Set the property named in the sParam from the Data Element id in the iParam with value stored in corresponding field (int in vParam[0], double in dParam, string in sParam in the form of "[property]\n[value]") 
public const int TMTS_MSG_CREATE = unchecked((int)0x00000035);	// For Database plug-in: Creates a new database element (specify type in iParam using TMTS_DATA_TYPE_*). Each type requires different arguments: Marker(vParam[0] is id of any track element in the desired spline, dParam is t value for insertion location, vParam[1] is the desired serialId base (optional)). See wiki for more details.
public const int TMTS_MSG_DESTROY = unchecked((int)0x00000036);	// For Database plug-in: Removes element from database (iParam is database id of element)
public const int TMTS_MSG_GET_WORK_THREAD = unchecked((int)0x00000037);
public const int TMTS_MSG_SCENERY_LOADED = unchecked((int)0x00000038);
public const int TMTS_MSG_3D_MODEL_RELOADED = unchecked((int)0x00000039);	// TMRender entity has been recreated; refresh the state of associated scripts and data.
public const int TMTS_MSG_PLAY_SOUND = unchecked((int)0x0000003a);	// For Sound plug-in:  Plays from note(iParam) from PRG2 file(sParam) at specific volume(dParam) and a specific pitch bend(vParam[0]), vParam[1] indicates 1 if sound loops, vParam[2] indicates a Database object for the sound to originate from
public const int TMTS_MSG_STOP_SOUND = unchecked((int)0x0000003b);	// For Sound plug-in:  Stops playing note(iParam) from PRG2 file(sParam)
public const int TMTS_MSG_SET_SKYBOX = unchecked((int)0x0000003c);	// Load a new skybox model from the LSL file
public const int TMTS_MSG_LIGHTING = unchecked((int)0x0000003d);	// Set lights used by rendering, as defined by iParam=TMTS_LIGHT_xxxx constants 
public const int TMTS_MSG_ADD_WEATHER = unchecked((int)0x0000003e);	// sParam: texture file (in LSL), iParam: texture size (meters), dParam: precipitation speed (m/sec)
public const int TMTS_MSG_CLEAR_WEATHER = unchecked((int)0x0000003f);
public const int TMTS_MSG_WEATHER_PARAMETER = unchecked((int)0x00000040);
public const int TMTS_MSG_REGISTER_SCORE = unchecked((int)0x00000041);
public const int TMTS_MSG_INCREMENT_SCORE = unchecked((int)0x00000042);
public const int TMTS_MSG_ADD_SCORE = unchecked((int)0x00000043);
public const int TMTS_MSG_EVALUATE_SCORE = unchecked((int)0x00000044);
public const int TMTS_MSG_SCORE_FAILURE = unchecked((int)0x00000045);
public const int TMTS_MSG_DIALOG_CLOSED = unchecked((int)0x00000046);
public const int TMTS_MSG_SCORE_FORMAT = unchecked((int)0x00000047);
public const int TMTS_MSG_DISPLAY_SCORE = unchecked((int)0x00000048);
public const int TMTS_MSG_ANIMATION_COMPLETE = unchecked((int)0x00000049);
public const int TMTS_MSG_LOCK_VIEW = unchecked((int)0x0000004a);	// iParam: 1=Locked, 0=Unlocked. If locked, then mouse and keyboard won't change camera's orientation. 
public const int TMTS_MSG_MOVE_CHARACTER = unchecked((int)0x0000004b);	// iParam: 0=rest, 1=forward, 2=left and forward, 3=right and forward. dParam: how hard turn is
public const int TMTS_MSG_SWITCH_TOGGLE_NEXT = unchecked((int)0x0000004c);
public const int TMTS_MSG_SWITCH_TOGGLE_PREV = unchecked((int)0x0000004d);
public const int TMTS_MSG_FOCUS_LOCO = unchecked((int)0x0000004e);
public const int TMTS_MSG_BEFORE_SHUTDOWN = unchecked((int)0x0000004f);
public const int TMTS_MSG_CONTROLLER_BUTTON_DOWN = unchecked((int)0x00000050);
public const int TMTS_MSG_CONTROLLER_BUTTON_UP = unchecked((int)0x00000051);

public const int TMTS_MSG_VIEW_STATE = unchecked((int)3001);
public const int TMTS_MSG_VIEW_ANGLE = unchecked((int)3002);

public const int TMTS_MSG_FRAME_COPY = unchecked((int)3501);

public const int TMTS_MSG_LOCOMOTIVE_STATE_BRAKE_CONTROLS = unchecked((int)4001);
public const int TMTS_MSG_LOCOMOTIVE_STATE_POWER_CONTROLS = unchecked((int)4002);
public const int TMTS_MSG_LOCOMOTIVE_STATE_GAUGE_DISPLAY = unchecked((int)4003);
public const int TMTS_MSG_LOCOMOTIVE_STATE_ACCESSORY_CONTROLS = unchecked((int)4004);
public const int TMTS_MSG_LOCOMOTIVE_STATE_ENGINE_DATA = unchecked((int)4005);
public const int TMTS_MSG_LOCOMOTIVE_STATE_PERFORMANCE_DATA = unchecked((int)4006);
public const int TMTS_MSG_LOCOMOTIVE_STATE_ENGINE_PANEL = unchecked((int)4007);
public const int TMTS_MSG_LOCOMOTIVE_STATE_FUEL_DATA = unchecked((int)4008);

public const int TMTS_MSG_NETWORK_CONNECTED = unchecked((int)5001);
public const int TMTS_MSG_NETWORK_DISCONNECTED = unchecked((int)5002);
public const int TMTS_MSG_NETWORK_DATA = unchecked((int)5003);

public const int TMTS_MSG_NETWORK_START_SERVER = unchecked((int)5004);
public const int TMTS_MSG_NETWORK_STOP_SERVER = unchecked((int)5005);
public const int TMTS_MSG_NETWORK_SEND_MESSAGE = unchecked((int)5006);
public const int TMTS_MSG_NETWORK_GET_MESSAGE = unchecked((int)5007);
public const int TMTS_MSG_NETWORK_GET_DATA = unchecked((int)5008);
public const int TMTS_MSG_NETWORK_RELEASE_MESSAGE = unchecked((int)5009);

public const int TMTS_MSG_SET_CONTROL_VEHICLE = unchecked((int)6000);

// EditTools Messages:
public const int TMTS_MSG_GET_TERRAIN_HEIGHT = unchecked((int)7001);
public const int TMTS_MSG_UPDATE_PT_HEIGHTS = unchecked((int)7002);
public const int TMTS_MSG_GET_TRACK = unchecked((int)7004);
public const int TMTS_MSG_GET_SCENERY = unchecked((int)7005);
public const int TMTS_MSG_GET_OBJECT_DATA = unchecked((int)7006);
public const int TMTS_MSG_GET_SCENERY_TYPE_DATA = unchecked((int)7007);
public const int TMTS_MSG_GET_OBJECT_HEIGHTS = unchecked((int)7008);
public const int TMTS_MSG_SET_POINT = unchecked((int)7009);
public const int TMTS_MSG_DEL_POINT = unchecked((int)7010);
public const int TMTS_MSG_DEL_SPLINE = unchecked((int)7011);
public const int TMTS_MSG_NEW_SPLINE = unchecked((int)7012);
public const int TMTS_MSG_FIX_SPLINE_END = unchecked((int)7013);
public const int TMTS_MSG_SPLINE_DATA = unchecked((int)7014);
public const int TMTS_MSG_NEW_POINT = unchecked((int)7015);
public const int TMTS_MSG_NEW_MIXED = unchecked((int)7016);
public const int TMTS_MSG_NEW_POINT_LIST = unchecked((int)7017);
public const int TMTS_MSG_NODE_LINK = unchecked((int)7018);
public const int TMTS_MSG_NODE_UNLINK = unchecked((int)7019);
public const int TMTS_MSG_POINT_LINK = unchecked((int)7020);
public const int TMTS_MSG_DIVIDE_SPLINE = unchecked((int)7021);
public const int TMTS_MSG_NEW_OBJECT = unchecked((int)7022);
public const int TMTS_MSG_DEL_OBJECT = unchecked((int)7023);
public const int TMTS_MSG_MOVE_OBJECT = unchecked((int)7024);
public const int TMTS_MSG_OBJECT_PRP = unchecked((int)7025);
public const int TMTS_MSG_NEW_NODE = unchecked((int)7026);
public const int TMTS_MSG_MOVE_CAMERA = unchecked((int)7027);
public const int TMTS_MSG_SELECT_SPLINE = unchecked((int)7028);
public const int TMTS_MSG_SELECT_SCENERY = unchecked((int)7029);
public const int TMTS_MSG_SET_DBG_RAY = unchecked((int)7030);
public const int TMTS_MSG_EDITOR_SAVE = unchecked((int)7031);
public const int TMTS_MSG_CONFIRM_SAVE = unchecked((int)7032);
public const int TMTS_MSG_CLOSING = unchecked((int)7033);
public const int TMTS_MSG_SELECT_SCENERY_SET = unchecked((int)7034);
public const int TMTS_MSG_SET_DRAG_TYPE = unchecked((int)7035);
public const int TMTS_MSG_START_DRAG = unchecked((int)7036);
public const int TMTS_MSG_STOP_DRAG = unchecked((int)7037);
public const int TMTS_MSG_DRAG_DATA = unchecked((int)7038);
public const int TMTS_MSG_SHOW_AXIS = unchecked((int)7039);
public const int TMTS_MSG_GET_SPLINE_DATA = unchecked((int)7040);
public const int TMTS_MSG_GET_TEM_DATA = unchecked((int)7041);
public const int TMTS_MSG_DEFER_TRACK_UPDATE = unchecked((int)7042);

public const int TMTS_MSG_NEW_MARKER = unchecked((int)7050);
public const int TMTS_MSG_DEL_MARKER = unchecked((int)7051);
public const int TMTS_MSG_MARKER_PRP = unchecked((int)7052);
public const int TMTS_MSG_MOVE_MARKER = unchecked((int)7053);

public const int TMTS_MSG_SENDING_SPLINES = unchecked((int)7100);
public const int TMTS_MSG_SENDING_MARKERS = unchecked((int)7104);
public const int TMTS_MSG_SENDING_NODE_SCENERY = unchecked((int)7105);
public const int TMTS_MSG_SENDING_TRK_TYPE = unchecked((int)7106);
public const int TMTS_MSG_TRACK_LOADED = unchecked((int)7107);
public const int TMTS_MSG_DATA_LOADED = unchecked((int)7108);
public const int TMTS_MSG_SET_POINT_ID = unchecked((int)7109);
public const int TMTS_MSG_SET_SPLINE_ID = unchecked((int)7110);
public const int TMTS_MSG_SET_OBJECT_ID = unchecked((int)7111);
public const int TMTS_MSG_CAMERA = unchecked((int)7112);
public const int TMTS_MSG_FIXED_SPLINE_OBJECT = unchecked((int)7113);
public const int TMTS_MSG_OBJECT_HEIGHTS = unchecked((int)7114);
public const int TMTS_MSG_LOAD_PATH = unchecked((int)7115);
public const int TMTS_MSG_SENDING_SCENERY = unchecked((int)7116);
public const int TMTS_MSG_GET_PREVIEW_IMG = unchecked((int)7117);
public const int TMTS_MSG_GET_PREVIEW_IMG_SIZE = unchecked((int)7118);
public const int TMTS_MSG_RELOAD_TERRAIN = unchecked((int)7119);

public const int TMTS_MSG_ERROR_MSG = unchecked((int)7120);
public const int TMTS_MSG_REQUEST_FOCUS = unchecked((int)7121);
public const int TMTS_MSG_KEY_COMMAND = unchecked((int)7122);
public const int TMTS_MSG_YIELD_TIME = unchecked((int)7123);
public const int TMTS_MSG_APPEND_MODEL_TO_CONTENT_INDEX = unchecked((int)7124);

public const int TMTS_MSG_SPLAT_READY = unchecked((int)8000);
public const int TMTS_MSG_SPLAT_ALLOW_SHUTDOWN = unchecked((int)8001);
public const int TMTS_MSG_SPLAT_CHANGE = unchecked((int)8002);
public const int TMTS_MSG_SPLAT_SAVE_TILE = unchecked((int)8003);
public const int TMTS_MSG_SPLAT_SAVE_TEX = unchecked((int)8004);
public const int TMTS_MSG_SPLAT_RELOAD_TEX = unchecked((int)8005);
public const int TMTS_MSG_SPLAT_RELOAD_COPY_TEX = unchecked((int)8006);
public const int TMTS_MSG_SPLAT_LOAD_COPY_TEX = unchecked((int)8007);
public const int TMTS_MSG_SPLAT_REQUEST_TEX = unchecked((int)8008);
public const int TMTS_MSG_SPLAT_REPLACE_TEX = unchecked((int)8009);
public const int TMTS_MSG_SPLAT_ENABLE_TEX = unchecked((int)8010);
public const int TMTS_MSG_SPLAT_DISABLE_TEX = unchecked((int)8011);
public const int TMTS_MSG_SPLAT_SET_TEX_SCALE = unchecked((int)8012);
public const int TMTS_MSG_SPLAT_UNDO_SPLAT = unchecked((int)8013);
public const int TMTS_MSG_SPLAT_REDO_SPLAT = unchecked((int)8014);
public const int TMTS_MSG_SPLAT_RENDER_ENABLE = unchecked((int)8015);
public const int TMTS_MSG_SPLAT_SHOW_GRID = unchecked((int)8016);
public const int TMTS_MSG_SPLAT_HIDE_GRID = unchecked((int)8017);
public const int TMTS_MSG_SPLAT_TEXTURING_ENABLE = unchecked((int)8018);
public const int TMTS_MSG_SPLAT_GLOBAL_DEL = unchecked((int)8019);
public const int TMTS_MSG_SPLAT_LOCAL_COUNT = unchecked((int)8020);
public const int TMTS_MSG_SPLAT_TEXTURE_SELECT = unchecked((int)8021);
public const int TMTS_MSG_SPLAT_DONE = unchecked((int)8022);

public const int TMTS_MSG_VTEMPLATE_CREATE = unchecked((int)9000);
public const int TMTS_MSG_VTEMPLATE_RELEASE = unchecked((int)9001);
public const int TMTS_MSG_VTEMPLATE_SET_VALUES = unchecked((int)9002);
public const int TMTS_MSG_VTEMPLATE_ENCODE_VALUES = unchecked((int)9003);

public const int TMTS_MSG_REMOTE_CONNECT = unchecked((int)10000);
public const int TMTS_MSG_REMOTE_DISCONNECT = unchecked((int)10001);
public const int TMTS_MSG_REMOTE_CLIENT_CONNECT = unchecked((int)10002);
public const int TMTS_MSG_REMOTE_CLIENT_DISCONNECT = unchecked((int)10003);
public const int TMTS_MSG_REMOTE_CLIENT_MESSAGE = unchecked((int)10004);
public const int TMTS_MSG_REMOTE_GET_PLUGIN_ID = unchecked((int)10005);
public const int TMTS_MSG_REMOTE_DEFAULT_PLUGIN_ID = unchecked((int)10006);
public const int TMTS_MSG_REMOTE_POST_MSG = unchecked((int)10007);
public const int TMTS_MSG_REMOTE_SEND_MSG = unchecked((int)10008);
public const int TMTS_MSG_REMOTE_PUSH_DATA = unchecked((int)10009);

public const int USER_ACTION_SET_REVERSER_FORWARD = unchecked((int)1);
public const int USER_ACTION_SET_REVERSER_REVERSE = unchecked((int)2);
public const int USER_ACTION_SET_REVERSER_NEUTRAL = unchecked((int)3);
public const int USER_ACTION_INCREASE_THROTTLE = unchecked((int)4);
public const int USER_ACTION_DECREASE_THROTTLE = unchecked((int)5);
public const int USER_ACTION_INCREASE_DYNAMIC_BRAKE = unchecked((int)6);
public const int USER_ACTION_DECREASE_DYNAMIC_BRAKE = unchecked((int)7);
public const int USER_ACTION_INCREASE_AUTO_BRAKE = unchecked((int)8);
public const int USER_ACTION_DECREASE_AUTO_BRAKE = unchecked((int)9);
public const int USER_ACTION_INCREASE_INDEPENDENT_BRAKE = unchecked((int)10);
public const int USER_ACTION_DECREASE_INDEPENDENT_BRAKE = unchecked((int)11);
public const int USER_ACTION_BAILOFF = unchecked((int)12);
public const int USER_ACTION_CHANGE_WIPER_SWITCH = unchecked((int)13);
public const int USER_ACTION_CHANGE_LIGHT_SWITCH = unchecked((int)14);
public const int USER_ACTION_SOUND_HORN = unchecked((int)15);
public const int USER_ACTION_SOUND_BELL = unchecked((int)16);
public const int USER_ACTION_CRUISE_CONTROL = unchecked((int)17);
public const int USER_ACTION_OK = unchecked((int)18);
public const int USER_ACTION_CANCEL = unchecked((int)19);
public const int USER_ACTION_YES = unchecked((int)20);
public const int USER_ACTION_NO = unchecked((int)21);
public const int USER_ACTION_PAUSE = unchecked((int)22);
public const int USER_ACTION_ANIMATE = unchecked((int)23);

public const int USER_ACTION_TOGGLE_TRACK_SWITCH = unchecked((int)1101);

public const int USER_ACTION_AUTO_BRAKE_POSITION = unchecked((int)1103);
public const int USER_ACTION_INDEPENDENT_BRAKE_POSITION = unchecked((int)1104);
public const int USER_ACTION_DYNAMIC_BRAKE_POSITION = unchecked((int)1105);
public const int USER_ACTION_REVERSER_POSITION = unchecked((int)1106);
public const int USER_ACTION_CHANGE_BRAKE_CUTOFF_VALVE = unchecked((int)1107);
public const int USER_ACTION_CHANGE_BRAKE_TRAINLINE = unchecked((int)1108);
public const int USER_ACTION_REMOVE_BRAKE_HANDLE = unchecked((int)1109);
public const int USER_ACTION_CHANGE_BRAKE_MU_VALVE = unchecked((int)1110);
public const int USER_ACTION_APPLY_SAND = unchecked((int)1111);
public const int USER_ACTION_FUEL_CUT_OFF = unchecked((int)1112);
public const int USER_ACTION_INCREASE__HAND_BRAKE = unchecked((int)1113);
public const int USER_ACTION_DECREASE__HAND_BRAKE = unchecked((int)1114);
public const int USER_ACTION_HAND_BRAKE_POSITION = unchecked((int)1115);
public const int USER_ACTION_OPEN_CLOSE_WINDOW = unchecked((int)1116);
public const int USER_ACTION_OPEN_CLOSE_DOOR = unchecked((int)1116);
public const int USER_ACTION_EMERGENCY_BRAKE = unchecked((int)1118);
public const int USER_ACTION_RESET_ALERTER = unchecked((int)1119);
public const int USER_ACTION_SIMPLE_TRAIN_SPEED = unchecked((int)1120);
public const int USER_ACTION_SIMPLE_TRAIN_STOP = unchecked((int)1121);
public const int USER_ACTION_PANTOGRAPH_POSITION = unchecked((int)1123);
public const int USER_ACTION_ENGINE_SHUTDOWN = unchecked((int)1124);
public const int USER_ACTION_ENGINE_START = unchecked((int)1125);
public const int USER_ACTION_GROUND_FAULT_RESET = unchecked((int)1126); 
public const int USER_ACTION_GEN_FIELD = unchecked((int)1127); 
public const int USER_ACTION_OPEN_FIRE_DOOR = unchecked((int)1128);
public const int USER_ACTION_CLOSE_FIRE_DOOR = unchecked((int)1129);
public const int USER_ACTION_FIRE_DOOR_POSITION = unchecked((int)1130);
public const int USER_ACTION_INCREASE_FUEL_RATE = unchecked((int)1131);
public const int USER_ACTION_DECREASE_FUEL_RATE = unchecked((int)1132);
public const int USER_ACTION_SET_BLOWER = unchecked((int)1133);
public const int USER_ACTION_SET_DAMPER = unchecked((int)1134);
public const int USER_ACTION_RUN_COMPRESSOR = unchecked((int)1135);
public const int USER_ACTION_CHANGE_CYLINDER_COCKS = unchecked((int)1136);
public const int USER_ACTION_THROTTLE_POSITION = unchecked((int)1139); 
public const int USER_ACTION_INJECTOR_WATER_RATE = unchecked((int)1140);
public const int USER_ACTION_INJECTOR_POSITION = unchecked((int)1141);
public const int USER_ACTION_OPEN_COUPLER = unchecked((int)1142);
public const int USER_ACTION_ATTACH_AIR_HOSE = unchecked((int)1143);
public const int USER_ACTION_CHANGE_AIR_COCK = unchecked((int)1144);
public const int USER_ACTION_CLUTCH_FACTOR = unchecked((int)1145);
public const int USER_ACTION_REVERSER_HANDLE = unchecked((int)1146);
public const int USER_ACTION_CHANGE_FEED_VALVE = unchecked((int)1147);
public const int USER_ACTION_PCS = unchecked((int)1148);
public const int USER_ACTION_DP_INDEPENDANT_BRAKE_POSITION = unchecked((int)1149);
public const int USER_ACTION_DP_DB_THROTTLE_POSITION = unchecked((int)1150);
public const int USER_ACTION_EOT_EMERGENCY_TRIGGER = unchecked((int)1151);
public const int USER_ACTION_DP_BAILOFF = unchecked((int)1152);
public const int USER_ACTION_DP_SYNCH_TOGGLE = unchecked((int)1153);
public const int USER_ACTION_SANDING_NB_ONE = unchecked((int)1154);
public const int USER_ACTION_CONTROL_FUEL = unchecked((int)1155);
public const int USER_ACTION_RUN_ISOLATE_SWITCH = unchecked((int)1156);
public const int USER_ACTION_LOCO_STATUS_FOR_LCU = unchecked((int)1157);
public const int USER_ACTION_FOOTPEDAL = unchecked((int)1158);
public const int USER_ACTION_DP_MOVE_FENCE = unchecked((int)1159);
public const int USER_ACTION_DP_CHANGE_TRACTION_IDLE_BRAKE_MODE = unchecked((int)1160);
public const int USER_ACTION_DP_CHANGE_THROTTLE = unchecked((int)1161);
public const int USER_ACTION_DP_CHANGE_BRAKE = unchecked((int)1162);
public const int USER_ACTION_OVERRIDE_BRAKEVALVE = unchecked((int)1163);
public const int USER_ACTION_SET_EQUALIZING_RES_PRESSURE = unchecked((int)1164);
public const int USER_ACTION_SET_SKIP_TE_UPDATE = unchecked((int)1165);
public const int USER_ACTION_SET_HEADLIGHT_MODE = unchecked((int)1166);
public const int USER_ACTION_SET_HEADLIGHT_DIRECTIONAL = unchecked((int)1167);


public const int USER_ACTION_SHOW_SPEED_WINDOW = unchecked((int)2001);
public const int USER_ACTION_SHOW_GAUGE_WINDOW = unchecked((int)2002);
public const int USER_ACTION_SHOW_POWER_CONTROL_WINDOW = unchecked((int)2003);
public const int USER_ACTION_SHOW_AIR_BRAKE_WINDOW = unchecked((int)2004);
public const int USER_ACTION_SHOW_ACCESSORIES_WINDOW = unchecked((int)2005);
public const int USER_ACTION_SHOW_SIMPLE_CONTROL_WINDOW = unchecked((int)2006);
public const int USER_ACTION_SHOW_ENGINE_PANEL_WINDOW = unchecked((int)2007);



public const int SCRIPT_TYPE_UNKNOWN = unchecked((int)0);
public const int SCRIPT_TYPE_KEYBOARD_INPUT = unchecked((int)1);
public const int SCRIPT_TYPE_MOUSE_INPUT = unchecked((int)2);
public const int SCRIPT_TYPE_SPLAT_INPUT = unchecked((int)3);
public const int SCRIPT_TYPE_CTC = unchecked((int)4);
public const int SCRIPT_TYPE_MAX = unchecked((int)4);

public const int BROADCAST_HANDLE = unchecked((int)int.MinValue);

public const int TMTS_ENGINE_VARIABLE_REVERSER = unchecked((int)1);
public const int TMTS_ENGINE_VARIABLE_THROTTLE = unchecked((int)2);
public const int TMTS_ENGINE_VARIABLE_DYNAMIC_BRAKE = unchecked((int)3);
public const int TMTS_ENGINE_VARIABLE_AUTO_BRAKE = unchecked((int)4);
public const int TMTS_ENGINE_VARIABLE_INDEPENDENT_BRAKE = unchecked((int)5);
public const int TMTS_ENGINE_VARIABLE_INDEPENDENT_BRAKE_REL = unchecked((int)6);
public const int TMTS_ENGINE_VARIABLE_WIPER_SWITCH = unchecked((int)7);
public const int TMTS_ENGINE_VARIABLE_LIGHT_SWITCH = unchecked((int)8);
public const int TMTS_ENGINE_VARIABLE_HORN = unchecked((int)9);
public const int TMTS_ENGINE_VARIABLE_BELL = unchecked((int)10);
public const int TMTS_PHYSICS_VARIABLE_TRACTIVE_EFFORT = unchecked((int)11);
public const int TMTS_PHYSICS_VARIABLE_BRAKING_FORCE = unchecked((int)12);
public const int TMTS_PHYSICS_VARIABLE_AIR_PRESSURE = unchecked((int)13);
public const int TMTS_PHYSICS_VARIABLE_FRONT_COUPLER = unchecked((int)14);
public const int TMTS_PHYSICS_VARIABLE_REAR_COUPLER = unchecked((int)15);
public const int TMTS_PHYSICS_VARIABLE_FRONT_COCK = unchecked((int)16);
public const int TMTS_PHYSICS_VARIABLE_REAR_COCK = unchecked((int)17);
public const int TMTS_PHYSICS_VARIABLE_CAR_ORIENTATION = unchecked((int)18);
public const int TMTS_PHYSICS_VARIABLE_CAR_SELECTED = unchecked((int)19);
public const int TMTS_PHYSICS_VARIABLE_CAR_VEHNUM = unchecked((int)20);
public const int TMTS_PHYSICS_VARIABLE_CAR_VELOCITY = unchecked((int)21);
public const int TMTS_PHYSICS_VARIABLE_CAR_IND_BRAKE_PRESS = unchecked((int)22);
public const int TMTS_PHYSICS_VARIABLE_CAR_BRAKE_PIPE_PRESS = unchecked((int)23);
public const int TMTS_PHYSICS_VARIABLE_CAR_BAILOFF = unchecked((int)24);
public const int USER_ACTION_STOP_HORN = unchecked((int)25);
public const int TMTS_PHYSICS_VARIABLE_CAR_DIRECTION = unchecked((int)26);
public const int TMTS_GLOBAL_SETTINGS_EDIT_MODE = unchecked((int)27);
public const int TMTS_GLOBAL_SETTINGS_SELECTION_MODE = unchecked((int)28);
public const int TMTS_PHYSICS_CAR_FLASH_COLOR = unchecked((int)29);
public const int TMTS_PHYSICS_VARIABLE_RELEASE = unchecked((int)30);
public const int TMTS_PHYSICS_VARIABLE_HANDBRAKE = unchecked((int)31);
public const int TMTS_PHYSICS_VARIABLE_RETAINER = unchecked((int)32);
public const int TMTS_PHYSICS_STICKY = unchecked((int)33);
public const int TMTS_PHYSICS_CONTROL_VALVE_STATE = unchecked((int)34);
public const int TMTS_PHYSICS_TIPPING = unchecked((int)35);
public const int TMTS_PHYSICS_RAIL_ROLL = unchecked((int)36);
public const int TMTS_PHYSICS_FLANGE_SQUEEK = unchecked((int)37);
public const int TMTS_PHYSICS_L_OVER_V = unchecked((int)38);
public const int TMTS_PHYSICS_BRAKE_PIPE_PRESS = unchecked((int)39);
public const int TMTS_PHYSICS_EMER_RES_PRESS = unchecked((int)40);
public const int TMTS_PHYSICS_AUX_RES_PRESS = unchecked((int)41);
public const int TMTS_PHYSICS_BRAKE_CYL_PRESS = unchecked((int)42);
public const int TMTS_PHYSICS_TENSION_LIMIT_1 = unchecked((int)43);
public const int TMTS_PHYSICS_TENSION_LIMIT_2 = unchecked((int)44);
public const int TMTS_PHYSICS_SOUND_FOCUS_FRONT = unchecked((int)45);

 
public const int TMTS_GLOBAL_SETTINGS_SELECTION_TYPE = unchecked((int)46);
public const int TMTS_GLOBAL_SETTINGS_DEBUGGING_HUD = unchecked((int)47);
public const int TMTS_GLOBAL_SETTINGS_MODEL_DBG_FLAGS = unchecked((int)48);
public const int TMTS_PHYSICS_VARIABLE_CAR_SLIPPING = unchecked((int)49);
public const int TMTS_PHYSICS_VARIABLE_CAR_ADHESIONQUALITY = unchecked((int)50);
public const int TMTS_PHYSICS_VARIABLE_CAR_ADHESION = unchecked((int)51);
public const int TMTS_PHYSICS_VARIABLE_CAR_ADHESIONRATIO = unchecked((int)52);
public const int TMTS_GLOBAL_SETTINGS_GAME_TIME = unchecked((int)53);
public const int TMTS_GLOBAL_SETTINGS_REAL_TIME = unchecked((int)54);
public const int TMTS_GLOBAL_SETTINGS_TIME_OF_DAY = unchecked((int)55);
public const int TMTS_GLOBAL_SETTINGS_WIND_VELOCITY_E = unchecked((int)56);
public const int TMTS_GLOBAL_SETTINGS_WIND_VELOCITY_N = unchecked((int)57);
public const int TMTS_GLOBAL_SETTINGS_TEMPERATURE = unchecked((int)58);

public const int TMTS_GLOBAL_SETTINGS_FPS = unchecked((int)59);
public const int TMTS_GLOBAL_SETTINGS_SOUND_CARD = unchecked((int)60);
public const int TMTS_GLOBAL_SETTINGS_CHANNEL_NUMBER = unchecked((int)61);
public const int TMTS_GLOBAL_SETTINGS_RENDER = unchecked((int)62);
public const int TMTS_GLOBAL_SETTINGS_ACTIVE = unchecked((int)63);

public const int TMTS_RENDER_HIDE_EXT_MODEL = unchecked((int)64);  // MH says "RENDER" is more descriptive.
public const int TMTS_RENDER_HIDE_INTERIOR_MODEL = unchecked((int)65); 
public const int TMTS_RENDER_HIDE_LOAD_MODEL = unchecked((int)66); 
public const int TMTS_RENDER_HIDE_COUPLER_MODEL = unchecked((int)67); 
public const int TMTS_RENDER_HIDE_TRUCK_MODEL = unchecked((int)68);
public const int TMTS_PHYSICS_SOUND_FOCUS_REAR = unchecked((int)69);
public const int TMTS_REALTIME_PHYSICS = unchecked((int)70);
public const int TMTS_FAKE_REALTIME_PHYSICS = unchecked((int)71);
public const int TMTS_PHYSICS_INTEGRATION_TIME = unchecked((int)72);
public const int TMTS_HINSTANCE = unchecked((int)73);
public const int TMTS_TOTAL_CAR_COUNT = unchecked((int)74);
public const int TMTS_VEHICLE_PLUG_IN_ID = unchecked((int)75);
public const int TMTS_VEHICLE_ID = unchecked((int)76);
public const int TMTS_CONSIST_INTEGRATION_LEVEL = unchecked((int)77);
public const int TMTS_VIEW_TRANSITIONS = unchecked((int)78);	// whether view transitions are animated or immediate (1 or 0)
public const int TMTS_SCORE_CARD_PATH = unchecked((int)79); // full path to the score card file
public const int TMTS_VEHICLE_NUMBER = unchecked((int)80); // reverse of TMTS_VEHICLE_ID
public const int TMTS_GLOBAL_TRACK = unchecked((int)81); // returns DataAddress for Global Track object
public const int TMTS_CAR_NAME = unchecked((int)82);
public const int TMTS_LAST_CAR_ID = unchecked((int)83);

// Lighting settings parameter values; used for TMTS_MSG_SET_LIGHTING

public const int TMTS_LIGHT_SET_ILLUMINATION = unchecked((int)1);	// vParam[0]=intensity, vParam[1]=contrast; both 0-100%. Contrast 0 >> only ambient lighting. Contrast 100 >> only diffuse lighting. 
public const int TMTS_LIGHT_GET_ILLUMINATION = unchecked((int)2);
public const int TMTS_LIGHT_DIFFUSE_COLOR = unchecked((int)3);
public const int TMTS_LIGHT_AMBIENT_COLOR = unchecked((int)4);
public const int TMTS_LIGHT_HEADLIGHT_INTENSITY = unchecked((int)11);
public const int TMTS_LIGHT_HEADLIGHT_CONE = unchecked((int)12);
public const int TMTS_LIGHT_HEADLIGHT_PITCH = unchecked((int)13);
public const int TMTS_LIGHT_HEADLIGHT_ATTENUATION = unchecked((int)14);

// Weather parameters; used with TMTS_MSG_WEATHER_PARAMETER. 
public const int TMTS_WEATHER_DISTANCE = unchecked((int)1);
public const int TMTS_WEATHER_SPEED = unchecked((int)2);
public const int TMTS_WEATHER_SCALE = unchecked((int)3);
public const int TMTS_WEATHER_DRIFT = unchecked((int)4);
public const int TMTS_WEATHER_DRIFT_CYCLE = unchecked((int)5);

public const int TMTS_DIALOG_SPLASH = unchecked((int)1);
public const int TMTS_DIALOG_PAUSED = unchecked((int)2);
public const int TMTS_DIALOG_CRITICAL = unchecked((int)3);
public const int TMTS_DIALOG_OOB = unchecked((int)4);
public const int TMTS_DIALOG_WARNING = unchecked((int)5);
public const int TMTS_DIALOG_WARNING_ALT = unchecked((int)6);
public const int TMTS_DIALOG_SCORE = unchecked((int)7);
public const int TMTS_DIALOG_CONFIRM = unchecked((int)8);
public const int TMTS_DIALOG_MULTIPLE_CHOICE = unchecked((int)9);

public const int TMTS_BUTTON_YES = unchecked((int)1);
public const int TMTS_BUTTON_NO = unchecked((int)2);
public const int TMTS_BUTTON_CANCEL = unchecked((int)3);
public const int TMTS_BUTTON_DONE = unchecked((int)4);
public const int TMTS_BUTTON_HALT = unchecked((int)5);
public const int TMTS_BUTTON_SAVE = unchecked((int)6);
public const int TMTS_BUTTON_SCORE = unchecked((int)7);
public const int TMTS_BUTTON_RESTART = unchecked((int)8);
public const int TMTS_BUTTON_MOVE_BACK = unchecked((int)9);

public const int CHARACTER_DIRECTION_UNKNOWN = unchecked((int)0);
public const int CHARACTER_DIRECTION_FORWARD = unchecked((int)1);
public const int CHARACTER_DIRECTION_FWD_RIGHT = unchecked((int)2);
public const int CHARACTER_DIRECTION_RIGHT = unchecked((int)3);
public const int CHARACTER_DIRECTION_BACK_RIGHT = unchecked((int)4);
public const int CHARACTER_DIRECTION_BACK = unchecked((int)5);
public const int CHARACTER_DIRECTION_BACK_LEFT = unchecked((int)6);
public const int CHARACTER_DIRECTION_LEFT = unchecked((int)7);
public const int CHARACTER_DIRECTION_FWD_LEFT = unchecked((int)8);
public const int CHARACTER_TURN_LEFT = unchecked((int)9);
public const int CHARACTER_TURN_RIGHT = unchecked((int)10);
public const int CHARACTER_DIRECTION_STOPPED = unchecked((int)11);

public const int TMTS_TEMPLATE_TYPE_INT32 = unchecked((int)0);
public const int TMTS_TEMPLATE_TYPE_FLOAT = unchecked((int)1);
public const int TMTS_TEMPLATE_TYPE_DOUBLE = unchecked((int)2);
public const int TMTS_TEMPLATE_TYPE_CHAR_STAR = unchecked((int)3);
public const int TMTS_TEMPLATE_TYPE_NULL = unchecked((int)4);
public const int TMTS_TEMPLATE_TYPE_MAX = unchecked((int)4);

public const int TMTS_MOUSE_FLAG_CLICKS = unchecked((int)1);
public const int TMTS_MOUSE_FLAG_MOVEMENT = unchecked((int)2);
public const int TMTS_MOUSE_FLAG_DRAGGING = unchecked((int)4);
public const int TMTS_MOUSE_FLAG_WHEELS = unchecked((int)8);

public const int TMTS_KEYBOARD_FLAG_DOWN = unchecked((int)1);
public const int TMTS_KEYBOARD_FLAG_REPEAT = unchecked((int)2);
public const int TMTS_KEYBOARD_FLAG_UP = unchecked((int)4);

public const int TMTS_CONTROLLER_FLAG_DOWN = unchecked((int)1);
public const int TMTS_CONTROLLER_FLAG_REPEAT = unchecked((int)2);
public const int TMTS_CONTROLLER_FLAG_UP = unchecked((int)4);

//Central Train Control events

public const int CTC_EVENT_PHYSICS_RAIL_ROLL = unchecked((int)0);	//vParam[0] is ID of offending car. vParam[1] is ID of Consist.
public const int CTC_EVENT_PHYSICS_CAR_TIP = unchecked((int)1);	//vParam[0] is ID of offending car. vParam[1] is ID of Consist.
public const int CTC_EVENT_RAN_OFF_TRACK = unchecked((int)2);	//vParam[0] is ID of offending car. vParam[1] is ID of Consist. vParam[2] is ID of Track.
public const int CTC_EVENT_WRONG_WAY_THROUGH_SWITCH = unchecked((int)3);	//vParam[0] is ID of offending car. vParam[1] is ID of Consist. vParam[2] is ID of Node (switch).
public const int CTC_EVENT_COLLISION = unchecked((int)4);	//vParam[0] is ID of 1 offending car. vParam[1] is ID of other offending car. vParam[2] is ID of node. vParam[3] is ID of node in track file.
public const int CTC_EVENT_SWITCH_CHANGED = unchecked((int)5);	//vParam[0] is ID of Node (switch).
public const int CTC_EVENT_SWITCH_ENTERED = unchecked((int)6);	//vParam[0] is ID of switch node. vParam[1] is the ID of the car. vParam[2] is the entering branch. vParam[3] is the exiting branch.
public const int CTC_EVENT_SWITCH_EXITED = unchecked((int)7);	//vParam[0] is ID of switch node. vParam[1] is the ID of the car. vParam[2] is the entering branch. vParam[3] is the exiting branch.
public const int CTC_EVENT_PASSED_MARKER = unchecked((int)8);	//vParam[0] is ID of marker. vParam[1] is the ID of the car. vParam[2] is the direction of the marker relative to the direcition the car is passing (not the cars sign but the sign of the travel)
public const int CTC_EVENT_MARKER_CHANGED = unchecked((int)9);	//vParam[0] is ID of marker. vParam[1] is the new state. vParam[2] is the last state.
public const int CTC_EVENT_CONSIST_JOIN = unchecked((int)10);	//vParam[0] is ID of new consist. vParam[1] is ID car1 one side of joint. vParam[2] is ID car2 other side of joint. IE joint is between car1 and car2. vParam[3] is ID of Track where this occured
public const int CTC_EVENT_CONSIST_BREAK = unchecked((int)11);	//vParam[0] is ID of old consist. vParam[1] is ID car1 one side of break. vParam[2] is ID car2 other side of break. IE break was between car1 and car2. vParam[3] is ID of new consist with car1. vParam[4] is ID of new consist with car2. vParam[5] is ID of Track where this occured.
public const int CTC_EVENT_EXCEEDED_SPEED = unchecked((int)12);	//dParam is speed exceeded
public const int CTC_EVENT_WALKING_ON_TRACK = unchecked((int)13);	//
public const int CTC_EVENT_WALKING_OFF_TRACK = unchecked((int)14);	//
public const int CTC_EVENT_WALKING_CAR_COLLISION = unchecked((int)15);	//vParam[0] is ID of collided car
public const int CTC_EVENT_PASSED_SIGNAL = unchecked((int)16);



public const int DXUI_MSG_MIN = unchecked((int)(0x0D000000));
public const int DXUI_MSG_INITIALIZE = unchecked((int)(0x0D000000 + 0));
public const int DXUI_MSG_TEXT_CHANGED = unchecked((int)(0x0D000000 + 1));
public const int DXUI_MSG_SLIDER_CHANGING = unchecked((int)(0x0D000000 + 2));
public const int DXUI_MSG_BUTTON_CLICKED = unchecked((int)(0x0D000000 + 3));
public const int DXUI_MSG_SLIDER_MOUSE_RELEASED = unchecked((int)(0x0D000000 + 4));
public const int DXUI_MSG_CLOSE = unchecked((int)(0x0D000000 + 5));
public const int DXUI_MSG_MOUSE_UP = unchecked((int)(0x0D000000 + 6));
public const int DXUI_MSG_MAX = unchecked((int)(0x0D000000 + 6));

public const int DXUI_STOCK_BUTTON_TYPE_RADIO = unchecked((int)1);
public const int DXUI_STOCK_BUTTON_TYPE_CHECKBOX = unchecked((int)2);

// Node types
public const int NODE_TYPE_SPRING = unchecked((int)0);
public const int NODE_TYPE_CUSTOM = unchecked((int)256);

public const int FW_NORMAL = unchecked((int)400);
public const int FW_BOLD = unchecked((int)700);

public const int WHITE = unchecked((int)0xFFFFFFFF);
public const int BLACK = unchecked((int)0xFF000000);
public const int RED = unchecked((int)0xFFFF0000);
public const int GREEN = unchecked((int)0xFF00FF00);
public const int BLUE = unchecked((int)0xFF0000FF);
public const int GRAY = unchecked((int)0xFF808080);
public const int YELLOW = unchecked((int)0xFFFFFF00);
public const int CYAN = unchecked((int)0xFF00FFFF);
public const int MAGENTA = unchecked((int)0xFFFF00FF);
public const int ORANGE = unchecked((int)0xFFFF8000);
public const int DARK_RED = unchecked((int)0xFFA00000);
public const int DARK_GREEN = unchecked((int)0xFF008000);
public const int DARK_BLUE = unchecked((int)0xFF0000A0);
public const int BROWN = unchecked((int)0xFF806000);

public const int MOUSE_BTN_LEFT = unchecked((int)1);
public const int MOUSE_BTN_RIGHT = unchecked((int)2);
public const int MOUSE_BTN_MIDDLE = unchecked((int)4);

public const int KBD_MODIFIER_SHIFT = unchecked((int)1);
public const int KBD_MODIFIER_CTRL = unchecked((int)2);
public const int KBD_MODIFIER_ALT = unchecked((int)4);

public const int VW_DEFAULT = unchecked((int)0);
public const int VW_ROAM = unchecked((int)0);
public const int VW_CAB = unchecked((int)1);
public const int VW_INTERIOR = unchecked((int)2);
public const int VW_VEHICLE = unchecked((int)3);
public const int VW_COUPLER = unchecked((int)4);
public const int VW_LOCATION = unchecked((int)5);
public const int VW_SPOTTER = unchecked((int)6);
public const int VW_WALK = unchecked((int)7);
public const int VW_YARD = unchecked((int)8);
public const int VW_MAX = unchecked((int)9);

// The following must match enumerated type AMType:
public const int ATTACHED_INTERIOR = unchecked((int)2);
public const int ATTACHED_TRUCK = unchecked((int)3);
public const int ATTACHED_COUPLER = unchecked((int)4);
public const int ATTACHED_LOAD = unchecked((int)5);

public const int SELECTION_MODE_ALL = unchecked((int)0);
public const int SELECTION_MODE_CARS = unchecked((int)1);
public const int SELECTION_MODE_TRACK = unchecked((int)2);
public const int SELECTION_MODE_SCENERY = unchecked((int)3);
public const int SELECTION_MODE_CAB = unchecked((int)4);
public const int SELECTION_MODE_TERRAIN = unchecked((int)5);
public const int SELECTION_MODE_OTHER = unchecked((int)6);

public const int SELECTED_CONTROL = unchecked((int)0);
public const int SELECTED_PART = unchecked((int)1);
public const int SELECTED_ATTACHMENT = unchecked((int)2);
public const int SELECTED_ATTACHMENT_PART = unchecked((int)3);

public const int CTRL_UNDEFINED = unchecked((int)0);	// Undefined
public const int CTRL_ABRAKE = unchecked((int)1);	// Automatic/ Train Brake
public const int CTRL_ASLIP = unchecked((int)2);	// Anti Slip control
public const int CTRL_AUXHEATER = unchecked((int)3);	// Cab Heat control Aux
public const int CTRL_AWS = unchecked((int)4);	// Automatic Warning System
public const int CTRL_BCUTOFFVV = unchecked((int)5);	// Brake Cutoff Valve
public const int CTRL_BELL = unchecked((int)6);	// Bell 
public const int CTRL_BLOWER = unchecked((int)7);	// Steam Blower
public const int CTRL_BOFF = unchecked((int)8);	// Bail off (Independent brake)
public const int CTRL_COMBOBRAKE = unchecked((int)9);	// Combination Brake/Throttle 
public const int CTRL_CONTROL = unchecked((int)10);	// Control Switch
public const int CTRL_CUTOFF = unchecked((int)11);	// Cutoff Valve
public const int CTRL_CYLCOCK = unchecked((int)12);	// Cylinder Cocks (Steam)
public const int CTRL_DLIGHTS = unchecked((int)13);	// Ditch Lights On/Off
public const int CTRL_DLIGHTSTIMER = unchecked((int)14);	// Ditch lights Timer
public const int CTRL_DYNBRAKE = unchecked((int)15);	// Dynamic Brake lever
public const int CTRL_DYNBRAKESWITCH = unchecked((int)16);	// Dynamic Brakes Switch
public const int CTRL_ESTOP = unchecked((int)17);	// Emergency stop Valve
public const int CTRL_ESTOPRESET = unchecked((int)18);	// Emergency stop reset switch
public const int CTRL_ETH = unchecked((int)19);	// Electric Train heat switch
public const int CTRL_FCUTOFF = unchecked((int)20);	// Fuel Cutoff
public const int CTRL_FDAMPER = unchecked((int)21);	// Dampers (Steam)
public const int CTRL_FDOOR = unchecked((int)22);	// Firebox door (Steam)
public const int CTRL_FEEDVV = unchecked((int)23);	// Feed Valve or Trainline Air pressure Knob
public const int CTRL_FUELFLOW = unchecked((int)24);	// Fuel flow control for oil fired locos
public const int CTRL_FUELPUMP = unchecked((int)25);	// Fuel Pump on/off
public const int CTRL_GAUGELIGHTS = unchecked((int)26);	// Gauge Lights switch
public const int CTRL_GEARSHIFT = unchecked((int)27);	// Gear shift control for mechanical transmissions
public const int CTRL_GENFIELD = unchecked((int)28);	// Generator field switch
public const int CTRL_GENLIGHT = unchecked((int)29);	// General Lights switch
public const int CTRL_GRDLIGHTS = unchecked((int)30);	// Ground lights
public const int CTRL_HBRAKE = unchecked((int)31);	// Handbrake
public const int CTRL_HCODELIGHTS = unchecked((int)32);	// Headcode lights
public const int CTRL_HLIGHT = unchecked((int)33);	// Headlight control
public const int CTRL_HLIGHTMODE = unchecked((int)34);	// Headlight Mode Select
public const int CTRL_HORN = unchecked((int)35);	// Horn
public const int CTRL_HUMP = unchecked((int)36);	// Hump Selector
public const int CTRL_IBRAKE = unchecked((int)37);	// Independent/ loco brake
public const int CTRL_INJSTEAM = unchecked((int)38);	// Steam Injectors
public const int CTRL_INJWATER = unchecked((int)39);	// Steam Water injector
public const int CTRL_INSIDELIGHT = unchecked((int)40);	// Inside lights switch
public const int CTRL_ISOSWITCH = unchecked((int)41);	// Isolation switch 
public const int CTRL_LEADTRAIL = unchecked((int)42);	// Lead/ Trail Switch or MU Valve
public const int CTRL_LGEJECT = unchecked((int)43);	// Large Ejector (Steam)
public const int CTRL_MECHSTOKE = unchecked((int)44);	// Mechanical stoker (Steam)
public const int CTRL_NUMLIGHT = unchecked((int)45);	// Number board lights switch
public const int CTRL_PANTO = unchecked((int)46);	// Pantograph switch
public const int CTRL_RCMODE = unchecked((int)47);	// RC Mode select (On Control box)
public const int CTRL_RCO_COSWITCH = unchecked((int)48);	// RCO Remote Manual selector
public const int CTRL_RCOALARMRESET = unchecked((int)49);	// RCO Alarm reset/Yes
public const int CTRL_RCOBREAKER = unchecked((int)50);	// RCO Circuit Breaker
public const int CTRL_RCOFUNCTION = unchecked((int)51);	// RCO Function Button/No
public const int CTRL_RCORESET = unchecked((int)52);	// Reset button on LCU
public const int CTRL_RDAMPER = unchecked((int)53);	// Steam Dampers
public const int CTRL_REVERSER = unchecked((int)54);	// Reverser control
public const int CTRL_RMTRANSVV = unchecked((int)55);	// Remote/Manual Transfer Valve
public const int CTRL_SAND = unchecked((int)56);	// Sander
public const int CTRL_SHOEDOWN = unchecked((int)57);	// Pickup shoes down
public const int CTRL_SHOEUP = unchecked((int)58);	// Pickup shoes Up
public const int CTRL_SHOVEL = unchecked((int)59);	// Shovel/Fireman control (Steam)
public const int CTRL_SMEJECT = unchecked((int)60);	// Small ejector (Steam)
public const int CTRL_SMEJECTCOMP = unchecked((int)61);	// Small ejector/Compressor (Steam)
public const int CTRL_STEPLIGHTS = unchecked((int)62);	// Steplights switch
public const int CTRL_THROTTLE = unchecked((int)63);	// Throttle Lever
public const int CTRL_TLIGHT = unchecked((int)64);	// Tail Light switch
public const int CTRL_VIGIL = unchecked((int)65);	// Vigilance alarm
public const int CTRL_WATERSCOOP = unchecked((int)66);	// Waterscoop up/down
public const int CTRL_WHISTLE = unchecked((int)67);	// Whistle (Steam)
public const int CTRL_WIPERS = unchecked((int)68);	// Windshield Wipers
public const int CTRLKEY_COR = unchecked((int)69);	// Correction key
public const int CTRLKEY_CURSOR = unchecked((int)70);	// 01 = Up, 02 = Down, 03 = Right, 04 = Left
public const int CTRLKEY_F = unchecked((int)71);	// Function Key 
public const int CTRLKEY_MENU = unchecked((int)72);	// Menu Key
public const int CTRLKEY_NUM = unchecked((int)73);	// Numeric Keypad Key
public const int CTRLKEY_OK = unchecked((int)74);	// OK/Enter
public const int CTRLKEY_PROG = unchecked((int)75);	// Programming Key 
public const int CTRLKEY_RESET = unchecked((int)76);	// Reset Key
public const int CTRLKEY_STOP = unchecked((int)77);	// Stop Key
public const int CTRLKEY_WINTER = unchecked((int)78);	// Winter Operations Key
public const int CTRL_MAX_INPUT = unchecked((int)78);
public const int GAUGE_ABRAKECYL = unchecked((int)79);	// Autobrake Cylinder Gauge
public const int GAUGE_ABRAKEPIPE = unchecked((int)80);	// Autobrake Pipe Gauge
public const int GAUGE_ACCEL = unchecked((int)81);	// Acceleration display
public const int GAUGE_AFM = unchecked((int)82);	// Airflow meter
public const int GAUGE_AMMETER = unchecked((int)83);	// Ammeter
public const int GAUGE_AWS = unchecked((int)84);	// Automatic Warning System indicator
public const int GAUGE_BCYL = unchecked((int)85);	// Brake Cylinder pressure
public const int GAUGE_BOILERPRESS = unchecked((int)86);	// Boiler pressure
public const int GAUGE_BRAKEWARN = unchecked((int)87);	// Brake warning light
public const int GAUGE_CDISPLAY = unchecked((int)88);	// Combo brake/ Throttle display
public const int GAUGE_CUTOFF = unchecked((int)89);	// Cutoff Vv Setting display
public const int GAUGE_DYNBRAKE = unchecked((int)90);	// Dynamic brake setting
public const int GAUGE_DYNBRAKECURR = unchecked((int)91);	// Dynamic Brake Current
public const int GAUGE_EQRES = unchecked((int)92);	// Equalizing reservoir display
public const int GAUGE_FNR = unchecked((int)93);	// Forward Neutral reverse display
public const int GAUGE_FUEL = unchecked((int)94);	// Fuel Gauge
public const int GAUGE_GEAR = unchecked((int)95);	// Gear indicator
public const int GAUGE_IBRAKEEQ = unchecked((int)96);	// Independent (Loco) brake Equalising res display
public const int GAUGE_IBRAKEMAIN = unchecked((int)97);	// Independent (Loco) brake main res display
public const int GAUGE_MAINRES = unchecked((int)98);	// Main Reservoir display
public const int GAUGE_NEXTSIGNAL = unchecked((int)99);	// Next Signal aspect display
public const int GAUGE_NEXTTRACKSPEED = unchecked((int)100);	// Next speed display
public const int GAUGE_OLOAD = unchecked((int)101);	// Overload indicator
public const int GAUGE_OSCHDLT = unchecked((int)102);	// OSC Headlight Indicator
public const int GAUGE_OSPEED = unchecked((int)103);	// Overspeed Indicator
public const int GAUGE_PANTO = unchecked((int)104);	// Pantograph position indicator
public const int GAUGE_PCS = unchecked((int)105);	// PCS Indicator
public const int GAUGE_RPM = unchecked((int)106);	// RPM Gauge
public const int GAUGE_SAND = unchecked((int)107);	// Sand on/Off indicator
public const int GAUGE_SHOE = unchecked((int)108);	// Pickup shoes up/Down indicator
public const int GAUGE_SPEEDOMETER = unchecked((int)109);	// Speedometer
public const int GAUGE_STEAMCHEST = unchecked((int)110);	// Steam Chest Pressure Gauge
public const int GAUGE_SUPPVOL = unchecked((int)111);	// Supply Voltage Gauge
public const int GAUGE_THROTTLE = unchecked((int)112);	// Throttle position
public const int GAUGE_TPA = unchecked((int)113);	// Train pipe Air Pressure
public const int GAUGE_TPVAC = unchecked((int)114);	// Vacuum pipe Pressure
public const int GAUGE_WATERSCOOP = unchecked((int)115);	// Water Scoop position
public const int GAUGE_WLEVELBOILER = unchecked((int)116);	// Boiler water level (Steam)
public const int GAUGE_WLEVELTANK = unchecked((int)117);	// Tank Water level (Steam)
public const int GAUGE_WLEVELTEND = unchecked((int)118);	// Tender Water level (Steam)
public const int GAUGE_WSLIP = unchecked((int)119);	// Wheel slip Indicator
public const int GAUGE_MAX_OUTPUT = unchecked((int)119);
public const int ACC_BLIND = unchecked((int)120);	// Window Blind
public const int ACC_DOOR = unchecked((int)121);	// Door
public const int ACC_FAN = unchecked((int)122);	// Cooling fan
public const int ACC_LOCKOUT = unchecked((int)123);	// RCO Reverser Lockout Device
public const int ACC_RCSIGN = unchecked((int)124);	// RCO Sign for Control Stands
public const int ACC_WINDOW = unchecked((int)125);	// Window
public const int ACC_WIPE = unchecked((int)126);	// Windshield Wiper
public const int ACC_BCYL = unchecked((int)127);	// Brake Cylinder component for freightcars
public const int ACC_BPIS = unchecked((int)128);	// Brake Cylinder piston for freightcars
public const int ACC_BARM = unchecked((int)129);	// Brake Arm component for freight cars

public const int PART_MAIN = unchecked((int)130);	// Main body of a car
public const int PART_BOGIE = unchecked((int)131);	// Train car bogie (truck)
public const int PART_DWHEEL = unchecked((int)132);	// Driving wheel
public const int PART_RWHEEL = unchecked((int)133);	// Running wheel
public const int PART_COUPLER = unchecked((int)134);	// Coupler
public const int PART_HOSE = unchecked((int)135);	// Brake hose
public const int PART_WIPER = unchecked((int)136);	// Windshield wiper (but not in a cab, which is mpAcc_Wipe)
public const int PART_RLOUVERS = unchecked((int)137);	// Radiator louvers
public const int PART_RFAN = unchecked((int)138);	// Radiator fan
public const int PART_DOOR = unchecked((int)139);	// Door
public const int PART_PANTOGRAPH = unchecked((int)140);	// Pantograph (of an electric train car)
public const int PART_RODS = unchecked((int)141);	// ???
public const int PART_LOAD = unchecked((int)142);	// Load
public const int PART_HANDBRAKE = unchecked((int)143);
public const int PART_BARRIER = unchecked((int)144);	// Level, grade crossing barrier
public const int PART_GATE = unchecked((int)145);	// Level, grade crossing barrier
public const int PART_HEADCODE = unchecked((int)146);	// Train head code
public const int PART_SIGNALARM = unchecked((int)147);	// Signal arm (of a semaphore signal)
public const int PART_SWITCH = unchecked((int)148);	// "Animated parts of a point/turnout/switch"
public const int PART_SWITCHSTAND = unchecked((int)149); // Animated part of a switchstand ("FLAG")

public const int PART_SIGNAL = unchecked((int)157);

public const int PART_FENCE = unchecked((int)173);  // Fence gate

public const int PART_SWITCHMOTOR = unchecked((int)176);

public const int MOTION_HALT = unchecked((int)-1);
public const int MOTION_BOUNDED = unchecked((int)0);
public const int MOTION_CONTINUOUS = unchecked((int)1);

public const int BUMP_X = unchecked((int)0);  // VMVMT_X
public const int BUMP_Y = unchecked((int)1);  // VMVMT_Y

public const int TMTS_HUD_DBG_OFF = unchecked((int)0);
public const int TMTS_HUD_DBG_FPS = unchecked((int)1);
public const int TMTS_HUD_DBG_POSITION = unchecked((int)2);
public const int TMTS_HUD_DBG_TIMES = unchecked((int)4);
public const int TMTS_HUD_DBG_RAW = unchecked((int)8);
public const int TMTS_HUD_DBG_CST_POS = unchecked((int)16);
public const int TMTS_HUD_DBG_POLY = unchecked((int)32);

public const int TMTS_MODEL_DBG_CTRL_BOUNDS = unchecked((int)1);

public const int TMTS_TERRAIN_BRUSH_MODE_OFF = unchecked((int)0);
public const int TMTS_TERRAIN_BRUSH_MODE_ADD = unchecked((int)1);
public const int TMTS_TERRAIN_BRUSH_MODE_SUBTRACT = unchecked((int)2);
public const int TMTS_TERRAIN_BRUSH_MODE_ADD_STEP = unchecked((int)3);
public const int TMTS_TERRAIN_BRUSH_MODE_SUB_STEP = unchecked((int)4);
public const int TMTS_TERRAIN_BRUSH_MODE_MAX = unchecked((int)5);
public const int TMTS_TERRAIN_BRUSH_MODE_MIN = unchecked((int)6);
public const int TMTS_TERRAIN_BRUSH_MODE_HILL = unchecked((int)7);
public const int TMTS_TERRAIN_BRUSH_MODE_SMOOTH = unchecked((int)8);
public const int TMTS_TERRAIN_BRUSH_MODE_GRADE = unchecked((int)9);

public const int TMTS_INVALID_DATA_ID = unchecked((int)(-1));
public const int TMTS_DATA_TYPE_TRACK_POINT = unchecked((int)0);
public const int TMTS_DATA_TYPE_TRACK_NAME = unchecked((int)1);
public const int TMTS_DATA_TYPE_TRACK_NODE = unchecked((int)2);
public const int TMTS_DATA_TYPE_TRACK_MARKER = unchecked((int)3);
public const int TMTS_DATA_TYPE_CONSIST_HEAD = unchecked((int)4);
public const int TMTS_DATA_TYPE_CONSIST_CAR = unchecked((int)5);
public const int TMTS_DATA_TYPE_SCRIPT = unchecked((int)6);
public const int TMTS_DATA_TYPE_INTERACTION = unchecked((int)7);
public const int TMTS_DATA_TYPE_SETTINGS = unchecked((int)8);
public const int TMTS_DATA_TYPE_INSTANCE_GLOBALS = unchecked((int)9);
public const int TMTS_DATA_TYPE_SCENERY_TILE = unchecked((int)10);
public const int TMTS_DATA_TYPE_SCENERY_MODEL = unchecked((int)11);
public const int TMTS_DATA_TYPE_MODEL_REFERENCE = unchecked((int)12);
public const int TMTS_DATA_TYPE_MODEL_TYPE = unchecked((int)13);
public const int TMTS_DATA_TYPE_ROAD = unchecked((int)14);
public const int TMTS_DATA_TYPE_EXTRUSION = unchecked((int)15);
// TODO remove TMTS_DATA_TYPE_TRACK_END
public const int TMTS_DATA_TYPE_TRACK_END = unchecked((int)16);
public const int TMTS_DATA_TYPE_CAR_VIEW = unchecked((int)17);
public const int TMTS_DATA_TYPE_CHARACTER = unchecked((int)18);
public const int TMTS_DATA_TYPE_CAR_ATTACHMENT = unchecked((int)19);
public const int TMTS_DATA_TYPE_SPLINE = unchecked((int)20);
public const int TMTS_MAX_DATA_TYPE = unchecked((int)21);












}



