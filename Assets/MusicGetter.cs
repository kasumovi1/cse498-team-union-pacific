using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicGetter : MonoBehaviour
{
    // Start is called before the first frame update
    public int SongNumber;

    AudioSource Song;

    void Start()
    {
        if (SongNumber == 1)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song001");
        }
        if (SongNumber == 2)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song002");
        }
        if (SongNumber == 3)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song003");
        }
        if (SongNumber == 4)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song004");
        }
        if (SongNumber == 5)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song005");
        }
        if (SongNumber == 6)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song006");
        }
        if (SongNumber == 7)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song007");
        }
        if (SongNumber == 8)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song008");
        }
        if (SongNumber == 9)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song009");
        }
        if (SongNumber == 10)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song010");
        }
        if (SongNumber == 11)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song011");
        }
        if (SongNumber == 12)
        {
            Song = GetComponent<AudioSource>();
            Song.clip = Resources.Load<AudioClip>("Audio/Music/Song012");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
