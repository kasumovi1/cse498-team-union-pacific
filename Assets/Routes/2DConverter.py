import xml.dom.minidom as md
import os


def magnitude(a, b):
    return (a*a + b*b)**0.5


def main():
    # assign directory
    directory = '.'

    # iterate over files in
    # that directory
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        # checking if it is a track file
        if os.path.isfile(f) and "meta" not in f.lower() and "trk" in f.lower() and "fixed" not in f.lower() and ".py" not in f.lower():
            # print(f)
            file = md.parse(f)

            points = file.getElementsByTagName("pt")
            x_intervals = []
            z_intervals = []
            prev_x = None
            prev_z = None
            for point in points:
                curr_x = float(point.getAttribute("x"))
                curr_z = float(point.getAttribute("z"))
                if prev_x is None:
                    prev_x = curr_x
                    prev_z = curr_z
                else:
                    x_intervals.append(curr_x - prev_x)
                    z_intervals.append(curr_z - prev_z)
                    prev_x = curr_x
                    prev_z = curr_z

            prev_x = None
            for i, point in enumerate(points):
                curr_x = float(point.getAttribute("x"))
                if prev_x is None:
                    prev_x = curr_x
                else:
                    point.setAttribute("x", str(prev_x + magnitude(x_intervals[i-1], z_intervals[i-1])))
                    prev_x = float(point.getAttribute("x"))
                point.setAttribute("z", "0")

            # Remove all mileposts
            current_milepost = 0
            mileposts = file.getElementsByTagName("marker")
            old_mileposts = []
            for milepost in mileposts:
                milepost.setAttribute("text1", str(current_milepost))
                current_milepost += 1
                old_mileposts.append(milepost)
                milepost.parentNode.removeChild(milepost)

            # Replace mileposts every 1609 meters
            new_mile = True
            mile_start = -1
            mile_curr = -1
            milepost_num = 0
            for point in points:
                if new_mile and milepost_num < len(old_mileposts):
                    # Insert milepost
                    point.parentNode.insertBefore(old_mileposts[milepost_num], point)
                    mile_start = float(point.getAttribute("x"))
                    new_mile = False
                else:
                    mile_curr = float(point.getAttribute("x"))
                    dist = abs(mile_curr - mile_start)
                    if dist >= 1609.34:
                        milepost_num += 1
                        new_mile = True

            with open(f.replace(".TRK", "").replace(".trk", "") + "_fixed.TRK", "w") as fs:

                fs.write(file.toxml())
                fs.close()


if __name__ == "__main__":
    main()
