import xml.dom.minidom as md
import os
import math

def magnitude(a, b):
    return (a*a + b*b)**0.5


def main():
    # assign directory
    directory = '.'

    print("Input the filename:")
    filename = input()

    print("How many meters between sampling point?")
    meters_per_point = float(input())

    print("Which track do you want to create? (uphill, downhill, fullhill, or fullvalley)")
    track = input().lower()

    f = os.path.join(directory, filename)
    # checking if it is a track file
    if os.path.isfile(f) and "meta" not in f.lower() and "trk" in f.lower() and "fixed" not in f.lower():
        file = md.parse(f)

        # Set up points
        points = file.getElementsByTagName("pt")
        x = 0
        y = 0
        for point in points:
            if track == "uphill":
                if x > (8.142 * 1609.34):
                    y = (0.04 * 1609.34)
                elif x > (1.858 * 1609.34):
                    y = ((math.sin(((x / 1609.34)/2) - 2.5) + 1) / 50) * 1609.34
            elif track == "downhill":
                if x > (8.142 * 1609.34):
                    y = -(0.04 * 1609.34)
                elif x > (1.858 * 1609.34):
                    y = -((math.sin(((x / 1609.34)/2) - 2.5) + 1) / 50) * 1609.34
            elif track == "fullhill":
                if x > (9.015 * 1609.34):
                    y = 0
                elif x > (1.162 * 1609.34):
                    y = ((math.sin(((x / 1609.34)/1.25) - 2.5) + 1) / 100) * 1609.34
            elif track == "fullvalley":
                if x > (9.015 * 1609.34):
                    y = 0
                elif x > (1.162 * 1609.34):
                    y = -((math.sin(((x / 1609.34)/1.25) - 2.5) + 1) / 100) * 1609.34
            else:
                print("Bad Track String Input")

            point.setAttribute("x", str(x))
            point.setAttribute("y", str(y))

            x += meters_per_point
            if x > (14 * 1609.34):
                break

        # Set up mileposts
        mileposts = file.getElementsByTagName("marker")
        current_milepost = 0
        for milepost in mileposts:
            milepost.setAttribute("text1", str(current_milepost))
            current_milepost += 1

        # Save file
        with open(f, "w") as fs:
            fs.write(file.toxml())
            fs.close()


if __name__ == "__main__":
    main()
