using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class VolumeSettings : MonoBehaviour
{

    [SerializeField] AudioMixer mixer;
    [SerializeField] Slider masterSlider;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider uiSlider;
    [SerializeField] Slider trainSlider;
    [SerializeField] Slider warningsSlider;
    [SerializeField] Slider backgroundSlider;


    //strings to call the mixers
    public const string MIXER_MASTER = "MasterVolume";
    public const string MIXER_MUSIC = "MusicVolume";
    public const string MIXER_UI = "UIVolume";
    public const string MIXER_TRAIN = "TrainVolume";
    public const string MIXER_WARNINGS = "WarningsVolume";
    public const string MIXER_BACKGROUND = "BackgroundVolume";


    private void Awake()
    {
        //create listens when the scene is opens
        masterSlider.onValueChanged.AddListener(SetMasterVolume);
        musicSlider.onValueChanged.AddListener(SetMusicVolume);
        uiSlider.onValueChanged.AddListener(SetUIVolume);
        trainSlider.onValueChanged.AddListener(SetTrainVolume);
        warningsSlider.onValueChanged.AddListener(SetWarningsVolume);
        backgroundSlider.onValueChanged.AddListener(SetBackgroundVolume);

    }

    private void Start()
    {
        //Load the sound values on start. if no values, set to 1f(100% volume)
        masterSlider.value = PlayerPrefs.GetFloat(AudioManager.MASTER_KEY, 1f);
        musicSlider.value = PlayerPrefs.GetFloat(AudioManager.MUSIC_KEY, 1f);
        uiSlider.value = PlayerPrefs.GetFloat(AudioManager.UI_KEY, 1f);
        trainSlider.value = PlayerPrefs.GetFloat(AudioManager.TRAIN_KEY, 1f);
        warningsSlider.value = PlayerPrefs.GetFloat(AudioManager.WARNINGS_KEY, 1f);
        backgroundSlider.value = PlayerPrefs.GetFloat(AudioManager.BACKGROUND_KEY, 1f);
        
        
    }

    void OnDisable()
    {

        // Save when the scene changes
        PlayerPrefs.SetFloat(AudioManager.MASTER_KEY, masterSlider.value);
        PlayerPrefs.SetFloat(AudioManager.MUSIC_KEY, musicSlider.value);
        PlayerPrefs.SetFloat(AudioManager.UI_KEY, uiSlider.value);
        PlayerPrefs.SetFloat(AudioManager.TRAIN_KEY, trainSlider.value);
        PlayerPrefs.SetFloat(AudioManager.WARNINGS_KEY, warningsSlider.value);
        PlayerPrefs.SetFloat(AudioManager.BACKGROUND_KEY, backgroundSlider.value);

    }


    // Functions to set volume
    void SetMasterVolume(float value)
    {
        mixer.SetFloat(MIXER_MASTER, Mathf.Log10(value) * 20);
    }

    void SetMusicVolume(float value)
    {
        mixer.SetFloat(MIXER_MUSIC, Mathf.Log10(value) * 20);
    }
    void SetUIVolume(float value)
    {
        mixer.SetFloat(MIXER_UI, Mathf.Log10(value) * 20);
    }
    void SetTrainVolume(float value)
    {
        mixer.SetFloat(MIXER_TRAIN, Mathf.Log10(value) * 20);
    }
    void SetWarningsVolume(float value)
    {
        mixer.SetFloat(MIXER_WARNINGS, Mathf.Log10(value) * 20);
    }
    void SetBackgroundVolume(float value)
    {
        mixer.SetFloat(MIXER_BACKGROUND, Mathf.Log10(value) * 20);
    }
}
