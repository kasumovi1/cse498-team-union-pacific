using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;


    //serialized fields
    [SerializeField] AudioMixer mixer;


    //keys - used to show playerPrefs where to save
    public const string MASTER_KEY = "masterVolume";
    public const string MUSIC_KEY = "musicVolume";
    public const string UI_KEY = "uiVolume";
    public const string TRAIN_KEY = "trainVolume";
    public const string WARNINGS_KEY = "warningsVolume";
    public const string BACKGROUND_KEY = "backgroundVolume";


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        LoadVolume();
    }

    private void Start()
    {
        LoadVolume();
    }

    public void LoadVolume()  //volume saved in VolumeSettings.cs (playerPrefs)
    {
        //get the float values for each mixer
        float masterVolume = PlayerPrefs.GetFloat(MASTER_KEY, 1f);
        float musicVolume = PlayerPrefs.GetFloat(MUSIC_KEY, 1f);
        float uiVolume = PlayerPrefs.GetFloat(UI_KEY, 1f);
        float trainVolume = PlayerPrefs.GetFloat(TRAIN_KEY, 1f);
        float warningsVolume = PlayerPrefs.GetFloat(WARNINGS_KEY, 1f);
        float backgroundVolume = PlayerPrefs.GetFloat(BACKGROUND_KEY, 1f);

        //convert to log and set
        mixer.SetFloat(VolumeSettings.MIXER_MASTER, Mathf.Log10(masterVolume) * 20);
        mixer.SetFloat(VolumeSettings.MIXER_MUSIC, Mathf.Log10(musicVolume) * 20);
        mixer.SetFloat(VolumeSettings.MIXER_UI, Mathf.Log10(uiVolume) * 20);
        mixer.SetFloat(VolumeSettings.MIXER_TRAIN, Mathf.Log10(trainVolume) * 20);
        mixer.SetFloat(VolumeSettings.MIXER_WARNINGS, Mathf.Log10(warningsVolume) * 20);
        mixer.SetFloat(VolumeSettings.MIXER_BACKGROUND, Mathf.Log10(backgroundVolume) * 20);

    }

}
