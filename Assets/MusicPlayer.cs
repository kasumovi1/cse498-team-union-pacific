using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{


    public AudioSource musicSource;
    public AudioSource[] musicArray;


    // Start is called before the first frame update
    void Start()
    {
        musicArray[Random.Range(0, musicArray.Length)].Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
