Set-Location "SimulationBuild\General Content\Trains"
$ndllpath = Resolve-Path -Path "DieselElectrics.ndll"
sp $ndllpath IsReadOnly $false
unblock-file -path $ndllpath -Confirm:$false -Verbose